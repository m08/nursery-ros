#include <ros/ros.h>
#include <std_msgs/Int8.h>
//#include "sensor_msgs/PointCloud2.h"
//#include "pcl_ros/point_cloud.h"
#include <geometry_msgs/Transform.h>
#include <actionlib/client/simple_action_client.h>
#include <move_base_msgs/MoveBaseAction.h>
//#include <agrr1_msgs/PointCloudStamped.h>
//#include <agrr1_msgs/TransformListStamped.h>
#include <FPGA/Click.h>
#include <vector> 

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

class Tray {
public:
geometry_msgs::Transform location;

Tray(double x, double y, double w) {
		location = geometry_msgs::Transform();
		location.translation.x=x; location.translation.y=y; location.rotation.w=w;
}

};


class Area {
public:
double x,y,w,h;

Area(double xs, double ys, double ws, double hs) {
		x=xs; y=ys; w=ws; h=hs; 
}

};

class AGRR1Robot {
protected:
	ros::NodeHandle nh_;
	//ros::Subscriber recog_sub_, pcd_sub_, cmd_sub_;
	//ros::Publisher recog_pub_, frk_pub_;
	ros::Subscriber cmd_sub_;
	ros::Publisher frk_pub_;
	ros::ServiceClient fpga;


	MoveBaseClient ac;
	void moveTo(double x, double y, double w);

	bool getTray(Tray tray);
	bool enterTray(Tray tray);
	void liftTray();
	void lowerTray();
	void returnTray();

	//TODO define vars
	uint8_t opmode;
	std::vector<Tray> trays;
	std::vector<Tray> strayTrays;
	std::vector<Tray> collectedTrays;
	std::vector<Area> areas;

	bool idle;
	//bool sendRecogCmd;
	//ros::Time sendRecogCmdStamp;
	//ros::Duration recogCmdTimeout;
protected:
//	void RecognitionCallback(const geometry_msgs::Transform::ConstPtr &msg);
//	void PointcloudCallback(const sensor_msgs::PointCloud2::ConstPtr &msg);
	void CommandCallback(const std_msgs::Int8::ConstPtr &msg);

public:
	/*AGRR1Robot() : nh_("~"), ac("move_base",true), recogCmdTimeout(10){
		init();
	}*/

	AGRR1Robot() : nh_("~"), ac("move_base",true) {
		init();
	}

	~AGRR1Robot() {
		nh_.shutdown();
	}

	/// Initialize the robot
	void init();

	/// Await and process commands
	void spin();
};
