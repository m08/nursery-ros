#include <agrr1_main/agrr1_main.h>

/// Entry point for AGRR1 robot controller node
int main(int argc, char **argv)
{
  ros::init(argc, argv, "agrr1_main");

  AGRR1Robot agrr1_main;
  agrr1_main.spin();

  return 0;
}
