#include "agrr1_main/agrr1_main.h"

void AGRR1Robot::init()
{
	ROS_INFO("Initializing AGRR1 robot");

	opmode = 0;
	idle = true;
	//sendRecogCmd = false;

	// Subscribe to topics
	cmd_sub_ = nh_.subscribe("/cmd_agrr1", 10, &AGRR1Robot::CommandCallback, this);
	//recog_sub_ = nh_.subscribe("/recognised", 10, &AGRR1Robot::RecognitionCallback, this);
	//pcd_sub_ = nh_.subscribe<pcl::PointCloud<pcl::PointXYZ> >("/camera/depth/points", 1, &AGRR1Robot::PointcloudCallback, this);

	//recog_pub_ = nh_.advertise<pcl::PointCloud<pcl::PointXYZ> >("/recognise_object", 1);
	frk_pub_ = nh_.advertise<std_msgs::Int8>("/cmd_frk", 1);

	fpga = nh_.serviceClient<FPGA::Click>("cSensor");

	while(!AGRR1Robot::ac.waitForServer(ros::Duration(5.0))) {
		ROS_WARN_ONCE("Waiting for the move_base action server to come up...");
	}

}

void AGRR1Robot::spin()
{
	ROS_INFO("Spinning");
	ros::Rate r(100);

	trays = std::vector<Tray>();
	strayTrays = std::vector<Tray>();
	collectedTrays = std::vector<Tray>();
	areas = std::vector<Area>();

	Tray tray (1.0,0.0,1.0);
	trays.push_back(tray);
	strayTrays.push_back(tray);
	
	Area area (2.0,-1.0,0.5,0.3);
areas.push_back(area);

	while(ros::ok())
	{
		ros::spinOnce();
	
		// ROBOT MAIN ACTIONS PER CYCLE	
		switch(opmode){
			case 0 : 
				ROS_DEBUG("Operating in idle mode");
				if(!idle){
					moveTo(0,0,1); // move to base
				}
				break;
			case 1 : 
				ROS_DEBUG("Operating in roaming mode");
				//TODO roaming actions
				if( !strayTrays.empty() && getTray(strayTrays[0])) {
					returnTray();
					collectedTrays.push_back(strayTrays[0]);
					strayTrays.erase(strayTrays.begin());
				}
				break;
			case 2 : 
				ROS_DEBUG("Operating in action mode");
				//TODO action actions
				break;
			default : ROS_WARN("Unkown operation mode!");
		}	

		r.sleep();
	}
}

bool AGRR1Robot::getTray(Tray tray) {
	//TODO move to tray, enter tray and lift tray
	moveTo(tray.location.translation.x,tray.location.translation.y,tray.location.rotation.w); //TODO don't move on tray, but in front of it
	if(enterTray(tray)) {
		liftTray(); 
		return true;
	} else return false;
}

void AGRR1Robot::moveTo(double x, double y, double w) {
	//TODO move to x,y with pose w
	move_base_msgs::MoveBaseGoal goal;

  //we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "base_link";
  goal.target_pose.header.stamp = ros::Time::now();

  goal.target_pose.pose.position.x = x;
  goal.target_pose.pose.position.y = y;
  goal.target_pose.pose.orientation.w = w;

  ac.sendGoal(goal);

  ac.waitForResult();

  if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    ROS_DEBUG("Robot moved to %G, %G with orientation %G", x, y, w);
  else
    ROS_DEBUG("The base failed to move to %G, %G with orientation %G", x, y, w);
}

bool AGRR1Robot::enterTray(Tray tray) {
	//TODO move forks inside tray with limit switch callback
	return true;
}

void AGRR1Robot::liftTray() {
	std_msgs::Int8 msg;
	msg.data = 2;
	frk_pub_.publish(msg); //TODO wait for forklift movement completion
}

void AGRR1Robot::lowerTray() {
	std_msgs::Int8 msg;
	msg.data = 1;
	frk_pub_.publish(msg); //TODO wait for forklift movement completion
}

void AGRR1Robot::returnTray() {
	moveTo(areas[0].x,areas[0].y,1); //TODO move to an epmty spot in a collecting area
	lowerTray();
}

void AGRR1Robot::CommandCallback(const std_msgs::Int8::ConstPtr &msg) {
	// 8 bit integer message containing command for robot
	opmode = msg->data;
	if (opmode != 0 && idle)
		idle = false;
}
/*
void AGRR1Robot::RecognitionCallback(const geometry_msgs::Transform::ConstPtr &msg) {
	// Stamped tranformlist message containing 6DOF of recognised object and the recognition timestamp
	
}
*/
/*
void AGRR1Robot::PointcloudCallback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr& msg) {
	// Pointcloud message containing current frame of kinect
	if(sendRecogCmd && sendRecogCmdStamp-ros::Time::now()>recogCmdTimeout) {
		recog_pub_.publish(msg);
		sendRecogCmd = false;
	}
}
*/
