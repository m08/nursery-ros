
(cl:in-package :asdf)

(defsystem "agrr1_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :sensor_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "TransformListStamped" :depends-on ("_package_TransformListStamped"))
    (:file "_package_TransformListStamped" :depends-on ("_package"))
    (:file "PointCloudStamped" :depends-on ("_package_PointCloudStamped"))
    (:file "_package_PointCloudStamped" :depends-on ("_package"))
  ))