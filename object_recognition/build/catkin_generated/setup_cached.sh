#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/agudek/Workspace/src/nursery/object_recognition/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/agudek/Workspace/src/nursery/object_recognition/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/agudek/Workspace/src/nursery/object_recognition/build/devel/lib:/home/agudek/Workspace/src/nursery/object_recognition/build/devel/lib/x86_64-linux-gnu:/home/agudek/Workspace/devel/lib/x86_64-linux-gnu:/opt/ros/jade/lib/x86_64-linux-gnu:/home/agudek/Workspace/devel/lib:/opt/ros/jade/lib"
export PATH="/home/agudek/Workspace/src/nursery/object_recognition/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/agudek/Workspace/src/nursery/object_recognition/build/devel/lib/pkgconfig:/home/agudek/Workspace/src/nursery/object_recognition/build/devel/lib/x86_64-linux-gnu/pkgconfig:/home/agudek/Workspace/devel/lib/x86_64-linux-gnu/pkgconfig:/opt/ros/jade/lib/x86_64-linux-gnu/pkgconfig:/home/agudek/Workspace/devel/lib/pkgconfig:/opt/ros/jade/lib/pkgconfig"
export PYTHONPATH="/home/agudek/Workspace/src/nursery/object_recognition/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/agudek/Workspace/src/nursery/object_recognition/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/agudek/Workspace/src/nursery/object_recognition:$ROS_PACKAGE_PATH"