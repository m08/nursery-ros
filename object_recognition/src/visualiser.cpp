#include "ros/ros.h"
#include "ros/assert.h"
#include "pcl_ros/point_cloud.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>
#include <geometry_msgs/Transform.h>
#include <agrr1_msgs/TransformListStamped.h>
#include <agrr1_msgs/PointCloudStamped.h>
#include <eigen_conversions/eigen_msg.h>

typedef pcl::PointXYZ PointType;

pcl::PointCloud<PointType>::Ptr model (new pcl::PointCloud<PointType> ());
pcl::visualization::PCLVisualizer viewer ("Object Recognition");
pcl::visualization::PointCloudColorHandlerCustom<PointType> sceneColor (255, 255, 255);
pcl::visualization::PointCloudColorHandlerCustom<PointType> sceneColorRed (255, 0, 0);
int modelinstances = 0;

ros::Time stamp;

std::string concat(std::string str, int num){
	std::ostringstream oss;
	oss << str << num;
	return oss.str();
}

void PclCallback(const agrr1_msgs::PointCloudStamped &pcdmsg) {
	stamp = pcdmsg.header.stamp;
	const pcl::PointCloud<PointType>::Ptr msg (new pcl::PointCloud<PointType> ());
	pcl::fromROSMsg(pcdmsg.cloud , *msg);
	viewer.removeAllPointClouds(0);
	modelinstances=0;
	viewer.addPointCloud(msg, sceneColor, "scene");
}


void RecCallback(const agrr1_msgs::TransformListStamped& msg) {
	if(msg.header.stamp==stamp) {
		for(std::vector<geometry_msgs::Transform>::const_iterator it = msg.transforms.begin(); it != msg.transforms.end(); ++it) {
			Eigen::Vector3d translation;
			Eigen::Quaterniond rotation;
			tf::vectorMsgToEigen ((*it).translation, translation);
			tf::quaternionMsgToEigen ((*it).rotation, rotation);
			pcl::PointCloud<PointType>::Ptr rotated_model (new pcl::PointCloud<PointType> ());
	    		pcl::transformPointCloud (*model, *rotated_model, translation, rotation);
			viewer.addPointCloud(rotated_model, sceneColorRed, concat("model", modelinstances));
			modelinstances++;
		}
	} else { 
		ROS_WARN("Out of sync recognition received!");
	}
}

int main (int argc, char *argv[]) {

  ros::init(argc, argv, "recognition");

  ros::NodeHandle nh;

 std::string s;
    nh.getParam("/object_recognition/model", s);

bool axes;

nh.param("/object_recognition/draw_axes",axes,false);

double pos_x;
double pos_y;
double pos_z;
double view_x;
double view_y;
double view_z;
double up_x;
double up_y;
double up_z;

nh.param("/recognition_visualiser/campos_x",pos_x,0.49);
nh.param("/recognition_visualiser/campos_y",pos_y,0.49);
nh.param("/recognition_visualiser/campos_z",pos_z,3.90299);
nh.param("/recognition_visualiser/camview_x",view_x,0.49);
nh.param("/recognition_visualiser/camview_y",view_y,0.49);
nh.param("/recognition_visualiser/camview_z",view_z,0.49);
nh.param("/recognition_visualiser/camup_x",up_x,0.00);
nh.param("/recognition_visualiser/camup_y",up_y,1.00);
nh.param("/recognition_visualiser/camup_z",up_z,0.00);

double near;
double far;

nh.param("/object_recognition/clip_near",near,0.011);
nh.param("/object_recognition/clip_far",far,11.612);

ROS_INFO("File \"%s\" given", s.c_str());

ros::Subscriber sub1 = nh.subscribe("/recognise_object", 1, PclCallback);
ros::Subscriber sub2 = nh.subscribe("/recognised", 1, RecCallback);

  //
  //  Load cloud
  //
  if (pcl::io::loadPCDFile (s, *model) < 0)
  {
    std::cout << "Error loading model cloud." << std::endl;
    return (-1);
  }

//Visualisation
  if(axes)
	viewer.addCoordinateSystem(1.0f,"axes",0);

  viewer.setCameraPosition(pos_x,pos_y,pos_z,view_x,view_y,view_z,up_x,up_y,up_z,0); 
  viewer.setCameraClipDistances (near,far,0);	
  
  while (!viewer.wasStopped ()) {
	ros::spinOnce();
   	viewer.spinOnce ();
  }

  return (0);
}
