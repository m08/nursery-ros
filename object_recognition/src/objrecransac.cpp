#include <pcl/io/pcd_io.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/console/parse.h>
#include <pcl/recognition/ransac_based/obj_rec_ransac.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sstream>

typedef pcl::PointXYZ PointType;
typedef pcl::Normal NormalType;

std::string model_filename_;
std::string scene_filename_;

void
showHelp (char *filename)
{
  std::cout << std::endl;
  std::cout << "***************************************************************************" << std::endl;
  std::cout << "*                                                                         *" << std::endl;
  std::cout << "*            Object Recognition RANSAC         - Usage Guide              *" << std::endl;
  std::cout << "*                                                                         *" << std::endl;
  std::cout << "***************************************************************************" << std::endl << std::endl;
  std::cout << "Usage: " << filename << " model_filename.pcd scene_filename.pcd" << std::endl << std::endl;
}

void parseCommandLine (int argc, char *argv[]) {
  //Show help
  if (pcl::console::find_switch (argc, argv, "-h"))
  {
    showHelp (argv[0]);
    exit (0);
  }

  //Model & scene filenames
  std::vector<int> filenames;
  filenames = pcl::console::parse_file_extension_argument (argc, argv, ".pcd");
  if (filenames.size () != 2) {
    std::cout << "Filenames missing.\n";
    showHelp (argv[0]);
    exit (-1);
  }

  model_filename_ = argv[filenames[0]];
  scene_filename_ = argv[filenames[1]];
}

std::string concat(std::string str, int num){
	std::ostringstream oss;
	oss << str << num;
	return oss.str();
}

int main (int argc, char *argv[]) {
  parseCommandLine (argc, argv);

  pcl::PointCloud<PointType>::Ptr model (new pcl::PointCloud<PointType> ());
  pcl::PointCloud<PointType>::Ptr scene (new pcl::PointCloud<PointType> ());
  pcl::PointCloud<NormalType>::Ptr model_normals (new pcl::PointCloud<NormalType> ());
  pcl::PointCloud<NormalType>::Ptr scene_normals (new pcl::PointCloud<NormalType> ());

  //
  //  Load clouds
  //
  if (pcl::io::loadPCDFile (model_filename_, *model) < 0)
  {
    std::cout << "Error loading model cloud." << std::endl;
    showHelp (argv[0]);
    return (-1);
  }
  if (pcl::io::loadPCDFile (scene_filename_, *scene) < 0)
  {
    std::cout << "Error loading scene cloud." << std::endl;
    showHelp (argv[0]);
    return (-1);
  }

  //
  //  Compute Normals
  //
  pcl::NormalEstimationOMP<PointType, NormalType> norm_est;
  norm_est.setKSearch (10);
  norm_est.setInputCloud (model);
  norm_est.compute (*model_normals);

  norm_est.setInputCloud (scene);
  norm_est.compute (*scene_normals);
  
	double pairWidth_d = 0.015;
	float voxelSize_f = 0.01;
	double successProbability_d = 0.99;

	//init visualizer
	pcl::visualization::PCLVisualizer::Ptr visualizer_o_Ptr (new pcl::visualization::PCLVisualizer());
	visualizer_o_Ptr->setSize(640, 480);
	visualizer_o_Ptr->setPosition(640, 0);
	visualizer_o_Ptr->setBackgroundColor(0x00, 0x00, 0x00);
	visualizer_o_Ptr->addCoordinateSystem(1.0);
	visualizer_o_Ptr->initCameraParameters();

	//show scene
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> sceneColor_o (scene, 255, 0, 0);
	visualizer_o_Ptr->addPointCloud<pcl::PointXYZ>(scene, "scene");
	visualizer_o_Ptr->spinOnce(10000);

	pcl::recognition::ObjRecRANSAC recognition_o(pairWidth_d, voxelSize_f);
	recognition_o.addModel(*model,*model_normals,"model");

	std::list<pcl::recognition::ObjRecRANSAC::Output> matchingList_o;

	recognition_o.recognize(*scene, *scene_normals, matchingList_o, successProbability_d);

	//new list for terminal information
	std::list<pcl::recognition::ObjRecRANSAC::Output> temporaryMatchingList_o = matchingList_o;

int num = 0;
	//put out the matched objects on terminal
	while (temporaryMatchingList_o.size() > 0)
	{
		pcl::recognition::ObjRecRANSAC::Output actualMatch_o = temporaryMatchingList_o.back();
		temporaryMatchingList_o.pop_back();
		std::string name_s = actualMatch_o.object_name_;

		std::cout << concat(name_s,num) << "  " << actualMatch_o.match_confidence_*100 << "%" << " match" << std::endl;
		std::cout << actualMatch_o.rigid_transform_[0] << " " << actualMatch_o.rigid_transform_[1] << " " << actualMatch_o.rigid_transform_[2] << " " << actualMatch_o.rigid_transform_[3] << " " << actualMatch_o.rigid_transform_[4] << " " << actualMatch_o.rigid_transform_[5] << " " << actualMatch_o.rigid_transform_[6] << " " << actualMatch_o.rigid_transform_[7] << " " << actualMatch_o.rigid_transform_[8] << " " << actualMatch_o.rigid_transform_[9] << " " << actualMatch_o.rigid_transform_[10] << " " << actualMatch_o.rigid_transform_[11] << std::endl;

		pcl::PointCloud<pcl::PointXYZ> cloud_o = recognition_o.getModel(name_s)->getPointsForRegistration();

		visualizer_o_Ptr->addPointCloud(cloud_o.makeShared(), sceneColor_o, concat(name_s,num));
num++;
	}

	//reload visualizer content
	visualizer_o_Ptr->spinOnce(10000000);

	return 0;

  return (0);
}
