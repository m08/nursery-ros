#include "ros/ros.h"
#include "sensor_msgs/PointCloud2.h"
#include <agrr1_msgs/PointCloudStamped.h>

ros::Publisher pub;
bool exitprog = false;

void pcd2Callback(const sensor_msgs::PointCloud2ConstPtr& msg) {  
	agrr1_msgs::PointCloudStamped ret;
	ret.header.stamp=ros::Time::now();
	ret.cloud=*msg;
	pub.publish(ret);
	exitprog=true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "recog_control");
  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("/camera/depth/points", 1, pcd2Callback);
  pub = n.advertise<agrr1_msgs::PointCloudStamped>("/recognise_object", 1);

  ros::Rate loop_rate(0.1);

  while (ros::ok() && !exitprog)
  {
    ros::spinOnce();
    loop_rate.sleep();
  }

 n.shutdown();

  return 0;
}
