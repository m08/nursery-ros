#include "ros/ros.h"
#include "ros/assert.h"
#include "pcl_ros/point_cloud.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <agrr1_msgs/PointCloudStamped.h>

typedef pcl::PointXYZ PointType;

  pcl::PointCloud<PointType>::Ptr scene (new pcl::PointCloud<PointType> ());
  sensor_msgs::PointCloud2 scenepcd;

ros::Publisher pub;

int main (int argc, char *argv[]) {

  ros::init(argc, argv, "recognition");

  ros::NodeHandle nh;

 std::string s;
    nh.getParam("/pcd_file_broadcaster/scene", s);

ROS_INFO("File \"%s\" given", s.c_str());

  pub = nh.advertise<agrr1_msgs::PointCloudStamped>("/recognise_object", 0);

  //
  //  Load cloud
  //
  if (pcl::io::loadPCDFile (s, *scene) < 0)
  {
    std::cout << "Error loading cloud." << std::endl;
    return (-1);
  }

pcl::toROSMsg(*scene,scenepcd);

agrr1_msgs::PointCloudStamped msg;

ROS_INFO("Broadcasting message in 10 seconds.");

ros::Rate r(0.1);
r.sleep();

msg.cloud = scenepcd;
msg.header.stamp = ros::Time::now();

pub.publish(msg);

nh.shutdown();

  return (0);
}
