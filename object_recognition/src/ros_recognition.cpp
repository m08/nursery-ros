#include "ros/ros.h"
#include "ros/assert.h"
#include "pcl_ros/point_cloud.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/correspondence.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/board.h>

#include <pcl/filters/passthrough.h>

#include <pcl/features/shot_lrf.h>

#include <pcl/keypoints/uniform_sampling.h>
#include <pcl/recognition/cg/hough_3d.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
//#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>

#include <pcl/filters/crop_box.h>

#include <geometry_msgs/Transform.h>
#include <agrr1_msgs/TransformListStamped.h>
#include <agrr1_msgs/PointCloudStamped.h>

//typedef pcl::PointXYZRGBA PointType;
typedef pcl::PointXYZ PointType;
typedef pcl::Normal NormalType;
typedef pcl::ReferenceFrame RFType;
typedef pcl::SHOT352 DescriptorType;

//Algorithm params
bool use_cloud_resolution_ (false);
float model_ss_ ;
float scene_ss_ ;
float rf_rad_ ;
float descr_rad_ ;
float cg_size_ ;
float cg_thresh_ ;

  pcl::PointCloud<PointType>::Ptr model (new pcl::PointCloud<PointType> ());
  pcl::PointCloud<PointType>::Ptr model_keypoints (new pcl::PointCloud<PointType> ());
  pcl::PointCloud<NormalType>::Ptr model_normals (new pcl::PointCloud<NormalType> ());
  pcl::PointCloud<DescriptorType>::Ptr model_descriptors (new pcl::PointCloud<DescriptorType> ());


  pcl::PointCloud<PointType>::Ptr scene_keypoints (new pcl::PointCloud<PointType> ());
  pcl::PointCloud<NormalType>::Ptr scene_normals (new pcl::PointCloud<NormalType> ());
  pcl::PointCloud<DescriptorType>::Ptr scene_descriptors (new pcl::PointCloud<DescriptorType> ());

    pcl::PointCloud<RFType>::Ptr model_rf (new pcl::PointCloud<RFType> ());
    pcl::PointCloud<RFType>::Ptr scene_rf (new pcl::PointCloud<RFType> ());

    //pcl::BOARDLocalReferenceFrameEstimation<PointType, NormalType, RFType> rf_est;
    pcl::SHOTLocalReferenceFrameEstimation<PointType, RFType> rf_est;

  pcl::NormalEstimationOMP<PointType, NormalType> norm_est;
  pcl::UniformSampling<PointType> uniform_sampling;
  pcl::SHOTEstimationOMP<PointType, NormalType, DescriptorType> descr_est;

ros::Publisher pub;

//pcl::visualization::PCLVisualizer viewer ("Correspondence Grouping");

bool crop;
bool clipbox (false);

float xmin, xmax, ymin, ymax, zmin, zmax;

Eigen::Vector4f min, max;

double
computeCloudResolution (const pcl::PointCloud<PointType>::ConstPtr &cloud)
{
  double res = 0.0;
  int n_points = 0;
  int nres;
  std::vector<int> indices (2);
  std::vector<float> sqr_distances (2);
  pcl::search::KdTree<PointType> tree;
  tree.setInputCloud (cloud);

  for (size_t i = 0; i < cloud->size (); ++i)
  {
    if (! pcl_isfinite ((*cloud)[i].x))
    {
      continue;
    }
    //Considering the second neighbor since the first is the point itself.
    nres = tree.nearestKSearch (i, 2, indices, sqr_distances);
    if (nres == 2)
    {
      res += sqrt (sqr_distances[1]);
      ++n_points;
    }
  }
  if (n_points != 0)
  {
    res /= n_points;
  }
  return res;
}


void PclCallback(const agrr1_msgs::PointCloudStamped &pcdmsg) {

 const pcl::PointCloud<PointType>::Ptr msg (new pcl::PointCloud<PointType> ());
pcl::fromROSMsg(pcdmsg.cloud , *msg);
 const pcl::PointCloud<PointType>::Ptr scene (new pcl::PointCloud<PointType> ());
 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);

ROS_INFO("Recognition command received!");

if(crop){
	pcl::CropBox<PointType> cropbox (false);
	cropbox.setInputCloud(msg);
	cropbox.setMin (min);
	cropbox.setMax (max);
	cropbox.filter(*scene);

  norm_est.setInputCloud (scene);
  uniform_sampling.setInputCloud (scene);
  descr_est.setSearchSurface (scene);
  rf_est.setSearchSurface (scene);
} else {
  norm_est.setInputCloud (msg);
  uniform_sampling.setInputCloud (msg);
  descr_est.setSearchSurface (msg);
  rf_est.setSearchSurface (msg);
}
std::cout << "Scene total points: " << msg->size () << "; After cropping: " << scene->size () << std::endl;

  norm_est.compute (*scene_normals);

  uniform_sampling.setRadiusSearch (scene_ss_);
  pcl::PointCloud<int> keypointIndicesScene;
  uniform_sampling.compute(keypointIndicesScene);
if(crop){
  pcl::copyPointCloud(*scene, keypointIndicesScene.points, *scene_keypoints);
  std::cout << "Scene total points: " << scene->size () << "; Selected Keypoints: " << scene_keypoints->size () << std::endl;
} else {
  pcl::copyPointCloud(*msg, keypointIndicesScene.points, *scene_keypoints);
  std::cout << "Scene total points: " << msg->size () << "; Selected Keypoints: " << scene_keypoints->size () << std::endl;
}
//viewer.removeAllPointClouds(0);
//viewer.addPointCloud(scene_keypoints,"scene");

  descr_est.setInputCloud (scene_keypoints);
  descr_est.setInputNormals (scene_normals);
  descr_est.compute (*scene_descriptors);


  //
  //  Find Model-Scene Correspondences with KdTree
  //
  pcl::CorrespondencesPtr model_scene_corrs (new pcl::Correspondences ());

  pcl::KdTreeFLANN<DescriptorType> match_search;
  match_search.setInputCloud (model_descriptors);

  //  For each scene keypoint descriptor, find nearest neighbor into the model keypoints descriptor cloud and add it to the correspondences vector.
  for (size_t i = 0; i < scene_descriptors->size (); ++i)
  {
    std::vector<int> neigh_indices (1);
    std::vector<float> neigh_sqr_dists (1);
    if (!pcl_isfinite (scene_descriptors->at (i).descriptor[0])) //skipping NaNs
    {
      continue;
    }
    int found_neighs = match_search.nearestKSearch (scene_descriptors->at (i), 1, neigh_indices, neigh_sqr_dists);
    if(found_neighs == 1 && neigh_sqr_dists[0] < 0.25f) //  add match only if the squared descriptor distance is less than 0.25 (SHOT descriptor distances are between 0 and 1 by design)
    {
      pcl::Correspondence corr (neigh_indices[0], static_cast<int> (i), neigh_sqr_dists[0]);
      model_scene_corrs->push_back (corr);
    }
  }
  std::cout << "Correspondences found: " << model_scene_corrs->size () << std::endl;

  //
  //  Actual Clustering
  //
  std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > rototranslations;
  std::vector<pcl::Correspondences> clustered_corrs;


    rf_est.setInputCloud (scene_keypoints);
    //rf_est.setInputNormals (scene_normals);
    rf_est.compute (*scene_rf);

    //  Clustering
    pcl::Hough3DGrouping<PointType, PointType, RFType, RFType> clusterer;
    clusterer.setHoughBinSize (cg_size_);
    clusterer.setHoughThreshold (cg_thresh_);
    clusterer.setUseInterpolation (true);
    clusterer.setUseDistanceWeight (false);

    clusterer.setInputCloud (model_keypoints);
    clusterer.setInputRf (model_rf);
    clusterer.setSceneCloud (scene_keypoints);
    clusterer.setSceneRf (scene_rf);
    clusterer.setModelSceneCorrespondences (model_scene_corrs);

    clusterer.cluster (clustered_corrs);
    clusterer.recognize (rototranslations, clustered_corrs);
  
   //
  //  Output results
  //
  std::cout << "Model instances found: " << rototranslations.size () << std::endl;

  agrr1_msgs::TransformListStamped ret;
  ret.header.stamp = pcdmsg.header.stamp;

  for (size_t i = 0; i < rototranslations.size (); ++i) {
    pcl::PointCloud<PointType>::Ptr rotated_model (new pcl::PointCloud<PointType> ());
    pcl::transformPointCloud (*model, *rotated_model, rototranslations[i]);

    std::stringstream ss_cloud;
    ss_cloud << "instance" << i;

    //pcl::visualization::PointCloudColorHandlerCustom<PointType> rotated_model_color_handler (rotated_model, 255, 0, 0);
    //viewer.addPointCloud (rotated_model, rotated_model_color_handler, ss_cloud.str ());


    std::cout << "\n    Instance " << i + 1 << ":" << std::endl;
    std::cout << "        Correspondences belonging to this instance: " << clustered_corrs[i].size () << std::endl;

    // Print the rotation matrix and translation vector
    Eigen::Quaternionf rotation (rototranslations[i].block<3,3>(0, 0));
    Eigen::Vector3f translation = rototranslations[i].block<3,1>(0, 3);

    geometry_msgs::Transform transform;
    transform.translation.x = (double)(translation(0));
    transform.translation.y = (double)(translation(1));
    transform.translation.z = (double)(translation(2));
    transform.rotation.x = rotation.x();
    transform.rotation.y = rotation.y();
    transform.rotation.z = rotation.z();
    transform.rotation.w = rotation.w();

    ret.transforms.push_back(transform);
  }
  pub.publish(ret);

}

int main (int argc, char *argv[]) {

  ros::init(argc, argv, "recognition");

  ros::NodeHandle nh;

 std::string s;
    nh.getParam("/object_recognition/model", s);


//bool visualisation;

//nh.param("/object_recognition/visualisation",visualisation,true);

bool axes;

//nh.param("/object_recognition/draw_axes",axes,false);

nh.param("/object_recognition/crop",crop,false);
if(crop){
	//nh.param("/object_recognition/draw_cropbox",clipbox,false);
	nh.param("/object_recognition/crop_xmin",xmin,-5.0f);
	nh.param("/object_recognition/crop_xmax",xmax,5.0f);
	nh.param("/object_recognition/crop_ymin",ymin,0.0f);
	nh.param("/object_recognition/crop_ymax",ymax,5.0f);
	nh.param("/object_recognition/crop_zmin",zmin,0.0f);
	nh.param("/object_recognition/crop_zmax",zmax,3.0f);
}

Eigen::Vector4f mintemp (xmin, ymin, zmin, 0.0f);
Eigen::Vector4f maxtemp (xmax, ymax, zmax, 0.0f);

min = mintemp;
max = maxtemp;

/*double pos_x;
double pos_y;
double pos_z;
double view_x;
double view_y;
double view_z;
double up_x;
double up_y;
double up_z;

nh.param("/object_recognition/campos_x",pos_x,0.49);
nh.param("/object_recognition/campos_y",pos_y,0.49);
nh.param("/object_recognition/campos_z",pos_z,3.90299);
nh.param("/object_recognition/camview_x",view_x,0.49);
nh.param("/object_recognition/camview_y",view_y,0.49);
nh.param("/object_recognition/camview_z",view_z,0.49);
nh.param("/object_recognition/camup_x",up_x,0.00);
nh.param("/object_recognition/camup_y",up_y,1.00);
nh.param("/object_recognition/camup_z",up_z,0.00);

double near;
double far;

nh.param("/object_recognition/clip_near",near,0.011);
nh.param("/object_recognition/clip_far",far,11.612);
*/
double model_ss_d (0.01);
double scene_ss_d (0.02);
double rf_rad_d (0.015);
double descr_rad_d (0.05);
double cg_size_d (0.01);
double cg_thresh_d (5.0);

nh.getParam("/object_recognition/use_cloud_reolution",use_cloud_resolution_);
nh.getParam("/object_recognition/model_ss",model_ss_d);
nh.getParam("/object_recognition/scene_ss",scene_ss_d);
nh.getParam("/object_recognition/rf_rad",rf_rad_d);
nh.getParam("/object_recognition/descr_rad",descr_rad_d);
nh.getParam("/object_recognition/cg_size",cg_size_d);
nh.getParam("/object_recognition/cg_thresh",cg_thresh_d);

model_ss_ = (float) model_ss_d;
scene_ss_ = (float) scene_ss_d;
rf_rad_ = (float) rf_rad_d;
descr_rad_ = (float) descr_rad_d;
cg_size_ = (float) cg_size_d;
cg_thresh_ = (float) cg_thresh_d;

ROS_INFO("model_ss_ is %f",model_ss_); 
ROS_INFO("scene_ss_ is %f",scene_ss_);
ROS_INFO("rf_rad_ is %f",rf_rad_ );
ROS_INFO("descr_rad_ is %f",descr_rad_);
ROS_INFO("cg_size_ is %f",cg_size_ );
ROS_INFO("cg_thresh_ is %f",cg_thresh_); 

ROS_INFO("File \"%s\" given", s.c_str());

  ros::Subscriber sub = nh.subscribe("/recognise_object", 1, PclCallback);
  pub = nh.advertise<agrr1_msgs::TransformListStamped>("/recognised", 100);

  //
  //  Load cloud
  //
  if (pcl::io::loadPCDFile (s, *model) < 0)
  {
    std::cout << "Error loading model cloud." << std::endl;
    return (-1);
  }


  //
  //  Set up resolution invariance
  //
  if (use_cloud_resolution_)
  {
    float resolution = static_cast<float> (computeCloudResolution (model));
    if (resolution != 0.0f)
    {
      model_ss_   *= resolution;
      scene_ss_   *= resolution;
      rf_rad_     *= resolution;
      descr_rad_  *= resolution;
      cg_size_    *= resolution;
    }

    std::cout << "Model resolution:       " << resolution << std::endl;
    std::cout << "Model sampling size:    " << model_ss_ << std::endl;
    std::cout << "Scene sampling size:    " << scene_ss_ << std::endl;
    std::cout << "LRF support radius:     " << rf_rad_ << std::endl;
    std::cout << "SHOT descriptor radius: " << descr_rad_ << std::endl;
    std::cout << "Clustering bin size:    " << cg_size_ << std::endl << std::endl;
  }

  //
  //  Compute Normals
  //
  norm_est.setKSearch (10);
  norm_est.setInputCloud (model);
  norm_est.compute (*model_normals);

  //
  //  Downsample Cloud to Extract keypoints
  //
	uniform_sampling.setInputCloud (model);
	uniform_sampling.setRadiusSearch (model_ss_);
	pcl::PointCloud<int> keypointIndices;
	uniform_sampling.compute(keypointIndices);
	pcl::copyPointCloud(*model, keypointIndices.points, *model_keypoints);
	std::cout << "Model total points: " << model->size () << "; Selected Keypoints: " << model_keypoints->size () << std::endl;

  //
  //  Compute Descriptor for keypoints
  //
  descr_est.setRadiusSearch (descr_rad_);

  descr_est.setInputCloud (model_keypoints);
  descr_est.setInputNormals (model_normals);
  descr_est.setSearchSurface (model);
  descr_est.compute (*model_descriptors);


  
  //  Using Hough3D
    //
    //  Compute (Keypoints) Reference Frames only for Hough
    //


pcl::search::KdTree<PointType>::Ptr kd(new pcl::search::KdTree<PointType> ());
  rf_est.setSearchMethod(kd);

    //rf_est.setFindHoles (true);
    rf_est.setRadiusSearch (rf_rad_);

    rf_est.setInputCloud (model_keypoints);
    //rf_est.setInputNormals (model_normals);
    rf_est.setSearchSurface (model);
    rf_est.compute (*model_rf);


/*if(visualisation){
	//Visualisation
	  pcl::PointCloud<PointType>::Ptr off_scene_model (new pcl::PointCloud<PointType> ());
	  pcl::PointCloud<PointType>::Ptr off_scene_model_keypoints (new pcl::PointCloud<PointType> ());

	if(axes)
		viewer.addCoordinateSystem(1.0f,"axes",0);
	viewer.setCameraPosition(pos_x,pos_y,pos_z,view_x,view_y,view_z,up_x,up_y,up_z,0); 
	viewer.setCameraClipDistances (near,far,0);	

if(clipbox)
	viewer.addCube (xmin,xmax,ymin,ymax,zmin,zmax, 1.0, 1.0, 1.0, "cube", 0);

	  if (show_correspondences_ || show_keypoints_)
	  {
	    //  We are translating the model so that it doesn't end in the middle of the scene representation
	    pcl::transformPointCloud (*model, *off_scene_model, Eigen::Vector3f (-1,0,0), Eigen::Quaternionf (1, 0, 0, 0));
	    pcl::transformPointCloud (*model_keypoints, *off_scene_model_keypoints, Eigen::Vector3f (-1,0,0), Eigen::Quaternionf (1, 0, 0, 0));

	    pcl::visualization::PointCloudColorHandlerCustom<PointType> off_scene_model_color_handler (off_scene_model, 255, 255, 128);
	    viewer.addPointCloud (off_scene_model, off_scene_model_color_handler, "off_scene_model");
	  }

	  if (show_keypoints_)
	  {
	    pcl::visualization::PointCloudColorHandlerCustom<PointType> scene_keypoints_color_handler (scene_keypoints, 0, 0, 255);
	    viewer.addPointCloud (scene_keypoints, scene_keypoints_color_handler, "scene_keypoints");
	    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "scene_keypoints");

	    pcl::visualization::PointCloudColorHandlerCustom<PointType> off_scene_model_keypoints_color_handler (off_scene_model_keypoints, 0, 0, 255);
	    viewer.addPointCloud (off_scene_model_keypoints, off_scene_model_keypoints_color_handler, "off_scene_model_keypoints");
	    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "off_scene_model_keypoints");
	  }

	  
	while (!viewer.wasStopped ()) {
    		ros::spinOnce();
    		viewer.spinOnce ();
  	}
} else {*/
	ros::spin();
//}

  return (0);
}
