#include <ros.h>
#include <arduino_rgbled/rgb_led.h>

ros::NodeHandle nh;
//define pin nummers
int red = 9;
int green = 10;
int blue = 11;


int collor_red=0;
int collor_green=0;
int collor_blue=0;
int mode=0;
float T_on=0;
float T_off=0;
unsigned long pervious_time = 0;
unsigned long time = 0;
boolean on = false;


void set_led(const arduino_rgbled::rgb_led &set){
  collor_red = set.red;
  collor_green = set.green;
  collor_blue = set.blue;
  mode = set.mode;
  T_on = set.T_on_ms;
  T_off = set.T_off_ms;
}
  
  
//seting rgb led
void spin(int collor_red, int collor_green, int collor_blue, int mode, float T_on, float T_off){
 if (mode == 0){
   analogWrite(red, 0);
   analogWrite(green, 0);
   analogWrite(blue, 0);
 }
 
 if(mode == 1){
   analogWrite(red, collor_red);
   analogWrite(green, collor_green);
   analogWrite(blue, collor_blue);
 }
 
 if(mode == 2){
   if(T_on == 0){
     T_on = 1000;
     T_off = 1000;
   }
   
   if(T_on != 0 && T_off == 0){
     T_off = T_on;
   }
   
   time = millis();
   
   if(on == false){
     if(time - pervious_time > T_off){
       analogWrite(red, collor_red);
       analogWrite(green, collor_green);
       analogWrite(blue, collor_blue);
       pervious_time = time;
       on = true;
     }
   }
   
   if(on == true){
     if(time - pervious_time > T_on){
       analogWrite(red, 0);
       analogWrite(green, 0);
       analogWrite(blue, 0);
       pervious_time = time;
       on = false;
     }
   }
 }
  
}
  
  

ros::Subscriber<arduino_rgbled::rgb_led> sub("/rgb_led", &set_led);

void setup(){
  time = 0;
  pervious_time = 0;
  on = false;
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);
}

void loop(){
  nh.spinOnce();
  Serial.print("s");
  spin(collor_red, collor_green, collor_blue, mode, T_on, T_off);
  delay(1);
}
