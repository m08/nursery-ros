//ROS
#include <ros.h>
#include "Wire.h"
#include "arduino_msgs/SensorFeedback.h"
#include "arduino_msgs/DisableSensors.h"
#include "arduino_msgs/NumberSensors.h"

#define SENSOR_ADDRESS(x) (0x70 + x)

using namespace arduino_msgs;

ros::NodeHandle nh;
int n_sensors;

//declare outgoing messages
arduino_msgs::SensorFeedback sensor_msg;

//topic to publish ultrasone sensor data on
ros::Publisher  feedback_pub("/ultrasone/distance_feedback", &sensor_msg);
boolean disable_rear, disable_all;

int count_sensors = 4;

/*
 * Polls to check whether sensors need to be disabled.
 */
void disableCB(const arduino_msgs::DisableSensors &msg) {
    disable_all  = msg.disable_all;
    disable_rear = msg.disable_rear;  
}

void numberSensors(const arduino_msgs::NumberSensors &msg) {
    count_sensors = msg.number_sensors;
}

ros::Subscriber<arduino_msgs::DisableSensors> disable_sub("/ultrasone/disable", &disableCB);
//ros::Subscriber<arduino_msgs::NumberSensors> number_sub("/ultrasone/number", &numberSensors);

void readSensors() {
  if (disable_all)
     return;
    
        //trigger sensors
        for (int sensor = 0; sensor < SensorFeedback::sensor_count; sensor = sensor++) {  
                doSRF02Pulse(0x70 + sensor);
        }
            
        //wait for sound to return
        delay(70);

        //read sensor values
        for (int sensor = 0; sensor < SensorFeedback::sensor_count; sensor = sensor++) {
                sensor_msg.data[sensor] = windowFilter(readSRF02(0x70 + sensor)); 
        }
        
        feedback_pub.publish(&sensor_msg);
    delay(100);
    }

   

/*
*  Filters the sensor data within a given window frame
*/
int windowFilter(int data) {
	if(data > 14 && data < 150)
		return data;
	
	return 150;
}

/*
 * Trigger sensor to do a burst
 */
void doSRF02Pulse(int address) {
	Wire.beginTransmission(address); // transmit to device
	Wire.write(byte(0x00));          // sets register pointer to the command register (0x00)  
	Wire.write(byte(0x51));          // use 0x51 for measurement in centimeters
	Wire.endTransmission();          // stop transmitting 
}

/*
 * Read address from SRF02 sensor register
 */
int readSRF02(int address) {
int range = 0;
Wire.beginTransmission(address);       // transmit to device
Wire.write(byte(0x02));                // sets register pointer to echo #1 register (0x02)
Wire.endTransmission();                // stop transmitting

// step 4: request reading from sensor
Wire.requestFrom(address,2);           // request 2 bytes from slave device

// step 5: receive reading from sensor
  if(2 <= Wire.available()) {          // if two bytes were received 
      range = Wire.read();             // receive high byte (overwrites previous reading)
      range = range << 8;              // shift high byte to be high 8 bits
      range |= Wire.read();            // receive low byte as lower 8 bits
  }

return range;
}

void setup() {
  Wire.begin();
  Serial.begin(57600);
  nh.initNode();
  nh.subscribe(disable_sub);
  //nh.subscribe(number_sub);
  nh.advertise(feedback_pub);
  /*while(!nh.connected())
  {
 nh.spinOnce();
  printf("waiting in param");
  }
  
  if(!nh.getParam("~number_sensors", &n_sensors,1))
    n_sensors = 4;
    
    Serial.print("parameter read " + n_sensors);*/
    
    sensor_msg.data_length = SensorFeedback::sensor_count;
}

void loop() {
  readSensors();
  nh.spinOnce();
}

