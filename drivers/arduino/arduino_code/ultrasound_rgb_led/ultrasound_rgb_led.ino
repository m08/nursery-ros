//ROS
#include <ros.h>
#include "Wire.h"
#include "arduino_msgs/SensorFeedback.h"
#include "arduino_msgs/DisableSensors.h"
#include <arduino_rgbled/rgb_led.h>

#define SENSOR_ADDRESS(x) (0x70 + x)

using namespace arduino_msgs;

ros::NodeHandle nh;
int n_sensors;

//declare outgoing messages
arduino_msgs::SensorFeedback sensor_msg;

//topic to publish ultrasone sensor data on
ros::Publisher  feedback_pub("/ultrasone/distance_feedback", &sensor_msg);
boolean disable_rear, disable_all;

int count_sensors = 4;

//define pin nummers rgb led
int red = 9;
int green = 10;
int blue = 11;

//define rgb led data
int collor_red=0;
int collor_green=0;
int collor_blue=0;
int mode=0;
float T_on=0;
float T_off=0;
long pervious_time = 0;
long time = 0;
boolean on = false;

/*
 * Polls to check whether sensors need to be disabled.
 */
void disableCB(const arduino_msgs::DisableSensors &msg) {
    disable_all  = msg.disable_all;
    disable_rear = msg.disable_rear;  
}

ros::Subscriber<arduino_msgs::DisableSensors> disable_sub("/ultrasone/disable", &disableCB);

void readSensors() {
  if (disable_all)
     return;
    
        //trigger sensors
        for (int sensor = 0; sensor < SensorFeedback::sensor_count; sensor = sensor++) {  
                doSRF02Pulse(0x70 + sensor);
        }
            
        //wait for sound to return
        delay(70);

        //read sensor values
        for (int sensor = 0; sensor < SensorFeedback::sensor_count; sensor = sensor++) {
                sensor_msg.data[sensor] = windowFilter(readSRF02(0x70 + sensor)); 
        }
        
        feedback_pub.publish(&sensor_msg);
    delay(100);
    }

   

/*
*  Filters the sensor data within a given window frame
*/
int windowFilter(int data) {
	if(data > 14 && data < 150)
		return data;
	
	return 150;
}

/*
 * Trigger sensor to do a burst
 */
void doSRF02Pulse(int address) {
	Wire.beginTransmission(address); // transmit to device
	Wire.write(byte(0x00));          // sets register pointer to the command register (0x00)  
	Wire.write(byte(0x51));          // use 0x51 for measurement in centimeters
	Wire.endTransmission();          // stop transmitting 
}

/*
 * Read address from SRF02 sensor register
 */
int readSRF02(int address) {
int range = 0;
Wire.beginTransmission(address);       // transmit to device
Wire.write(byte(0x02));                // sets register pointer to echo #1 register (0x02)
Wire.endTransmission();                // stop transmitting

// step 4: request reading from sensor
Wire.requestFrom(address,2);           // request 2 bytes from slave device

// step 5: receive reading from sensor
  if(2 <= Wire.available()) {          // if two bytes were received 
      range = Wire.read();             // receive high byte (overwrites previous reading)
      range = range << 8;              // shift high byte to be high 8 bits
      range |= Wire.read();            // receive low byte as lower 8 bits
  }

return range;
}


//recieving data for rgb led
void set_led(const arduino_rgbled::rgb_led &set){
  collor_red = set.red;
  collor_green = set.green;
  collor_blue = set.blue;
  mode = set.mode;
  T_on = set.T_on_ms;
  T_off = set.T_off_ms;
}

//subscriber for geb led
ros::Subscriber<arduino_rgbled::rgb_led> sub("/rgb_led", &set_led);


//seting rgb led
void spin(int collor_red, int collor_green, int collor_blue, int mode, float T_on, float T_off){
 if (mode == 0){
   analogWrite(red, 0);
   analogWrite(green, 0);
   analogWrite(blue, 0);
 }
 
 if(mode == 1){
   analogWrite(red, collor_red);
   analogWrite(green, collor_green);
   analogWrite(blue, collor_blue);
 }
 
 if(mode == 2){
   if(T_on == 0){
     T_on = 1000;
     T_off = 1000;
   }
   
   if(T_on != 0 && T_off == 0){
     T_off = T_on;
   }
   
   time = millis();
   
   if(on == false){
     if(time - pervious_time > T_off){
       analogWrite(red, collor_red);
       analogWrite(green, collor_green);
       analogWrite(blue, collor_blue);
       pervious_time = time;
       on = true;
     }
   }
   
   if(on == true){
     if(time - pervious_time > T_on){
       analogWrite(red, 0);
       analogWrite(green, 0);
       analogWrite(blue, 0);
       pervious_time = time;
       on = false;
     }
   }
 }
  
}
  
  

void setup() {
  Wire.begin();
  Serial.begin(57600);
  nh.initNode();
  nh.subscribe(disable_sub);
  //nh.subscribe(number_sub);
  nh.advertise(feedback_pub);
  /*while(!nh.connected())
  {
 nh.spinOnce();
  printf("waiting in param");
  }
  
  if(!nh.getParam("~number_sensors", &n_sensors,1))
    n_sensors = 4;
    
    Serial.print("parameter read " + n_sensors);*/
    
    sensor_msg.data_length = SensorFeedback::sensor_count;
  
  //pinmode for rgb led 
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);
  //subscriber for rgb led
  nh.subscribe(sub);
  //set blink
  time = 0;
  pervious_time = 0;
  on = false;
}

void loop() {
  readSensors();
  nh.spinOnce();
  spin(collor_red, collor_green, collor_blue, mode, T_on, T_off);

}

