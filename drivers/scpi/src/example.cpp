#include <scpi/scpi.h>

#define SINGLE

int main(int argc, char **argv)
{
  ros::init(argc, argv, "scpi_example");

  scpi::Interface scpi;
  
  scpi.init("/comm");
  sleep(1);
  
#ifdef SINGLE  
  scpi.configure(scpi::VoltageDC);
  sleep(1);
  printf("Measured %fV\n", scpi.read());
  printf("Measured %fV\n", scpi.read());
  printf("Measured %fV\n", scpi.read());
#else
  scpi.configure(101, scpi::VoltageDC);
  scpi.configure(102, scpi::Resistance);
  sleep(1);
  std::vector<int> channels;
  channels.push_back(101);
  channels.push_back(102);
  std::vector<float> values;
  for (int ii=0; ii < 10; ++ii)
  {
    values = scpi.scan(channels);
    if (values.size() != 2) break;
    
    printf("Measured %fV, %fOhm\n", values[0], values[1]);
  }
#endif
  
  return 0;
}
