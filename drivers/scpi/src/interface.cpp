#include <ros/ros.h>
#include <scpi/scpi.h>
#include <shared_serial/SendTo.h>
#include <shared_serial/Recv.h>
#include <shared_serial/Flush.h>

using namespace scpi;

void Interface::init(const char *path)
{
  serial_.init(path);

  ros::Rate loop_rate(1);
  while (ros::ok() && serial_.sendto(0, (const unsigned char *)"SYST:REM\n", 9, 0) < 0)
  {
    ROS_WARN_ONCE("Couldn't initialize SCPI device, retrying every second");
    loop_rate.sleep();
  }

  if (ros::ok())
    ROS_INFO("SCPI interface initialized");
}

void Interface::configure(int channel, MeasurementType type, double range, double res)
{
  std::ostringstream oss;
  
  oss << "CONF:" << typeToString(type);
  
  if (range)
  {
    oss << " " << range << ",";
    if (res)
      oss << res;
    else
      oss << "DEF";
  }
  
  if (channel)
    oss << " (@" << channel << ")";

  oss << "\n";
  
  if (serial_.sendto(0, (const unsigned char *)oss.str().c_str(), oss.str().size(), 0) < 0)
    ROS_WARN("Couldn't set SCPI configuration");
}

float Interface::read(void)
{
  serial_.flush(0, 0);

  int socket = serial_.sendto(0, (const unsigned char *)"READ?\n", 6, 1);

  if (socket <= 0)
  {
    ROS_WARN("Couldn't query SCPI measurement");
    return std::numeric_limits<float>::quiet_NaN();
  }

  unsigned char buf[18];
  size_t length=0;

  serial_.recv(socket, 17, 1, 0, buf, &length);
  buf[length] = '\0';

  if (!length)
  {
    ROS_WARN("Couldn't read SCPI measurement");
    return std::numeric_limits<float>::quiet_NaN();
  }

  return atof((char*)buf);
}

std::vector<float> Interface::scan(const std::vector<int> &channels)
{
  std::ostringstream oss;
  oss << "(@";
  for (size_t ii=0; ii < channels.size(); ++ii)
  {
    if (ii) oss << ",";
    oss << channels[ii];
  }
  oss << ")\n";
  
  std::string scan = "ROUT:SCAN " + oss.str();
  std::string delay = "ROUT:CHAN:DEL 0.1," + oss.str();
  
  int socket = serial_.sendto(0, (const unsigned char *)scan.c_str(), scan.size(), 1);
  serial_.sendto(socket, (const unsigned char *)delay.c_str(), delay.size(), 1);
  serial_.flush(socket, 1);
  serial_.sendto(socket, (const unsigned char *)"READ?\n", 6, 1);

  std::vector<float> values;
  if (socket <= 0)
  {
    ROS_WARN("Couldn't query SCPI measurements");
    return values;
  }

  unsigned char buf[4096];
  size_t expected = 16*channels.size()+1, length=0;
  serial_.recv(socket, expected, 10, 0, buf, &length);
  buf[length] = '\0';
  
  if (length != expected)
  {
    ROS_WARN("Couldn't read SCPI measurements");
    return values;
  }
  
  for (size_t ii=0; ii < channels.size(); ++ii)
    values.push_back(atof((char*)&buf[ii*16]));

  return values;
}

std::string Interface::typeToString(MeasurementType type) const
{
  switch (type)
  {
    case VoltageDC:
      return "VOLT:DC";
      break;
    case VoltageAC:
      return "VOLT:AC";
      break;
    case CurrentDC:
      return "CURR:DC";
      break;
    case CurrentAC:
      return "CURR:AC";
      break;
    case Resistance:
      return "RES";
      break;
    case Frequency:
      return "FREQ";
      break;
    case Period:
      return "PER";
      break;
    case Continuity:
      return "CONT";
      break;
    case Diode:
      return "DIOD";
      break;
    default:
      ROS_ERROR("Unknown measurement type");
      return "";
  }
}
