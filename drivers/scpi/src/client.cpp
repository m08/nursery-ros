#include <ros/ros.h>
#include <scpi/scpi.h>

using namespace scpi;

void Client::init(const char *path)
{
  nh_ = ros::NodeHandle(path);

  configure_client_ = nh_.serviceClient<scpi::Configure>("configure", true);
  configure_client_.waitForExistence();

  read_client_ = nh_.serviceClient<scpi::Read>("read", true);
  read_client_.waitForExistence();

  scan_client_ = nh_.serviceClient<scpi::Scan>("scan", true);
  scan_client_.waitForExistence();

  ROS_INFO("SCPI client initialized");
}

void Client::configure(int channel, MeasurementType type, double range, double res)
{
  scpi::Configure srv;

  srv.request.channel = channel;
  srv.request.type = type;
  srv.request.range = range;
  srv.request.resolution = res;
  configure_client_.call(srv);
}

float Client::read()
{
  scpi::Read srv;

  read_client_.call(srv);
  return srv.response.value;
}

std::vector<float> Client::scan(const std::vector<int> &channels)
{
  scpi::Scan srv;

  srv.request.channels = channels;
  scan_client_.call(srv);
  return srv.response.values;
}
