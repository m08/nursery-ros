#include <ros/ros.h>
#include <scpi/scpi.h>

using namespace scpi;

void Server::init(const char *path)
{
  interface_.init(path);

  configure_srv_ = nh_.advertiseService("configure", &Server::callbackConfigure, this);
  read_srv_ = nh_.advertiseService("read", &Server::callbackRead, this);
  scan_srv_ = nh_.advertiseService("scan", &Server::callbackScan, this);

  ROS_INFO("Initialized");
}

void Server::spin()
{
  ROS_INFO("Spinning");

  ros::spin();
}

bool Server::callbackConfigure(scpi::Configure::Request& req, scpi::Configure::Response& res)
{
  interface_.configure(req.channel, (MeasurementType)req.type, req.range, req.resolution);
  return true;
}

bool Server::callbackRead(scpi::Read::Request& req, scpi::Read::Response& res)
{
  res.value = interface_.read();
  return true;
}

bool Server::callbackScan(scpi::Scan::Request& req, scpi::Scan::Response& res)
{
  res.values = interface_.scan(req.channels);
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "scpi");

  const char *path="/comm";
  if (argc > 1)
    path = argv[1];

  scpi::Server scpi;

  scpi.init(path);
  scpi.spin();

  ROS_INFO("Exiting");

  return 0;
}
