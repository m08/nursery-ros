#ifndef __SCPI_H
#define __SCPI_H

#include <ros/ros.h>

#include <shared_serial/client.h>

#include <scpi/Configure.h>
#include <scpi/Read.h>
#include <scpi/Scan.h>

namespace scpi {

typedef enum { VoltageDC, VoltageAC, CurrentDC, CurrentAC, Resistance, Frequency, Period, Continuity, Diode } MeasurementType;

class Interface
{
  protected:
    SerialClient serial_;

  public:
    /// Initialize interface
    /** \param path path to shared_serial node */
    void init(const char *path="/comm");

    void configure(MeasurementType type, double range=0, double res=0) { configure(0, type, range, res); }
    void configure(int channel, MeasurementType type, double range=0, double res=0);
    float read();
    std::vector<float> scan(const std::vector<int> &channels);
    
  protected:
    std::string typeToString(MeasurementType type) const;
};

class Server
{
  protected:
    ros::NodeHandle nh_;          ///< ROS node handle
    ros::ServiceServer configure_srv_;
    ros::ServiceServer read_srv_;
    ros::ServiceServer scan_srv_;
    scpi::Interface interface_;

  public:
    /// Constructor
    Server() : nh_("~") { }

    /// Destructor
    ~Server()
    {
      nh_.shutdown();
    }

    /// Initialize node
    /** \param path path to shared_serial node */
    void init(const char *path="/comm");

    /// Spin
    void spin();

    bool callbackConfigure(scpi::Configure::Request& req, scpi::Configure::Response& res);
    bool callbackRead(scpi::Read::Request& req, scpi::Read::Response& res);
    bool callbackScan(scpi::Scan::Request& req, scpi::Scan::Response& res);
};

class Client
{
  protected:
    ros::NodeHandle nh_;                   ///< ROS node handle
    ros::ServiceClient read_client_;
    ros::ServiceClient scan_client_;
    ros::ServiceClient configure_client_;

  public:
    /// Initialize node
    /** \param path path to scpi server node */
    void init(const char *path="/scpi");

    void configure(MeasurementType type, double range=0, double res=0) { configure(0, type, range, res); }
    void configure(int channel, MeasurementType type, double range=0, double res=0);
    float read();
    std::vector<float> scan(const std::vector<int> &channels);
};

}

#endif /* __SCPI_H */
