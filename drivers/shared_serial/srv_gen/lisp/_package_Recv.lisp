(cl:in-package shared_serial-srv)
(cl:export '(SOCKET-VAL
          SOCKET
          LENGTH-VAL
          LENGTH
          RECV_TIMEOUT-VAL
          RECV_TIMEOUT
          SOCK_TIMEOUT-VAL
          SOCK_TIMEOUT
          SOCKET-VAL
          SOCKET
          DATA-VAL
          DATA
))