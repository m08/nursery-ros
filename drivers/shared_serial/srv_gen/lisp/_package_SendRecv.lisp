(cl:in-package shared_serial-srv)
(cl:export '(SOCKET-VAL
          SOCKET
          SEND_DATA-VAL
          SEND_DATA
          LENGTH-VAL
          LENGTH
          RECV_TIMEOUT-VAL
          RECV_TIMEOUT
          SOCK_TIMEOUT-VAL
          SOCK_TIMEOUT
          SOCKET-VAL
          SOCKET
          RECV_DATA-VAL
          RECV_DATA
))