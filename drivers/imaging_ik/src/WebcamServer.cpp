#include "imaging_ik/WebcamServer.h"

WebcamServer::WebcamServer() :
	mImageTransport(mNodeHandle),
	mCapture(CV_CAP_ANY)
{
	if (mCapture.isOpened() == false)
	{
		ROS_ERROR("Failed to open webcam.");
		return;
	}

	mCapture.set(CV_CAP_PROP_FRAME_HEIGHT, 800);
	mCapture.set(CV_CAP_PROP_FRAME_WIDTH, 1280);

	mImagePub = mImageTransport.advertise("/camera/rgb/image_color", 1);
	mRGBServer = mNodeHandle.advertiseService("/WebcamServer/RGBServer", &WebcamServer::RGBServer, this);
}

WebcamServer::~WebcamServer()
{
	mCapture.release();
}

void WebcamServer::spin()
{
	bool show_fps;
	double desired_fps;
	mNodeHandle.param<bool>("/WebcamServer/show_fps", show_fps, false);
	mNodeHandle.param<double>("/WebcamServer/fps", desired_fps, 30.0);
	double fps = 0.0;
	int fpsCount = 0;

	ros::Rate rate(desired_fps);

	while (ros::ok() && mCapture.isOpened())
	{
		ros::spinOnce();
		rate.sleep();

		if (show_fps)
		{
			double cycleTime = rate.cycleTime().toSec();
			if (cycleTime != 0.0)
			{
				fps += (1.0 / cycleTime);
				fpsCount++;

				if (fpsCount == 10)
				{
					ROS_INFO("Rate: %lf", std::min(desired_fps, fps / fpsCount));
					fps = 0.0;
					fpsCount = 0;
				}
			}
		}

		if (mImagePub.getNumSubscribers() == 0)
			continue;

		cv::Mat frame;
		mCapture >> frame;

		if (frame.empty())
			continue;

		mImagePub.publish(OpenCVTools::matToImage(frame));
	}
}

bool WebcamServer::RGBServer(imaging_ik::RGB::Request &req, imaging_ik::RGB::Response &res)
{
	if (mCapture.isOpened() == false)
	{
		ROS_WARN("Received RGB request while camera is not open.");
		return false;
	}

	cv::Mat frame;
	mCapture >> frame;
	res.image = *OpenCVTools::matToImage(frame);
	return true;
}

int main(int argc, char *argv[])
{
	// init ros
	ros::init(argc, argv, "WebcamServer");

	WebcamServer ws;
	ws.spin();

	return 0;
}
