/*
 * WebcamServer.h
 *
 *  Created on: Jun 14, 2012
 *      Author: hans
 */

#ifndef WEBCAMSERVER_H_
#define WEBCAMSERVER_H_

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "imaging_ik/OpenCVTools.h"
#include "imaging_ik/RGB.h"

class WebcamServer
{
private:
	ros::NodeHandle mNodeHandle;
	image_transport::ImageTransport mImageTransport;
	image_transport::Publisher mImagePub;
	ros::ServiceServer mRGBServer;

	cv::VideoCapture mCapture;

public:
	WebcamServer();
	~WebcamServer();

	void spin();

	bool RGBServer(imaging_ik::RGB::Request &req, imaging_ik::RGB::Response &res);
};


#endif /* WEBCAMSERVER_H_ */
