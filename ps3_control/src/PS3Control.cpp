#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Float64.h>
#include <cmath>

#define PS3_AXIS_STICK_LEFT_LEFTWARDS    0
#define PS3_AXIS_STICK_LEFT_UPWARDS      1
#define PS3_AXIS_STICK_RIGHT_LEFTWARDS   2
#define PS3_AXIS_STICK_RIGHT_UPWARDS     3
#define PS3_AXIS_BUTTON_REAR_LEFT_2      12
#define PS3_AXIS_BUTTON_REAR_RIGHT_2     13
#define PS3_BUTTON_ACTION_TRIANGLE   12
#define PS3_BUTTON_ACTION_CIRCLE     13
#define PS3_BUTTON_ACTION_CROSS      14
#define PS3_BUTTON_CROSS_UP          4
#define PS3_BUTTON_CROSS_RIGHT       5
#define PS3_BUTTON_CROSS_DOWN        6
#define PS3_BUTTON_CROSS_LEFT        7

#define PS3_BUTTON_START        3

#define DRIVE_SPEED 1
#define ROT_SPEED 2
#define LIFT_SPEED 1
class PS3Control
{
public:
	PS3Control() : nh("~")
	{
		init();
	}

void init();

private:
	void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
	
	ros::NodeHandle nh;

	ros::Publisher vel_pub;
	ros::Publisher pwm_pub;
	ros::Publisher frk_pub;
	ros::Publisher agrr_pub;
	ros::Subscriber joy_sub;
};

int oldTriangle, oldCircle, oldCross, oldUp, oldLeft, oldDown, oldStart;
float oldBase_vel, oldBase_rot, oldLift_up, oldLift_down;

void PS3Control::init()
{
	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel",1);
	pwm_pub = nh.advertise<std_msgs::Float64>("/cmd_frk_pwm",1);
	frk_pub = nh.advertise<std_msgs::Int8>("/cmd_frk",1);
	agrr_pub = nh.advertise<std_msgs::Int8>("/cmd_agrr1",1);
	joy_sub = nh.subscribe<sensor_msgs::Joy>("/joy", 10, &PS3Control::joyCallback, this); 
	
	oldTriangle = 0;
	oldCircle = 0;
	oldCross = 0;
	oldBase_vel = 0;
	oldBase_rot = 0;
	oldLift_up = 0;
	oldLift_down = 0;
	oldUp = 0;
	oldDown = 0;
	oldLeft = 0;
	oldStart = 0;

}

void PS3Control::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
	//read data
	int triangle =    joy->buttons[PS3_BUTTON_ACTION_TRIANGLE];
	int circle =      joy->buttons[PS3_BUTTON_ACTION_CIRCLE];
	int cross =       joy->buttons[PS3_BUTTON_ACTION_CROSS];
	int up =	  joy->buttons[PS3_BUTTON_CROSS_UP];
	int left =	  joy->buttons[PS3_BUTTON_CROSS_LEFT];
	int down =	  joy->buttons[PS3_BUTTON_CROSS_DOWN];
	float base_vel =  joy->axes[PS3_AXIS_STICK_LEFT_UPWARDS];
	float base_rot =  joy->axes[PS3_AXIS_STICK_RIGHT_LEFTWARDS];
	float lift_up =   joy->axes[PS3_AXIS_BUTTON_REAR_RIGHT_2];
	float lift_down = joy->axes[PS3_AXIS_BUTTON_REAR_LEFT_2];
	float start = joy->buttons[PS3_BUTTON_START];
	
	//init messages
	geometry_msgs::Twist vel;
	std_msgs::Int8 frk;
	std_msgs::Float64 pwm;
	std_msgs::Int8 agrr;

	
	//drive speed
	if(base_vel != oldBase_vel || base_rot != oldBase_rot)
	{
		//calculate speed
		vel.linear.x = DRIVE_SPEED * pow(base_vel,3);
		vel.angular.z = ROT_SPEED * pow(base_rot,3);
		
		//publish
		vel_pub.publish(vel);
		
		//set new old
		oldBase_vel = base_vel;
		oldBase_rot = base_rot;
	}
	
	//fork state
	if((triangle != oldTriangle || circle != oldCircle || cross != oldCross || start != oldStart) && (triangle == 1 || circle == 1 || cross == 1 || start == 1))
	{
		//check buttons
		if(start == 1) {
			frk.data = 4;
		} else if(triangle == 1)
		{
			frk.data = 3;
		}
		else if(circle == 1)
		{
			frk.data = 2;
		}
		else if(cross == 1)
		{
			frk.data = 1;
		} 

		//publish
		frk_pub.publish(frk);
		
		//set old buttons
		oldTriangle = triangle;
		oldCircle = circle;
		oldCross = cross;
		oldStart = start;
	}

	//robot state
	if((up != oldUp || left != oldLeft || down != oldDown) && (up == 1 || left == 1 || down == 1))
	{
		//check buttons
		if(down == 1)
		{
			agrr.data = 0;
		}
		else if(left == 1)
		{
			agrr.data = 1;
		}
		else if(up == 1)
		{
			agrr.data = 2;
		}

		//publish
		agrr_pub.publish(agrr);

		//set old buttons
		oldUp = up;
		oldLeft = left;
		oldDown = down;
	}
	
	//fork speed
	if(lift_up != oldLift_up || lift_down != oldLift_down)
	{
		//calculate lift speed
		pwm.data = LIFT_SPEED * (lift_down - lift_up);
		
		//publish
		pwm_pub.publish(pwm);
		
		//set old lift speeds
		oldLift_down = lift_down;
		oldLift_up = lift_up;
	}
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "ps3control");
	PS3Control ps3control;
	
	ros::spin();
}

