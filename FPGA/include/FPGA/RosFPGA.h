class RosFPGA
{
public:
	int main(int, char**);
	bool getCData(FPGA::Click::Request &req,
			FPGA::Click::Response &res);
	void getUData(sensor_msgs::LaserScan* scan);
}
