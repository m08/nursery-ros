#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <FPGA/SerialCommunicationFPGA.h>
#include "termios.h"
#include "string.h"
#include <FPGA/Click.h>


#define NSENSORS 6
#define NUM_READINGS 100
#define FREQUENCY 1.0
 
#define LOOP_RATE 10

#define DEVICE "/dev/ttyUSB2"
#define BUFSIZE 14
#define BAUDRATE B115200

#define MIN_ANGLE -0.122
#define MAX_ANGLE 0.122

SerialCommunicationFPGA s = SerialCommunicationFPGA(BUFSIZE, BAUDRATE, (char*)DEVICE);
ros::Publisher uSensor_pub;

bool getCData(FPGA::Click::Request &req,
		FPGA::Click::Response &res)
{
	char* click = s.getClick();

	res.cSensor1 = click[1] & 0x01 ? 1 : 0;
	res.cSensor2 = click[1] & 0x02 ? 1 : 0;
	res.cSensor3 = click[1] & 0x04 ? 1 : 0;
	res.cSensor4 = click[1] & 0x08 ? 1 : 0;
	res.cSensor5 = click[1] & 0x10 ? 1 : 0;
	res.cSensor6 = click[1] & 0x20 ? 1 : 0;

	res.last = click[0] & 0x01 ? 1 : click[0] & 0x02 ? 2 : click[0] & 0x04 ? 3 : 0;

	free(click);
	return true;
}

void pubUData()
{
	double ranges[NSENSORS][NUM_READINGS];
	//get data from ultrasonic sensors
	double* uData = s.getDistances();
	//fill ranges with ultrasonic sensor data
	for(int i = 0; i < NSENSORS; i++)
		for(int j = 0; j < NUM_READINGS; j++)
		{
			ranges[i][j] = uData[i];
		}

	free(uData);

		//get time
		ros::Time scan_time = ros::Time::now();

	//populate LaserScan messages
	sensor_msgs::LaserScan scan[NSENSORS];
	for(int i = 0; i < NSENSORS; i++)
	{

		scan[i].header.stamp = scan_time;
		std::ostringstream os;
		os << "uSensor_frame" << i;
		scan[i].header.frame_id = os.str();
		scan[i].angle_min = MIN_ANGLE;
		scan[i].angle_max = MAX_ANGLE;
		scan[i].angle_increment = (MAX_ANGLE - MIN_ANGLE)/ NUM_READINGS;
		scan[i].time_increment = 0;
		scan[i].range_min = 0.02;
		scan[i].range_max = 4.0;

		scan[i].ranges.resize(NUM_READINGS);
		scan[i].intensities.resize(NUM_READINGS);
		for(int j = 0; j < NUM_READINGS; j++)
		{
			scan[i].ranges[j] = ranges[i][j];
			scan[i].intensities[j] = 0;
		}
		uSensor_pub.publish(scan[i]);
	}
}

int main(int argc, char* argv[])
{
	//initialize com port
	s.openPort();
	s.initPort();

	//initialize ros
	ros::init(argc, argv, "sensorsFPGA");

	ros::NodeHandle n;

	//services ros
	//ros::ServiceServer serviceU = n.advertiseService("uSensor",getUData);
	ros::ServiceServer serviceC = n.advertiseService("cSensor",getCData);
	
	//publisher
	uSensor_pub = n.advertise<sensor_msgs::LaserScan>("/scan", 60);

	int count = 0;
	ros::Rate loop_rate(LOOP_RATE);
	while(n.ok())
	{
		//get data laserscan
		pubUData();		

		//
		count++;
		ros::spinOnce();
		loop_rate.sleep();
	}

	s.closePort();
	
	return 0;
}
