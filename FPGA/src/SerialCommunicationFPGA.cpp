#include <FPGA/SerialCommunicationFPGA.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#define NSENSORS 6

#define NSENSORS 6

int bufSize, port, baudrate;
char* device;



struct termios old_options, options;

SerialCommunicationFPGA::SerialCommunicationFPGA(int new_bufSize, int new_baudrate, char* new_device)
{	
	bufSize = new_bufSize;
	baudrate = new_baudrate;
	device = new_device;
}

double SerialCommunicationFPGA::getDistance(int sensor)
{
	/*test if sensor is in available*/
	if(sensor > NSENSORS - 1)
	{
		printf("Sensor %d is out of range: %d", sensor, NSENSORS);
		return -1;
	}

	writeBuf((char)0xFF);
	char data[bufSize];
	readBuf(data);

	unsigned char a = data[sensor * 2];
	unsigned char b = data[sensor * 2 + 1];

	return (a + b * 256)/1000.0;
}

double* SerialCommunicationFPGA::getDistances()
{
	writeBuf((char)0xFF);
	char data[bufSize];
	readBuf(data);

	double *distances = (double*)(malloc(NSENSORS * sizeof(double)));

	for(int i = 0; i < NSENSORS; i++)
	{
		distances[i] = ((unsigned char)data[i * 2] + (unsigned char)data[i * 2 + 1] * 256)/1000.0;
	}

	return distances;
}

char* SerialCommunicationFPGA::getClick()
{
	writeBuf((char)0xFF);
	char data[bufSize];
	readBuf(data);

	char *click = (char*)(malloc(2 * sizeof(char)));

	click[0] = data[12];
	click[1] = data[13];
	
	return click;	
}

void SerialCommunicationFPGA::readBuf(char* buf)
{
	read(port, buf, bufSize);   /* returns after bufSize chars have been input */
}

void SerialCommunicationFPGA::writeBuf(char data)
{
	write(port, &data, 1);
}

void SerialCommunicationFPGA::closePort()
{
	/*reset old options and close*/
	tcsetattr(port, TCSANOW, &old_options);
	close(port);
}

void SerialCommunicationFPGA::openPort()
{
	port = open(device, O_RDWR | O_NOCTTY | O_SYNC);
	if (port <0) { perror(device); exit(-1); }
}

void SerialCommunicationFPGA::initPort()
{
	tcgetattr(port, &old_options); /* save current port settings */
	
	memset (&options, 0, sizeof options);
	if (tcgetattr (port, &options) != 0)
	{
		fprintf(stderr, "error %d from tcgetattr", errno);
		exit(EXIT_FAILURE);
	}

	/*baud rate*/
	cfsetospeed(&options, baudrate);
	cfsetispeed(&options, baudrate);

	/* set input mode (non-canonical, no echo,...) */
	options.c_lflag = 0;
	options.c_iflag &= ~IGNBRK; //disable breakprocessing
	options.c_oflag = 0;

	options.c_cc[VTIME] = 0.01;   /* inter-character timer of 1 ms */
	options.c_cc[VMIN] = bufSize;   /* blocking read until 14 chars received */

	options.c_iflag &= ~(IXON | IXOFF | IXANY);

	/*set options*/
	options.c_cflag |= (CLOCAL | CREAD); //Enable receiver and set local mode
	options.c_cflag &= ~(PARENB | PARODD);
	options.c_cflag &= ~CSTOPB;	//1 stop bit
	options.c_cflag &= ~CRTSCTS;	//Disable hardware flow control

	/*flush buffers and set attributes*/
	tcflush(port, TCIFLUSH);
	if((tcsetattr(port, TCSANOW, &options)) < 0){
		fprintf(stderr, "failed to set atrr: %d, %s\n", port, strerror(errno));
		exit(EXIT_FAILURE);
	}
}