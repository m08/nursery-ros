; Auto-generated. Do not edit!


(cl:in-package FPGA-srv)


;//! \htmlinclude Click-request.msg.html

(cl:defclass <Click-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass Click-request (<Click-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Click-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Click-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name FPGA-srv:<Click-request> is deprecated: use FPGA-srv:Click-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Click-request>) ostream)
  "Serializes a message object of type '<Click-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Click-request>) istream)
  "Deserializes a message object of type '<Click-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Click-request>)))
  "Returns string type for a service object of type '<Click-request>"
  "FPGA/ClickRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Click-request)))
  "Returns string type for a service object of type 'Click-request"
  "FPGA/ClickRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Click-request>)))
  "Returns md5sum for a message object of type '<Click-request>"
  "518b0e8dc06cb76d93e0ab8c2dd4e6b0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Click-request)))
  "Returns md5sum for a message object of type 'Click-request"
  "518b0e8dc06cb76d93e0ab8c2dd4e6b0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Click-request>)))
  "Returns full string definition for message of type '<Click-request>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Click-request)))
  "Returns full string definition for message of type 'Click-request"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Click-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Click-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Click-request
))
;//! \htmlinclude Click-response.msg.html

(cl:defclass <Click-response> (roslisp-msg-protocol:ros-message)
  ((last
    :reader last
    :initarg :last
    :type cl:integer
    :initform 0)
   (cSensor1
    :reader cSensor1
    :initarg :cSensor1
    :type cl:integer
    :initform 0)
   (cSensor2
    :reader cSensor2
    :initarg :cSensor2
    :type cl:integer
    :initform 0)
   (cSensor3
    :reader cSensor3
    :initarg :cSensor3
    :type cl:integer
    :initform 0)
   (cSensor4
    :reader cSensor4
    :initarg :cSensor4
    :type cl:integer
    :initform 0)
   (cSensor5
    :reader cSensor5
    :initarg :cSensor5
    :type cl:integer
    :initform 0)
   (cSensor6
    :reader cSensor6
    :initarg :cSensor6
    :type cl:integer
    :initform 0))
)

(cl:defclass Click-response (<Click-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Click-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Click-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name FPGA-srv:<Click-response> is deprecated: use FPGA-srv:Click-response instead.")))

(cl:ensure-generic-function 'last-val :lambda-list '(m))
(cl:defmethod last-val ((m <Click-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader FPGA-srv:last-val is deprecated.  Use FPGA-srv:last instead.")
  (last m))

(cl:ensure-generic-function 'cSensor1-val :lambda-list '(m))
(cl:defmethod cSensor1-val ((m <Click-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader FPGA-srv:cSensor1-val is deprecated.  Use FPGA-srv:cSensor1 instead.")
  (cSensor1 m))

(cl:ensure-generic-function 'cSensor2-val :lambda-list '(m))
(cl:defmethod cSensor2-val ((m <Click-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader FPGA-srv:cSensor2-val is deprecated.  Use FPGA-srv:cSensor2 instead.")
  (cSensor2 m))

(cl:ensure-generic-function 'cSensor3-val :lambda-list '(m))
(cl:defmethod cSensor3-val ((m <Click-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader FPGA-srv:cSensor3-val is deprecated.  Use FPGA-srv:cSensor3 instead.")
  (cSensor3 m))

(cl:ensure-generic-function 'cSensor4-val :lambda-list '(m))
(cl:defmethod cSensor4-val ((m <Click-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader FPGA-srv:cSensor4-val is deprecated.  Use FPGA-srv:cSensor4 instead.")
  (cSensor4 m))

(cl:ensure-generic-function 'cSensor5-val :lambda-list '(m))
(cl:defmethod cSensor5-val ((m <Click-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader FPGA-srv:cSensor5-val is deprecated.  Use FPGA-srv:cSensor5 instead.")
  (cSensor5 m))

(cl:ensure-generic-function 'cSensor6-val :lambda-list '(m))
(cl:defmethod cSensor6-val ((m <Click-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader FPGA-srv:cSensor6-val is deprecated.  Use FPGA-srv:cSensor6 instead.")
  (cSensor6 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Click-response>) ostream)
  "Serializes a message object of type '<Click-response>"
  (cl:let* ((signed (cl:slot-value msg 'last)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'cSensor1)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'cSensor2)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'cSensor3)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'cSensor4)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'cSensor5)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'cSensor6)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Click-response>) istream)
  "Deserializes a message object of type '<Click-response>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'last) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cSensor1) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cSensor2) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cSensor3) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cSensor4) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cSensor5) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cSensor6) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Click-response>)))
  "Returns string type for a service object of type '<Click-response>"
  "FPGA/ClickResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Click-response)))
  "Returns string type for a service object of type 'Click-response"
  "FPGA/ClickResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Click-response>)))
  "Returns md5sum for a message object of type '<Click-response>"
  "518b0e8dc06cb76d93e0ab8c2dd4e6b0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Click-response)))
  "Returns md5sum for a message object of type 'Click-response"
  "518b0e8dc06cb76d93e0ab8c2dd4e6b0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Click-response>)))
  "Returns full string definition for message of type '<Click-response>"
  (cl:format cl:nil "int32 last~%int32 cSensor1~%int32 cSensor2~%int32 cSensor3~%int32 cSensor4~%int32 cSensor5~%int32 cSensor6~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Click-response)))
  "Returns full string definition for message of type 'Click-response"
  (cl:format cl:nil "int32 last~%int32 cSensor1~%int32 cSensor2~%int32 cSensor3~%int32 cSensor4~%int32 cSensor5~%int32 cSensor6~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Click-response>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Click-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Click-response
    (cl:cons ':last (last msg))
    (cl:cons ':cSensor1 (cSensor1 msg))
    (cl:cons ':cSensor2 (cSensor2 msg))
    (cl:cons ':cSensor3 (cSensor3 msg))
    (cl:cons ':cSensor4 (cSensor4 msg))
    (cl:cons ':cSensor5 (cSensor5 msg))
    (cl:cons ':cSensor6 (cSensor6 msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Click)))
  'Click-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Click)))
  'Click-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Click)))
  "Returns string type for a service object of type '<Click>"
  "FPGA/Click")