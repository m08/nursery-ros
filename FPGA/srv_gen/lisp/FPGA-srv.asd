
(cl:in-package :asdf)

(defsystem "FPGA-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Click" :depends-on ("_package_Click"))
    (:file "_package_Click" :depends-on ("_package"))
  ))