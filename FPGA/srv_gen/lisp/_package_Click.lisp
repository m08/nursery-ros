(cl:in-package FPGA-srv)
(cl:export '(LAST-VAL
          LAST
          CSENSOR1-VAL
          CSENSOR1
          CSENSOR2-VAL
          CSENSOR2
          CSENSOR3-VAL
          CSENSOR3
          CSENSOR4-VAL
          CSENSOR4
          CSENSOR5-VAL
          CSENSOR5
          CSENSOR6-VAL
          CSENSOR6
))