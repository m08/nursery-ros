#!/usr/bin/env python
import roslib; roslib.load_manifest('keyboard_control')
import rospy

from geometry_msgs.msg import Twist
from std_msgs.msg import Int8

import sys, select, termios, tty

msg = """
Reading from the keyboard  and Publishing to cmd_agrr1!
---------------------------
Change state:
   8    9    0

Reading from the keyboard  and Publishing to cmd_frk!
---------------------------
Change state:
   1    2    3

Reading from the keyboard  and Publishing to cmd_vel!
---------------------------
Moving around:    change speeds:
   q    w    e	       r   t   y
   a    s    d	       f   g   h
   z    x    c

anything else : 0,0,0 0,0,0 to cmd_vel (same as s)

CTRL-C to quit
"""

moveBindings = {
		'w':(1,0),
		'e':(1,-1),
		'a':(0,1),
		'd':(0,-1),
		'q':(1,1),
		'x':(-1,0),
		'c':(-1,1),
		'z':(-1,-1),
	       }

speedBindings={
		'r':(1.1,1.1),
		'f':(.9,.9),
		't':(1.1,1),
		'g':(.9,1),
		'y':(1,1.1),
		'h':(1,.9),
	      }
forkliftBindings={
	'1':1,
	'2':2,
	'3':3,
}

robotBindings={
	'8':1,
	'9':2,
	'0':0,
}

def getKey():
	tty.setraw(sys.stdin.fileno())
	select.select([sys.stdin], [], [], 0)
	key = sys.stdin.read(1)
	termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
	return key

speed = .5
turn = 1

def vels(speed,turn):
	return "currently:\tspeed %s\tturn %s " % (speed,turn)

if __name__=="__main__":
    	settings = termios.tcgetattr(sys.stdin)
	
	pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
	pub2 = rospy.Publisher('cmd_frk', Int8, queue_size=1)
	pub3 = rospy.Publisher('cmd_agrr1', Int8, queue_size=1)
	rospy.init_node('teleop_twist_keyboard')

	x = 0
	th = 0
	status = 0

	try:
		print msg
		print vels(speed,turn)
		while(1):
			key = getKey()
			if key in robotBindings.keys():
				ret = robotBindings[key]
				pub3.publish(ret)
			else:
				if key in forkliftBindings.keys():
					ret = forkliftBindings[key]
					pub2.publish(ret)
				else:
					if key in moveBindings.keys():
						x = moveBindings[key][0]
						th = moveBindings[key][1]
					elif key in speedBindings.keys():
						speed = speed * speedBindings[key][0]
						turn = turn * speedBindings[key][1]

						print vels(speed,turn)
						if (status == 14):
							print msg
						status = (status + 1) % 15
					else:
						x = 0
						th = 0
						if (key == '\x03'):
							break

					twist = Twist()
					twist.linear.x = x*speed; twist.linear.y = 0; twist.linear.z = 0
					twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = th*turn
					pub.publish(twist)

	except Exception as e:
		print  e

	finally:
		twist = Twist()
		twist.linear.x = 0; twist.linear.y = 0; twist.linear.z = 0
		twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
		pub.publish(twist)

    		termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)


