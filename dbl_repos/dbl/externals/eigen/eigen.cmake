# 
# CMake include file for eigen library
# Erik Schuitema 2010 (E.Schuitema@tudelft.nl)
#
# No need to build anything; just add include directory 
#

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/externals/eigen)
