# 
# CMake include file for ANN library
# Erik Schuitema 2010 (E.Schuitema@tudelft.nl)
# 

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/externals/ann/include)
TARGET_LINK_LIBRARIES(${TARGET} ann)

IF (NOT __ANN_CMAKE_INCLUDED)
  SET(__ANN_CMAKE_INCLUDED 1)

  ADD_SUBDIRECTORY(${WORKSPACE_DIR}/dbl/externals/ann ${WORKSPACE_DIR}/build/ann/${CMAKE_BUILD_TYPE}${COMPILER_VERSION})
ENDIF (NOT __ANN_CMAKE_INCLUDED)
