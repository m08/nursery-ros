// (c) Marcos F. Cardoso and Robert Nadler
// http://www.codeproject.com/KB/cpp/C___enums_to_strings.aspx

#ifndef ENUMTOSTRING_H
  #define ENUMTOSTRING_H
  #define ENUMQUOTE(x) #x
  #define ENUMDEFFILE(x) ENUMQUOTE(x.def)
  typedef struct { const char * desc; int type;} EnumDesc_t;
#endif // ENUMTOSTRING_H

#undef EE
#undef EE2
#undef ENUM_INIT

#define EE( element ) element,
#define EE2( element, val ) element = val,
#define ENUM_INIT(NAME)

enum ENUM_NAME {
#include ENUMDEFFILE(ENUM_NAME)
};

#undef EE
#undef EE2
#undef ENUM_INIT

#define EE( element ) { #element, (int)(element) },
#define EE2( element, val ) { #element, val },

#define ENUM_INIT(NAME) \
static void GetTable##NAME(EnumDesc_t **table, int *length);\
static const char* GetString##NAME(enum ENUM_NAME index)\
{\
  EnumDesc_t *table;\
  int length, i;\
  GetTable##NAME(&table, &length);\
  for (i = 0; i < length; i++)\
    if ((int)index == table[i].type)\
      return table[i].desc;\
  return "Unknown " #NAME " enum type";\
}\
\
static void GetTable##NAME(EnumDesc_t **table, int *length)\
{\
  static EnumDesc_t static_table [] = {
  
#include ENUMDEFFILE(ENUM_NAME)
};

  *table = static_table;\
  *length = sizeof(static_table)/sizeof(EnumDesc_t);\
}
