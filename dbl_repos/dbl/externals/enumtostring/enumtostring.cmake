#
# CMake include file for enumtostring library
# Wouter Caarls <w.caarls@tudelft.nl>
#
# 27-01-2011 (wcaarls): Initial revision
#

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/externals/enumtostring)

IF (NOT __ENUMTOSTRING_CMAKE_INCLUDED)
  SET(__ENUMTOSTRING_CMAKE_INCLUDED 1)

ENDIF (NOT __ENUMTOSTRING_CMAKE_INCLUDED)
