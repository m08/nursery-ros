#
# CMake include file for cma library
# Wouter Caarls <wcaarls@tudelft.nl>
#
# 27-08-2010 (wcaarls): Initial revision
#

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/externals/cma)
TARGET_LINK_LIBRARIES(${TARGET} cma)

IF (NOT __CMA_CMAKE_INCLUDED)
  SET(__CMA_CMAKE_INCLUDED 1)

  ADD_SUBDIRECTORY(${WORKSPACE_DIR}/dbl/externals/cma ${WORKSPACE_DIR}/build/cma/${CMAKE_BUILD_TYPE}${COMPILER_VERSION})
ENDIF (NOT __CMA_CMAKE_INCLUDED)
