#
# CMake include file for Qt ffmpeg wrapper library
# Erik Schuitema <e.schuitema@tudelft.nl>
#
# 03-05-2010: Initial revision
#

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/externals/qtffmpegwrapper/QTFFmpegWrapper)
TARGET_LINK_LIBRARIES(${TARGET} qtffmpegwrapper)

IF (NOT __QTFFMPEGWRAPPER_CMAKE_INCLUDED)
  SET(__QTFFMPEGWRAPPER_CMAKE_INCLUDED 1)

  ADD_SUBDIRECTORY(${WORKSPACE_DIR}/dbl/externals/qtffmpegwrapper ${WORKSPACE_DIR}/build/qtffmpegwrapper/${CMAKE_BUILD_TYPE}${COMPILER_VERSION})
ENDIF (NOT __QTFFMPEGWRAPPER_CMAKE_INCLUDED)
