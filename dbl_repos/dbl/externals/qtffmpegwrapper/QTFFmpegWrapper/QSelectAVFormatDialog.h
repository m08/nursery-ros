/*
 * QSelectFormatDialog.h
 *
 *  Created on: May 4, 2010
 *      Author: erik
 */

#ifndef QSELECTFORMATDIALOG_H_
#define QSELECTFORMATDIALOG_H_

#include "ui_selectformat.h"
#include "ffmpeg.h"

class QSelectAVFormatDialog : public QDialog, private Ui::AVOutputFormatSelectDialog
{
    Q_OBJECT

	protected:
		ffmpeg::AVOutputFormat *pOutputFormat;

	public:
		QSelectAVFormatDialog(QWidget *parent = 0);
		~QSelectAVFormatDialog();

		ffmpeg::AVOutputFormat *getFormat();

	public slots:
		void	selectFormat();
};
Q_DECLARE_METATYPE(ffmpeg::AVOutputFormat*);


#endif /* QSELECTFORMATDIALOG_H_ */
