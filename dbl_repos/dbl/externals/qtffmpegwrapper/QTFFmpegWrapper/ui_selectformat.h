/********************************************************************************
** Form generated from reading ui file 'selectformat.ui'
**
** Created: Tue May 4 21:11:55 2010
**      by: Qt User Interface Compiler version 4.4.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_SELECTFORMAT_H
#define UI_SELECTFORMAT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QListWidget>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AVOutputFormatSelectDialog
{
public:
    QVBoxLayout *verticalLayout;
    QListWidget *mLstOutputFormats;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *AVOutputFormatSelectDialog)
    {
    if (AVOutputFormatSelectDialog->objectName().isEmpty())
        AVOutputFormatSelectDialog->setObjectName(QString::fromUtf8("AVOutputFormatSelectDialog"));
    AVOutputFormatSelectDialog->setWindowModality(Qt::WindowModal);
    AVOutputFormatSelectDialog->resize(287, 354);
    verticalLayout = new QVBoxLayout(AVOutputFormatSelectDialog);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    mLstOutputFormats = new QListWidget(AVOutputFormatSelectDialog);
    new QListWidgetItem(mLstOutputFormats);
    new QListWidgetItem(mLstOutputFormats);
    mLstOutputFormats->setObjectName(QString::fromUtf8("mLstOutputFormats"));
    mLstOutputFormats->setSelectionRectVisible(false);

    verticalLayout->addWidget(mLstOutputFormats);

    buttonBox = new QDialogButtonBox(AVOutputFormatSelectDialog);
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    verticalLayout->addWidget(buttonBox);


    retranslateUi(AVOutputFormatSelectDialog);
    QObject::connect(buttonBox, SIGNAL(rejected()), AVOutputFormatSelectDialog, SLOT(reject()));
    QObject::connect(mLstOutputFormats, SIGNAL(itemSelectionChanged()), AVOutputFormatSelectDialog, SLOT(selectFormat()));
    QObject::connect(mLstOutputFormats, SIGNAL(doubleClicked(QModelIndex)), AVOutputFormatSelectDialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(accepted()), AVOutputFormatSelectDialog, SLOT(accept()));

    QMetaObject::connectSlotsByName(AVOutputFormatSelectDialog);
    } // setupUi

    void retranslateUi(QDialog *AVOutputFormatSelectDialog)
    {
    AVOutputFormatSelectDialog->setWindowTitle(QApplication::translate("AVOutputFormatSelectDialog", "Select output format", 0, QApplication::UnicodeUTF8));

    const bool __sortingEnabled = mLstOutputFormats->isSortingEnabled();
    mLstOutputFormats->setSortingEnabled(false);
    mLstOutputFormats->item(0)->setText(QApplication::translate("AVOutputFormatSelectDialog", "Format 1", 0, QApplication::UnicodeUTF8));
    mLstOutputFormats->item(1)->setText(QApplication::translate("AVOutputFormatSelectDialog", "Format 2", 0, QApplication::UnicodeUTF8));

    mLstOutputFormats->setSortingEnabled(__sortingEnabled);
    Q_UNUSED(AVOutputFormatSelectDialog);
    } // retranslateUi

};

namespace Ui {
    class AVOutputFormatSelectDialog: public Ui_AVOutputFormatSelectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTFORMAT_H
