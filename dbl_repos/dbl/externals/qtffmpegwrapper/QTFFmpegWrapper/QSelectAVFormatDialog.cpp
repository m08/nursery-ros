/*
 * QSelectAVFormatDialog.cpp
 *
 *  Created on: May 4, 2010
 *      Author: erik
 */
#include <QtGui>
#include "QSelectAVFormatDialog.h"
#include "ffmpeg.h"

QSelectAVFormatDialog::QSelectAVFormatDialog(QWidget *parent)
{
	pOutputFormat = NULL;
	setupUi(this); // this sets up GUI

	// Fill the list box with the available formats
	mLstOutputFormats->clear();
	ffmpeg::AVOutputFormat *format = ffmpeg::av_oformat_next(NULL);
	while (format != NULL)
	{
		if (ffmpeg::avcodec_find_encoder(format->video_codec))
		{
			//Add to list box: ;
			QListWidgetItem *newItem = new QListWidgetItem(QString(format->name) + " (" + QString(format->long_name) + ")");
			newItem->setData(Qt::UserRole, qVariantFromValue(format));
			mLstOutputFormats->addItem(newItem);
		}
		// Get next format
		format = ffmpeg::av_oformat_next(format);
	}
}

QSelectAVFormatDialog::~QSelectAVFormatDialog()
{

}

ffmpeg::AVOutputFormat* QSelectAVFormatDialog::getFormat()
{
	return pOutputFormat;
}

void QSelectAVFormatDialog::selectFormat()
{
	if (mLstOutputFormats->selectedItems().size() > 0)
	{
		pOutputFormat = mLstOutputFormats->selectedItems()[0]->data(Qt::UserRole).value<ffmpeg::AVOutputFormat *>();
		//printf("Selected output format is %s!\n", pOutputFormat->long_name);
	}
}
