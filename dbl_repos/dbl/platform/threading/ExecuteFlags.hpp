#ifndef EXECUTEFLAGS_HPP_
#define EXECUTEFLAGS_HPP_


/**
 * Class is more of a tuple containing information on the context in which
 * the call to ILoopingTask::execute() takes place.
 */
class ExecuteFlags
{
protected:
	bool		_overrun;
	bool		_scheduled;
	bool		_interrupted;

	uint64_t	_overruns;


public:
	ExecuteFlags() 
		: _overrun(false), _scheduled(false), _interrupted(false), _overruns(0)
	{}


	virtual ~ExecuteFlags() {}


	/**
	 * @return		true IFF the previous call took too long and overran its timeslice.
	 */
	bool overrun() const
	{
		return _overrun;
	}


	/**
	 * @return		true IFF this is a normal scheduled call (it was 'just time').
	 */
	bool scheduled() const
	{
		return _scheduled;
	}


	/**
	 * @return		true IFF this call is the result of the current thread / task being interrupted.
	 */
	bool interrupted() const
	{
		return _interrupted;
	}


	uint64_t overruns() const
	{
		return _overruns;
	}


	/**
	 * @param	val	Whether or not the current thread / task has overrun its slice.
	 */
	void overrun(const bool val)
	{
		_overrun = val;
	}


	/**
	 * @param	val	Whether or not the current thread / task was scheduled to be waken up.
	 */
	void scheduled(const bool val)
	{
		_scheduled = val;
	}


	/**
	 * @param	val	Whether or not the current thread / task was interrupted.
	 */
	void interrupted(const bool val)
	{
		_interrupted = val;
	}


	void overruns(const uint64_t val)
	{
		_overruns = val;
	}
};

#endif /*EXECUTEFLAGS_HPP_*/
