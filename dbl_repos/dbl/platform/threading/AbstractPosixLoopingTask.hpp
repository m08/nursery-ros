#ifndef ABSTRACTPOSIXLOOPINGTASK_HPP_
#define ABSTRACTPOSIXLOOPINGTASK_HPP_

#include <string>

#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#include <Stopwatch.hpp>

#include <ILoopingTask.h>
#include <ExecuteFlags.hpp>


/**
 * NOTE: THIS IS NOT FINISHED. DO NOT USE.
 *
 * @todo Finish implementation of the pthread based loopingtask
 */
class AbstractPosixLoopingTask : public ILoopingTask
{
private:
	/**
	 * The thread this class encapsulates.
	 */
	pthread_t				_thread;


	/**
	 * Attributes of this thread.
	 */
	pthread_attr_t			_thread_attr;


	/**
	 * Whether we've been requested to stop, but haven't had a chance to do
	 * so yet.
	 */
	bool					_stopping;


	/**
	 * Whether to keep calling execute().
	 */
	bool					_stopped;


	/**
	 * The measured frequency this task is running at.
	 */
	double					_frequency;


	int						_priority;


protected:
	/**
	 * The name of this task.
	 */
	const std::string&		_name;


	/**
	 * Inverse of task execution frequency (in sec).
	 */
	const double			_period;


public:
	/**
	 *
	 */
	AbstractPosixLoopingTask(const std::string& name, const int priority, const double period)
	: _stopping(false), _stopped(false), _frequency((1.0 / period)), _name(name), _period(period)
	{
		// check prio
		// @todo make priority configurable at runtime, so add setters/getters
		// @todo perform range checking on priority values, or copy RoboFrame priority abstraction
		_priority = priority;

		// initialise attributes
		pthread_attr_init(&_thread_attr);
		// create joinable
		pthread_attr_setdetachstate(&_thread_attr, PTHREAD_CREATE_JOINABLE);
		// params will be set in start() function
	};


	virtual ~AbstractPosixLoopingTask()
	{
		long status = 0;
		// @todo not nice, change?
		void** pstatus = static_cast<void**>(static_cast<void*>(&status));

		// wait for task to end
		pthread_join(_thread, pstatus);

		// we know for sure that the task has stopped here
		_stopping = false;
		_stopped = true;

		// free resources
		pthread_attr_destroy(&_thread_attr);
	};


	virtual void init()
	{
		// subclasses could override
	}


	void start()
	{
		// create thread here, since pthreads automatically begin execution as
		// soon as they are created
		int rc = pthread_create(&_thread, &_thread_attr, &__arg_run, static_cast<void*>(this));

		// get scheduler params for the thread
		int policy;
		sched_param param;
		pthread_getschedparam(_thread, &policy, &param);
		// set priority
		param.sched_priority = _priority;
		// set scheduler params back for the thread
	    rc = pthread_setschedparam(_thread, policy, &param);

		// @todo throw exceptions on thread creation problems
	}


	// implements the Looping behaviour
	void run()
	{
		// bookkeeping vars
		SystemTimer::TimeInterval_t ti_execTime     = 0;
		SystemTimer::TimeInterval_t ti_leftOfPeriod = 0;
		SystemTimer::TimeStamp_t    ts_current      = 0;
		SystemTimer::TimeStamp_t    ts_previous     = 0;
		double freqd;

		// timer vars
		struct timespec req, rem;
		bzero(&req, sizeof(req));
		bzero(&rem, sizeof(rem));

		// our timekeeper
		Stopwatch sw;

		// calc period in nanoseconds
		SystemTimer::TimeInterval_t ti_periodInNs = static_cast<SystemTimer::TimeInterval_t>(_period * 1e9);

		// create an ExecuteFlags object
		ExecuteFlags flags;

		// loop and call execute every period
		while(!_stopping)
		{
			// call execute; do some work
			sw.start();
			this->execute(flags);
			ti_execTime = sw.stop();
			sw.reset();

			// do bookkeeping
			ts_current = SystemTimer::value();
			freqd = ts_current - ts_previous;
			freqd /= 1e9;
			_frequency = (1.0 / freqd);
			// save for next loop
			ts_previous = ts_current;

			// see if we need to bail out already
			if(_stopping)
				break;

			// calc new value for still to wait (we ignore overhead of bookkeeping)
			ti_leftOfPeriod = ti_periodInNs - ti_execTime;
//			printf("[run] ti_leftOfPeriod: %ldns\n", (long int) ti_leftOfPeriod);

			// see if we have an overrun
			flags.overrun(ti_leftOfPeriod < 0);
			if(flags.overrun())
			{
				// still_to_wait should now be negative
				flags.overruns(-(ti_leftOfPeriod / ti_periodInNs));
				flags.interrupted(false);
				ti_leftOfPeriod = 0;
			}
			else
			{
				flags.overruns(0);
				flags.overrun(false);
				flags.interrupted(false);
			}

			// @todo maybe catch interrupted signal and set this accordingly?
			flags.scheduled(true);

			// sleep just (about) the time we still need to
			req.tv_nsec = ti_leftOfPeriod;
			// @todo maybe implement sleeping remaining time when interrupted
			nanosleep(&req, &rem);
		}
	}


	// subclasses should override
	virtual void execute(const ExecuteFlags& flags) = 0;


	void stop()
	{
		// store state
		_stopping = true;
		_stopped = false;
	}


	bool stopping()
	{
		return _stopping;
	}


	bool stopped()
	{
		return _stopped;
	}


	double period()
	{
		return (1.0 / _frequency);
	}


	double frequency()
	{
		return _frequency;
	}


private:
	/**
	 * Function calls the execute method of the PosixLoopingTask it was handed
	 * plus does some bookkeeping.
	 *
	 * @param	arg		Pointer which is assumed to be the PosixLoopingTask to run.
	 */
	static void* __arg_run(void* arg)
	{
		// cast
		AbstractPosixLoopingTask* t = static_cast<AbstractPosixLoopingTask*>(arg);

		// just call run method
		t->run();

		// signal we have ended
		t->setStopped();

		// could call pthread_exit(), but this is implied by returning from
		// this function.
		return NULL;
	}


	/**
	 * Setter to indicate the real-time task has exitted its while loop and the
	 * task has really stopped.
	 */
	void setStopped()
	{
		_stopped = true;
	}
};

#endif /*ABSTRACTPOSIXLOOPINGTASK_HPP_*/
