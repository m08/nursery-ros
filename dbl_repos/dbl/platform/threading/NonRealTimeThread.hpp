#ifndef NONREALTIMETHREAD_HPP_
#define NONREALTIMETHREAD_HPP_


#include <BaseThread.hpp>


/**
 * Class does not add any functionality but is only a 'decorator' in the sense
 * that if an 'end-user' requires a non-real-time thread implementation but
 * doesn't care which one he can ask for a @c NonRealTimeThread instead of a
 * @c BaseThread.
 *
 * @author	G.A. vd. Hoorn		Initial implementation.
 */
class NonRealTimeThread : public BaseThread
{
public:
	NonRealTimeThread()
	:
		BaseThread()
	{
		// empty constructor
	}


	NonRealTimeThread(const std::string name, const ThreadPriority priority)
	:
		BaseThread(name, priority)
	{
		// empty
	}


	virtual ~NonRealTimeThread()
	{
		// empty
	}
};


#endif /* NONREALTIMETHREAD_HPP_ */
