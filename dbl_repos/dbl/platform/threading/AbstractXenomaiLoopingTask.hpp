#ifndef ABSTRACTXENOMAILOOPINGTASK_HPP_
#define ABSTRACTXENOMAILOOPINGTASK_HPP_

#include <string>

#include <stdint.h>
#include <sys/mman.h>

#include <native/task.h>
#include <native/timer.h>

#include <ExecuteFlags.hpp>
#include <ILoopingTask.h>



/**
 * Class implements the basic behaviour of a Xenomai real-time task. End users
 * need only subclass this class and override (or more precisely, implement) the
 * ILoopingTask::execute() method to create their own real-time task.
 *
 * All Xenomai functionality that can be used in 'normal' (C based) task
 * implementations can be used, use the provided methods to access the necessary
 * variables.
 */
class AbstractXenomaiLoopingTask : public ILoopingTask
{
protected:
	/**
	 * The Xenomai task descriptor struct.
	 */
	RT_TASK				_task;
	int					_priority;
private:

	/**
	 * Whether we've been requested to stop, but haven't had a chance to do
	 * so yet.
	 */
	bool				_stopping;


	/**
	 * Indicates whether the task has started yet.
	 * Convenient when killing tasks (awaiting termination etc) that have never started.
	 */
	bool				_started;

	/**
	 * Whether to keep calling execute().
	 */
	bool				_stopped;


	/**
	 * The measured frequency this task is running at.
	 */
	const double		_frequency;


protected:
	/**
	 * The name of this task.
	 */
	std::string			_name;


	/**
	 * Inverse of task execution frequency (in sec).
	 */
	double				_period;


public:
	/**
	 *
	 * @param	period		Inverse of task update frequency (in sec).
	 */
	AbstractXenomaiLoopingTask(const std::string& name, const int priority)
		: _stopping(false), _started(false), _stopped(false), _frequency(0), _name(name), _period(0)
	{
		// check prio
		// @todo make priority configurable at runtime, so add a member variable and setters/getters
		_priority = priority;
		if (_priority > 99)
			_priority = 99;
		else if (_priority < 1)
			_priority = 1;

	}


	virtual ~AbstractXenomaiLoopingTask()
	{
		if (!_stopped)
			awaitTermination();

		// delete
		rt_task_delete(&_task);

		// cannot munlock as other tasks might still be running
	};


	// Set the period of this task. Only useful when called BEFORE start()!
	void setPeriod(const double period)
	{
		_period = period;
	}

	virtual bool init()
	{
		// subclasses could override
		return true;
	}

	virtual bool deinit()
	{
		// subclasses could override
		return true;
	}

	void start()
	{
		_started = true;

		// lock pages right before starting the task
		mlockall(MCL_CURRENT | MCL_FUTURE);

		// create task
		const int tstacksize = 0;
		// @todo setting T_JOINABLE seems to provide a way to wait for RT task termination; is there a better way?
		const int tflags = T_JOINABLE;

		/*
		 * Arguments: &task,
		 *            name,
		 *            stack size (0=default),
		 *            priority,
		 *            mode (FPU, start suspended, ...)
		 */
		// @todo throw exceptions on rt_task creation failure
//		int res = rt_task_create(&_task, name.c_str(), tstacksize, prio, tflags);
		rt_task_create(&_task, _name.c_str(), tstacksize, _priority, tflags);
//		printf("res: %d\n", res);

		// calc period in nanoseconds
		RTIME nanos = static_cast<RTIME>(_period * 1e9);

		/*
		 * Arguments: &task (NULL==self),
		 *            start time,
		 *            period
		 */
		rt_task_set_periodic(&_task, TM_NOW, nanos);

		/*
		 * Arguments: &task,
		 *            task function,
		 *            function argument
		 */
		rt_task_start(&_task, &__arg_run, static_cast<void*>(this));
	}


	// implements the Looping behaviour
	void run()
	{
		// bookkeeping vars
		RTIME stamp1 = 0;
		RTIME stamp2 = 0;
		double freqd;

		// create an ExecuteFlags object
		ExecuteFlags flags;

		// xenomai sets this
		unsigned long overruns;

		// loop and call execute every period
		while(!this->_stopping)
		{
			// if time_to_do_work < timer_period, wait for untill end of period
			rt_task_wait_period(&overruns);

			// do bookkeeping
			// @todo determine if this calculation causes a lot of overhead
			stamp1 = rt_timer_read();
			freqd = stamp1 - stamp2;
			freqd /= 1e9;
			this->_period = freqd;
			stamp2 = stamp1;

			// copy value and set bits
			flags.overruns(overruns);
			flags.overrun(overruns != 0);
			// @todo figure out a way how to set these things to actually meaningful values
			flags.interrupted(false);
			flags.scheduled(true);

			// see if we need to bail out already
			if(this->_stopping)
				break;

			// call execute; do some work
			this->execute(flags);
		}
//
//		// signal we have ended
//		this->_stopped = true;
	}


	// subclasses should override
	virtual void execute(const ExecuteFlags& flags) = 0;


	void stop()
	{
		// store state
		_stopping = true;
		//_stopped = false;
	}

	void awaitTermination()
	{
		if (_stopped)
			return;

		// If the task never started, we don't need to await its termination
		if (!_started)
			return;

		// wait for task to end
		rt_task_join(&_task);

		// suspend it
		rt_task_suspend(&_task);

		// we know for sure that the task has stopped here
		_stopping = false;
		_stopped = true;

	}

	bool stopping()
	{
		return _stopping;
	}


	bool stopped()
	{
		return _stopped;
	}

	// Returns measured period
	double period()
	{
		return _period;
	}

	// Returns measured frequency
	double frequency()
	{
		return 1.0/_period;
	}


public:
	RT_TASK& task()
	{
		return _task;
	}


private:
	/**
	 * Function just calls the run method of the AbstractXenomaiLoopingTask it was handed.
	 *
	 * @param	arg		Pointer which is assumed to be the XenomaiTask to run.
	 */
	static void __arg_run(void* arg)
	{
		// cast
		AbstractXenomaiLoopingTask* t = static_cast<AbstractXenomaiLoopingTask*>(arg);

		// just call run method
		t->run();

		// signal we have ended
		t->setStopped();
	}


	/**
	 * Setter to indicate the real-time task has exitted its while loop and the
	 * task has really stopped.
	 */
	void setStopped()
	{
		_stopped = true;
	}
};

#endif /*ABSTRACTXENOMAILOOPINGTASK_HPP_*/
