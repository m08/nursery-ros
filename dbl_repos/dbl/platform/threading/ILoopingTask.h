#ifndef ILOOPINGASK_H_
#define ILOOPINGTASK_H_

#include <stdint.h>

#include <ExecuteFlags.hpp>


/**
 * Interface describes a class that can be used to implement a body of
 * functionality that will be executed at a predefined rate (a certain frequency).
 *
 * Basically what a Module does in the RoboFrame. This class has been kept
 * similar to make migration to a real-time RoboFrame possible.
 */
class ILoopingTask
{
protected:
	/* protected constructor, this is an interface */
	ILoopingTask() {};


public:
	virtual ~ILoopingTask() {};


	/**
	 * This method is called after the constructor and before any other method.
	 * Subclasses should override this method to do anything that could not be
	 * done in the constructor.
	 */
	virtual bool init() = 0;


	/**
	 * Starts this task (causes ILoopingTask::execute() to be periodically
	 * called).
	 */
	virtual void start() = 0;


	/**
	 * This method is periodically called after ILoopingTask::start() has been
	 * called. The \e flags parameter can be used to access some information on
	 * the 'state' of the call, for instance whether the previous execution was
	 * too long and an 'overrun' occured.
	 *
	 * Subclasses should override this method to implement their own
	 * functionality.
	 *
	 * @param	flags		The flags with information on this call.
	 */
	virtual void execute(const ExecuteFlags& flags) = 0;


	/**
	 * Stops (requests) the execution of this task (asap).
	 */
	virtual void stop() = 0;


	/**
	 * @return			true IFF ILoopingTask::stop() has been called but task hasn't stopped yet.
	 */
	virtual bool stopping() = 0;


	/**
	 * @return			true IFF this task has been stopped (and has stopped).
	 */
	virtual bool stopped() = 0;


	/**
	 * @return			The inverse of the task execution frequency (in seconds).
	 */
	virtual double period() = 0;


	/**
	 * @return			The true frequency this task is executing at (in Hz).
	 */
	virtual double frequency() = 0;
};

#endif /*ILOOPINGTASK_H_*/
