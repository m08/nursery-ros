/*
 * Mutex.hpp
 *
 * Mutex class for pthread mutex
 *
 *  Created on: Aug 18, 2010
 *      Author: Erik Schuitema
 */

#ifndef MUTEX_HPP_
#define MUTEX_HPP_

#include <pthread.h>

class CMutex
{
	private:
		pthread_mutex_t	mMutex;
	public:
		CMutex()		{pthread_mutex_init(&mMutex, NULL);}
		~CMutex()		{pthread_mutex_destroy(&mMutex);}

		// A return value of zero indicates success; all other values indicate failure,
		// such as EBUSY for trylock() when the mutex was already locked
		int lock()		{return pthread_mutex_lock(&mMutex);}
		int unlock()	{return pthread_mutex_unlock(&mMutex);}
		int trylock()	{return pthread_mutex_trylock(&mMutex);}
};

class CSharedMutex
{
  private:
    pthread_rwlock_t mMutex;
  public:
    CSharedMutex()    {pthread_rwlock_init(&mMutex, NULL);}
    ~CSharedMutex()   {pthread_rwlock_destroy(&mMutex);}

    // A return value of zero indicates success; all other values indicate failure,
    // such as EBUSY for trylock() when the mutex was already locked
    int rdlock()    {return pthread_rwlock_rdlock(&mMutex);}
    int wrlock()    {return pthread_rwlock_wrlock(&mMutex);}
    int unlock()    {return pthread_rwlock_unlock(&mMutex);}
    int tryrdlock() {return pthread_rwlock_tryrdlock(&mMutex);}
    int trywrlock() {return pthread_rwlock_trywrlock(&mMutex);}
};

#endif /* MUTEX_HPP_ */
