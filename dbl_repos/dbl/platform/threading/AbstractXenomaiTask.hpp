#ifndef ABSTRACTXENOMAITASK_HPP_
#define ABSTRACTXENOMAITASK_HPP_

#include <string>

#include <stdint.h>
#include <sys/mman.h>

#include <native/task.h>
#include <native/timer.h>

#include <ExecuteFlags.hpp>


/**
 * Class implements the basic behaviour of a Xenomai real-time task. End users
 * need only subclass this class and override (or more precisely, implement) the
 * ILoopingTask::execute() method to create their own real-time task.
 *
 * All Xenomai functionality that can be used in 'normal' (C based) task
 * implementations can be used, use the provided methods to access the necessary
 * variables.
 */
class AbstractXenomaiTask
{
protected:
	/**
	 * The Xenomai task descriptor struct.
	 */
	RT_TASK				_task;
	int					_priority;
private:

	/**
	 * Whether we've been requested to stop, but haven't had a chance to do
	 * so yet.
	 */
	bool				_shouldstop;

	/**
	 * Indicates whether the task has started yet.
	 * Convenient when killing tasks (awaiting termination etc) that have never started.
	 */
	bool				_started;

	/**
	 * Whether to keep calling execute().
	 */
	bool				_stopped;

	/**
	 * Stack size used in rt_task_create() (see start())
	 */
	int					_stacksize;

protected:
	/**
	 * The name of this task.
	 */
	std::string			_name;

	// implements the Looping behaviour
	// subclasses should override Run()
	// the implementation of execute() should respond to the _shouldstop variable!
	virtual void run() = 0;

public:
	/**
	 *
	 * @param	period		Inverse of task update frequency (in sec).
	 */
	AbstractXenomaiTask(const std::string& name, const int priority, const int stacksize=0)
		: _shouldstop(false), _started(false), _stopped(false), _stacksize(stacksize), _name(name)
	{
		// check prio
		// @todo make priority configurable at runtime, so add a member variable and setters/getters
		_priority = priority;
		if (_priority > 99)
			_priority = 99;
		else if (_priority < 1)
			_priority = 1;

	}


	virtual ~AbstractXenomaiTask()
	{
		if (!_stopped)
			awaitTermination();

		if (_started)
			// delete
			rt_task_delete(&_task);

		// cannot munlock as other tasks might still be running
	};


	virtual bool init()
	{
		// subclasses could override
		return true;
	}

	virtual bool deinit()
	{
		// subclasses could override
		return true;
	}

	virtual bool start()
	{
		/*
		 * Arguments: &task,
		 *            task function,
		 *            function argument
		 */

		// lock pages right before starting the task
		mlockall(MCL_CURRENT | MCL_FUTURE);

		// create task
		// @todo setting T_JOINABLE seems to provide a way to wait for RT task termination; is there a better way?
		const int tflags = T_JOINABLE;

		/*
		 * Arguments: &task,
		 *            name,
		 *            stack size (0=default),
		 *            priority,
		 *            mode (FPU, start suspended, ...)
		 */
		// @todo throw exceptions on rt_task creation failure
//		int res = rt_task_create(&_task, name.c_str(), tstacksize, prio, tflags);
		rt_task_create(&_task, _name.c_str(), _stacksize, _priority, tflags);
//		printf("res: %d\n", res);

		_started = true;
		return (rt_task_start(&_task, &__arg_run, static_cast<void*>(this)) == 0);
	}

	virtual bool stop()
	{
		// store state
		_shouldstop = true;
		//_stopped = false;
		return true;
	}

	virtual void awaitTermination()
	{
		if (_stopped)
			return;

		// If the task never started, we don't need to await its termination
		if (!_started)
			return;

		// wait for task to end
		rt_task_join(&_task);

		// suspend it
		rt_task_suspend(&_task);

		// we know for sure that the task has stopped here
		_shouldstop = false;
		_stopped = true;

	}

	bool shouldStop()
	{
		return _shouldstop;
	}


	bool stopped()
	{
		return _stopped;
	}

public:
	RT_TASK& task()
	{
		return _task;
	}

private:
	/**
	 * Function just calls the run method of the AbstractXenomaiLoopingTask it was handed.
	 *
	 * @param	arg		Pointer which is assumed to be the XenomaiTask to run.
	 */
	static void __arg_run(void* arg)
	{
		// cast
		AbstractXenomaiTask* t = static_cast<AbstractXenomaiTask*>(arg);

		// just call run method
		t->run();

		// signal we have ended
		t->setStopped();
	}


	/**
	 * Setter to indicate the real-time task has exited its while loop and the
	 * task has really stopped.
	 */
	void setStopped()
	{
		_stopped = true;
	}
};

#endif /*ABSTRACTXENOMAITASK_HPP_*/
