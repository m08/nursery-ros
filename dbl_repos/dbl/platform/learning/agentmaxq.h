#ifndef _AGENTMAXQ_H_
#define _AGENTMAXQ_H_

#include "CCMem.h"
#include "AgentInterface.h"
#include "TileCodingTrace.h"
#include <vector>
#include <tilecoding.h>
#include <cstring>
#include <STG.h>
#include <randomc.h>
#include <Log.h>
#include <Log2.h>

// TODO: this shouldn't be included here
#include "precisions.h"

//using namespace std;

#define TILE_READ	true	// Read-only = true
#define TILE_WRITE	false

#define DEFAULT_TRACE_LENGTH 	1000
#define _MIN_TRACE_VALUE_MAXQ	0.001f		// minimum trace value. below this value, the state is not accounted for anymore in the trace

#define ComputationalType	double	// This type is used in all computations before eventually saving values in _QPrecision precision

// These constants are bits so they can be OR'd.
#define CONST_NON_TERMINAL			0
#define CONST_LOCALLY_TERMINAL		1
#define CONST_ANCESTOR_TERMINAL		2
#define CONST_TERMINAL_ABSORBING	4	// Must be non-zero
#define CONST_TERMINAL_NONABSORBING	8	// Must be non-zero


class CMaxqNode
{
	public:
		std::string			mName;	// For debugging
		bool				mReportDebugInfo;
//		 // Default constructor, if used: make sure you first set mName, then use SetLogStream()
//		CMaxqNode():
//			mLogStream(cout),
//			mName("")
//		{}
		CMaxqNode(const std::string name):
				mName(name),
				//mLogStream(logger->ccustom(logger->getCustomLogIndex("maxqnode-" + name)))
				mReportDebugInfo(false),
//				mLogStream(logger->cout()),
				//mLogStream2("maxqNode-" + name)
				mLogStream2(name)
				{}
		virtual ~CMaxqNode()	{}
		virtual bool 			Load(std::ifstream& fileInStream) { return true; }
		virtual bool			Save(std::ofstream& fileOutStream) { return true; }
		virtual void			Init() {}
		virtual _IndexPrecision	GetMemorySize() { return 0; }
		virtual _IndexPrecision	GetMemoryUsage() { return 0; }
		virtual void			ShowParameters() {}
	protected:
//		CLogStream&			mLogStream;
		CLog2				mLogStream2;
//		void				SetLogStream(CLog* logger){ mLogStream = logger->ccustom(logger->getCustomLogIndex("maxqnode-" + mName)); }
};

class CMaxqQNode;

class CMaxqMaxNode: public virtual CMaxqNode
{
	private:
		CMaxqMaxNode*		mExecutorNode;	// The Max-node that called this max node. Used during ancestor termination.
		CCMem				mLastC;			// The C value of previous time (set in Step(), used to update)
		int					mNumChildSteps; // Number of steps the child task took
		int					mTaskRepeated;	// Number of times the child task is repeated so far
		virtual char		ShouldTerminate(const bool silent=false);

		// Report debugging info like Q and C values
	protected:
		std::vector<CMaxqQNode*>	mQNodes;	// should be Q-nodes only, because a Q-node contains a ref to its task's Max-Node.
		CSTGState*			mCurrentSTGState;
	public:
		// The action it is performing right now.
		// This can be seen as the 'assignment' from a module above.
		// It's the formal parameter list for this module.
		CAgentAction		mAction;

		// Task action. This is the action that is taken by the CHILD TASK NODE (mAction).
		CAgentAction		mTaskAction;

		// Variables
		ComputationalType	mLearnRate;
		ComputationalType	mDiscountRate;
		ComputationalType	mExploreRate;
		ComputationalType	mTraceFallRate;

		int					mTaskToExecute;	// Subtask it's currently executing, -1 if it's not executing any
		int					mBestTask;	// This is the mQNodes index of the best action as calculated most recently

		int					mTaskRepeatFactor;	// The number of times the task action is repeated. Usually 1; for prim. actions usually > 1.

		bool				mTerminatedLocally;	// This bool is set at the end of Execute() and can be checked by the ancestor node

		bool				mHrGreedy;		// True if the policy is hierarchical greedy, default is false

		_QPrecision			mHrReward;	// Hierarchically assigned reward. Different from pseudo reward!
		_QPrecision			mPseudoReward;
		_QPrecision			mTotalReward;	// Total received hierarchical reward.

		// Functions
		CMaxqMaxNode(const std::string name);
		virtual ~CMaxqMaxNode()	{}
		void				AddTask(CMaxqQNode *task);	// a Q-node class should be defined for every subtask
		virtual void		Init();		// Initialises the complete tree below
		virtual void		Deinit()	{}
		virtual bool		Load(std::ifstream& fileInStream);	// Load from file
		virtual bool		Save(std::ofstream& fileOutStream);	// Save to file

		CMaxqMaxNode*		GetTaskNode(int taskIndex);
		void				ClearTraces();
		_QPrecision			GetTotalReward()	{ return mTotalReward;}
		void				ResetTotalReward()	{ mTotalReward = 0;}

		void				ClearTreeTraces();	// Clear the traces for the complete tree below this max node

		void				SetTreeLearnRate(ComputationalType alpha);		// Set learn rate for the complete tree below this max node
		void				SetTreeDiscountRate(ComputationalType gamma);	// Set discount rate for the complete tree below this max node
		void				SetTreeExploreRate(ComputationalType epsilon);	// Set explore rate for the complete tree below this max node
		void				SetTreeTraceFallRate(ComputationalType lambda);	// Set trace fall rate for the complete tree below this max node

		virtual void		ShowParameters();

		//		template <class T>
		virtual void		SetTreeState(CSTGState* currentSTGState);	// Set STGstate and agentState for the complete tree below this max node
/*							{
								mCurrentSTGState = currentSTGState;
								for (uint32_t i=0; i<mQNodes.size(); i++)
								{
									mQNodes[i]->UpdateState(currentSTGState);
									mQNodes[i]->GetTaskNode()->SetTreeState(currentSTGState);
								}
							}*/


		virtual void		ResetTreeMemory();	// Reset the memory for the complete tree below this max node
		//void				ResetTreeTotalReward();	// Reset the memory for the complete tree below this max node

		virtual void		ProcessAssignment()	{}	// Converts the mAction 'job definition' into state variables of the job/assignment plus extra processing

		virtual void		CalculateReward()	{ mHrReward = 0; mPseudoReward = 0;}	// This is the HIERARCHICAL reward for this Max-node only, as well as the pseudo reward
		virtual char		IsTerminalState(const bool silent=true)	{ return CONST_TERMINAL_ABSORBING;}	// Override this with your own conditions for this node
		virtual bool		IsTaskAllowed()		{ return true;}	// Can we call this task now? Assignment INDEPENDENT (but state dependent)

		virtual int			Execute(CMaxqMaxNode* executorNode);	// Executes mAction, fills mTerminatedLocally (in the absorbing way) and returns the number of primitive steps taken (recursively)
		virtual void		ExecuteBestAction(CMaxqMaxNode* executorNode);	// Executes mAction
		virtual int			ExecuteTimeStep(CMaxqMaxNode* executorNode, char ancestorTerminalStatus);	// Executes mAction, fills mTerminatedLocally (in the absorbing way) and returns the number of primitive steps taken (recursively)

		void				Step(bool isTerminalAbsorbingState, const int numChildSteps);
		virtual _QPrecision	GetBestAction(bool usePseudoC, bool silent=true);	// Returns the Q-value of the best action. The best action is saved in mBestTask, as index.

		virtual void		OnChildTaskExecuted(int taskIndex)	{}	// Perform any desired action after a child task is executed (its index is given in the parameter)
};



class CMaxqQNode: public virtual CMaxqNode
{
	private:
		inline ComputationalType	GetLearnRate()	{ return mParentNode->mLearnRate; }
		inline ComputationalType	GetDiscountRate()	{ return mParentNode->mDiscountRate; }
		inline ComputationalType	GetTraceFallRate()	{ return mParentNode->mTraceFallRate; }
		inline _QPrecision			GetReward()			{ return mParentNode->mHrReward; }
		inline _QPrecision			GetPseudoReward()	{ return mParentNode->mPseudoReward; }
	protected:
		CMaxqMaxNode				*mParentNode;
		CMaxqMaxNode				*mTaskNode;	// Child node

		bool						mActionAsInt;
		CFATileCoding<CCMem, _IndexPrecision>	*mC; // C-memory (both normal and pseudo
		_IndexPrecision				mMemsize;
		int							mNumTilings;
		CAgentState					mCurrentAgentState;

		// Q-value initialization
		_QPrecision					mQInitMin;
		_QPrecision					mQInitMax;

	public:
		// Variables
		CReplacingTraceTC			mTrace;
		CAgentAction				mBestAction;	// Latest calculated best action, in the internal action descriptor format.
		_QPrecision					mBestActionV;	// The V-value of the latest calculated best action

		// Functions
		CMaxqQNode(const std::string name);
		virtual ~CMaxqQNode()		{}
		void						SetParent(CMaxqMaxNode* parentNode);
		void						SetTaskNode(CMaxqMaxNode* taskNode);
		bool						SetNumTilings(int numTilings);
		bool						SetMemsize(_IndexPrecision memsize);
		virtual void				ShowParameters();
		void						RandomizeValues(_QPrecision min, _QPrecision max);
		virtual void				Init();
		virtual bool				Load(std::ifstream& fileInStream);
		virtual bool				Save(std::ofstream& fileOutStream);

		CMaxqMaxNode*				GetParentNode()		{ return mParentNode;}
		inline CMaxqMaxNode*		GetTaskNode()	{ return mTaskNode; }
		_IndexPrecision				GetMemorySize();
		_IndexPrecision				GetMemoryUsage();
		bool						GetTileValue(CAgentState& state, CAgentAction& action, _IndexPrecision* tiles, CCMem* value, bool bReadOnly);
//		bool						GetCValues(const CAgentAction& action, CCMem* C);
		bool						GetCValues(CAgentAction& action, CCMem* C);
		bool						GetBestValues(CCMem* C, _QPrecision* V);

//		bool						AddToTrace(const CAgentAction& action);
		bool						AddToTrace(CAgentAction& action);
		bool						Update(CCMem delta);
		void						ResetTreeMemory();	// Reset the memory for the complete tree below this max node
		virtual void				PrintValues(CAgentState abst); // Prints all pseudo C values for the given abstracted state

		// The PrepareAssignment function may alter simulator objects, but it cannot alter objects that require a CalculateSimStates (like the robot and the ball).
		virtual CAgentAction		PrepareAssignment(const CAgentAction& action)	{ CAgentAction result = action; return result; }	// Converts an action into a formal job definition
		//virtual CAgentState			AbstractState(const CAgentState& state)			{ CAgentState result = state; return result; }
//		template <class T>
		virtual void				UpdateState(CSTGState* currentSTGState) = 0;
		void						SetParameter(int index, _StatePrecision parameter) { mCurrentAgentState[index] = parameter; }	// Used when mAction of the parent node is chosen. (should be called in PrepareAssignment() of parent node)
		virtual _QPrecision			GetBestAction(bool pseudoC, bool silent=true);	// Returns the Q-value of the best action. The best action is saved in mBestAction.
};

// CMaxqPrimitiveNode: a range of primitive actions in a single 'node'.
// Provide the actual action taken as 'action' param in Update().
class CMaxqPrimitiveNode: public CMaxqMaxNode, public CMaxqQNode
{
	private:

	protected:
		ISTGActuation	*mActuationInterface;
		CAgentState		mLastAgentState;
	public:
		CMaxqPrimitiveNode(ISTGActuation *actuationInterface, const std::string name);
		virtual ~CMaxqPrimitiveNode()	{}
		void				Init();
		virtual bool		Load(std::ifstream& fileInStream);
		virtual bool		Save(std::ofstream& fileOutStream);

		virtual void		ShowParameters();

		void				SetTreeState(CSTGState* currentSTGState) { mCurrentSTGState = currentSTGState; UpdateState(currentSTGState); }
		virtual void		ResetTreeMemory() { mC->reset(); }

		void				ProcessAssignment() {};
		char				IsTerminalState(const bool silent=true) { return CONST_NON_TERMINAL; }
		bool				IsTaskAllowed() { return true; }	// By default, a primitive action should be able to execute
		int					Execute(CMaxqMaxNode* executorNode);	// Executes mAction
		void				ExecuteBestAction(CMaxqMaxNode* executorNode);
		int					ExecuteTimeStep(CMaxqMaxNode* executorNode, char ancestorTerminalStatus);
		virtual void		ExecutePrimitiveAction()	{}	// This is the one that actually performs an action! Either in sim or on the robot.
//		virtual bool		Update(const CAgentAction& action);
		virtual bool		Update(CAgentAction& action);
		virtual void		UpdateState(CSTGState* currentSTGState) { }

		virtual _QPrecision	GetBestAction(bool pseudoC, bool silent=true);
};

#endif
