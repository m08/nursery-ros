/*
 * GenericLearner.h
 * Basis of generic learning policies; deals with reading state, action, reward
 * and termination variables from XML.
 *
 * Wouter Caarls <w.caarls@tudelft.nl>
 */

#ifndef __GENERICLEARNER_H_INCLUDED
#define __GENERICLEARNER_H_INCLUDED

#include <GenericState.h>
#include <Log2.h>

class CGenericLearnerStateVar : public CGenericStateVar
{
  private:
    double mWrapping;

  public:
    CGenericLearnerStateVar() : mWrapping(0.0) { }
    bool readConfig(const CConfigSection &configNode)
    {
      configNode.get("wrapping", &mWrapping);

      return CGenericStateVar::readConfig(configNode);
    }
    double getWrapping()
    {
      return mWrapping;
    }
};

class CGenericLearnerActionVar : public CGenericActionVar
{
  public:
    int mActions;
    double mScaling;

  public:
    CGenericLearnerActionVar() : mActions(1), mScaling(1.0) { }
    bool readConfig(const CConfigSection &configNode)
    {
      configNode.get("actions", &mActions);
      configNode.get("scaling", &mScaling);

      return CGenericActionVar::readConfig(configNode);
    }
    int getActions()
    {
      return mActions;
    }

    double getScaling()
    {
      return mScaling;
    }
};

class CGenericLearner
{
  protected:
    CLog2 mLog;
    int mStateStartIndex;
    int mActionStartIndex;

    GenericState  mCurrentSTGState; // Backup of current state to determine reward and terminal state

    std::vector<CGenericLearnerStateVar> mStateVars;
    std::vector<CGenericLearnerActionVar> mActionVars;

    CGenericStateVar mTermination, mReward;

    ISTGGenericSensing *mGenericSensingInterface;
    ISTGGenericActuation *mGenericActuationInterface;

  public:
    CGenericLearner(ISTGGenericSensing *sensingInterface, ISTGGenericActuation *actuationInterface, int stateStartIndex=0, int actionStartIndex=0) :
          mLog("learning"), mStateStartIndex(stateStartIndex), mActionStartIndex(actionStartIndex),
          mTermination("termination"), mReward("reward"),
          mGenericSensingInterface(sensingInterface), mGenericActuationInterface(actuationInterface) { }
    virtual ~CGenericLearner() { }

    virtual bool readConfig(const CConfigSection &configNode);
    virtual bool init();
    virtual void updateState(GenericState* currentSTGState);
    virtual void updateAction(ISTGActuation* actuationInterface);
    virtual double calculateReward();
    virtual char isTerminalState(uint64_t trialTimeMicroseconds);
};

#endif /* __GENERICLEARNER_H_INCLUDED */
