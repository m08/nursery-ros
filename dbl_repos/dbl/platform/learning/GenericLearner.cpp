/*
 * GenericLearner.cpp
 * Basis of generic learning policies; deals with reading state, action, reward
 * and termination variables from XML.
 *
 * XML interface:
 *
 * <learning>
 *   <statevar>
 *     <expression>4.0*(robot.motorjoint1.angle+_pi)/(2*_pi)</expression>
 *     <wrapping>4.0</wrapping>
 *   </statevar>
 *
 *   <actionvar>
 *     <variable>robot.motorjoint1.torque</variable>
 *     <expression>(x*3)-1.5</expression>
 *     <actions>5</actions>
 *     <scaling>5.0</scaling>
 *   </actionvar>
 * </learning>
 *
 * Wouter Caarls <w.caarls@tudelft.nl>
 */

#include <GenericLearner.h>

bool CGenericLearner::readConfig(const CConfigSection &configNode)
{
  bool configresult = true;

  for (CConfigSection stateNode = configNode.section("statevar"); !stateNode.isNull();
      stateNode = stateNode.nextSimilarSection())
  {
    CGenericLearnerStateVar statevar;
    configresult &= statevar.readConfig(stateNode);
    configresult &= statevar.resolve(mGenericSensingInterface);
    mStateVars.push_back(statevar);
  }

  for (CConfigSection actionNode = configNode.section("actionvar"); !actionNode.isNull();
      actionNode = actionNode.nextSimilarSection())
  {
    CGenericLearnerActionVar actionvar;
    configresult &= actionvar.readConfig(actionNode);
    configresult &= actionvar.resolve(mGenericActuationInterface);
    mActionVars.push_back(actionvar);
  }

  CConfigSection terminationNode = configNode.section("termination");
  if (!terminationNode.isNull())
  {
    configresult &= mTermination.readConfig(terminationNode);
    configresult &= mTermination.resolve(mGenericSensingInterface);
  }

  CConfigSection rewardNode = configNode.section("reward");
  if (!rewardNode.isNull())
  {
    configresult &= mReward.readConfig(rewardNode);
    configresult &= mReward.resolve(mGenericSensingInterface);
  }

  return configresult;
}

bool CGenericLearner::init()
{
  return true;
}

void CGenericLearner::updateState(GenericState* currentSTGState)
{
  // Backup current state to facilitate determination of rewards and terminal states
  mCurrentSTGState  = *currentSTGState;
}

void CGenericLearner::updateAction(ISTGActuation* actuationInterface)
{
}

double CGenericLearner::calculateReward()
{
  return mReward.evaluate(&mCurrentSTGState);
}

char CGenericLearner::isTerminalState(uint64_t trialTimeMicroseconds)
{
  if (mTermination.evaluate(&mCurrentSTGState))
  {
    mLogInfoLn("Policy terminated after " << std::setiosflags(std::ios::fixed) << std::setprecision(3) << trialTimeMicroseconds/1000000.0 << "s");
    return 3;
  }
  else
    return 0;
}

// ************************************************************************************

