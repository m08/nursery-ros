/*
 * agentgps.cpp
 *
 *  Created on: Sep 10, 2010
 *      Author: Erik Schuitema
 */

#include <AgentGPS.h>
#include <stl_minmaxheap.h>

// FOR DEBUGGING
#include <QSpaceTileCoding.h>

CAgentPriorityQueue::CAgentPriorityQueue()
{
}

CAgentPriorityQueue::CAgentPriorityQueue(int maxQueueLength)
{
	setMaxLength(maxQueueLength);
}

void CAgentPriorityQueue::setMaxLength(const int maxLength)
{
	mMaxLength = maxLength;
	// Reserve one extra so that we can first push and then pop
	mElements.resize(maxLength+1);		// Resize: create real elements
	mLocations.clear();
	mLocations.reserve(maxLength+1);	// Reserve; add later
	// Add locations to the location vector, also the extra element (the +1)
	for (int i=0; i<=mMaxLength; i++)
		mLocations.push_back(&mElements[i]);
	// Set the heap pointers
	mHeapBegin = mHeapEnd = mLocations.begin();
}

void CAgentPriorityQueue::clear()
{
	// We don't clear the elements, we just reset the heap pointers (which means marking the vector's complete content as empty locations)
	mHeapBegin = mHeapEnd = mLocations.begin();
}

void CAgentPriorityQueue::push(const CAgentState& state, const double priority)
{
	// The first free element is the element just after the heap's end
	QueueElement* freeElement = *mHeapEnd;

	// Fill the free element
	freeElement->state		= state;
	freeElement->priority	= priority;

	// Put the new element in the heap
	mHeapEnd++;
	std::push_minmaxheap(mHeapBegin, mHeapEnd, compareQueueElements);

	// If we are overweight (we can be, we reserved 1 extra element), pop the element with the smallest priority
	if (distance(mHeapBegin, mHeapEnd) > mMaxLength)
	{
		std::pop_minmaxheap_min(mHeapBegin, mHeapEnd, compareQueueElements);
		mHeapEnd--;	// Decrease the heap size by one
	}

}

void CAgentPriorityQueue::pop()
{
	std::pop_minmaxheap_max(mHeapBegin, mHeapEnd, compareQueueElements);
	mHeapEnd--;
}

unsigned int CAgentPriorityQueue::size()
{
	return distance(mHeapBegin, mHeapEnd);
}

const CAgentPriorityQueue::QueueElement& CAgentPriorityQueue::top()
{
	return *mLocations.front();
}

const CAgentPriorityQueue::QueueElement& CAgentPriorityQueue::back()
{
	return **minmaxheap_min(mHeapBegin, mHeapEnd, compareQueueElements);
}

const CAgentPriorityQueue::QueueElement& CAgentPriorityQueue::heapElement(int index)
{
	return *mLocations[index];
}

//****** MG-PS: Prioritized Sweeping according to McMahan and Gordon (2005) **********//

CAgentMGPS::CAgentMGPS(int maxQueueLength):
		mLog("AgentMGPS"),
		mNumStateVars(0),
		mQueue(maxQueueLength),
		mStateAsInts(false),
		mRandomizeQueue(false)
{
	for (int iVar=0; iVar<AGENT_MAX_NUM_STATEVARS; iVar++)
	{
		mStateBoundsMin[iVar] = -_StatePrecision_MAX;
		mStateBoundsMax[iVar] = _StatePrecision_MAX;
	}
}

CAgentMGPS::~CAgentMGPS()
{
}

void CAgentMGPS::stateAsInts(bool enabled)
{
	mStateAsInts = enabled;
}

void CAgentMGPS::resetMemory()
{
	// Clear the model
	CAgentDyna::resetMemory();
	// Clear the priority queue
	mQueue.clear();
}

void CAgentMGPS::reportUpdate(CAgentState& state, double priority)
{
	// Add it to the priority queue
	mQueue.push(state, priority);
}

void CAgentMGPS::setStateBounds(CAgentState& stateBoundsMin, CAgentState& stateBoundsMax, int numStateVars)
{
	memcpy(mStateBoundsMin, stateBoundsMin.getState(), numStateVars*sizeof(_StatePrecision));
	memcpy(mStateBoundsMax, stateBoundsMax.getState(), numStateVars*sizeof(_StatePrecision));
	mNumStateVars = numStateVars;
}

bool CAgentMGPS::stateInsideBounds(const CAgentState& state)
{
	for (int iVar=0; iVar<state.getNumStateVars(); iVar++)
		if ((state[iVar] < mStateBoundsMin[iVar]) || (state[iVar] > mStateBoundsMax[iVar]))
			return false;

	return true;
}

int CAgentMGPS::currentQueueSize()
{
	return mQueue.size();
}

void CAgentMGPS::enableQueueRandomization(bool enabled)
{
	mRandomizeQueue = enabled;
}

void CAgentMGPSLogPause()
{
	//usleep(0.3E6);
}

void CAgentMGPS::randomize(CAgentState& state)
{
	// Randomize state
	state.setNumStateVars(mNumStateVars);

	if (mStateAsInts)
	{
		for (int i=0; i<state.getNumStateVars(); i++)
			state.setStateVar(i, (_StatePrecision)gRanrotB.IRandom((int)mStateBoundsMin[i], (int)mStateBoundsMax[i]));
	}
	else
	{
		for (int i=0; i<state.getNumStateVars(); i++)
			state.setStateVar(i, mStateBoundsMin[i] + (mStateBoundsMax[i]-mStateBoundsMin[i])*(_StatePrecision)gRanrotB.Random());
	}
}

int CAgentMGPS::singleBackup(double learningRate)
{
	if (mQueue.size() == 0)
	{
		if (mRandomizeQueue)
		{
			CAgentState state;
			randomize(state);
			if (mAgentQ->isTerminalState(state, 0) & CONST_STATE_TERMINAL)
			{
				mLogNoticeLn("Randomized to a terminal state");
				return 0;
			}
			else
				mQueue.push(state, 0);
		}
		else
			return 0;
	}

	int numBackups=0;

	// Take state from the queue top and iterate over all actions to determine all lead-in states
	int stepIndex[AGENT_MAX_NUM_ACTIONVARS];	// Fixed array to avoid allocation of memory in real-time applications
	memset(stepIndex, 0, mAgentAction.getNumActionVars() * sizeof(stepIndex[0]));
	mAgentAction.setStartValues();

	// Copy the heap's top state, because the heap's top may change when we insert predecessors
	const CAgentPriorityQueue::QueueElement &top = mQueue.top();

	if (top.priority < 2) // TODO: Make into setting
	{
	  // Don't bother with small updates.
	  mQueue.clear();
	  return 0;
	}

	CAgentState topState = top.state;
	// Pop the queue
	mQueue.pop();

	// Calculate number of actions in the action space
	int numActions = 1;
	for (int iActionVar = 0; iActionVar < mAgentAction.getNumActionVars(); iActionVar++)
		numActions *= mAgentAction.mDescriptors[iActionVar].mNumSteps;

	// Iterate over all values in the action space
	for (int iAction = 0; iAction < numActions; iAction++)
	{
		ComputationalType reward;
		if (mModel->predictBackward(topState, mAgentAction, &mResultState, &reward))
		{
			if (mStateAsInts)
				// Round result
				for (int i=0; i<mResultState.getNumStateVars(); i++)
					mResultState[i] = floor(mResultState[i] + 0.5f);

			// Check state space bounds
			if (stateInsideBounds(mResultState))
			{
				// Overwrite reward with calculated one
				if (!mPredictReward)
				{
				  ComputationalType calculatedReward = mAgentQ->calculateReward(mAgentAction, topState);
				  reward = calculatedReward;
				}

				// Throw away terminal lead-in states
				if (!mAgentQ->isTerminalState(mResultState, 0))
				{
					if (fabs(reward) > 110)
					{
						mLogWarningLn("predictBackward() predicted a reward with an incredible value: " << reward);
						CAgentMGPSLogPause();
					}

					// Learning update

					// Calculate argmax_{a'} Q(topState, a'). Take care of terminal absorbing states.
					ComputationalType Qnext;
					char topStateIsTerminal = mAgentQ->isTerminalState(topState, 0);	// Assume time 0 as if it was the first state of the trial
					if ((topStateIsTerminal & CONST_STATE_TERMINAL) && (topStateIsTerminal & CONST_STATE_ABSORBING))
					{
						Qnext = 0;
						mLogNoticeLn("Terminal absorbing state detected");
					}
					else
						Qnext = mAgentQ->getQSpace()->getBestActionQValue(topState, mTempAction);	// We don't need the resulting best action for this state

					if (fabs(Qnext) > 150)
					{
						mLogWarningLn("getBestActionQValue(mQueue.top()) produced an incredible value: " << Qnext);
						mLogWarningLn("mQueue.top().state: " << topState.toStr());
						CAgentMGPSLogPause();
					}

					// The call to getQValue() stores the tiles of mResultState, which we can use directly afterwards in setStoredQValue()
					ComputationalType Qcurr = mAgentQ->getQSpace()->getQValue(mResultState, mAgentAction);
					if (fabs(Qcurr) > 150)
					{
						mLogWarningLn("getQValue(mResultState, mAgentAction) produced an incredible value: " << Qcurr);
						CAgentMGPSLogPause();
					}

					// The update rule
					ComputationalType Qnew = Qcurr + learningRate*(reward + mAgentQ->getDiscountRate()*Qnext - Qcurr);
					if (fabs(Qnew) > 150)
					{
						mLogWarningLn("trying to adjust Q-value from " << Qcurr << " to " << Qnew << " for reward " << reward << " and Qnext " << Qnext);
						mLogWarningLn("top state: " << topState.toStr());
						mLogWarningLn("mResultState: " << mResultState.toStr());

						CAgentMGPSLogPause();
					}

					// Update the Q-value
					mAgentQ->getQSpace()->setStoredQValue((_QPrecision)Qnew);

					// Residual gradient update
					if (mResidualWeight > 0 && (!(topStateIsTerminal & CONST_STATE_TERMINAL) || !(topStateIsTerminal & CONST_STATE_ABSORBING)))
					{
					  ComputationalType Qres = Qnext - mResidualWeight*learningRate*mAgentQ->getDiscountRate()*(reward + mAgentQ->getDiscountRate()*Qnext - Qcurr);
					  mAgentQ->getQSpace()->setStoredBestActionQValue((_QPrecision)Qres);
					}

					// Add this state to the queue with its priority
					mQueue.push(mResultState, fabs(Qnew-Qcurr));

					numBackups++;
				}
				else
				{
					//mLogNoticeLn("Discarding terminal lead-in state " << mResultState.toStr() << " for s=" << topState.toStr() << ", a=" << mAgentAction.toStr());
				}
			}
			else
			{
				//mLogNoticeLn("Discarding state outside state bounds: s:" << mResultState.toStr());
			}
		}

		// Increment value with index 0
		stepIndex[0]++;
		mAgentAction[0] = mAgentAction.mDescriptors[0].getValByIndex(stepIndex[0]);

		// Increment the stepindices who need incrementing
		for (int iActionVar = 0; iActionVar < mAgentAction.getNumActionVars(); iActionVar++)
		{
			if (stepIndex[iActionVar] >= mAgentAction.mDescriptors[iActionVar].mNumSteps)
			{
				stepIndex[iActionVar] = 0;
				mAgentAction[iActionVar] = mAgentAction.mDescriptors[iActionVar].mStartVal;
				if (iActionVar < mAgentAction.getNumActionVars()-1)
				{
					stepIndex[iActionVar+1]++;
					mAgentAction[iActionVar+1] = mAgentAction.mDescriptors[iActionVar+1].getValByIndex(stepIndex[iActionVar+1]);
				}
			}
		} // end for (iActionVar ..
	} // end for (iAction ..

	return numBackups;
}
