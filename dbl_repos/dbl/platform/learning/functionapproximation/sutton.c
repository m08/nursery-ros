/*
 *  Created on: Jul 23, 2009
 *      Author: Erik Schuitema
 */

int create_hash_sum(int *ints, int num_ints, long wraplength, int seed)
{
	//-----------------------------------------------------------------------------
	// MurmurHash2, by Austin Appleby

	// It has a few limitations:
	// 1. It will not work incrementally.
	// 2. It will not produce the same results on little-endian and big-endian machines.
	// 3. It generates 32-bits hash sums only. See the websites for 64-bit implementations.

	// Check for example the following URLs for more info on hashing algorithm:
	//  - http://www.strchr.com/hash_functions for a comparison of hash functions
	//  - http://en.wikipedia.org/wiki/Hash_function for background info and a list of hash functions

	// 'm' and 'r' are mixing constants generated offline.
	// They're not really 'magic', they just happen to work well.
	unsigned int i, k;
	const unsigned int m = 0x5bd1e995;
	const int r = 24;

	// Initialize the hash to a 'random' value
	unsigned int h = seed ^ num_ints;

	// Mix int values into the hash
	for (i=0; i<num_ints; i++)
	{
		k = (unsigned int)ints[i];

		k *= m;
		k ^= k >> r;
		k *= m;

		h *= m;
		h ^= k;
	}
	// Do a few final mixes of the hash
   	h ^= h >> 13;
   	h *= m;
   	h ^= h >> 15;

	return (int)(h % (unsigned int)wraplength);
}
