#include <stdio.h>
#include <stdlib.h>
#include "tilecoding.h"
#include <stdint.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <platform/math/randomc.h>
#include <string.h>
#include <fastfunc.h>

#ifndef clip
#define clip(x, minval, maxval) (std::max(std::min(x, maxval), minval))
#endif

#define CONST_MAPSIZE		128
//#define CONST_NumTilings	1
int gNumTilings=1;
#define CONST_MemSize		1024*1024*16 //4*512	//4194304
//#define WRAPTILES
#define USE_EXTENDED_FEATURES

#define valueType float
//CFATileCodingBasic<valueType, CONST_NumTilings, uint32_t/*, uint8_t*/>	gTileCoder;
CFATileCoding<valueType, uint32_t> *gTileCoder=NULL;

valueType getFunctionValue(float x, float y, float mapScale)
{
	return (sqrt(x*x + y*y)<(mapScale/2.0))?1.0:0.0;	// quarter circle
	//return 0.5*(1.0 + cos(x*M_PI/mapScale)*cos(y*M_PI/mapScale));	// gaussian landscape
	//return (x<y?1.0:0);	// half triangle
	//return ((x < mapScale/2.0) && (y < mapScale/2.0))?1.0:0.0;	// upper-left square
}

// location should be an array of length 2.
inline valueType readMapValue(float mapScale, float* location)
{
	valueType faValue;
	unsigned int tiles[gTileCoder->getNumTilings()];
#ifdef WRAPTILES
	int wrapWidths[2] = {(int)mapScale, (int)mapScale};
#ifdef USE_EXTENDED_FEATURES
	float featureValues[gTileCoder->getNumTilings()];
	gTileCoder->getEFValue(location, 2, wrapWidths, NULL, 0, tiles, featureValues, &faValue, true);
#else
	gTileCoder->getValue(location, 2, wrapWidths, NULL, 0, tiles, &faValue, true);
#endif
#elif defined USE_EXTENDED_FEATURES
	float featureValues[gTileCoder->getNumTilings()];
	gTileCoder->getEFValue(location, 2, NULL, 0, tiles, featureValues, &faValue, true);
#else
	gTileCoder->getValue(location, 2, NULL, 0, tiles, &faValue, true);
#endif
	return faValue;
}

inline valueType readFeatureValue(float mapScale, float* location)
{
	valueType faValue;
	unsigned int tiles[gTileCoder->getNumTilings()];
#ifdef WRAPTILES
	int wrapWidths[2] = {(int)mapScale, (int)mapScale};
#ifdef USE_EXTENDED_FEATURES
	float featureValues[gTileCoder->getNumTilings()];
	gTileCoder->getEFValue(location, 2, wrapWidths, NULL, 0, tiles, featureValues, &faValue, true);
	return featureValues[0];
#else
	return 1.0;
#endif
#elif defined USE_EXTENDED_FEATURES
	float featureValues[gTileCoder->getNumTilings()];
	gTileCoder->getEFValue(location, 2, NULL, 0, tiles, featureValues, &faValue, true);
	return featureValues[0];
#else
	return 1.0;
#endif
}

// Returns an error code
int writeMapValue(float mapScale, float* location, valueType newValue, float adjustmentFactor)
{
	int error=0;
	valueType faValue;
	unsigned int tiles[gTileCoder->getNumTilings()];
// Get current value
#ifdef WRAPTILES
	int wrapWidths[2] = {(int)mapScale, (int)mapScale};
#ifdef USE_EXTENDED_FEATURES
	float featureValues[gTileCoder->getNumTilings()];
	error = gTileCoder->getEFValue(location, 2, wrapWidths, NULL, 0, tiles, featureValues, &faValue, false);
#else
	error = gTileCoder->getValue(location, 2, wrapWidths, NULL, 0, tiles, &faValue, false);
#endif

#elif defined USE_EXTENDED_FEATURES
	float featureValues[gTileCoder->getNumTilings()];
	error = gTileCoder->getEFValue(location, 2, NULL, 0, tiles, featureValues, &faValue, false);
#else
	error = gTileCoder->getValue(location, 2, NULL, 0, tiles, &faValue, false);
#endif

// Set new value
#ifdef USE_EXTENDED_FEATURES
		gTileCoder->setEFValue(tiles, featureValues, faValue, newValue, adjustmentFactor);
#else
		gTileCoder->setValue(tiles, faValue, newValue, adjustmentFactor);
#endif
	return error;
}

// Returns RMSE
double getApproximationError(float mapScale)
{
	double rmse=0.0;	// Calculate root mean squared error
	for (int x=0; x<CONST_MAPSIZE; x++)
	{
		for (int  y=0; y<CONST_MAPSIZE; y++)
		{
			float location[2];
			location[0] = mapScale*(float)x/(float)CONST_MAPSIZE;
			location[1] = mapScale*(float)y/(float)CONST_MAPSIZE;
			valueType faValue = readMapValue(mapScale, location);
			double error = faValue - getFunctionValue(location[0], location[1], mapScale);
			rmse += error*error;
		}
	}
	rmse /= CONST_MAPSIZE*CONST_MAPSIZE;
	return sqrt(rmse);
}

void generateQualityCurve(float mapScale, int numIterations, double falloffRate, const char* filename)
{
	FILE *fOut = fopen(filename, "wt");
	double adjustmentFactor = 1.0;
	const int numTests = 200;
	int numItersBetweenTests = numIterations/numTests;
	for (int i=0; i<numIterations; i++)
	{
		// Test if we need to generate a performance measure
		if ((i % numItersBetweenTests) == 0)
		{
			double rmse = getApproximationError(mapScale);
			fprintf(fOut, "%d\t%.6f\n", i, rmse);
		}

		float location[2];
		location[0] = mapScale*gRanrotB.Random();
		location[1] = mapScale*gRanrotB.Random();
		valueType faValue;
		int error = writeMapValue(mapScale, location, getFunctionValue(location[0], location[1], mapScale), adjustmentFactor);
//		//valueType valueDiff = ((location[0]<location[1]?1.0:0) - faValue)/gTileCoder->getNumTilings();
//		valueType valueDiff = (((sqrt(location[0]*location[0] + location[1]*location[1])<(mapScale/2.0))?1.0:0.0) - faValue)/(valueType)gTileCoder->getNumTilings();
//		for (int iTile=0; iTile<gTileCoder->getNumTilings(); iTile++)
//			gTileCoder->adjustFeatureValue(tiles[iTile], valueDiff);

		if (error & ERR_MAX_REHASH)
			printf("Max number of rehashes reached!\n");
		if (error & ERR_OUT_OF_MEM)
			printf("Out of memory!\n");

		adjustmentFactor *= falloffRate;
	}
	fclose(fOut);
}

void printStats()
{
	// Print statistics
	printf("NumTilings: %d\tClaims: %d\tCalls: %d\tClearHits: %d\tCollisions: %d\tMemsize: %d\tBytesPerClaim: %d\n",
			gTileCoder->getNumTilings(),
			gTileCoder->getNumClaims(),
			gTileCoder->getNumCalls(),
			gTileCoder->getNumClearHits(),
			gTileCoder->getNumCollisions(),
			gTileCoder->getMemorySize(),
			gTileCoder->getNumBytesPerClaim());
}

int doubleBit(const double* dbl, const int bit)
{
	uint64_t intval;
	memcpy(&intval, dbl, 8);
	return (intval & (1ULL<<bit))?1:0;
}

void writeFAPPM(float mapScale, const char* filename)
{
	FILE* fOut = fopen(filename, "wt");
	fprintf(fOut, "P3 %d %d 255\n", CONST_MAPSIZE, CONST_MAPSIZE);
	for (int x=0; x<CONST_MAPSIZE; x++)
		for (int  y=0; y<CONST_MAPSIZE; y++)
		{
			float location[2];
			location[0] = mapScale*(float)x/(float)CONST_MAPSIZE;
			location[1] = mapScale*(float)y/(float)CONST_MAPSIZE;
			valueType faValue = readMapValue(mapScale, location);
			int color		=  (int)clip(255.0*faValue, 0.0, 255.0);
			int underflow	= -(int)clip(255.0*faValue, -255.0, 0.0);
			int overflow	=  (int)clip(255.0*(faValue-1.0), 0.0, 255.0);
			// New line after each pixels. Line can be 70 characters max.
			fprintf(fOut, "%d %d %d\n", color, overflow, underflow);
		}
	fclose(fOut);
}

void writeFeaturePPM(float mapScale, const char* filename)
{
	// We know that the first feature has base 0,0.
	// Therefore we can plot featureValues[0] for the corner in the positive quadrant originating in (0.0,0.0).
	FILE* fOut = fopen(filename, "wt");
	fprintf(fOut, "P3 %d %d 255\n", CONST_MAPSIZE, CONST_MAPSIZE);
	for (int x=0; x<CONST_MAPSIZE; x++)
		for (int  y=0; y<CONST_MAPSIZE; y++)
		{
			float location[2];
			location[0] = (float)x/(float)CONST_MAPSIZE;
			location[1] = (float)y/(float)CONST_MAPSIZE;
			valueType featureValue = readFeatureValue(mapScale, location);
			int color		=  (int)clip(255.0*featureValue, 0.0, 255.0);
			int underflow	= -(int)clip(255.0*featureValue, -255.0, 0.0);
			int overflow	=  (int)clip(255.0*(featureValue - 1.0), 0.0, 255.0);
			// New line after each pixels. Line can be 70 characters max.
			fprintf(fOut, "%d %d %d\n", color, overflow, underflow);
		}
	fclose(fOut);
}

int main(int argc, char** argv)
{
/*
	const int mod = 9;
	for (int i=10; i>-10; i--)
		printf("Def.: %d %% %d = %d. Safe: %d. Simple: %d\n", i, mod, i%mod, (i>=0)?(i%mod):(mod-1 - (-i-1)%mod), safe_mod(i, mod));
	return -1;
*/
	if (argc < 3)
	{
		printf("Usage:\n\tfaqualitytest [mapscale] [numtilings]\n\t* mapscale is the number of tile widths in each dimension of the map\n\t* numtilings is the number of overlapping tilings\n");
		return -1;
	}
	// Get mapscale
	float mapScale = atof(argv[1]);

	// Get num tilings
	gNumTilings = atoi(argv[2]);
	switch (gNumTilings)
	{
		case 1:
			gTileCoder = new CFATileCodingSafe<valueType, 1, uint32_t, uint8_t>;
			break;
		case 2:
			gTileCoder = new CFATileCodingSafe<valueType, 2, uint32_t, uint8_t>;
			break;
		case 4:
			gTileCoder = new CFATileCodingSafe<valueType, 4, uint32_t, uint8_t>;
			break;
		case 8:
			gTileCoder = new CFATileCodingSafe<valueType, 8, uint32_t, uint8_t>;
			break;
		case 16:
			gTileCoder = new CFATileCodingSafe<valueType, 16, uint32_t, uint8_t>;
			break;
		case 32:
			gTileCoder = new CFATileCodingSafe<valueType, 32, uint32_t, uint8_t>;
			break;
		case 64:
			gTileCoder = new CFATileCodingSafe<valueType, 64, uint32_t, uint8_t>;
			break;
		case 128:
			gTileCoder = new CFATileCodingSafe<valueType, 128, uint32_t, uint8_t>;
			break;
		case 256:
			gTileCoder = new CFATileCodingSafe<valueType, 256, uint32_t, uint8_t>;
			break;
		case 512:
			gTileCoder = new CFATileCodingSafe<valueType, 512, uint32_t, uint8_t>;
			break;
		case 1024:
			gTileCoder = new CFATileCodingSafe<valueType, 1024, uint32_t, uint8_t>;
			break;
		default:
			printf("Number of tilings not supported! Quitting.\n");
			return -1;
			break;
	}

	// Set memory size. Resets the parameters to 0 by default.
	gTileCoder->setMemorySize(CONST_MemSize);

	// Print statistics
	printStats();

	// Generate quality curve
	std::stringstream ssQfilename;
	ssQfilename << "performance_" << (int)mapScale << "x" << (int)mapScale << "_" << gTileCoder->getNumTilings() << "tilings";
#ifdef USE_EXTENDED_FEATURES
	ssQfilename << "_EF";
#endif
	ssQfilename << ".txt";

	int numiters = (int)1.0E4;
	double falloffRate = pow(0.05, 1.0/(double)numiters);
	generateQualityCurve(mapScale, numiters, falloffRate, ssQfilename.str().c_str());

	// Write approximation
	std::stringstream ssPPMfilename;
	ssPPMfilename << "perf_endmap_" << (int)mapScale << "x" << (int)mapScale << "_" << gTileCoder->getNumTilings() << "tilings";
#ifdef USE_EXTENDED_FEATURES
	ssPPMfilename << "_EF";
#endif
	ssPPMfilename << ".ppm";
	writeFAPPM(mapScale, ssPPMfilename.str().c_str());

	std::stringstream ssFeaturePPMFilename;
	ssFeaturePPMFilename << "featuremap_" << (int)mapScale << "x" << (int)mapScale << "_" << gTileCoder->getNumTilings() << "tilings";
#ifdef USE_EXTENDED_FEATURES
	ssFeaturePPMFilename << "_EF";
#endif
	ssFeaturePPMFilename << ".ppm";
	writeFeaturePPM(mapScale, ssFeaturePPMFilename.str().c_str());

	// Delete gTileCoder
	if (gTileCoder != NULL)
		delete gTileCoder;

	return 0;
}
