#ifndef __KDTREE_H_INCLUDED
#define __KDTREE_H_INCLUDED

// ******************************************************************************** //
// *** This file contains two template classes: CKDTreeNode and CKDTreeRootNode *** //
// ******************************************************************************** //
// *** (C) 2008 Erik Schuitema @ Delft University of Technology. ****************** //
// *** Faculty of 3ME, Dept. of Biomechanical Engineering. Delft Biorobotics Lab ** //
// *** e.schuitema@tudelft.nl ***************************************************** //
// ******************************************************************************** //

// TODO: We don't have to create both children at once!! Only create the one that is
// directly needed. Todo: create/destroy check on NULL; SplitUseChildLo, SplitUseChildHi?

#define C_State_Precision float

struct SDimInfo
{
	double	width;
	double	centre;
};

template<class TYPE> class CKDTreeNode
{
	protected:
		CKDTreeNode<TYPE>	*mParent;
	public:
		// The region of the split dimension is:
		//     [mSplitDim.centre - 0.5*mSplitDim.width, mSplitDim.centre + 0.5*mSplitDim.width]
		// Each child gets half of that range, and according centre
		int			mSplitDimIndex;
		SDimInfo	mSplitDim;
	protected:

		// Override this class factory function in derived classes
		virtual CKDTreeNode<TYPE>*	CreateNewNode(CKDTreeNode<TYPE>* parent);

		virtual void CreateData();
		virtual void DeleteData();

		// Retrieve the node that belongs to the given state.
		CKDTreeNode<TYPE>*	GetNodeByState(C_State_Precision *state);
		// This version also fills a dimInfos array of the leaf node of the state
		CKDTreeNode<TYPE>*	GetNodeByState(C_State_Precision *state, SDimInfo *dimInfos);

	public:
		union
		{
			// Either this node contains a data point
			TYPE			*mData;
			// .. OR it is split into two nodes in the mSplitDim dimension
			// with mChildLo representing the numerical lower part
			// and mChildHi the numerical upper part.
			struct
			{
				CKDTreeNode*	mChildHi;
				CKDTreeNode*	mChildLo;
			};
		};

		CKDTreeNode(CKDTreeNode<TYPE>* parent=NULL);
		~CKDTreeNode();

		// This function is probably not used that often anymore.
		// In most situations, you already retrieved the node using GetNodeByState(C_State_Precision *state, SDimInfo *dimInfos)
		// which supply you with the dimension descriptors already.
		virtual SDimInfo	GetDimInfo(const int dim, CKDTreeNode<TYPE> *whosAsking);

		void	Clear();
		void	Zero();
		int		GetSize();	// Returns the number of nodes

		// The most important functionality: Split (and Merge).
		// Nodes are always split in half (in two equal parts) in one dimension at a time.
		virtual void	Split(int dim, SDimInfo *dimInfos=NULL);	// pass ALL dimInfos of the state
		virtual void	Merge();

		inline bool		IsSplit() const
		{
			return mSplitDimIndex >= 0;
		}

		// Data handling functions
		inline virtual void		SetDataPtr(C_State_Precision* state, TYPE *value)
		{
			GetNodeByState(state)->mData = value;
		}
		inline virtual void		SetData(C_State_Precision* state, TYPE value)
		{
			*GetNodeByState(state)->mData = value;
		}

		inline virtual TYPE*	GetDataPtr(C_State_Precision* state)
		{
			return GetNodeByState(state)->mData;
		}
		inline virtual TYPE		GetData(C_State_Precision* state)
		{
			return *GetNodeByState(state)->mData;
		}

		inline virtual TYPE*	GetDataPtr(C_State_Precision* state, SDimInfo* dimInfos)	// This version also returns the position and size of the node
		{
			return GetNodeByState(state, dimInfos)->mData;
		}
		inline virtual TYPE		GetData(C_State_Precision* state, SDimInfo* dimInfos)	// This version also returns the position and size of the node
		{
			return *GetNodeByState(state, dimInfos)->mData;
		}

		// Operators
		CKDTreeNode<TYPE>* operator =(const CKDTreeNode<TYPE>& nodeToCopy)
		{
			// First, completely clean this node
			Clear();

			// Copy the two standard fields
			// Handle the union
			if (nodeToCopy.IsSplit())
			{
				// Because of Clear(), we are in data mode. Go to split mode (delete data)
				DeleteData();

				// Copy split info
				mSplitDimIndex	= nodeToCopy.mSplitDimIndex;
				mSplitDim		= nodeToCopy.mSplitDim;

				// Create and copy child nodes
				mChildLo	= CreateNewNode(this);
				*mChildLo	= *nodeToCopy.mChildLo;
				mChildHi	= CreateNewNode(this);
				*mChildHi	= *nodeToCopy.mChildHi;
			}
			else
			{
				// Copy data
				*mData = *nodeToCopy.mData;
			}
			return this;
		}

		// Assign a double to the complete node/tree
		CKDTreeNode<TYPE>* operator=(const double &dVal)
		{
			if (IsSplit())
			{
				*mChildLo = dVal;
				*mChildHi = dVal;
			}
			else
				*mData = dVal;

			return this;
		}

		CKDTreeNode<TYPE>* operator+=(const double &dVal)
		{
			if (IsSplit())
			{
				*mChildLo += dVal;
				*mChildHi += dVal;
			}
			else
				*mData += dVal;

			return this;
		}

		CKDTreeNode<TYPE>* operator+=(const CKDTreeNode<TYPE> &node1)
		{
			ASSERT(false);
			return this;
		}

		CKDTreeNode<TYPE>* operator*=(const double& dVal)
		{
			if (IsSplit())
			{
				*mChildLo *= dVal;
				*mChildHi *= dVal;
			}
			else
				*mData *= dVal;

			return this;
		}

		// Casting operator
		operator const TYPE ()
		{
			if (IsSplit())
			{
				TYPE result;
				result = (TYPE)*mChildLo;
				result += (TYPE)*mChildHi;
				result *= 0.5;
				return *mData;
			}
			else
				return *mData;
		}
		
};

template<class TYPE> class CKDTreeRootNode: public CKDTreeNode<TYPE>
{
	protected:
		SDimInfo		*mDimInfos;
		unsigned int	mNumDims;
		virtual SDimInfo	GetDimInfo(const int dim, CKDTreeNode<TYPE> *whosAsking);

	public:
		CKDTreeRootNode();
		CKDTreeRootNode(unsigned int numberOfDimensions, SDimInfo* dimInfos);
		~CKDTreeRootNode();


		void	SetResolution(C_State_Precision *state, SDimInfo *desiredDims);

		// Dimension management
		void		SetDimensions(unsigned int numDims, SDimInfo *dimInfos=NULL);

		inline void	SetDimInfos(SDimInfo* dimInfos)
		{
			// Copy the dimension info structs
			memcpy(mDimInfos, dimInfos, mNumDims*sizeof(SDimInfo));
		}

		inline void	GetDimInfos(SDimInfo* dimInfos)
		{
			memcpy(dimInfos, mDimInfos, mNumDims*sizeof(SDimInfo));
		}

		// Operators
		CKDTreeRootNode<TYPE>* operator=(const CKDTreeRootNode<TYPE>& nodeToCopy)
		{
			SetDimensions(nodeToCopy.mNumDims, nodeToCopy.mDimInfos);
			CKDTreeNode<TYPE>::operator=(nodeToCopy);
			return this;
		}

		CKDTreeRootNode<TYPE>* operator=(const double &dVal)
		{
			CKDTreeNode<TYPE>::operator=(dVal);
			return this;
		}
};

// Not needed anymore?? Forget this class for now
/*
template<class TYPE, int NUM_DIMENSIONS> class CKDTreeNodeRanged: public CKDTreeNode<TYPE>
{
	protected:
		SDimInfo	mDimInfos[NUM_DIMENSIONS];
		// Override this class factory function in derived classes
		virtual CKDTreeNode<TYPE>*	CreateNewNode()
		{
			return new CKDTreeNodeRanged<TYPE, NUM_DIMENSIONS>;
		}
		// typecased version of our children
		CKDTreeNodeRanged<TYPE, NUM_DIMENSIONS>* GetChildLo()
		{
			return (CKDTreeNodeRanged<TYPE, NUM_DIMENSIONS>*)mChildLo;
		}
		CKDTreeNodeRanged<TYPE, NUM_DIMENSIONS>* GetChildHi()
		{
			return (CKDTreeNodeRanged<TYPE, NUM_DIMENSIONS>*)mChildHi;
		}

		virtual SDimInfo	GetDimInfo(const int dim, CKDTreeNode<TYPE> *whosAsking)
		{
			if (whosAsking == this)
				return mDimInfos[dim];
			else
			{
				SDimInfo result;
				result.width = mSplitDim.width/2;
				if (whosAsking == mChildLo)
					result.centre = mSplitDim.centre - mSplitDim.width/4;
				else
					result.centre = mSplitDim.centre + mSplitDim.width/4;
				return result;
			}
		}
	public:
		virtual void Split(int dim)
		{
			CKDTreeNode<TYPE>::Split(dim);
			// Copy dim width and dim centres to the child nodes
			memcpy(GetChildLo()->mDimInfos, mDimInfos, NUM_DIMENSIONS*sizeof(SDimInfo));
			memcpy(GetChildHi()->mDimInfos, mDimInfos, NUM_DIMENSIONS*sizeof(SDimInfo));

			// And update the splitted dimension for the children
			GetChildLo()->mDimInfos[dim].width = mSplitDim.width/2;
			GetChildHi()->mDimInfos[dim].width = mSplitDim.width/2;
			GetChildLo()->mDimInfos[dim].centre = mSplitDim.centre - mSplitDim.width/4;
			GetChildHi()->mDimInfos[dim].centre = mSplitDim.centre + mSplitDim.width/4;
		}
};
*/

// *************************************************************************************** //
// **************************** IMPLEMENTATION OF CKDTreeNode **************************** //
// *************************************************************************************** //
template<class TYPE> CKDTreeNode<TYPE>::CKDTreeNode(CKDTreeNode<TYPE>* parent)
{
	mChildLo			= NULL;
	mChildHi			= NULL;
	mSplitDimIndex		= -1;	// Indicates that this node is not split
	mSplitDim.width		= 0;
	mSplitDim.centre	= 0;
	mData				= NULL;	// Do this before calling CreateData()!
	mParent				= parent;
	// Not in split mode but in data mode
	CreateData();
	*mData = 0;
}

template<class TYPE> CKDTreeNode<TYPE>::~CKDTreeNode()
{
	if (IsSplit())
	{
		delete mChildLo;
		delete mChildHi;
	}
	else
		DeleteData();
}

template<class TYPE> CKDTreeNode<TYPE>* CKDTreeNode<TYPE>::CreateNewNode(CKDTreeNode<TYPE>* parent)
{
	return new CKDTreeNode<TYPE>(parent);
}

template<class TYPE> void CKDTreeNode<TYPE>::CreateData()
{
	if (mData == NULL)
		mData = new TYPE;
}

template<class TYPE> void CKDTreeNode<TYPE>::DeleteData()
{
	if (mData != NULL)
	{
		delete mData;
		mData = NULL;
	}
}

template<class TYPE> void CKDTreeNode<TYPE>::Clear()
{
	if (IsSplit())
	{
		delete mChildLo;
		mChildLo = NULL;
		delete mChildHi;
		mChildHi = NULL;

		// Set that this node is not split any longer
		mSplitDimIndex		= -1;
		mSplitDim.centre	= 0;
		mSplitDim.width		= 0;
		// It was split, so mData was nonexistent
		CreateData();
		*mData = 0;
	}
	else
		// It was in data mode. But perhaps, no data was created yet
		*mData = 0;
}

template<class TYPE> void CKDTreeNode<TYPE>::Zero()
{
	if (IsSplit())
	{
		mChildLo->Zero();
		mChildHi->Zero();
	}
	else
		*mData = 0;
}

template<class TYPE> int CKDTreeNode<TYPE>::GetSize()
{
	if (IsSplit())
		return mChildHi->GetSize() + mChildLo->GetSize();
	else
		return 1;
}

template<class TYPE> CKDTreeNode<TYPE>* CKDTreeNode<TYPE>::GetNodeByState(C_State_Precision *state)
{
	if (IsSplit())
	{
		if (state[mSplitDimIndex] < mSplitDim.centre)
			return mChildLo->GetNodeByState(state);
		else
			return mChildHi->GetNodeByState(state);
	}
	else
		return this;
}

template<class TYPE> CKDTreeNode<TYPE>* CKDTreeNode<TYPE>::GetNodeByState(C_State_Precision *state, SDimInfo *dimInfos)
{
	if (IsSplit())
	{
		// Overwrite the width of the dimension we split in
		dimInfos[mSplitDimIndex].width = mSplitDim.width/2;

		if (state[mSplitDimIndex] < mSplitDim.centre)
		{
			dimInfos[mSplitDimIndex].centre = mSplitDim.centre - mSplitDim.width/4;
			return mChildLo->GetNodeByState(state);
		}
		else
		{
			dimInfos[mSplitDimIndex].centre = mSplitDim.centre + mSplitDim.width/4;
			return mChildHi->GetNodeByState(state);
		}

	}
	else
		return this;
}

template<class TYPE> SDimInfo CKDTreeNode<TYPE>::GetDimInfo(const int dim, CKDTreeNode<TYPE> *whosAsking)
{
	if (mSplitDimIndex == dim)
	{
		// This node was split in the dimension of interest
		// The child node requesting the answer has a split
		// section of this dimension's region
		SDimInfo result;
		result.width = mSplitDim.width/2;
		if (whosAsking == mChildLo)
			result.centre = mSplitDim.centre - mSplitDim.width/4;
		else
		{
			result.centre = mSplitDim.centre + mSplitDim.width/4;
			ASSERT(whosAsking == mChildHi);	// Only mChildLo and mChildHi can be asking this
		}
		return result;
	}
	else
		return mParent->GetDimInfo(dim, this);

}

template<class TYPE> void CKDTreeNode<TYPE>::Split(int dim, SDimInfo *dimInfos=NULL)
{
	// If already split, assert false and return
	if (IsSplit())
	{
		ASSERT(false);	// We are already split
		return;
	}

	// Split this node.
	// First, obtain the width and centre of
	// the dimension in which it is split
	if (dimInfos != NULL)
		mSplitDim	= dimInfos[dim];
	else
		mSplitDim	= GetDimInfo(dim, this);

	// then, set the mSplitDimIndex (not before GetDimInfo!)
	mSplitDimIndex	= dim;

	// **** The node is now in data mode (see union structure) **** //
	// Backup data: the Hi node gets the old data pointer.
	// So we have to call DeleteData for mChildHi and NOT call DeleteData() for this node.
	// and the Lo node gets a copy of the data.
	TYPE *oldData = mData;

	// **** The node is now in split mode (see union structure) **** //
	// Hi node
	mChildHi	= CreateNewNode(this);
	// Delete the data first; we already had it in this->mData
	mChildHi->DeleteData(); 
	mChildHi->mData		= oldData;

	// Lo node
	mChildLo	= CreateNewNode(this);
	// Data is already created in the constructor of mChildLo, so now copy the actual data from oldData
	*mChildLo->mData	= *oldData;


	// WE DO NOT NEED TO CALL DeleteData() SINCE WE GAVE OUR DATA POINTER TO mChildHi!
}

template<class TYPE> void CKDTreeNode<TYPE>::Merge()
{
	if (IsSplit())
	{
		//**** The node is still in split mode ****//
		// First merge the children into data points
		if (mChildLo->IsSplit())
			mChildLo->Merge();
		if (mChildHi->IsSplit())
			mChildHi->Merge();

		// Backup data pointers
		TYPE *childLoData = mChildLo->mData;
		TYPE *childHiData = mChildHi->mData;

		//TYPE tempData = (*mChildLo->mData + *mChildHi->mData)/2;
		delete mChildLo;
		mChildLo		= NULL;
		delete mChildHi;
		mChildHi		= NULL;
		mSplitDimIndex	= -1;

		//**** The node is now in data mode ****//
		CreateData();	// Create data
		// Copy actual data to mData by merging the child data
		*mData  = *childLoData;
		*mData += *childHiData;
		*mData /= 2;	
		// Delete child data
		delete childLoData;
		delete childHiData;
	}
}

// *************************************************************************************** //
// ************************** IMPLEMENTATION OF CKDTreeRootNode ************************** //
// *************************************************************************************** //

template<class TYPE> CKDTreeRootNode<TYPE>::CKDTreeRootNode()
{
	mNumDims = 1;
	mDimInfos = new SDimInfo[mNumDims];
	SDimInfo dimInfo;
	dimInfo.width	= 1.0;
	dimInfo.centre	= 0.0;
	SetDimInfos(&dimInfo);
}

template<class TYPE> CKDTreeRootNode<TYPE>::CKDTreeRootNode(unsigned int numberOfDimensions, SDimInfo* dimInfos)
{
	mNumDims = numberOfDimensions;
	mDimInfos = new SDimInfo[mNumDims];
	SetDimInfos(dimInfos);
}

template<class TYPE> CKDTreeRootNode<TYPE>::~CKDTreeRootNode()
{
	delete[] mDimInfos;
}

template<class TYPE> SDimInfo CKDTreeRootNode<TYPE>::GetDimInfo(const int dim, CKDTreeNode<TYPE> *whosAsking)
{
	if (whosAsking == this)
		return mDimInfos[dim];
	else
	{
		if (dim == mSplitDimIndex)
		// Then they got only half
		{
			SDimInfo result;
			result.width = mDimInfos[dim].width/2;
			if (whosAsking == mChildLo)
				result.centre = mDimInfos[dim].centre - mDimInfos[dim].width/4;
			else
				result.centre = mDimInfos[dim].centre + mDimInfos[dim].width/4;
			return result;
		}
		else
		// They inherited the whole size
			return mDimInfos[dim];
	}
}

template<class TYPE> void CKDTreeRootNode<TYPE>::SetDimensions(unsigned int numDims, SDimInfo *dimInfos=NULL)
{
	delete[] mDimInfos;
	mNumDims = numDims;
	mDimInfos = new SDimInfo[mNumDims];
	Clear();	// All child nodes are not guaranteed to work anymore when numDims changed.
	if (dimInfos != NULL)
		SetDimInfos(dimInfos);
}

template<class TYPE> void CKDTreeRootNode<TYPE>::SetResolution(C_State_Precision *state, SDimInfo *desiredDims)
{
	SDimInfo	*currentDims = new SDimInfo[mNumDims];
	// Start with the dimensions of the root node
	memcpy(currentDims, mDimInfos, mNumDims*sizeof(SDimInfo));
	// Request the node that belongs to our state, including its current dimensions
	CKDTreeNode<TYPE> *stateNode = GetNodeByState(state, currentDims);

	// Split (breadth first?) until we satisfy the desired dimensions
	bool allDimsSatisfied = false;
	while (!allDimsSatisfied)
	{
		allDimsSatisfied = true;
		for (unsigned int iDim=0; iDim<mNumDims; iDim++)
		{
			if (currentDims[iDim].width > desiredDims[iDim].width)
			{
				stateNode->Split(iDim, currentDims);
				currentDims[iDim].width *= 0.5;
				if (state[iDim] < stateNode->mSplitDim.centre)
				{
					stateNode = stateNode->mChildLo;
					currentDims[iDim].centre -= currentDims[iDim].width/2;
				}
				else
				{
					stateNode = stateNode->mChildHi;
					currentDims[iDim].centre += currentDims[iDim].width/2;
				}

				allDimsSatisfied &= (currentDims[iDim].width < desiredDims[iDim].width);
			}	// else allDimsSatisfied &= true; // Has no effect
		}
	}

	delete[] currentDims;
}


#endif
