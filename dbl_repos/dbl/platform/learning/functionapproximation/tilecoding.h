/*
 * tilecoding.h
 *
 *  Tile Coding function approximation
 *
 *  Author: Erik Schuitema, Delft University of Technology, The Netherlands / e.schuitema@tudelft.nl
 *  Contributors: Richard Sutton (University of Alberta), Sebastiaan Troost (Delft University of Technology), ...
 *
 *
 *  This is a modified copy of the implementation by Richard Sutton et al., see http://www.cs.ualberta.ca/~sutton/tiles2.html for more details.
 *  The modifications should result in higher performance and more flexibility.
 *  Although the implementation has become more complex, it's very easy to actually use and performance has increased.
 *  Also see University of New Hampshire (UNH) for the CMAC background of tile coding: http://www.ece.unh.edu/robots/cmac.htm
 *
 *  USAGE NOTES:
 *  - This file contains 4 classes, which are all templates:
 *  	* CFATileCoding		: Empty base class. Define a pointer of this type if you're not sure which class you'll eventually create.
 *  	* CFATileCodingBasic: Basic, unsafe tile coding: uses the least memory at the cost of making an unknown number of hashing collisions.
 *  	* CFATileCodingSafe	: Safe version of the hash table: keeps track of collisions and solves them if desired.
 *  	* CFATileCodingExt	: Extendable version of the hash table: it can grow in size if the memory is full, or if maxNumRehashes is reached for a query
 *  - For help on functions and their parameters, see the comments in the CFATileCoding class definition.
 *  - The template variables indexType and hashcheckType MUST be unsigned and integer, e.g. uint16_t, uint32_t or uint64_t.
 *	  Use 'int' as indexType if the amount of features won't grow over ~4 billion.
 *  - You can use shorter int types such as 'short' or 'char' as hashcheckType to save memory, but the smaller the check size,
 *    the less safe it is (the more collisions you'll get). It's a tradeoff between memory and safety.
 *    ** Make sure you #define PACK_STRUCTS, otherwise using shorter types will have no effect in memory savings! **
 *    Packing structs in general means slower code, but since this is probably not the bottle neck, it's enabled by default.
 *  - The number of tilings should be a power of two. If you don't do this, the effective 'resolution' may differ greatly
 *    between the dimensions of your state space.
 *  - valueType can be of any type, whether it's a float, double or a class containing e.g. an array,
 *    as long as the type supports the following operators: =, +=, -, =(double), /=(int/double), *(double), constructor((double))
 *    When using a vector class as valueType, one can approximate multidimensional data.
 *  - You can request function approximation values / tile indices in 'read-only mode'. In the Safe and Extendable implementations,
 *    this means you can read feature values while not claiming memory for them. This saves huge amounts of memory in e.g. Q-learning,
 *    where the Q-value for all possible action is requested during the argmax operation, but only one is executed and gets updated.
 *    The consequence is that read-only requests may not always return the same values (because memory got claimed in the meanwhile).
 *
 *
 *  DESIGN CONSIDERATIONS:
 *  - The hashing algorithm used is MurmurHash2, by Austin Appleby. It's fast and does a fine job under almost all conditions.
 *    It also doesn't require a table with random numbers (e.g. in tiles2.h, tiles2.cpp), which could wrap around in certain caes.
 *	  Check the following URLs for the selection of the hashing algorithm:
 *	    * http://www.strchr.com/hash_functions for a comparison of hash functions
 *      * http://en.wikipedia.org/wiki/Hash_function for background info and a list of hash functions
 *  - Because of hashing, features are stored in 'random' locations in RAM. When requesting several features,
 *    this means that read-ahead caching by the processor is no good for large approximators, and most feature reads
 *    will cause a cache mis hit, which are very expensive in terms of clock cycles.
 *    The implementations of CFATileCodingSafe and CFATileCodingExt store the hash check values right next to
 *    the approximated function values. This reduces the number of cache misses approx. by a factor 2 compared
 *    to implementations in which the function values are stored in a separate table (e.g. tiles2.h, tiles2.cpp).
 *  - When requesting tile indices, the approximated function value is returned as well. This is done because
 *    the additional cost is very low (see point above on cache misses) and you usually want it anyway.
 *    The current approximated value is also needed when adjusting feature values.
 *  - In Sutton & Barto, Reinforcement Learning: An Introduction, it is assumed that the learn rate alpha
 *    is chosen appropriately when updating feature values, i.e. smaller than 1/NUM_TILINGS.
 *    In this implementation, however, every feature stores a value comparable to the approximated value,
 *    which is calculated as (feature_value1 + feature_value2 + ... )/NUM_TILINGS.
 *    This removes the need to let the learn rate alpha depend on the number of tilings.
 *  - The number of tilings is also a template parameter to allow for (slightly?) more optimization during compilation.
 *    This is a little bit inconvenient, since you have to create a new object if the number of tilings changes
 *    during run-time. However, for us, performance was more important.
 *
 *
 *    (C) COPYRIGHT 2009 DELFT UNIVERSITY OF TECHNOLOGY / Erik Schuitema
 *
 */

#ifndef FATILECODING_H_
#define FATILECODING_H_

#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <Log2.h>
#include <fastfunc.h>

// Define the random number generator. getRandDouble() should return a double between 0 and 1.
#if 1
	#include <randomc.h>	// We will use the globally defined gRanrotB generator
	#define getRandDouble()	gRanrotB.Random()
#else
	#define getRandDouble()	((double)rand()/(double)RAND_MAX)
#endif

// Define a compiler-safe version of modulo (remainder)
template <typename _inttype>
inline _inttype safe_mod(_inttype X, _inttype Y)
{
	_inttype result = X%Y;
	if (result < 0)
		return result+Y;
	else
		return result;
}

// Error codes. Should be single bits so that they can be bitwise OR'd.
#define ERR_SUCCESS		0		// No error
#define ERR_OUT_OF_MEM	0x01	// All memory is used up
#define ERR_MAX_REHASH	0x02	// Maximum number of rehashing attempts has been reached

//#define MAX_SIGNED_INT(X)	((X)(((unsigned X)-1)>>1))			// Also works, but assumes that 'unsigned' can be put in front of the type, and assumes that -1 can be subtracted from an unsigned integer type
#define MAX_SIGNED_INT(X)	((X)~(X)((X)1<<(sizeof(X)*8-1)))	// This is completely evaluated to a constant by the (pre)compiler

#define MAX_NUM_VARS 20        // Maximum number of variables in a grid-tiling

#define HASHCHECK_NOTCLAIMED	0	// Hash value of non-claimed elements in the hash table

// Define this to pack (byte-align) memory structs. Necessary to benefit from short hash check types.
//#define PACK_STRUCTS

// Base class
template<typename valueType, typename indexType>
class CFATileCoding
{
	public:
		virtual ~CFATileCoding()	{}

		// className(): returns the class name.
		// Useful in determining the type of approximator when storing instances of derived classes in a base class pointer.
		virtual const char*	className()	{return "CFATileCoding-baseclass";}

		// reset(): completely resets the class. Statistics are reset and the data is reinitialized (also see setValueInitPolicy).
		virtual void		reset()	{}

		// setMemorySize(): sets the maximum number of features that can be contained in this function approximator.
		// numFeatures should be a power of two. This function also resets the data (always).
		virtual bool		setMemorySize(indexType numFeatures)	{return false;}

		// setMaxNumRehashes(): sets the maximum number of allowable rehashes in case of collisions.
		// Only meaningful in derived classes that store hash checks.
		virtual void		setMaxNumRehashes(const indexType maxNumRehashes)	{}

		// setValueInitPolicy(): sets the range from which random numbers are drawn during reset()
		// Call this BEFORE calling setMemorySize() or reset().
		virtual void		setValueInitPolicy(const valueType minValue, const valueType maxValue)	{}

		// getNumTilings(): returns the number of tilings
		virtual int			getNumTilings()			{return 0;}
		// getNumTilings(): returns the number of features used/claimed in memory
		virtual indexType	getNumClaims()			{return 0;}
		// getNumTilings(): returns the number of feature requests since the last reset
		virtual indexType	getNumCalls()			{return 0;}
		// getNumTilings(): returns the number of clear (no-collision) feature requests
		virtual indexType	getNumClearHits()		{return 0;}
		// getNumTilings(): returns the number of collisions
		virtual indexType	getNumCollisions()		{return 0;}
		// getNumTilings(): returns the memory size as the number of features
		virtual indexType	getMemorySize()			{return 0;}
		// getNumTilings(): returns the size in bytes required to store one feature (function value + hash check + ...)
		// The total amount of bytes spent by the main memory of the approximator is: getMemorySize()*getNumBytesPerClaim();
		virtual int			getNumBytesPerClaim()	{return 0;}

		// getValue(): request tile/feature indices and approximated value by providing floats and ints as state
		virtual int	getValue(
			const float* floats,		// array of floating point variables
			int num_floats,				// number of floating point variables
			const int* ints,			// array of integer variables
			int num_ints,				// number of integer variables
			indexType* tiles,			// provided array that will receive returned tile indices
			valueType* value,			// the approximated value at the specified location
			bool bReadOnly)				// read-only: only read tiles but don't claim them in memory
		{return 0;}

		// getValue(): request tile/feature indices and approximated value by providing floats and ints as state
		// This version uses extended features instead of binary features
		virtual int	getEFValue(
			const float* floats,		// array of floating point variables
			int num_floats,				// number of floating point variables
			const int* ints,			// array of integer variables
			int num_ints,				// number of integer variables
			indexType* tiles,			// provided array that will receive returned tile indices
			float* featureValues,		// provided array (obligatory!) that will receive the feature values (= height of membership functions)
			valueType* value,			// the approximated value at the specified location
			bool bReadOnly)				// read-only: only read tiles but don't claim them in memory
		{return 0;}

		// getValue(): request tile/feature indices and approximated value by providing floats and ints as state
		// This version of getValue() also takes a wrap_widths array, which defines for every float the wrap-around width of its range (0 = no wrapping)
		virtual int	getValue(
			const float* floats,		// array of floating point variables
			int num_floats,				// number of floating point variables
			const int* wrap_widths,		// array of widths (length and units as in floats)
			const int* ints,			// array of integer variables
			int num_ints,				// number of integer variables
			indexType* tiles,			// provided array that will receive returned tile indices
			valueType* value,			// the approximated value at the specified location
			bool bReadOnly)				// read-only: only read tiles but don't claim them in memory
		{return 0;}

		// getValue(): request tile/feature indices and approximated value by providing floats and ints as state
		// This version of getValue() also takes a wrap_widths array, which defines for every float the wrap-around width of its range (0 = no wrapping)
		// This version uses extended features instead of binary features
		virtual int	getEFValue(
			const float* floats,		// array of floating point variables
			int num_floats,				// number of floating point variables
			const int* wrap_widths,		// array of widths (length and units as in floats)
			const int* ints,			// array of integer variables
			int num_ints,				// number of integer variables
			indexType* tiles,			// provided array that will receive returned tile indices
			float* featureValues,		// provided array (obligatory!) that will receive the feature values (= height of membership functions)
			valueType* value,			// the approximated value at the specified location
			bool bReadOnly)				// read-only: only read tiles but don't claim them in memory
		{return 0;}

		// setValue(): sets the desired approximated value at a certain location,
		// providing the current value and the newly desired value.
		// The current value usually follows from a preceding call to getValue.
		virtual void	setValue(
		    indexType*	tiles,				// provided array containing tile indices of the location
			valueType	currentValue,		// the current approximated value at the specified location
			valueType	newValue			// the desired approximated value at the specified location
		    )
		{}

		// setValue(): sets the desired approximated value at a certain location,
		// providing the current value and the newly desired value.
		// The current value usually follows from a preceding call to getValue.
		// This version uses extended features instead of binary features
		virtual void	setEFValue(
		    indexType*	tiles,				// provided array containing tile indices of the location
		    float*		featureValues,		// provided array containing feature values (= height of membership functions)
		    valueType	currentValue,		// the current approximated value at the specified location
			valueType	newValue			// the desired approximated value at the specified location
		    )
		{}

		// setValue(): sets the desired approximated value at a certain location,
		// providing the current value and the newly desired value.
		// This version of setValue() also takes an adjustmentFactor [0..1] to only partially adjust towards newValue.
		virtual void	setValue(
		    indexType*	tiles,				// provided array containing tile indices of the location
			valueType	currentValue,		// the current approximated value at the specified location
			valueType	newValue,			// the desired approximated value at the specified location
			double		adjustmentFactor	// use this to take a smaller step towards the newValue
		    )
		{}

		// setValue(): sets the desired approximated value at a certain location,
		// providing the current value and the newly desired value.
		// This version of setValue() also takes an adjustmentFactor [0..1] to only partially adjust towards newValue.
		virtual void	setEFValue(
		    indexType*	tiles,				// provided array containing tile indices of the location
		    float*		featureValues,		// provided array containing feature values (= height of membership functions)
			valueType	currentValue,		// the current approximated value at the specified location
			valueType	newValue,			// the desired approximated value at the specified location
			double		adjustmentFactor	// use this to take a smaller step towards the newValue
		    )
		{}

		// adjustParameterValue(): adjust the parameter value of a single feature, providing the increment (valueAdjustment).
		virtual void		adjustParameterValue(const indexType featureLocation, const valueType valueAdjustment)	{}

		// getParameterValue(): get the parameter value of a single feature.
		virtual valueType	getParameterValue(const indexType featureLocation) { return 0; }

		// Memory doubling functions. Only meaningful for the memory doubling derived class.

		// doubleMemoryStart(): prepare to double memory
		virtual bool		doubleMemoryStart()									{return false;}
		// doubleMemoryConvertLocation(): convert old feature location to the new one.
		// This is useful when feature locations are stored elsewhere, e.g. in an eligibility trace
		virtual indexType	doubleMemoryConvertLocation(indexType oldLocation)	{return 0;}
		// doubleMemoryFinish(): finish up the memory doubling process
		virtual void		doubleMemoryFinish()								{}

		// Save the function approximator to a file stream
		virtual bool		save(std::ofstream& fileOutStream)					{return false;}

		// Load the function approximator from a file stream
		virtual bool		load(std::ifstream& fileInStream)					{return false;}
};


// Basic, unsafe version of tile coding
template<typename valueType, int numTilings, typename indexType>
class CFATileCodingBasic: public CFATileCoding<valueType, indexType>
{
	protected:
		// Debugging log object
		CLog2				mLog;
		// The data. Implemented as void pointer to help the implementation of derived classes.
		// Cast to something sensible when using the data, e.g. MyType* getData() {return (MyType*)mData;}
		void				*mData;
		// mData cast
		inline valueType*	data()	{return (valueType*)mData;}
		// Book-keeping
		indexType			mMemorySize;
		indexType			mNumClaims;
		indexType			mNumCalls;
		indexType			mNumClearHits;
		indexType			mNumCollisions;
		indexType			mMaxRehashes;

		// Value function initialization: min and max value to specify range from which random numbers are picked
		valueType			mInitMin;
		valueType			mInitMax;

		float				mFeatureScale;		// Parameter that specifies the feature width = 1/featureScale. For speed purposes, this value represents 1/(2*sigma^2) for gassian features.
		CFastFunc			mFastFeatureFunc;	// Fast approximation of the feature function

		// createHashSum() should return an unsigned integer type!
		indexType			createHashSum(const int *ints, const unsigned int num_ints, unsigned int seed)
		{
			// MurmurHash2, by Austin Appleby

			// Quote of external documentation:
			// It has a few limitations:
			// 1. It will not work incrementally.
			// 2. It will not produce the same results on little-endian and big-endian machines.
			// 3. It is 32-bits. See the websites for 64-bit implementations.

			// 'm' and 'r' are mixing constants generated offline.
			// They're not really 'magic', they just happen to work well.
			const unsigned int m = 0x5bd1e995;
			const int r = 24;

			// Initialize the hash to a 'random' value
			unsigned int h = seed ^ num_ints;

			// Mix 4 bytes at a time into the hash
			for (unsigned int i=0; i<num_ints; i++)
			{
				unsigned int k = static_cast<unsigned int>(ints[i]);

				k *= m;
				k ^= k >> r;
				k *= m;

				h *= m;
				h ^= k;
			}
			// Do a few final mixes of the hash
		   	h ^= h >> 13;
		   	h *= m;
		   	h ^= h >> 15;

			return (indexType)h;
		}

		virtual void		allocData()
		{
			// mData is just an array of valueType elements.
			mData = malloc(mMemorySize*sizeof(valueType));
		}
		virtual void		deleteData()
		{
			if (mData != NULL)
			{
				free(mData);
				mData = NULL;
			}
		}
		virtual void		resetData()
		{
			logInfoLn(this->mLog, "Resetting " << mMemorySize << " feature values to values between " << this->mInitMin << " and " << this->mInitMax);
			for (indexType i=0; i<mMemorySize; i++)
				data()[i] = mInitMin + (mInitMax - mInitMin)*(valueType)getRandDouble();
		}

		void				resetStats()
		{
			mNumCalls		= 0;
			mNumClearHits	= 0;
			mNumClaims		= 0;
			mNumCollisions	= 0;
		}

		// getFeature() returns an error code. The location of the feature is placed in *featureLocation, its corresponding parameter value in *parameterValue.
		virtual int		getFeature(const int *ints, const int num_ints, indexType *featureLocation, valueType* parameterValue, const bool bReadOnly)
		{
			// Hashing algo. No collision detection, nor solving.
			mNumCalls++;
			indexType sum = createHashSum(ints, num_ints, 449);
			*featureLocation = sum % mMemorySize;
			*parameterValue = data()[*featureLocation];
			return ERR_SUCCESS;
		}

		virtual indexType	getFeatureLocation(const int *ints, const int num_ints, const bool bReadOnly)
		{
			indexType sum = createHashSum(ints, num_ints, 449) % mMemorySize;
			__builtin_prefetch(data() + sum);
			return sum;
		}

		// getFeatureValue() calculates the feature value, or in other words, the height of the membership function.
		// For regular tile coding with binary features, this is 1.0 for 'activated' tiles, and 0.0 otherwise.
		virtual float	getFeatureValue(float sqrDistToFeatureCenter)
		{
			// Binary features:
			//return 1.0;

			// Gaussian shape:
			return exp(-sqrDistToFeatureCenter*mFeatureScale);

			// Gaussian shape, fast version:
			//return mFastFeatureFunc.getNNInRange(sqrDistToFeatureCenter*mFeatureScale);

			// Another option from (Atkeson, Moore and Schaal. Locally Weighted Learning. 1996).
			// This option is bad since the function is really 0.0 at some places and gives trouble when normalizing.
			/*
			float d = sqrt(sqrDistToFeatureCenter)*2;
			if (d < 1.0)
			{
				float x = (1.0-d*d*d);
				return x*x*x;
			}
			else
				return 0.0;
			*/
		}

	public:
		CFATileCodingBasic():
			mLog("CFATileCodingBasic"),
			mData(NULL),
			mMemorySize(0),
			mMaxRehashes(1000),
			mInitMin(0.0),
			mInitMax(0.0),
			mFeatureScale(1.0/0.25)
		{
			resetStats();
			// The minimum distance is 0.0; the maximum is sqrt( (0.5^2 + 0.5^2 + ... (numDimensions times))/(2*sigma^2) ).
			// Gauss2 is a function of the *squared* distance.
			//TODO: how to get numdims here??
			mFastFeatureFunc.init((_fastfunc_datatype)0.0, (_fastfunc_datatype)(0.25*12*mFeatureScale), 128, &CFastFunc::gauss2);
		}

		virtual ~CFATileCodingBasic()
		{
			deleteData();
		}

		virtual const char*	className()	{return "CFATileCoding";}
		virtual int			getNumTilings()			{return numTilings;}

		void				reset()
		{
			resetData();
			resetStats();
		}

		// setMemorySize() resizes the memory. Warning: it also resets it.
		bool				setMemorySize(indexType numFeatures)
		{
			// Check if we already have the right size. In that case, just reset.
			if (mMemorySize == numFeatures)
			{
				reset();
				return true;
			}
			indexType tmp = numFeatures;
			bool sizeOK = true;
			while (tmp > 2)
			{
				if (tmp % 2 != 0)
				{
					printf("\nError: number of features in tile coding must be power of 2 (requested: %d)\n", numFeatures);
					sizeOK = false;
					break;
				}
				tmp /= 2;
			}
			// Delete old memory
			deleteData();
			// Allocate new memory if size is ok
			if (sizeOK)
			{
				mMemorySize	= numFeatures;
				allocData();
				reset();
			}
			return sizeOK;
		}

		void				setMaxNumRehashes(const indexType maxNumRehashes)	{mMaxRehashes = maxNumRehashes;}
		void				setValueInitPolicy(const valueType minValue, const valueType maxValue)	{mInitMin = minValue; mInitMax = maxValue;}

		indexType			getNumClaims()			{return mNumClaims;}
		indexType			getNumCalls()			{return mNumCalls;}
		indexType			getNumClearHits()		{return mNumClearHits;}
		indexType			getNumCollisions()		{return mNumCollisions;}
		indexType			getMemorySize()			{return mMemorySize;}
		virtual int			getNumBytesPerClaim()	{return sizeof(data()[0]);}

		// getValue(): version with floats and ints and binary features
		int	getValue(const float* floats, int num_floats, const int* ints, int num_ints, indexType* tiles, valueType* value, bool bReadOnly)
		{
			int qstate[MAX_NUM_VARS];
			int base[MAX_NUM_VARS];
			int coordinates[MAX_NUM_VARS * 2 + 1];   /* one interval number per relevant dimension */
			int num_coordinates = num_floats + num_ints + 1;

			for (int i=0; i<num_ints; i++)
				coordinates[num_floats+1+i] = ints[i];

			/* quantize state to integers (henceforth, tile widths == numTilings) */
			for (int i = 0; i < num_floats; i++)
			{
				qstate[i] = (int) floor(floats[i] * numTilings);
				base[i] = 0;
			}

			int error = ERR_SUCCESS;
			/*compute the tile numbers */
			for (int j = 0; j < numTilings; j++)
			{
				/* loop over each relevant dimension */
				for (int i = 0; i < num_floats; i++)
				{
					/* find coordinates of activated tile in tiling space */
					coordinates[i] = qstate[i] - safe_mod((qstate[i] - base[i]), numTilings);

					/* compute displacement of next tiling in quantized space */
					base[i] += 1 + (2 * i);
				}
				/* add additional indices for tiling and hashing_set so they hash differently */
				coordinates[num_floats] = j;

				tiles[j] = getFeatureLocation(coordinates, num_coordinates, bReadOnly);
			}
			
			valueType q=0;
			for (int j = 0; j < numTilings; j++)
				q += getParameterValue(tiles[j]);
			*value = q/numTilings;
			return error;
		}

		// getEFValue(): version with floats and ints and extended features: featureValues contains values from [0,1] while for binary features it's a vector of 1's.
		int	getEFValue(const float* floats, int num_floats, const int* ints, int num_ints, indexType* tiles, float* featureValues, valueType* value, bool bReadOnly)
		{
			int i,j;
			int qstate[MAX_NUM_VARS];
			int base[MAX_NUM_VARS];
			valueType parameterValues[numTilings];	// Cached parameter values of the selected features
			//float sqr_dist;						// Squared distance to feature center
			float max_dist;							// Maximum coordinate distance to the feature center
			float sum_of_feature_values = 0;		// The sum of values of all activated features, used in normalization
			int coordinates[MAX_NUM_VARS * 2 + 1];   /* one interval number per relevant dimension */
			int num_coordinates = num_floats + num_ints + 1;

			for (int i=0; i<num_ints; i++)
				coordinates[num_floats+1+i] = ints[i];

			/* quantize state to integers (henceforth, tile widths == numTilings) */
			for (i = 0; i < num_floats; i++)
			{
				qstate[i] = (int) floor(floats[i] * numTilings);
				base[i] = 0;
			}

			int error = ERR_SUCCESS;
			/*compute the tile numbers */
			for (j = 0; j < numTilings; j++)
			{
				//sqr_dist = 0;
				max_dist = 0;
				/* loop over each relevant dimension */
				for (i = 0; i < num_floats; i++)
				{
					/* find coordinates of activated tile in tiling space */
					coordinates[i] = qstate[i] - safe_mod((qstate[i] - base[i]), numTilings);

					// Add this coordinate's component to the squared distance to the feature center
					// The tile begins at coordinates[i], hence its center is located at (coordinates[i] + numTilings/2).
					// The location of this variable is qstate[i] + center_dists[i].
					// Note that one 'unit' of coordinates, qstate and rest_dists is 1/numTilings in state space
					float dist = floats[i] - (float)coordinates[i]/(float)numTilings - 0.5f;
					//sqr_dist += dist*dist;
					//sqr_dist += dist*dist*dist*dist;
					dist = fabs(dist);
					if (dist > max_dist)
						max_dist = dist;

					/* compute displacement of next tiling in quantized space */
					base[i] += 1 + (2 * i);
				}
				//sqr_dist = sqrt(sqr_dist); // 4-norm
				/* add additional indices for tiling and hashing_set so they hash differently */
				coordinates[i] = j;
				featureValues[j] = 0.50001f-max_dist; //getFeatureValue(sqr_dist);
				sum_of_feature_values += featureValues[j];
				valueType paramValue;
				error |= getFeature(coordinates, num_coordinates, tiles+j, &paramValue, bReadOnly);
				parameterValues[j] = paramValue;
			}
			*value = 0;
			float normalization_factor = (float)numTilings/sum_of_feature_values;
			//float normalization_factor = 1.0;
			for (j=0; j<numTilings; j++)
			{
				featureValues[j] *= normalization_factor;	// Return normalized featureValues to the user
				*value += parameterValues[j]*featureValues[j];
			}
			*value /= numTilings;	// Because we normalized, we can divide by numTilings
			//*value /= sum_of_feature_values;	// Because we did not normalize, we should divide by the sum of feature values
			return error;
		}

		// getValue(): version with wrapping floats and ints and binary features
		int	getValue(const float* floats, int num_floats, const int* wrap_widths, const int* ints, int num_ints, indexType* tiles, valueType* value, bool bReadOnly)
		{
			int i,j;
			int qstate[MAX_NUM_VARS];
			int base[MAX_NUM_VARS];
			int wrap_widths_times_num_tilings[MAX_NUM_VARS];
			int coordinates[MAX_NUM_VARS * 2 + 1];   /* one interval number per relevant dimension */
			int num_coordinates = num_floats + num_ints + 1;

			for (int i=0; i<num_ints; i++)
				coordinates[num_floats+1+i] = ints[i];

			/* quantize state to integers (henceforth, tile widths == numTilings) */
			for (i = 0; i < num_floats; i++)
			{
				qstate[i] = (int) floor(floats[i] * numTilings);
				base[i] = 0;
				wrap_widths_times_num_tilings[i] = wrap_widths[i] * numTilings;
			}

			/*compute the tile numbers */
			int error = ERR_SUCCESS;
			*value = 0;
			for (j = 0; j < numTilings; j++)
			{
				/* loop over each relevant dimension */
				for (i = 0; i < num_floats; i++)
				{
					/* find coordinates of activated tile in tiling space */
					coordinates[i] = qstate[i] - safe_mod((qstate[i] - base[i]), numTilings);

					if (wrap_widths[i] != 0)
						coordinates[i] = safe_mod(coordinates[i], wrap_widths_times_num_tilings[i]);

					/* compute displacement of next tiling in quantized space */
					base[i] += 1 + (2 * i);
				}
				/* add additional indices for tiling and hashing_set so they hash differently */
				coordinates[i] = j;
				tiles[j] = getFeatureLocation(coordinates, num_coordinates, bReadOnly);
			}
			
			valueType q=0;
			for (int j = 0; j < numTilings; j++)
				q += getParameterValue(tiles[j]);
			*value = q/numTilings;
			return error;
		}

		// getEFValue(): version with wrapping floats and ints and extended features: featureValues contains values from [0,1] while for binary features it's a vector of 1's.
		int	getEFValue(const float* floats, int num_floats, const int* wrap_widths, const int* ints, int num_ints, indexType* tiles, float* featureValues, valueType* value, bool bReadOnly)
		{
			int i,j;
			int qstate[MAX_NUM_VARS];
			int base[MAX_NUM_VARS];
			int wrap_widths_times_num_tilings[MAX_NUM_VARS];
			valueType parameterValues[numTilings];	// Cached parameter values of the selected features
			//float sqr_dist;							// Squared distance to feature center
			float max_dist;							// Maximum coordinate distance to the feature center
			float sum_of_feature_values = 0;		// The sum of values of all activated features, used in normalization
			int coordinates[MAX_NUM_VARS * 2 + 1];   /* one interval number per relevant dimension */
			int num_coordinates = num_floats + num_ints + 1;

			for (int i=0; i<num_ints; i++)
				coordinates[num_floats+1+i] = ints[i];

			/* quantize state to integers (henceforth, tile widths == numTilings) */
			for (i = 0; i < num_floats; i++)
			{
				qstate[i] = (int) floor(floats[i] * numTilings);
				base[i] = 0;
				wrap_widths_times_num_tilings[i] = wrap_widths[i] * numTilings;
			}

			/*compute the tile numbers */
			int error = ERR_SUCCESS;
			*value = 0;
			for (j = 0; j < numTilings; j++)
			{
				//sqr_dist = 0;
				max_dist = 0;
				/* loop over each relevant dimension */
				for (i = 0; i < num_floats; i++)
				{
					/* find coordinates of activated tile in tiling space */
					coordinates[i] = qstate[i] - safe_mod((qstate[i] - base[i]), numTilings);

					// Add this variable's component to the squared distance to the feature center
					// The tile begins at coordinates[i], hence its center is located at (coordinates[i] + numTilings/2).
					// The location of this variable is qstate[i] + center_dists[i].
					// Note that one 'unit' of coordinates, qstate and rest_dists is 1/numTilings in state space
					float dist = floats[i] - (float)coordinates[i]/(float)numTilings - 0.5f;
					//sqr_dist += dist*dist;
					dist = fabs(dist);
					if (dist > max_dist)
						max_dist = dist;


					if (wrap_widths[i] != 0)
						coordinates[i] = safe_mod(coordinates[i], wrap_widths_times_num_tilings[i]);

					/* compute displacement of next tiling in quantized space */
					base[i] += 1 + (2 * i);
				}
				/* add additional indices for tiling and hashing_set so they hash differently */
				coordinates[i] = j;

				featureValues[j] = 0.50001f-max_dist; //getFeatureValue(sqr_dist);
				sum_of_feature_values += featureValues[j];
				valueType paramValue;
				error |= getFeature(coordinates, num_coordinates, tiles+j, &paramValue, bReadOnly);
				parameterValues[j] = paramValue;
			}
			*value = 0;
			float normalization_factor = (float)numTilings/sum_of_feature_values;
			//float normalization_factor = 1.0;
			for (j=0; j<numTilings; j++)
			{
				featureValues[j] *= normalization_factor;	// Return normalized featureValues to the user
				*value += parameterValues[j]*featureValues[j];
			}
			*value /= numTilings;	// Because we normalized, we can divide by numTilings
			//*value /= sum_of_feature_values;	// Because we did not normalize, we should divide by the sum of feature values
			return error;
		}

		// Set desired value of the function approximator at the current tiles
		void	setValue(indexType* tiles, valueType currentValue, valueType newValue)
		{
			valueType valueDiff = (newValue - currentValue);
			for (int iTile=0; iTile<numTilings; iTile++)
				adjustParameterValue(tiles[iTile], valueDiff);
		}

		// Set desired value of the function approximator at the current tiles with extended features
		void	setEFValue(indexType* tiles, float *featureValues, valueType currentValue, valueType newValue)
		{
			valueType valueDiff = (newValue - currentValue);
			for (int iTile=0; iTile<numTilings; iTile++)
				adjustParameterValue(tiles[iTile], valueDiff*featureValues[iTile]);
		}

		// Adjust the value of the function approximator towards a desired value at the current tiles
		void	setValue(indexType* tiles, valueType currentValue, valueType newValue, double adjustmentFactor)
		{
			valueType valueDiff = (newValue - currentValue)*(valueType)adjustmentFactor;
			for (int iTile=0; iTile<numTilings; iTile++)
				adjustParameterValue(tiles[iTile], valueDiff);
		}

		// Adjust the value of the function approximator towards a desired value at the current tiles with extended features
		void	setEFValue(indexType* tiles, float *featureValues, valueType currentValue, valueType newValue, double adjustmentFactor)
		{
			valueType valueDiff = (newValue - currentValue)*(valueType)adjustmentFactor;
			for (int iTile=0; iTile<numTilings; iTile++)
				adjustParameterValue(tiles[iTile], valueDiff*featureValues[iTile]);
		}

		virtual void	adjustParameterValue(const indexType featureLocation, const valueType valueAdjustment)
		{
			data()[featureLocation] += valueAdjustment;
		}

		virtual valueType	getParameterValue(const indexType featureLocation)
		{
			return data()[featureLocation];
		}

		// Load/save interface
		virtual bool	save(std::ofstream& fileOutStream)
		{
			if (fileOutStream.bad())
				return false;

			// Book-keeping
			fileOutStream.write((char*)&mMaxRehashes,	sizeof(mMaxRehashes));
			fileOutStream.write((char*)&mMemorySize,	sizeof(mMemorySize));
			fileOutStream.write((char*)&mNumClaims,		sizeof(mNumClaims));
			fileOutStream.write((char*)&mNumCalls,		sizeof(mNumCalls));
			fileOutStream.write((char*)&mNumClearHits,	sizeof(mNumClearHits));
			fileOutStream.write((char*)&mNumCollisions,	sizeof(mNumCollisions));
			fileOutStream.write((char*)&mInitMin,		sizeof(mInitMin));
			fileOutStream.write((char*)&mInitMax,		sizeof(mInitMax));

			// The data. Implemented as void pointer to help the implementation of derived classes.
			fileOutStream.write((char*)mData, mMemorySize*getNumBytesPerClaim());

			return !fileOutStream.bad();
		}

		virtual bool	load(std::ifstream& fileInStream)
		{
			if (!fileInStream.good())
				return false;
			// Book-keeping
			indexType	desiredMemorySize;
			fileInStream.read((char*)&mMaxRehashes,		sizeof(mMaxRehashes));
			fileInStream.read((char*)&desiredMemorySize,sizeof(desiredMemorySize));

			// Stats are going to be reset by setMemorySize()
			indexType numClaims;
			indexType numCalls;
			indexType numClearHits;
			indexType numCollisions;
			fileInStream.read((char*)&numClaims,		sizeof(numClaims));
			fileInStream.read((char*)&numCalls,			sizeof(numCalls));
			fileInStream.read((char*)&numClearHits,		sizeof(numClearHits));
			fileInStream.read((char*)&numCollisions,	sizeof(numCollisions));

			fileInStream.read((char*)&mInitMin,			sizeof(mInitMin));
			fileInStream.read((char*)&mInitMax,			sizeof(mInitMax));
			// Allocate memory of desired size. Resets the data as well
			setMemorySize(desiredMemorySize);
			// The data
			fileInStream.read((char*)mData, mMemorySize*getNumBytesPerClaim());

			mNumClaims		= numClaims;
			mNumCalls		= numCalls;
			mNumClearHits	= numClearHits;
			mNumCollisions	= numCollisions;

			return fileInStream.good();
		}
};

// Controlled version of the hash table: keeps track of collisions and solves them if desired
template<typename valueType, int numTilings, typename indexType, typename hashcheckType>
class CFATileCodingSafe: public CFATileCodingBasic<valueType, numTilings, indexType>
{
	protected:
		// Data now consists of structs with a valueType member and a hash check
		struct CFATileCodingSafeData
		{
			valueType		mParamValue;	// Parameter value corresponding to this feature
			hashcheckType	mCheck;
		}
#ifdef PACK_STRUCTS
		__attribute__((__packed__))
#endif
		;
		// mData cast
		using CFATileCodingBasic<valueType, numTilings, indexType>::mData;
		inline CFATileCodingSafeData*	data()	{return (CFATileCodingSafeData*)mData;}

		virtual void		allocData()
		{
			mData	= malloc(this->mMemorySize*sizeof(CFATileCodingSafeData));
		}
		virtual void		deleteData()
		{
			if (mData != NULL)
			{
				free(mData);
				mData = NULL;
			}
		}
		virtual void		resetData()
		{
			logInfoLn(this->mLog, "Resetting " << this->mMemorySize << " feature values to values between " << this->mInitMin << " and " << this->mInitMax);
			// Set all check hashes to HASHCHECK_NOTCLAIMED
			for (indexType i=0; i<this->mMemorySize; i++)
			{
				data()[i].mParamValue = this->mInitMin + (this->mInitMax - this->mInitMin)*(valueType)getRandDouble();
				data()[i].mCheck = HASHCHECK_NOTCLAIMED;
			}
		}
		virtual hashcheckType	checkMemLocation(const indexType location)
		{
			return data()[location].mCheck;
		}
		virtual void		claimMemLocation(const indexType location, const indexType sum, const hashcheckType ccheck)
		{
			this->mNumClaims++;
			data()[location].mCheck = ccheck;
		}

		inline int hashBySum(const indexType sum, const hashcheckType ccheck, indexType *featureLocation, valueType* featureValue, bool bReadOnly)
		{
			this->mNumCalls++;
			indexType j = sum % this->mMemorySize;

			// Check for a collision
			hashcheckType locationCheck = checkMemLocation(j);
			if (locationCheck == ccheck)
			{
				//  No collision: a re-hit
				this->mNumClearHits++;
			}
			else if (locationCheck == HASHCHECK_NOTCLAIMED)
			{
				// Not claimed memory: a clear hit
				this->mNumClearHits++;
				// Claim it if not in read-only mode
				if (!bReadOnly)
				{
					claimMemLocation(j, sum, ccheck);
				}
			}
			else
			{
				// Solve collision.
				indexType h2 = 1 + 2 * (sum % (MAX_SIGNED_INT(indexType)/4));
				indexType i = 0;
				while (++i)
				{
					this->mNumCollisions++;
					j = (j+h2) % (this->mMemorySize);
					if (i > this->mMemorySize)
						return ERR_OUT_OF_MEM;
					if (i > this->mMaxRehashes)
						return ERR_MAX_REHASH;

					locationCheck = checkMemLocation(j);
					if (locationCheck == ccheck)
						// We found it!
						break;

					if (locationCheck == HASHCHECK_NOTCLAIMED)
					{
						// Not claimed memory; claim it if not in read-only mode
						if (!bReadOnly)
							claimMemLocation(j, sum, ccheck);
						break;
					}
				}
			}

			*featureLocation	= j;
			*featureValue		= getParameterValue(j);
			return ERR_SUCCESS;
		}

		virtual int 		getFeature(const int *ints, const int num_ints, indexType *featureLocation, valueType* parameterValue, const bool bReadOnly)
		{
			// Hashing algo
			indexType sum			= this->createHashSum(ints, num_ints, 449);
			// Generate hash check with different seed. Make sure this is never equal to HASHCHECK_NOTCLAIMED.
			hashcheckType ccheck	= (hashcheckType)(this->createHashSum(ints, num_ints, 457)) | 1;	// Always set LSB so that ccheck is never 0 (= HASHCHECK_NOTCLAIMED).
			return hashBySum(sum, ccheck, featureLocation, parameterValue, bReadOnly);
		}

		virtual indexType	getFeatureLocation(const int *ints, const int num_ints, const bool bReadOnly)
		{
			indexType featureLocation;
			valueType parameterValue;
			
			if (getFeature(ints, num_ints, &featureLocation, &parameterValue, bReadOnly) != ERR_SUCCESS)
				throw std::bad_alloc();
			else
				return featureLocation;
		}
		
	public:
		virtual const char*	className()	{return "CFATileCodingSafe";}
		virtual int			getNumBytesPerClaim()	{return sizeof(data()[0]);}
		virtual void		adjustParameterValue(const indexType featureLocation, const valueType valueAdjustment)
		{
			data()[featureLocation].mParamValue += valueAdjustment;
		}
		virtual valueType	getParameterValue(const indexType featureLocation)
		{
			return data()[featureLocation].mParamValue;
		}

};

// Extendable version of the hash table: it can grow in size if the memory is full, or if maxNumRehashes is reached for a query
template<typename valueType, int numTilings, typename indexType, typename hashcheckType>
class CFATileCodingExt: public CFATileCodingSafe<valueType, numTilings, indexType, hashcheckType>
{
	private:
		// Define memory type
		struct CFATileCodingExtData
		{
			valueType		mParamValue;	// Parameter value corresponding to this feature
			hashcheckType	mCheck;
			indexType		mRawSum;
		}
#ifdef PACK_STRUCTS
		__attribute__((__packed__))
#endif
		;

		// Memory pointer used in the memory doubling process
		void*				mOldData;
		inline CFATileCodingExtData*	oldData()	{return (CFATileCodingExtData*)mOldData;}

	protected:
		// mData cast
		using CFATileCodingSafe<valueType, numTilings, indexType, hashcheckType>::mData;
		inline CFATileCodingExtData*	data()	{return (CFATileCodingExtData*)mData;}

		virtual void		allocData()
		{
			mData = malloc(this->mMemorySize*sizeof(CFATileCodingExtData));
		}
		virtual void		deleteData()
		{
			if (mData != NULL)
			{
				free(mData);
				mData = NULL;
			}
		}
		virtual void		resetData()
		{
			logInfoLn(this->mLog, "Resetting " << this->mMemorySize << " feature values to values between " << this->mInitMin << " and " << this->mInitMax);
			for (indexType i=0; i<this->mMemorySize; i++)
			{
				data()[i].mParamValue = this->mInitMin + (this->mInitMax - this->mInitMin)*(valueType)getRandDouble();
				data()[i].mCheck = HASHCHECK_NOTCLAIMED;
			}
		}
		virtual hashcheckType	checkMemLocation(const indexType location)
		{
			return data()[location].mCheck;
		}
		virtual void		claimMemLocation(const indexType location, const indexType sum, const hashcheckType ccheck)
		{
			this->mNumClaims++;
			data()[location].mCheck		= ccheck;
			data()[location].mRawSum	= sum;
		}
		virtual void		setParameterValue(const indexType featureLocation, const valueType parameterValue)
		{
			data()[featureLocation].mParamValue = parameterValue;
		}
	public:
		virtual const char*	className()	{return "CFATileCodingExt";}
		virtual int			getNumBytesPerClaim()	{return sizeof(data()[0]);}
		virtual void		adjustParameterValue(const indexType featureLocation, const valueType valueAdjustment)
		{
			data()[featureLocation].mParamValue += valueAdjustment;
		}
		virtual valueType	getParameterValue(const indexType featureLocation)
		{
			return data()[featureLocation].mParamValue;
		}

		// Doubles the memory while keeping the data
		bool				doubleMemoryStart()
		{
			// Backup memory
			mOldData			= this->mData;
			// Allocate twice as much
			this->mData			= NULL;
			this->mMemorySize	*= 2;
			allocData();
			// Check alloc result
			if (mData == NULL)
				return false;

			// Reset it
			this->reset();
			// Insert old data into new data by rehashing (this is necessary to have proper collision solving
			for (indexType i=0; i<this->mMemorySize/2; i++)
				if (oldData()[i].mCheck != HASHCHECK_NOTCLAIMED)
					setParameterValue(doubleMemoryConvertLocation(i), oldData()[i].mParamValue);

			return true;
		}
		// Function to convert memory locations while doubling, for example mem locations used in an eligibility trace or a learnt state transition model
		inline indexType	doubleMemoryConvertLocation(indexType oldLocation)
		{
			indexType	featureLocation=0;
			valueType	featureValue;
			this->hashBySum(oldData()[oldLocation].mRawSum, oldData()[oldLocation].mCheck, &featureLocation, &featureValue, false);	// Read-only version. We don't check for returned errors.
			return featureLocation;
		}
		void				doubleMemoryFinish()
		{
			free(mOldData);
			mOldData = NULL;
		}
};

#endif /* FATILECODING_H_ */
