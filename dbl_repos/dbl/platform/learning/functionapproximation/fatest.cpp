#include <stdio.h>
#include <stdlib.h>
#include "tilecoding.h"
#include <stdint.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <platform/math/randomc.h>
#include <string.h>

#ifndef clip
#define clip(x, minval, maxval) (std::max(std::min(x, maxval), minval))
#endif

#define CONST_MAPSIZE		256
//#define CONST_NumTilings	1
int gNumTilings=1;
#define CONST_MemSize		1024*1024*16 //4*512	//4194304
//#define WRAPTILES

#define USE_EXTENDED_FEATURES

#define valueType float
//CFATileCodingBasic<valueType, CONST_NumTilings, uint32_t/*, uint8_t*/>	gTileCoder;
CFATileCoding<valueType, uint32_t> *gTileCoder=NULL;

/*
void compilerFooler()
{
	int numTilings=1;
	for (int i=0; i<10; i++)
	{
		CFATileCodingSafe<valueType, numTilings, uint32_t, uint8_t> piet;
		printf("%s", piet.className());
		numTilings *= 2;
	}
}
*/

void getTimeDiff(timeval* start, timeval* end, int *secs, int *usecs)
{
	int s = end->tv_sec - start->tv_sec;
	int us = end->tv_usec - start->tv_usec;
	if (us < 0)
	{
		s--;
		us += 1E6;
	}
	*secs = s;
	*usecs = us;
}

valueType getFunctionValue(float x, float y, float mapScale)
{
	 return (sqrt(x*x + y*y)<(mapScale/2.0))?1.0:0.0;
	 //return (x<y?1.0:0);
}

void writePPM(float mapScale, const char* filename)
{
	FILE* fOut = fopen(filename, "wt");
	fprintf(fOut, "P3 %d %d 255\n", CONST_MAPSIZE, CONST_MAPSIZE);
	int writtenPixels = 0;
	for (int x=0; x<CONST_MAPSIZE; x++)
		for (int  y=0; y<CONST_MAPSIZE; y++)
		{
			fprintf(fOut, "0 %d 0 ", (int)(255.0*getFunctionValue(mapScale*(float)x/(float)CONST_MAPSIZE, mapScale*(float)y/(float)CONST_MAPSIZE, mapScale)));

			// New line after 8 pixels. Line can be 70 characters max.
			if (writtenPixels % 8 == 0)
				fprintf(fOut, "\n");

			writtenPixels=0;
		}
	fclose(fOut);
}

void writeFAPPM(float mapScale, const char* filename)
{
	FILE* fOut = fopen(filename, "wt");
	fprintf(fOut, "P3 %d %d 255\n", CONST_MAPSIZE, CONST_MAPSIZE);
	int writtenPixels = 0;
	for (int x=0; x<CONST_MAPSIZE; x++)
		for (int  y=0; y<CONST_MAPSIZE; y++)
		{
			float location[2];
			location[0] = mapScale*(float)x/(float)CONST_MAPSIZE;
			location[1] = mapScale*(float)y/(float)CONST_MAPSIZE;
			valueType faValue;
			unsigned int tiles[gTileCoder->getNumTilings()];
#ifdef WRAPTILES
			int wrapWidths[2] = {(int)mapScale, (int)mapScale};
			gTileCoder->getValue(location, 2, wrapWidths, NULL, 0, tiles, &faValue, true);
#elif defined USE_EXTENDED_FEATURES
			float featureValues[gTileCoder->getNumTilings()];
			gTileCoder->getEFValue(location, 2, NULL, 0, tiles, featureValues, &faValue, true);
#else
			gTileCoder->getValue(location, 2, NULL, 0, tiles, &faValue, true);
#endif
			int color		=  (int)clip(255.0*faValue, 0.0, 255.0);
			int underflow	= -(int)clip(255.0*faValue, -255.0, 0.0);
			int overflow	=  (int)clip(255.0*(faValue-1.0), 0.0, 255.0);
			fprintf(fOut, "%d %d %d ", color, overflow, underflow);

			// New line after 8 pixels. Line can be 70 characters max.
			if (writtenPixels % 8 == 0)
				fprintf(fOut, "\n");

			writtenPixels=0;
		}
	fclose(fOut);
}

void fillFA(float mapScale)
{
	for (int x=0; x<CONST_MAPSIZE; x++)
		for (int  y=0; y<CONST_MAPSIZE; y++)
		{
			float location[2];
			location[0] = mapScale*(float)x/(float)CONST_MAPSIZE;
			location[1] = mapScale*(float)y/(float)CONST_MAPSIZE;
			valueType faValue, newValue;
			unsigned int tiles[gTileCoder->getNumTilings()];
#ifdef WRAPTILES
			int wrapWidths[2] = {(int)mapScale, (int)mapScale};
			int result = gTileCoder->getValue(location, 2, wrapWidths, NULL, 0, tiles, &faValue, false);
#elif defined USE_EXTENDED_FEATURES
			float featureValues[gTileCoder->getNumTilings()];
			int result = gTileCoder->getEFValue(location, 2, NULL, 0, tiles, featureValues, &faValue, false);
#else
			int result = gTileCoder->getValue(location, 2, NULL, 0, tiles, &faValue, false);
#endif
			newValue = getFunctionValue(location[0], location[1], mapScale);
#ifdef USE_EXTENDED_FEATURES
			gTileCoder->setEFValue(tiles, featureValues, faValue, newValue);
#else
			gTileCoder->setValue(tiles, faValue, newValue);
#endif
//			valueType valueDiff = ((x<y?1.0:0) - faValue)/gTileCoder->getNumTilings();
//			for (int iTile=0; iTile<gTileCoder->getNumTilings(); iTile++)
//				gTileCoder->adjustFeatureValue(tiles[iTile], valueDiff);

			if (result & ERR_MAX_REHASH)
				printf("Max number of rehashes reached at pixel %d,%d!\n", x, y);
			if (result & ERR_OUT_OF_MEM)
				printf("Out of memory at pixel %d,%d!\n", x, y);
		}
}

void randomFillFA(float mapScale, int numIterations, double falloffRate)
{
	double adjustmentFactor = 1.0;
	for (int i=0; i<numIterations; i++)
	{
		float location[2];
		location[0] = mapScale*gRanrotB.Random();
		location[1] = mapScale*gRanrotB.Random();
		valueType faValue, newValue;
		unsigned int tiles[gTileCoder->getNumTilings()];
#ifdef WRAPTILES
		int wrapWidths[2] = {(int)mapScale, (int)mapScale};
		int result = gTileCoder->getValue(location, 2, wrapWidths, NULL, 0, tiles, &faValue, false);
#elif defined USE_EXTENDED_FEATURES
		float featureValues[gTileCoder->getNumTilings()];
		int result = gTileCoder->getEFValue(location, 2, NULL, 0, tiles, featureValues, &faValue, false);
#else
		int result = gTileCoder->getValue(location, 2, NULL, 0, tiles, &faValue, false);
#endif
		newValue = getFunctionValue(location[0], location[1], mapScale);
#ifdef USE_EXTENDED_FEATURES
		gTileCoder->setEFValue(tiles, featureValues, faValue, newValue, adjustmentFactor);
#else
		gTileCoder->setValue(tiles, faValue, newValue, adjustmentFactor);
#endif
//		//valueType valueDiff = ((location[0]<location[1]?1.0:0) - faValue)/gTileCoder->getNumTilings();
//		valueType valueDiff = (((sqrt(location[0]*location[0] + location[1]*location[1])<(mapScale/2.0))?1.0:0.0) - faValue)/(valueType)gTileCoder->getNumTilings();
//		for (int iTile=0; iTile<gTileCoder->getNumTilings(); iTile++)
//			gTileCoder->adjustFeatureValue(tiles[iTile], valueDiff);

		if (result & ERR_MAX_REHASH)
			printf("Max number of rehashes reached!\n");
		if (result & ERR_OUT_OF_MEM)
			printf("Out of memory!\n");

		adjustmentFactor *= falloffRate;
	}
}

double speedTest(float mapScale, int numDims, int numIterations)
{
	valueType maxVal = -100;
	float *location = new float[numDims];
	for (int i=0; i<numIterations; i++)
	{
		/*
		// Find a location on a 'hyperline' of type [x, x, x, x, x] so that we won't fill the complete state space
		float loc = (float)mapScale*(float)rand()/(float)RAND_MAX;
		for (int l=0; l<numDims; l++)
			location[l] = loc + l*0.1f;
		*/
		// Find a random location
		float loc = 100.0f*(float)rand()/(float)RAND_MAX;
		for (int l=0; l<numDims; l++)
			location[l] = loc + (float)l;

		unsigned int tiles[gTileCoder->getNumTilings()];
		valueType faValue;

#ifdef WRAPTILES
		int wrapWidths[2] = {(int)mapScale, (int)mapScale};
		int result = gTileCoder->getValue(location, numDims, wrapWidths, NULL, 0, tiles, &faValue, true);
#elif defined USE_EXTENDED_FEATURES
		float featureValues[gTileCoder->getNumTilings()];
		int result = gTileCoder->getEFValue(location, numDims, NULL, 0, tiles, featureValues, &faValue, true);
#else
		int result = gTileCoder->getValue(location, numDims, NULL, 0, tiles, &faValue, true);
#endif

		if (result != ERR_SUCCESS)
		{
			printf("Out of mem at iteration %d. Stopping.\n", i);
			break;
		}
		else
		{
			if (faValue > maxVal)
			{
				maxVal = faValue;
			}
		}
	}
	delete[] location;
	return maxVal;
	//printf("Maximum value found: %.3f\n", maxVal);
}

void printStats()
{
	// Print statistics
	printf("NumTilings: %d\tClaims: %d\tCalls: %d\tClearHits: %d\tCollisions: %d\tMemsize: %d\tBytesPerClaim: %d\n",
			gTileCoder->getNumTilings(),
			gTileCoder->getNumClaims(),
			gTileCoder->getNumCalls(),
			gTileCoder->getNumClearHits(),
			gTileCoder->getNumCollisions(),
			gTileCoder->getMemorySize(),
			gTileCoder->getNumBytesPerClaim());
}

int doubleBit(const double* dbl, const int bit)
{
	uint64_t intval;
	memcpy(&intval, dbl, 8);
	return (intval & (1ULL<<bit))?1:0;
}

int main(int argc, char** argv)
{
	if (argc < 3)
	{
		printf("Usage:\n\tfatest [mapscale] [numtilings]\n\t* mapscale is the number of tile widths in each dimension of the map\n\t* numtilings is the number of overlapping tilings\n");
		return -1;
	}
	// Get map scale
	float mapScale = atof(argv[1]);
	printf("Using map scale of %.1f\n", mapScale);
	// Get num tilings
	gNumTilings = atoi(argv[2]);
	switch (gNumTilings)
	{
		case 1:
			gTileCoder = new CFATileCodingSafe<valueType, 1, uint32_t, uint8_t>;
			break;
		case 2:
			gTileCoder = new CFATileCodingSafe<valueType, 2, uint32_t, uint8_t>;
			break;
		case 4:
			gTileCoder = new CFATileCodingSafe<valueType, 4, uint32_t, uint8_t>;
			break;
		case 8:
			gTileCoder = new CFATileCodingSafe<valueType, 8, uint32_t, uint8_t>;
			break;
		case 16:
			gTileCoder = new CFATileCodingSafe<valueType, 16, uint32_t, uint8_t>;
			break;
		case 32:
			gTileCoder = new CFATileCodingSafe<valueType, 32, uint32_t, uint8_t>;
			break;
		case 64:
			gTileCoder = new CFATileCodingSafe<valueType, 64, uint32_t, uint8_t>;
			break;
		case 128:
			gTileCoder = new CFATileCodingSafe<valueType, 128, uint32_t, uint8_t>;
			break;
		case 256:
			gTileCoder = new CFATileCodingSafe<valueType, 256, uint32_t, uint8_t>;
			break;
		case 512:
			gTileCoder = new CFATileCodingSafe<valueType, 512, uint32_t, uint8_t>;
			break;
		case 1024:
			gTileCoder = new CFATileCodingSafe<valueType, 1024, uint32_t, uint8_t>;
			break;
		default:
			printf("Number of tilings not supported! Quitting.\n");
			return -1;
			break;
	}
	//CFATileCodingExt<valueType, gTileCoder->getNumTilings(), uint32_t, uint8_t>	gTileCoder;

//	double piet = 2.0;
//	printf("Piet: ", piet);
//	for (int i=0; i<8*sizeof(piet); i++)
//		printf("%d", doubleBit(&piet, i));
//	printf("\n");
//	piet = -2.0;
//	printf("Piet: ", piet);
//	for (int i=0; i<8*sizeof(piet); i++)
//		printf("%d", doubleBit(&piet, i));
//	printf("\n");
//	piet = 0;
//	printf("Piet in binary after \"piet = 0\": ");
//	for (int i=0; i<8*sizeof(piet); i++)
//		printf("%d", doubleBit(&piet, i));
//	printf("\n");
//	memset(&piet, 0, sizeof(piet));
//	printf("Piet after memsetting to zero: %.3f\n", piet);
//	return 0;

	gTileCoder->setMemorySize(CONST_MemSize);
#if 1
	int numiters = (int)1E4;
	double falloffRate = pow(0.05, 1.0/(double)numiters);
	randomFillFA(mapScale, numiters, falloffRate);
#else
	fillFA(mapScale);
#endif

	// Print statistics
	printStats();

#if 0
	printf("Note: fstream bufsize is %d\n", BUFSIZ);
	const char* filename = "fa.dat";
	// Write FA
	std::ofstream fileOutStream(filename, std::ios::out | std::ios::binary);
	gTileCoder->save(fileOutStream);
	fileOutStream.close();
	// Reset FA
	gTileCoder->reset();
	// Load FA
	std::ifstream fileInStream(filename, std::ios::in | std::ios::binary);
	gTileCoder->load(fileInStream);
	fileInStream.close();
#endif


#if 1
	// Speed test series
	printf("Starting speedtest for \"%s\"\n", gTileCoder->className());
	const int numDims = 12;
	timeval tStart, tEnd;
	int	tdifSecs1, tdifUsecs1;
	for (int i=4; i>=0; i--)
	{
		int numIterations = pow(10, abs(i));
		printf("Speedtests with %d iterations:\n", numIterations);
		for(int k=0; k<4; k++)
		{
			gettimeofday(&tStart, NULL);
			speedTest(mapScale, numDims, numIterations);
			gettimeofday(&tEnd, NULL);
			getTimeDiff(&tStart, &tEnd, &tdifSecs1, &tdifUsecs1);
			printf("Speedtest %d (%d dims, %7d iterations) done in %d.%06ds.\n", k, numDims, numIterations, tdifSecs1, tdifUsecs1);
		}
	}
	printf("\n");
	// Print statistics again
	//printStats();
#endif

#if 0
	// Double twice before writing bitmaps
	gTileCoder->doubleMemoryStart();
	gTileCoder->doubleMemoryFinish();
	gTileCoder->doubleMemoryStart();
	gTileCoder->doubleMemoryFinish();
	printf("Stats after doubling the memory twice:\n");
	printStats();
#endif

#if 1
	// Write reference
	writePPM(mapScale, "reference.ppm");
	std::stringstream ss;
	ss << "tilecoding_" << (int)mapScale << "x" << (int)mapScale << "_" << gTileCoder->getNumTilings() << "tilings";
#ifdef USE_EXTENDED_FEATURES
	ss << "_EF";
#endif
	ss << ".ppm";
	// Write approximation
	writeFAPPM(mapScale, ss.str().c_str());
#endif

	// Delete gTileCoder
	if (gTileCoder != NULL)
		delete gTileCoder;

	return 0;
}
