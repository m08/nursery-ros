/*
 * fatest.cpp
 *
 *  Creation date: 27 Oct 2009
 *         Author: Erik Schuitema
 *
 * Test program to show the use of tilecoding.h.
 *
 * The program approximates a function, of which a pixel map will be written to file
 * both for the reference function and for the approximated function.
 *
 * (C) COPYRIGHT 2009 DELFT UNIVERSITY OF TECHNOLOGY / Erik Schuitema
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "tilecoding.h"
#include <stdint.h>
#include <math.h>
#include <string.h>

#ifndef clip
#define clip(x, minval, maxval) (std::max(std::min(x, maxval), minval))
#endif

#define CONST_MAPSIZE		512			// Size in pixels (width = height) of our approximated pixel map
#define CONST_NumTilings	32
#define CONST_MapScale		16.0f		// Number of tile widths per map width
#define CONST_MemSize		16*1024		// Memory size in number of features

// Define the approximated valueType
#define valueType float

// Define the function that we will be trying to approximate
valueType getFunctionValue(float x, float y)
{
	 //return (sqrt(x*x + y*y)<(CONST_MapScale/2.0))?1.0:0.0;
	bool bx=false, by=false;
	if ((int)(x/1.0) % 5 == 0)
		bx = true;

	if ((int)(y/1.0) % 5 == 0)
		by = true;

	return (valueType)(bx^by);
}

// Randomly fill the function approximator using getFunctionValue()
void randomFillFA(CFATileCoding<valueType, uint32_t>* tileCoder, int numIterations)
{
	for (int i=0; i<numIterations; i++)
	{
		float location[2];
		location[0] = CONST_MapScale*(double)rand()/(double)RAND_MAX;
		location[1] = CONST_MapScale*(double)rand()/(double)RAND_MAX;
		valueType faValue, newValue;
		// Define memory for the tile indices
		unsigned int tiles[CONST_NumTilings];
		// Request current value
		int result = tileCoder->getValue(location, 2, NULL, 0, tiles, &faValue, false);
		// Determine new value
		newValue = getFunctionValue(location[0], location[1]);
		// Set value in function approximator
		tileCoder->setValue(tiles, faValue, newValue);

		if (result & ERR_MAX_REHASH)
			printf("Max number of rehashes reached!\n");
		if (result & ERR_OUT_OF_MEM)
			printf("Out of memory!\n");
	}
}

// Write a graphical PPM file (very simple format) of the reference map
void writePPM(const char* filename)
{
	FILE* fOut = fopen(filename, "wt");
	fprintf(fOut, "P3 %d %d 255\n", CONST_MAPSIZE, CONST_MAPSIZE);
	int writtenPixels = 0;
	for (int x=0; x<CONST_MAPSIZE; x++)
		for (int  y=0; y<CONST_MAPSIZE; y++)
		{
			fprintf(fOut, "0 %d 0 ", (int)(255.0*getFunctionValue(CONST_MapScale*(float)x/(float)CONST_MAPSIZE, CONST_MapScale*(float)y/(float)CONST_MAPSIZE)));

			// New line after 8 pixels. Line can be 70 characters max.
			if (writtenPixels % 8 == 0)
				fprintf(fOut, "\n");

			writtenPixels=0;
		}
	fclose(fOut);
}

// Write a graphical PPM file (very simple format) of the approximated map.
// Values within range [0..1] will be red. Values < 0 will turn up blue, values > 1 will turn up yellow.
void writeFAPPM(CFATileCoding<valueType, uint32_t>* tileCoder, const char* filename)
{
	FILE* fOut = fopen(filename, "wt");
	fprintf(fOut, "P3 %d %d 255\n", CONST_MAPSIZE, CONST_MAPSIZE);
	int writtenPixels = 0;
	for (int x=0; x<CONST_MAPSIZE; x++)
		for (int  y=0; y<CONST_MAPSIZE; y++)
		{
			float location[2];
			location[0] = CONST_MapScale*(float)x/(float)CONST_MAPSIZE;
			location[1] = CONST_MapScale*(float)y/(float)CONST_MAPSIZE;
			valueType faValue;
			unsigned int tiles[CONST_NumTilings];
			tileCoder->getValue(location, 2, NULL, 0, tiles, &faValue, true);

			int color		=  (int)clip(255.0*faValue, 0.0, 255.0);
			int underflow	= -(int)clip(255.0*faValue, -255.0, 0.0);
			int overflow	=  (int)clip(255.0*(faValue-1.0), 0.0, 255.0);
			fprintf(fOut, "%d %d %d ", color, overflow, underflow);

			// New line after 8 pixels. Line can be 70 characters max.
			if (writtenPixels % 8 == 0)
				fprintf(fOut, "\n");

			writtenPixels=0;
		}
	fclose(fOut);
}

// Print statistics
void printStats(CFATileCoding<valueType, uint32_t>* tileCoder)
{
	// Print statistics
	printf("Claims: %d\tCalls: %d\tClearHits: %d\tCollisions: %d\tMemsize: %d\tBytesPerClaim: %d\n",
			tileCoder->getNumClaims(),
			tileCoder->getNumCalls(),
			tileCoder->getNumClearHits(),
			tileCoder->getNumCollisions(),
			tileCoder->getMemorySize(),
			tileCoder->getNumBytesPerClaim());
}

// Main program
int main(int argc, char** argv)
{
	// Seed the random number generator
	srand(0);

	// Create a function approximator instance: provide value type, number of tilings, index type and hash check type.

	// Simple version: not safe
	//CFATileCodingBasic<valueType, CONST_NumTilings, uint32_t>				tileCoder;
	// Safe version: 16-bit hash check
	//CFATileCodingSafe<valueType, CONST_NumTilings, uint32_t, uint16_t>	tileCoder;
	// Extendable version (memory can be doubleD): 32-bit hash check
	CFATileCodingExt<valueType, CONST_NumTilings, uint32_t, uint16_t>		tileCoder;

	// Set its memory size
	tileCoder.setMemorySize(CONST_MemSize);

	// Randomly fill it using the defined function (see getFunctionValue())
	randomFillFA(&tileCoder, (int)2E4);

	// Print statistics
	printStats(&tileCoder);

// Enable this part to test the function approximator after doubling its capacity (twice)
#if 0
	// Double twice before writing bitmaps
	tileCoder.doubleMemoryStart();
	tileCoder.doubleMemoryFinish();
	tileCoder.doubleMemoryStart();
	tileCoder.doubleMemoryFinish();
	printf("Stats after doubling the memory twice:\n");
	printStats(&tileCoder);
#endif

	// Write reference image
	writePPM("FA-reference.ppm");
	// Write approximated image
	writeFAPPM(&tileCoder, "FA-tilecoding.ppm");

	return 0;
}
