/*
 * PolicySpace.h
 *
 *  Created on: Oct 8, 2009
 *      Author: Erik Schuitema
 */

#ifndef POLICYSPACE_H_
#define POLICYSPACE_H_

#include "QSpace.h"
#include <Log2.h>
#include "AgentInterface.h"

class CPolicySpace
{
	protected:
		CLog2				mLog;		// Custom log stream for "memory"

	public:
		CPolicySpace():
			mLog("polspace")
		{}

		virtual ~CPolicySpace()
		{}


		// The stored policy
		virtual bool	consultPolicy(CAgentState& state, CAgentAction& action)
		{return false;}
		virtual bool	updateConsultedPolicy(CAgentAction& action, double learnRate)
		{return false;}
		virtual bool	updatePolicy(CAgentState& state, CAgentAction& action, double learnRate)
		{return false;}

		// Get the Q value of an explorative action. The action has to be filled in advance and might be ignored by this function.
		virtual bool	consultExporationPolicy(CAgentState& state, CAgentAction& action)
		{return false;}


		virtual bool	setMemorySize(_IndexPrecision memorySize)
		{ return false; }

		virtual void			resetMemory() {}
		virtual _IndexPrecision getMemorySize()
		{return 0;}
		virtual _IndexPrecision	getMemoryUsage()
		{return 0;}
};

#endif /* POLICYSPACE_H_ */
