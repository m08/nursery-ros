/*
 * QSpaceMultiResTileCoding.h
 *
 *  Created on: Wed 27 Jul 2011
 *      Author: Erik Schuitema
 */

#include <QSpaceTileCoding.h>
#include <randomc.h>

#define CONST_MaxNumTilingResolutions	4

class CQSpaceMultiResTileCoding: public CQSpaceTileCoding
{
	protected:
		// Additional ("sub") resolutions on top of the regular one as proposed by mAgentState
		int												mNumResolutions;
		double											mResolutions[CONST_MaxNumTilingResolutions];
		CFATileCoding<_QPrecision, _IndexPrecision>*	mTileCoders[CONST_MaxNumTilingResolutions];	// Sub tile coding function approximators
		CReplacingTraceTC								mTraces[CONST_MaxNumTilingResolutions];	// Because feature indices come from different memories, we need distinct traces as well

	public:
		CQSpaceMultiResTileCoding();
		virtual ~CQSpaceMultiResTileCoding();

		// addResolution() can ONLY be called BEFORE:
		// - init()
		// - setNumTilings()
		// - setMemorySize()
		void	addResolution(double scaleFact);

		// Overloaded functions
		virtual void	setInitialQValuePolicy(_QPrecision valMin = 0, _QPrecision valMax = 1);
		virtual bool	getQValueTileCodingScaled			(const _StatePrecision *scaledState, const int* wrapData, const int numStateVars, const _StatePrecision *scaledAction, const int numActionVars, CQValueTileCoding& QTV, bool bReadOnly);
		virtual bool	getQValueTileCodingScaledIntActions	(const _StatePrecision *scaledState, const int* wrapData, const int numStateVars, const int *action, const int numActionVars, CQValueTileCoding& QTV, bool bReadOnly);

		// Set a specific Q value
		virtual bool		setQValue(CAgentState& state, CAgentAction& action, _QPrecision value);					//  set a specific Q value
		virtual bool		setStoredQValue(_QPrecision value);										//  stored Q value
		virtual bool		setStoredBestActionQValue(_QPrecision value);							//  stored best action
		virtual bool		setStoredExplorativeActionQValue(_QPrecision value);					//  stored explorative action

		// Trace options
		virtual bool			traceInit(_QPrecision DiscountFactor, _QPrecision minVal, bool adjustTraceLength=true);
		virtual _IndexPrecision	traceGetLength();												//  amount of state-action pairs in trace
		virtual _QPrecision		traceGetDiscountFactor();										//  trace discount factor lambda

		// Discount all previous states
		virtual bool			traceFall();

		// Add a State-Action Pair to the trace (and discount all previous states)
		virtual bool			traceAdd(CAgentState& state, CAgentAction& action);						//  use SetState()
		virtual bool			traceAddStored();														//  stored state action pair
		virtual bool			traceAddStoredBest();													//  stored best action
		virtual bool			traceAddStoredExplorative();											//  stored random action

		// Clear the trace
		virtual bool			traceClear();

		// Do a Q space trace update
		virtual bool			traceUpdateQValues(_QPrecision Delta);

		// ***************************************
		// ** Disk Functions
		// ***************************************

		// Load QSpace from a stream
		virtual bool	load(std::ifstream& fileInStream);	//TODO

		// Save QSpace to a stream
		virtual bool	save(std::ofstream& fileOutStream);	//TODO

		// ***************************************
		// ** Basic memory Functions
		// ***************************************

		virtual bool	setMemorySize(_IndexPrecision MemorySize);
		virtual void	resetMemory();
		virtual _IndexPrecision getMemorySize();
		virtual _IndexPrecision getMemorySizeBytes();
		virtual _IndexPrecision	getMemoryUsage();
		bool			setNumTilings(int numTilings);


};
