/*
 * agentdyna.h
 *
 * CAgentDyna can be created on top of a CAgentQ class to provide additional Dyna functionality.
 *
 *  Created on: Sep 3, 2010
 *      Author: Erik Schuitema
 */

#ifndef AGENTDYNA_H_
#define AGENTDYNA_H_

#include <agentq.h>
#include <Log2.h>

// Model of the agent's environment (technically, this includes the agent's own hardware plus its surroundings).
class CAgentEnvModel
{
	public:
		CAgentEnvModel()			{}
		virtual ~CAgentEnvModel()	{}

		// predictForward() and predictBackward() return a boolean whether the prediction was successful (i.e., confident)
		virtual void	resetMemory()=0;
		virtual bool	predictForward(const CAgentState& stateT0, const CAgentAction& action, CAgentState* stateT1, ComputationalType* reward)=0;
		virtual bool	predictBackward(const CAgentState& stateT1, const CAgentAction& action, CAgentState* stateT0, ComputationalType* reward)=0;
		virtual void	reportTransition(double* rawStateT0, double* rawAction, double* rawStateT1, double reward)=0;
};

class CAgentDyna
{
	protected:
		CAgentQ*				mAgentQ;
		CAgentEnvModel*			mModel;
		CAgentState				mAgentState;
		CAgentState				mResultState;	// Result state container for model predictions
		CAgentAction			mAgentAction;
		CAgentAction			mTempAction;	// Temporary action with correct descriptors (used in, e.g., DynaRandom)
		bool					mPredictReward;
		double					mResidualWeight;

	public:
		CAgentDyna():
			mAgentQ(NULL),
			mModel(NULL),
			mPredictReward(true)
		{}
		virtual ~CAgentDyna()	{}
		// Make sure that agentQ::mAgentState and agentQ::mAgentAction are initialized before calling init()
		virtual bool	init(CAgentQ* agentQ, CAgentEnvModel* model);
		virtual void	deinit()	{}
		// resetMemory() resets any internal memory, such as priority queues
		virtual void	resetMemory()		{mModel->resetMemory();}
		// With reportUpdate(), you can report that a state has been updated.
		// You can add a priority value to add states to, e.g., a priority queue.
		virtual void	reportUpdate(CAgentState& state, double priority=0.0)	{}
		// backup() performs a number of backups and returns the number of successful backups.
		int				backup(int numBackups, double learningRate)
		{
			int backups = 0;
			int errors = 0;
			for (int iBackup=0; iBackup<numBackups; iBackup++)
			{
			  int temp = singleBackup(learningRate);
			  if (temp < 0)
				errors++;
			  else if (temp > 0)
				backups++;
			}

			return errors?-errors:backups;
		}

		// singleBackup() returns 1 if the backup was actually performed, or 0 otherwise.
		// Backups with bad model prediction may be aborted, which results in returning 0.
		virtual int		singleBackup(double learningRate)=0;

		// If this is false, the Dyna agent will use mAgent's calculateReward function instead of
		// the model for predicting the reward.
		void			enableRewardPrediction(bool enabled)
		{
		  mPredictReward = enabled;
		}

		// Weight (phi in [Baird, 1995]) of the residual gradient update. 0 is standard TD, 1 is
		// standard residual gradient.
		void			setResidualWeight(double weight)
		{
		  mResidualWeight = weight;
		}
};

class CAgentDynaRandom: public CAgentDyna
{
	protected:
		CLog2			mLog;
		_StatePrecision	mStateBoundsMin[AGENT_MAX_NUM_STATEVARS];
		_StatePrecision	mStateBoundsMax[AGENT_MAX_NUM_STATEVARS];
		void			randomize(CAgentState& state, CAgentAction& action);
		bool			mStateAsInts;	// For gridworlds
	public:
		CAgentDynaRandom();
		// setStateBounds() accepts min (clustered in stateBoundsMin) and max (clustered in stateBoundsMax) values for all state variables.
		// Therefore, setStateBounds() assumes state space boundaries in the form of a hyper-cube (i.e., no fancy shapes)
		// By providing numStateVars here, setStateBounds() can be called before CAgentQ::mAgentState is initialized.
		// Please provide *scaled* bounds if you scale your state variables.
		void			stateAsInts(bool enabled);
		void			setStateBounds(CAgentState& stateBoundsMin, CAgentState& stateBoundsMax, int numStateVars);
		virtual int		singleBackup(double learningRate);
};

class CAgentDynaSequential: public CAgentDyna
{
	protected:
		CLog2			mLog;
		_StatePrecision	mStateBoundsMin[AGENT_MAX_NUM_STATEVARS];
		_StatePrecision	mStateBoundsMax[AGENT_MAX_NUM_STATEVARS];
		void			randomize(CAgentState& state, CAgentAction& action);
		bool			mStateAsInts;	// For gridworlds
	public:
		CAgentDynaSequential();
		// setStateBounds() accepts min (clustered in stateBoundsMin) and max (clustered in stateBoundsMax) values for all state variables.
		// Therefore, setStateBounds() assumes state space boundaries in the form of a hyper-cube (i.e., no fancy shapes)
		// By providing numStateVars here, setStateBounds() can be called before CAgentQ::mAgentState is initialized.
		// Please provide *scaled* bounds if you scale your state variables.
		void			stateAsInts(bool enabled);
		void			setStateBounds(CAgentState& stateBoundsMin, CAgentState& stateBoundsMax, int numStateVars);
		void			reset();
		virtual int		singleBackup(double learningRate);
};

#endif /* AGENTDYNA_H_ */
