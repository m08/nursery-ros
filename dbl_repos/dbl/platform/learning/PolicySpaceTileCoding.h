/*
 * PolicySpaceTileCoding.h
 *
 *  Created on: Oct 8, 2009
 *      Author: Erik Schuitema
 */

#ifndef POLICYSPACETILECODING_H_
#define POLICYSPACETILECODING_H_

#include <StaticVector.h>
#include "PolicySpace.h"
#include <tilecoding.h>
#include <half.h>

template<int numActionVars>
class CPolicySpaceTileCoding: public CPolicySpace
{
	protected:
		typedef common::math::StaticVector<_StatePrecision, numActionVars> ActionVector;

		// The function approximator
		CFATileCoding<ActionVector, _IndexPrecision>	*mFATileCoding;
		int												mNumTilings;

		// Store the results of the last call
		ActionVector	mStoredActionValue;
		_IndexPrecision	mStoredTiles[MAX_NUM_TILINGS];

		virtual bool	consultPolicy(CAgentState& state, CAgentAction& action)
		{
			// Request tiles for this state
			int				err = 0;
			if (state.wrapState())
				err = mFATileCoding->getValue(state.getState(), state.getNumStateVars(), state.getStateWrapData(), NULL, 0, mStoredTiles, &mStoredActionValue, TILE_WRITE);
			else
				err = mFATileCoding->getValue(state.getState(), state.getNumStateVars(), NULL, 0, mStoredTiles, &mStoredActionValue, TILE_WRITE);

			if (err != ERR_SUCCESS)
			{
				printf("KUT: %d\n",err);
				return false;
			}

			// Fill the action with the recommended action
			for (int i=0; i<numActionVars; i++)
				action[i] = mStoredActionValue(i);

			return true;
		}

		// Updates the policy value at the point in state space provided
		// to the last call of consultPolicy() or consultExplorativePolicy()
		virtual bool	updateConsultedPolicy(CAgentAction& action, double learnRate)
		{
			// Set new value
			ActionVector newValue(action.values());	// WARNING: this only works if the elements of ActionVector are of type _StatePrecision!!
			mFATileCoding->setValue(mStoredTiles, mStoredActionValue, newValue, learnRate);
			mLogDebugLn("[POLICY MEM] Adjusted action (" << action[0] - newValue(0) << "/" << action[1] - newValue(1) << ")" << ": " << newValue(0) - mStoredActionValue(0) << ", " << newValue(1) - mStoredActionValue(1));
			return true;
		}

		// Updates the policy for the provided state and action
		// It's slower to use consult*Policy() with this function,
		// than using consult*Policy() + updateConsultedPolicy().
		virtual bool	updatePolicy(CAgentState& state, CAgentAction& action, double learnRate)
		{
			int				err = 0;
			ActionVector	oldValue;
			_IndexPrecision	tiles[MAX_NUM_TILINGS];

			// Request tiles
			if (state.wrapState())
				err = mFATileCoding->getValue(state.getState(), state.getNumStateVars(), state.getStateWrapData(), NULL, 0, tiles, &oldValue, TILE_WRITE);
			else
				err = mFATileCoding->getValue(state.getState(), state.getNumStateVars(), NULL, 0, tiles, &oldValue, TILE_WRITE);

			if (err != ERR_SUCCESS)
				return false;

			// Set new value
			ActionVector newValue(action.values());	// WARNING: this only works if the elements of ActionVector are of type _StatePrecision!!

			mFATileCoding->setValue(tiles, oldValue, newValue, learnRate);
			//printf("Stored: %.2f, %.2f. Desired: %.2f, %.2f. ", mStoredActionValue(0), mStoredActionValue(1), newValue(0), newValue(1));
			return true;
		}

		// Exploration:
		virtual bool	consultExporationPolicy(CAgentState& state, CAgentAction& action)
		{
			// Request tiles for this state
			// The filled mStoredTiles and mStoredActionValue can be used in updateConsultedPolicy()
			int				err = 0;
			if (state.wrapState())
				err = mFATileCoding->getValue(state.getState(), state.getNumStateVars(), state.getStateWrapData(), NULL, 0, mStoredTiles, &mStoredActionValue, TILE_WRITE);
			else
				err = mFATileCoding->getValue(state.getState(), state.getNumStateVars(), NULL, 0, mStoredTiles, &mStoredActionValue, TILE_WRITE);

			if (err != ERR_SUCCESS)
				return false;

			return true;
		}


	public:
		CPolicySpaceTileCoding():
			mFATileCoding(NULL),
			mNumTilings(0)
		{
		}

		bool			setNumTilings(_IndexPrecision numTilings)
		{
			mNumTilings = numTilings;
			if (mFATileCoding != NULL)
			{
				delete mFATileCoding;
				mFATileCoding = NULL;
			}

			switch (numTilings)
			{
				case 2:
					mFATileCoding = new CFATileCodingExt<ActionVector, 2, uint32_t, uint16_t>;
					break;
				case 4:
					mFATileCoding = new CFATileCodingExt<ActionVector, 4, uint32_t, uint16_t>;
					break;
				case 8:
					mFATileCoding = new CFATileCodingExt<ActionVector, 8, uint32_t, uint16_t>;
					break;
				case 16:
					mFATileCoding = new CFATileCodingExt<ActionVector, 16, uint32_t, uint16_t>;
					break;
				case 32:
					mFATileCoding = new CFATileCodingExt<ActionVector, 32, uint32_t, uint16_t>;
					break;
				case 64:
					mFATileCoding = new CFATileCodingExt<ActionVector, 64, uint32_t, uint16_t>;
					break;

				default:
				{
					mLogErrorLn("Number of tilings, " << numTilings << ", was either < 2, too large or not a power of 2!");
					return false;
				}
			}

			return true;
		}

		bool	setMemorySize(_IndexPrecision memorySize)
		{
			return mFATileCoding->setMemorySize(memorySize);
		}

		void	resetMemory()
		{
			ActionVector nullVec(0.0);
			mFATileCoding->setValueInitPolicy(nullVec, nullVec);
			mFATileCoding->reset();
		}

		_IndexPrecision getMemorySize()
		{
			return mFATileCoding->getMemorySize();
		}

		_IndexPrecision	getMemoryUsage()
		{
			return mFATileCoding->getNumClaims();
		}
};


#endif /* POLICYSPACETILECODING_H_ */
