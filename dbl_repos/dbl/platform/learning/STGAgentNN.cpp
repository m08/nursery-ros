#include <cfloat>
#include "STGAgentNN.h"

#define MAX(x, y) (((x)>(y))?(x):(y))
#define MIN(x, y) (((x)<(y))?(x):(y))
#define BOUND(x, l, u) MIN(MAX(x, l), u)

std::ostream& operator<<(std::ostream &os, const ANN &obj)
{
  os << std::setiosflags(std::ios::fixed) << std::setprecision(2) << "[";
  for (unsigned int ii=0; ii != obj.mWeights.size(); ii++)
  {
    os << std::setw(5) << obj.mWeights[ii] << ", ";
  }
  os << std::setw(5) << obj.mActivationSteepness << "]";

  return os;
}

// **************************************************************************
// *** ANN ******************************************************************
// **************************************************************************

double ANN::RandomNormal() const
{
  // Box-Muller
  double u = gRanrotB.Random(), v = gRanrotB.Random();
  return sqrt(-2*log(u))*cos(2*M_PI*v);
}

double ANN::Activate(double x) const
{
  x *= 10;//mActivationSteepness*10;

  switch (mActivationFunction)
  {
    case afLinear:
      return x;
      break;
    case afSigmoid:
      return 1/(1+exp(-x));
      break;
    case afTanH:
      return tanh(x);
      break;
    default:
      return 0; // Never reached, because switch is exhaustive
  }
}

double ANN::Normalize(double x) const
{
  switch (mActivationFunction)
  {
    case afLinear:
      return tanh(x)/2+0.5;
      break;
    case afSigmoid:
      return x;
      break;
    case afTanH:
      return x/2+0.5;
      break;
    default:
      return 0.5; // Never reached, because switch is exhaustive
  }
}

double ANN::InvNormalize(double x) const
{
  switch (mActivationFunction)
  {
    case afLinear:
      return x*2-1;
      break;
    case afSigmoid:
      return x;
      break;
    case afTanH:
      return x*2-1;
      break;
    default:
      return 0; // Never reached, because switch is exhaustive
  }
}

void ANN::init()
{

  for (unsigned int ii=0; ii != mWeights.size(); ++ii)
    mWeights[ii] = gRanrotB.Random()*2-1;
  mActivationSteepness = gRanrotB.Random();

  /*
  mWeights[0] = 2;
  mWeights[1] = -1.5;
  mWeights[2] = 2;
  mWeights[3] = -0.5;
  mWeights[4] = -1;
  mWeights[5] = 0;
  mWeights[6] = 0.5;
  mWeights[7] = 0;
  mWeights[8] = 1;
  mWeights[9] = -0.5;
  */

/*
  mWeights[0] = 1/12.;
  mWeights[1] = -0.5;
  mWeights[2] = 1;
  mWeights[3] = .1;
*/
}

bool ANN::mutate(double prob)
{
  bool result = false;

  for (unsigned int ii=0; ii != mWeights.size(); ++ii)
    if (gRanrotB.Random() < prob)
    {
      result = true;
      if (gRanrotB.Random() < 0.9)
      {
        //mWeights[ii] += mWeights[ii]*RandomNormal();
        mWeights[ii] += RandomNormal()/5;
        mWeights[ii] = BOUND(mWeights[ii], -1, 1);
      }
      else
        mWeights[ii] = gRanrotB.Random()*2-1;
    }

  if (gRanrotB.Random() < prob)
  {
    result = true;
    if (gRanrotB.Random() < 0.9)
    {
//      mActivationSteepness += mActivationSteepness*RandomNormal();
      mActivationSteepness += RandomNormal()/5;
      mActivationSteepness = BOUND(mActivationSteepness, 0, 1);
    }
    else
      mActivationSteepness = gRanrotB.Random();
  }

  return result;
}

void ANN::crossover(const ANN &rhs, ANN &res) const
{
  res = *this;

  for (unsigned int ii=0; ii != mWeights.size(); ++ii)
    if (gRanrotB.Random() < 0.5)
      res.mWeights[ii] = rhs.mWeights[ii];

  if (gRanrotB.Random() < 0.5)
    res.mActivationSteepness = rhs.mActivationSteepness;
}

ANN &ANN::operator=(const ANN &rhs)
{
  ANN &rhsa = rhs.getGenotype();
  mWeights = rhsa.mWeights;
  mInputs = rhsa.mInputs;
  mHiddens = rhsa.mHiddens;
  mOutputs = rhsa.mOutputs;
  mActivationFunction = rhsa.mActivationFunction;
  mActivationSteepness = rhsa.mActivationSteepness;
  return *this;
}

void ANN::setSize(int inputs, int hiddens, int outputs, bool bias)
{
  mBias = bias;
  mInputs.resize(inputs);
  mHiddens.resize(hiddens);
  mOutputs.resize(outputs);
  mWeights.resize((inputs+mBias)*hiddens+(hiddens+bias)*outputs);
  init();
}

void ANN::setActivationFunction(EActivationFunction act)
{
  mActivationFunction = act;
}

EActivationFunction ANN::getActivationFunction()
{
  return mActivationFunction;
}

void ANN::setActivationSteepness(double steepness)
{
  mActivationSteepness = steepness;
}

double ANN::getActivationSteepness()
{
  return mActivationSteepness;
}

void ANN::setInputs(std::vector<double> &inputs)
{
  mInputs = inputs;
}

void ANN::setInput(int idx, double input)
{
  mInputs[idx] = input;
}

double ANN::getInput(int idx)
{
  return mInputs[idx];
}

std::vector<double> ANN::getOutputs()
{
  return mOutputs;
}

double ANN::getOutput(int idx)
{
  return mOutputs[idx];
}

void ANN::run()
{
#if DEBUG
  std::ostringstream ss;
  ss << "Activation: [";

  for (unsigned int ii=0; ii != mInputs.size(); ii++)
  {
    ss << mInputs[ii];
    if (ii < mInputs.size()-1) ss << ", "; else ss << "; ";
  }
#endif

  for (unsigned int hh=0; hh != mHiddens.size(); hh++)
  {
    double act = 0;
    if (mBias)
      act = mWeights[(hh+1)*(mInputs.size()+1)-1];
    for (unsigned int ii=0; ii != mInputs.size(); ii++)
      act += mInputs[ii]*mWeights[hh*(mInputs.size()+mBias)+ii];
    mHiddens[hh] = Activate(act);

#if DEBUG
    ss << mHiddens[hh];
    if (hh < mHiddens.size()-1) ss << ", "; else ss << "; ";
#endif
  }

  for (unsigned int oo=0; oo != mOutputs.size(); oo++)
  {
    double act = 0;
    if (mBias)
      act = mWeights[(mInputs.size()+1)*mHiddens.size()+(oo+1)*(mHiddens.size()+1)-1];
    for (unsigned int hh=0; hh != mHiddens.size(); hh++)
      act += mHiddens[hh]*mWeights[(mInputs.size()+mBias)*mHiddens.size()+oo*(mHiddens.size()+mBias)+hh];
    mOutputs[oo] = Normalize(Activate(act));

#if DEBUG
    ss << mOutputs[oo];
    if (oo < mOutputs.size()-1) ss << ", "; else ss << "]";
#endif
  }

#if DEBUG
  logCrawlLn(CLog2("ann"), ss.str());
#endif
}

void ANN::backPropagate(std::vector<double> &desired)
{

}

bool ANN::exportPhenotype(std::ofstream& fileOutStream)
{
  const float eps = 0.0000001f;
  int ii;

  if (!fileOutStream.good())
    return false;

  for (ii=0; ii != (int)mInputs.size(); ii++)
    mInputs[ii] = -1;
  
  do
  {
    for (unsigned int jj=0; jj != mInputs.size(); jj++)
      fileOutStream << mInputs[jj] <<  "\t";

    run();

    for (unsigned int jj=0; jj != mOutputs.size(); jj++)
    {
      fileOutStream << mOutputs[jj];
      if (jj != mOutputs.size()-1)
        fileOutStream << "\t";
      else
        fileOutStream << std::endl;
    }

    for (ii=mInputs.size()-1; ii >= 0; ii--)
    {
      mInputs[ii] += 0.1;
      if (mInputs[ii] > 1.0+eps)
      {
        mInputs[ii] = -1;
        fileOutStream << std::endl;
      }
      else
        break;
    }
  } while (ii >= 0);

  return true;
}

int ANN::getNumGenes()
{
  return mWeights.size();
}

void ANN::setGenes(const std::vector<double> &weights)
{
  if (weights.size() != mWeights.size())
    logErrorLn(CLog2("ann"), "Tried to set illegal number of ANN weights");
  else
    mWeights = weights;
}

std::vector<double> ANN::getGenes()
{
  return mWeights;
}

// **************************************************************************
// *** CSTGAgentNN **********************************************************
// **************************************************************************

bool CSTGAgentNN::readConfig(const CConfigSection &configNode)
{
  bool configresult = true;

  configNode.get("activationFunction", &mActivationFunction);
  configNode.get("biasNodes", &mBias, true);
  configNode.get("actionNodes", &mActionNodes, 0);

  configresult &= mLogAssert(configNode.get("hiddenNodes", &mHiddenNodes));

  return configresult && CGenericLearner::readConfig(configNode);
}

bool CSTGAgentNN::init()
{
  switch (mActionNodes)
  {
    case 0:
      // One output for each action variable
      mLogNoticeLn("Initializing direct continuous policy NN");
      mAnn.setSize(mStateVars.size(), mHiddenNodes, mActionVars.size(), mBias);
      break;
    case 1:
      // One extra input for each action variable.
      mLogNoticeLn("Initializing discrete evaluation policy NN");
      mAnn.setSize(mStateVars.size()+mActionVars.size(), mHiddenNodes, 1, mBias);
      break;
    default:
    {
      // One output for each combination of discrete actions.
      int sz=1;
      for (int ii=0; ii != (int)mActionVars.size(); ++ii)
        sz *= mActionVars[ii].getActions();

      mLogNoticeLn("Initializing discrete output policy NN with " << sz << " outputs");

      mAnn.setSize(mStateVars.size(), mHiddenNodes, sz, mBias);
    }
  }

  mAnn.init();
  mActions.resize(mActionVars.size());

  if (mActivationFunction == "linear")
    mAnn.setActivationFunction(afLinear);
  else if (mActivationFunction == "sigmoid")
    mAnn.setActivationFunction(afSigmoid);
  else if (mActivationFunction == "tanh")
    mAnn.setActivationFunction(afTanH);
  else
  {
    mLogErrorLn("Unknown activation function " << mActivationFunction);
    return false;
  }

  return CGenericLearner::init();
}

int CSTGAgentNN::executePolicy(GenericState* currentState, uint64_t absoluteTimeMicroseconds)
{
  updateState(currentState);

  if (mActionNodes == 0)
  {
    // One output for each action variable
    mAnn.run();
    for (int ii=0; ii != (int)mActions.size(); ++ii)
      mActions[ii] = mAnn.getOutput(ii);
  }
  else if (mActionNodes == 1)
  {
    // One extra input for each action variable.
    // Run NN for each combination of discrete actions, take action with highest
    // value of the single output node.
    std::vector<int> counter;
    counter.resize(mActionVars.size());

    double bestQ = -DBL_MAX;
    int ii;

    do
    {
      for (ii=0; ii != (int)counter.size(); ++ii)
        mAnn.setInput(mStateVars.size()+ii, mAnn.InvNormalize(counter[ii]/(mActionVars[ii].getActions()-1.0)));

      mAnn.run();

      if (mAnn.getOutput(0) > bestQ)
      {
        bestQ = mAnn.getOutput(0);
        for (ii=0; ii != (int)mActions.size(); ++ii)
          mActions[ii] = counter[ii]/(mActionVars[ii].getActions()-1.0);
      }

      for (ii=counter.size()-1; ii >= 0; --ii)
      {
        if (++counter[ii] == mActionVars[ii].getActions())
          counter[ii] = 0;
        else
          break;
      }
    } while (ii >= 0);
  }
  else
  {
    // One output for each combination of discrete actions.
    // Run NN once, take action whose output has the highest value.
    int sz=1;
    for (int ii=0; ii != (int)mActionVars.size(); ++ii)
      sz *= mActionVars[ii].getActions();

    mAnn.run();

    std::vector<int> counter;
    counter.resize(mActionVars.size());

    double bestQ = -DBL_MAX;

    for (int aa=0; aa != sz; ++aa)
    {
      if (mAnn.getOutput(aa) > bestQ)
      {
        bestQ = mAnn.getOutput(aa);
        for (int ii=0; ii != (int)mActions.size(); ++ii)
          mActions[ii] = counter[ii]/(mActionVars[ii].getActions()-1.0);
      }

      for (int ii=counter.size()-1; ii >= 0; --ii)
      {
        if (++counter[ii] == mActionVars[ii].getActions())
          counter[ii] = 0;
        else
          break;
      }
    }
  }

  updateAction(getActuationInterface());

  mReward += CGenericLearner::calculateReward();
  return 0;
}

void CSTGAgentNN::reset(uint64_t absoluteTimeMicroseconds)
{
  mReward = 0;
  CSTGPolicy<GenericState>::reset(absoluteTimeMicroseconds);
}

double CSTGAgentNN::calculateReward()
{
  return mReward;
}

void CSTGAgentNN::updateState(GenericState* currentSTGState)
{
  for (unsigned int ii=0; ii != mStateVars.size(); ++ii)
    mAnn.setInput(ii, mStateVars[ii].evaluate(currentSTGState));

  CGenericLearner::updateState(currentSTGState);
}

void CSTGAgentNN::updateAction(ISTGActuation* actuationInterface)
{
  for (unsigned int ii=0; ii != mActionVars.size(); ++ii)
    mActionVars[ii].actuate(mActions[ii], actuationInterface);

  CGenericLearner::updateAction(actuationInterface);
}

ANN& CSTGAgentNN::getController()
{
  return mAnn;
}

void CSTGAgentNN::setController(const ANN &ann)
{
  mAnn = ann;

  mLogCrawlLn("Controller set to " << mAnn);
}
