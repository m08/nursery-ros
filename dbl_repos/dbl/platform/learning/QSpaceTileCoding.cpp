/*
 * QSpaceTileCoding.cpp
 *
 *  Created on: Jul 29, 2009
 *      Author: Erik Schuitema
 */

#include "QSpaceTileCoding.h"
#include "stdio.h"
#include "stdlib.h"

// ********************* CQSpaceTileCoding ******************** //

CQSpaceTileCoding::~CQSpaceTileCoding()
{
	if (mFATileCoding != NULL)
		delete mFATileCoding;
	if (mVisits != NULL)
		delete[] mVisits;
}

void CQSpaceTileCoding::generalizeActions(bool enabled)
{
	mGeneralizeActions = enabled;

}

void CQSpaceTileCoding::trackVisits(bool enabled)
{
	mTrackVisits = enabled;
	initVisits();
}

void CQSpaceTileCoding::setInitialQValuePolicy(_QPrecision valMin, _QPrecision valMax)
{
	mFATileCoding->setValueInitPolicy(valMin, valMax);
}

_QPrecision CQSpaceTileCoding::probeQValue(CAgentState& state, CAgentAction& action)
{
	CQValueTileCoding tempVal;
	if (!getQValueTileCoding(state, action, tempVal, TILE_READ))
		return CONST_QVALUE_INVALID;

	return tempVal.QValue;
}

_QPrecision CQSpaceTileCoding::getQValue(CAgentState& state, CAgentAction& action)
{
	if (!getQValueTileCoding(state, action, mStoredQValue, TILE_WRITE))
		return CONST_QVALUE_INVALID;

	return mStoredQValue.QValue;
}

_QPrecision CQSpaceTileCoding::getStoredQValue()
{
	return mStoredQValue.QValue;
}

_QPrecision CQSpaceTileCoding::getStoredQValue(CAgentAction& action)
{
	action.restore(mStoredQValue.Action);
	return mStoredQValue.QValue;
}

_IndexPrecision* CQSpaceTileCoding::getStoredQValueTiles()
{
	return mStoredQValue.Tiles;
}

// Get the Q value of the best action
_QPrecision CQSpaceTileCoding::getBestActionQValue(const CAgentState& state, CAgentAction& action, CAgentActionValueList* avList)
{
	/// find the best action
	//mStoredBestActionQValue.QValue = -_QPrecision_MAX;

	int stepIndex[AGENT_MAX_NUM_ACTIONVARS];	// Fixed array to avoid allocation of memory in real-time applications
	memset(stepIndex, 0, action.getNumActionVars() * sizeof(stepIndex[0]));


	_StatePrecision scaledState[AGENT_MAX_NUM_STATEVARS];
	state.getScaledState(scaledState);
	action.setStartValues();

	int numSteps = action.getNumActions();	// Evaluate this only once

	// TODO: is this really necessary? User should do this in advance?
	if (avList != NULL)
		avList->resize(numSteps);

	CQValueTileCoding	tempSQV;
	_QPrecision			bestQValue = -_QPrecision_MAX;
	_StatePrecision		bestAction[AGENT_MAX_NUM_ACTIONVARS];


	// Iterate over all values in the action space
	for (int i = 0; i < numSteps; i++)
	{
		// Evaluate
		if (!mGeneralizeActions)
		{
			int intAction[AGENT_MAX_NUM_ACTIONVARS];
			action.getIntAction(intAction);
			if (!getQValueTileCodingScaledIntActions(scaledState, state.getStateWrapData(), state.getNumStateVars(), intAction, action.getNumActionVars(), tempSQV, TILE_READ))
				return CONST_QVALUE_INVALID;
		}
		else
		{
			_StatePrecision scaledAction[AGENT_MAX_NUM_ACTIONVARS];
			action.getScaledAction(scaledAction);
			if (!getQValueTileCodingScaled(scaledState, state.getStateWrapData(), state.getNumStateVars(), scaledAction, action.getNumActionVars(), tempSQV, TILE_READ))
				return CONST_QVALUE_INVALID;
		}

		if (tempSQV.QValue > bestQValue)
		{
			// Store best Q-value
			bestQValue = tempSQV.QValue;
			// Store best action
			action.store(bestAction);
			if (avList != NULL)
				avList->mBestAction = i;
		}
		if (avList != NULL)
		{
			action.store((*avList)[i].mAction);
			(*avList)[i].mValue = tempSQV.QValue;
			if (mTrackVisits)
				(*avList)[i].mVisits = getVisits(tempSQV);
		}

		// Increment value with index 0
		stepIndex[0]++;
		action[0] = action.mDescriptors[0].getValByIndex(stepIndex[0]);

		// Increment the ones who need incrementing
		for (int iActionVar = 0; iActionVar < action.getNumActionVars(); iActionVar++)
			if (stepIndex[iActionVar] >= action.mDescriptors[iActionVar].mNumSteps)
			{
				stepIndex[iActionVar] = 0;
				action[iActionVar] = action.mDescriptors[iActionVar].mStartVal;
				if (iActionVar < action.getNumActionVars()-1)
				{
					stepIndex[iActionVar+1]++;
					action[iActionVar+1] = action.mDescriptors[iActionVar+1].getValByIndex(stepIndex[iActionVar+1]);
				}
			}
	}
	// Check whether we don't have bugs
	assert(bestQValue != -_QPrecision_MAX);

	// Restore best action
	action.restore(bestAction);

	// Get the tiles for the best action and this time also claim the memory
	if (!getQValueTileCoding(state, action, mStoredBestActionQValue, TILE_WRITE))
		return CONST_QVALUE_INVALID;

	return mStoredBestActionQValue.QValue;
}

// Get the Q value of the best action
_QPrecision CQSpaceTileCoding::getBestRelativeActionQValue(const CAgentState& state, CAgentAction& action, const std::vector<int> &relative_bound_int, const std::vector<int> & under_relative_bound_int,bool useAbsoluteBounds, CAgentActionValueList* avList)
{
	bool assigned=false;
	/// find the best action
	//mStoredBestActionQValue.QValue = -_QPrecision_MAX;
	int vars = action.getNumActionVars();
	int previousActions[vars];
	int varNumSteps[vars];

	for (int i = 0; i<vars; i++)
	{
		if(useAbsoluteBounds)
		{

			previousActions[i]=action.mDescriptors[i].getIndexByVal(0.0);
			varNumSteps[i]=action.mDescriptors[i].mNumSteps;
		}
		else
		{
			previousActions[i] = action.mDescriptors[i].getIndexByVal(action.values()[i]);
			varNumSteps[i]=action.mDescriptors[i].mNumSteps;
		}

	}
	//std::cout<<"\n"<<"previousActions[2]: "<<previousActions[0]<<" "<< previousActions[1]<<"\n";


	int stepIndex[AGENT_MAX_NUM_ACTIONVARS];	// Fixed array to avoid allocation of memory in real-time applications
	memset(stepIndex, 0, action.getNumActionVars() * sizeof(stepIndex[0]));

	_StatePrecision scaledState[AGENT_MAX_NUM_STATEVARS];
	state.getScaledState(scaledState);
	action.setStartValues();

	int numSteps = action.getNumActions();	// Evaluate this only once
	//std::cout<<"action.getNumActions(): ";
	//std::cout<<action.getNumActions()<<"\n";

	// TODO: is this really necessary? User should do this in advance?
	if (avList != NULL)
		avList->resize(numSteps);

	CQValueTileCoding	tempSQV;
	_QPrecision			bestQValue = -_QPrecision_MAX;
	_StatePrecision		bestAction[AGENT_MAX_NUM_ACTIONVARS];
	// DEFINE ACTION BOUND

	//std::cout<<"----------getBestRelativeActionQValue \n";
	// Iterate over all values in the action space
	for (int i = 0; i < numSteps; i++)
	{
		//std::cout<<"----------In the loop \n";
		// Evaluate
		if (!mGeneralizeActions)
		{
			int intAction[AGENT_MAX_NUM_ACTIONVARS];
			action.getIntAction(intAction);
			if (!getQValueTileCodingScaledIntActions(scaledState, state.getStateWrapData(), state.getNumStateVars(), intAction, action.getNumActionVars(), tempSQV, TILE_READ))
				return CONST_QVALUE_INVALID;
		}
		else
		{
			_StatePrecision scaledAction[AGENT_MAX_NUM_ACTIONVARS];
			action.getScaledAction(scaledAction);
			if (!getQValueTileCodingScaled(scaledState, state.getStateWrapData(), state.getNumStateVars(), scaledAction, action.getNumActionVars(), tempSQV, TILE_READ))
				return CONST_QVALUE_INVALID;
		}
		int candidateActions[vars];
		for (int k = 0; k<vars; k++)
		{
			candidateActions[k] = action.mDescriptors[k].getIndexByVal(action.getActionVar(k));
		}
		//std::cout<<"\n"<<"candidateActions[i]: "<<candidateActions[0]<<" "<< candidateActions[1]<<"\n";
		//std::cout<<"\n relative_bound_int"<<relative_bound_int.at(0)<< relative_bound_int.at(1)<<endl;

		if (tempSQV.QValue > bestQValue)
		{
			bestUnboundedAction=tempSQV.QValue;
		}
		if (tempSQV.QValue > bestQValue && getWithinAllBounds(previousActions, varNumSteps, vars, relative_bound_int,  under_relative_bound_int, candidateActions ,useAbsoluteBounds))
		{
			assigned=true;
			// Store best Q-value
			bestQValue = tempSQV.QValue;
			// Store best action
			action.store(bestAction);
			if (avList != NULL)
				avList->mBestAction = i;
		}

		if (avList != NULL)
		{
			action.store((*avList)[i].mAction);
			(*avList)[i].mValue = tempSQV.QValue;
			if (mTrackVisits)
				(*avList)[i].mVisits = getVisits(tempSQV);
		}

		// Increment value with index 0
		stepIndex[0]++;
		action[0] = action.mDescriptors[0].getValByIndex(stepIndex[0]);

		// Increment the ones who need incrementing
		for (int iActionVar = 0; iActionVar < action.getNumActionVars(); iActionVar++)
			if (stepIndex[iActionVar] >= action.mDescriptors[iActionVar].mNumSteps)
			{
				stepIndex[iActionVar] = 0;
				action[iActionVar] = action.mDescriptors[iActionVar].mStartVal;
				if (iActionVar < action.getNumActionVars()-1)
				{
					stepIndex[iActionVar+1]++;
					action[iActionVar+1] = action.mDescriptors[iActionVar+1].getValByIndex(stepIndex[iActionVar+1]);
				}
			}

	}

	if(!assigned)
	{
		std::cout<<"\n Action was not assigned! \n";
		exit(0);
	}



	// Check whether we don't have bugs
	assert(bestQValue != -_QPrecision_MAX);

	// Restore best action
	action.restore(bestAction);

	//exit(0);
    //std::cout<<probeQValue(state, action);
    //std::cout<<"selected action is: " <<action.mDescriptors[0].getIndexByVal(action.values()[0])<<"\n";
    //std::cout<<"bestAction: "<<bestAction;
    //added by H Meijdam

	// Get the tiles for the best action and this time also claim the memory
	if (!getQValueTileCoding(state, action, mStoredBestActionQValue, TILE_WRITE))
		return CONST_QVALUE_INVALID;

	return mStoredBestActionQValue.QValue;

}

bool CQSpaceTileCoding::getWithinAllBounds(int  *previousActions, int *varNumSteps, int vars, const std::vector<int> &relative_bound, const std::vector<int> &under_relative_bound, int *i, bool useAbsoluteBounds)
{

	bool ans = true;
	for(int k=0; k<vars ; k++)
	{
		//std::cout<<"\n relative_bound.at(k): "<<relative_bound.at(k)<<"k:"<<k<<endl;
		//if(!getWithinBounds(varNumSteps[k] ,  previousActions[k], relative_bound.at(k), under_relative_bound, i[k]))
		if(!getWithinBounds(varNumSteps[k] ,  previousActions[k], relative_bound.at(k), under_relative_bound.at(k), i[k]))
		{
			ans=false;
			break;
		}
	}

	return ans;
}

bool CQSpaceTileCoding::getWithinBounds(int numSteps, int previousAction, int relative_bound, int under_relative_bound, int Action)
{
//	std::cout<<"----------getwithinBounds \n";
//	std::cout<<"numSteps: "<<numSteps<<"\n";
	bool ans;
	int lower_lower_bound=0;
	int upper_bound=numSteps-1;
	if(lower_lower_bound<previousAction-relative_bound)
	{
		lower_lower_bound=previousAction-relative_bound;

	}
	if(upper_bound>previousAction+relative_bound)
	{
		upper_bound=previousAction+relative_bound;
	}
	int lower_bound=0;
	int lower_upper_bound=numSteps-1;

	if(lower_bound<previousAction-under_relative_bound)
	{
		lower_bound=previousAction-under_relative_bound;
	}
	if(lower_upper_bound>previousAction+under_relative_bound)
	{
		lower_upper_bound=previousAction+under_relative_bound;
	}
	//std::cout<<"\n"<<"lower_lower_bound: "<<lower_lower_bound<<"\n";
	//std::cout<<"\n"<<"lower_bound: "<<lower_bound<<"\n";
	//std::cout<<"\n"<<"upper_bound: "<<upper_bound<<"\n";
	//std::cout<<"\n"<<"lower_upper_bound: "<<lower_upper_bound<<"\n";
	//std::cout<<"\n"<<"previousAction: "<<previousAction<<"\n";
	//std::cout<<"\n"<<"Action: "<<Action<<"\n";
	if((lower_lower_bound<=Action && lower_bound>=Action )|| (upper_bound>=Action && lower_upper_bound<=Action)||Action==previousAction)
	{
		ans = true;
	}
	else
	{
		ans = false;
	}
	//std::cout<<"\n"<<"Within bounds: "<<ans<<"\n";
	return ans;
}

_QPrecision CQSpaceTileCoding::getBestActionQValue_GivenAction(const CAgentState& state, const CAgentAction& action)
{
	if (!getQValueTileCoding(state, action, mStoredBestActionQValue, TILE_WRITE))
		return CONST_QVALUE_INVALID;

	return mStoredBestActionQValue.QValue;
}

// Get the Q value of the best action - approximate version.
// In a first pass, the action space is undersampled.
// In the second pass, it iterates over all nearest neighbors around the max. from the first pass.
_QPrecision CQSpaceTileCoding::getBestActionQValue_Approx(const CAgentState& state, CAgentAction& action, int undersamplingFactor)
{
	/// PASS 1: find the best action using undersampling
	CAgentAction tempAction(action);
	for (int iVar=0; iVar<action.getNumActionVars(); iVar++)
	{
		int newNumSteps = (action.mDescriptors[iVar].mNumSteps-1)/undersamplingFactor + 1;
		tempAction.mDescriptors[iVar].set(action.mDescriptors[iVar].mStartVal, action.mDescriptors[iVar].mStartVal+action.mDescriptors[iVar].mIntervalSize, newNumSteps, action.mDescriptors[iVar].mScale);
	}
	getBestActionQValue(state, tempAction);
	// PASS 2: check the neighbors of the found maximum
	for (int iVar=0; iVar<action.getNumActionVars(); iVar++)
	{
		int index		= action.mDescriptors[iVar].getIndexByVal(tempAction[iVar]);
		int startIndex	= std::max(index-(undersamplingFactor-1), 0);
		int endIndex	= std::min(index+(undersamplingFactor-1), action.mDescriptors[iVar].mNumSteps-1);
		tempAction.mDescriptors[iVar].set(action.mDescriptors[iVar].getValByIndex(startIndex), action.mDescriptors[iVar].getValByIndex(endIndex), endIndex-startIndex+1, action.mDescriptors[iVar].mScale);
	}
	return getBestActionQValue(state, tempAction);
}

_QPrecision CQSpaceTileCoding::getStoredBestActionQValue()
{
	return mStoredBestActionQValue.QValue;
}

_QPrecision CQSpaceTileCoding::getStoredBestActionQValue(CAgentAction& action)
{
	action.restore(mStoredBestActionQValue.Action);
	return mStoredBestActionQValue.QValue;
}

// Get the Q value of an explorative action
_QPrecision CQSpaceTileCoding::getExplorativeActionQValue(const CAgentState& state, const CAgentAction& action)
{
	if (!getQValueTileCoding(state, action, mStoredExplorativeActionQValue, TILE_WRITE))
		return CONST_QVALUE_INVALID;
	return mStoredExplorativeActionQValue.QValue;
}

_QPrecision CQSpaceTileCoding::getStoredExplorativeActionQValue()
{
	return mStoredExplorativeActionQValue.QValue;
}

_QPrecision CQSpaceTileCoding::getStoredExplorativeActionQValue(CAgentAction& action)
{
	action.restore(mStoredExplorativeActionQValue.Action);
	return mStoredExplorativeActionQValue.QValue;
}

bool CQSpaceTileCoding::getQValueTileCoding(const CAgentState& state, const CAgentAction& action, CQValueTileCoding& QTV, bool bReadOnly)
{
	bool result=false;
	_StatePrecision scaledState[AGENT_MAX_NUM_STATEVARS];
	state.getScaledState(scaledState);
	if (!mGeneralizeActions)
	{
		int intAction[AGENT_MAX_NUM_ACTIONVARS];
		action.getIntAction(intAction);
		result = getQValueTileCodingScaledIntActions(scaledState, state.getStateWrapData(), state.getNumStateVars(), intAction, action.getNumActionVars(), QTV, bReadOnly);
	}
	else
	{
		_StatePrecision scaledAction[AGENT_MAX_NUM_ACTIONVARS];
		action.getScaledAction(scaledAction);
		result = getQValueTileCodingScaled(scaledState, state.getStateWrapData(), state.getNumStateVars(), scaledAction, action.getNumActionVars(), QTV, bReadOnly);
	}
	action.store(QTV.Action);
	return result;
}

//TODO: add action.store(QTV.Action); yourself, because it was removed!
bool CQSpaceTileCoding::getQValueTileCodingScaled(const _StatePrecision *scaledState, const int* wrapData, const int numStateVars, const _StatePrecision *scaledAction, const int numActionVars, CQValueTileCoding& QTV, bool bReadOnly)
{
	int err = 0;
	// Concatenate state and action spaces
	_StatePrecision stateActionSpace[AGENT_MAX_NUM_STATEVARS + AGENT_MAX_NUM_ACTIONVARS];	//TODO: We could use numStateVars etc. here (and in a few other similar places), but MSVC probably does not support it
	memcpy(stateActionSpace, scaledState, sizeof(_StatePrecision)*numStateVars);
	memcpy(stateActionSpace+numStateVars, scaledAction, sizeof(_StatePrecision)*numActionVars);

	if (wrapData != NULL)
	{
		// Concatenate wrap space by adding zeros
		int	wrapSpace[AGENT_MAX_NUM_STATEVARS + AGENT_MAX_NUM_ACTIONVARS];
		memcpy(wrapSpace, wrapData, sizeof(int)*numStateVars);
		memset(wrapSpace+numStateVars, 0, sizeof(int)*numActionVars);
#ifdef EXTENDED_FEATURES
		err = mFATileCoding->getEFValue(stateActionSpace, numStateVars + numActionVars, wrapSpace, NULL, 0, QTV.Tiles, QTV.FeatureValues, &QTV.QValue, bReadOnly);
#else
		err = mFATileCoding->getValue(stateActionSpace, numStateVars + numActionVars, wrapSpace, NULL, 0, QTV.Tiles, &QTV.QValue, bReadOnly);
#endif
	}
	else
#ifdef EXTENDED_FEATURES
		err = mFATileCoding->getEFValue(stateActionSpace, numStateVars + numActionVars, NULL, 0, QTV.Tiles, QTV.FeatureValues, &QTV.QValue, bReadOnly);
#else
		err = mFATileCoding->getValue(stateActionSpace, numStateVars + numActionVars, NULL, 0, QTV.Tiles, &QTV.QValue, bReadOnly);
#endif

	if (err != ERR_SUCCESS)
	{
		// TODO: double memory
		mLogWarningLn("Memory should be doubled!");
		return false;
	}
	return true;
}

bool CQSpaceTileCoding::getQValueTileCodingScaledIntActions(const _StatePrecision *scaledState, const int* wrapData, const int numStateVars, const int *action, const int numActionVars, CQValueTileCoding& QTV, bool bReadOnly)
{
	int err = 0;
	if (wrapData != NULL)
#ifdef EXTENDED_FEATURES

		err = mFATileCoding->getEFValue(scaledState, numStateVars, wrapData, action, numActionVars, QTV.Tiles, QTV.FeatureValues, &QTV.QValue, bReadOnly);
#else
{

		err = mFATileCoding->getValue(scaledState, numStateVars, wrapData, action, numActionVars, QTV.Tiles, &QTV.QValue, bReadOnly);
}

#endif
	else
#ifdef EXTENDED_FEATURES

		err = mFATileCoding->getEFValue(scaledState, numStateVars, action, numActionVars, QTV.Tiles, QTV.FeatureValues, &QTV.QValue, bReadOnly);
#else

		err = mFATileCoding->getValue(scaledState, numStateVars, action, numActionVars, QTV.Tiles, &QTV.QValue, bReadOnly);
#endif

	if (err != ERR_SUCCESS)
	{
		// TODO: double memory
		mLogWarningLn("Memory should be doubled!");
		return false;
	}
	return true;
}

// Set a specific Q value
bool CQSpaceTileCoding::setQValue(CAgentState& state, CAgentAction& action, _QPrecision value)
{
	CQValueTileCoding QTV;
	//QTV.Reset(mFATileCoding->getNumTilings(), mAgentAction->NumValues());	// No need to reset?
	if (!getQValueTileCoding(state, action, QTV, TILE_WRITE))
		return false;
#ifdef EXTENDED_FEATURES
	mFATileCoding->setEFValue(QTV.Tiles, QTV.FeatureValues, QTV.QValue, value);
#else
	mFATileCoding->setValue(QTV.Tiles, QTV.QValue, value);
#endif
	return true;
}

bool CQSpaceTileCoding::setStoredQValue(_QPrecision value)
{
#ifdef EXTENDED_FEATURES
	mFATileCoding->setEFValue(mStoredQValue.Tiles, mStoredQValue.FeatureValues, mStoredQValue.QValue, value);
#else
	mFATileCoding->setValue(mStoredQValue.Tiles, mStoredQValue.QValue, value);
#endif
	return true;
}

bool CQSpaceTileCoding::setStoredBestActionQValue(_QPrecision value)
{
#ifdef EXTENDED_FEATURES
	mFATileCoding->setEFValue(mStoredBestActionQValue.Tiles, mStoredBestActionQValue.FeatureValues, mStoredBestActionQValue.QValue, value);
#else
	mFATileCoding->setValue(mStoredBestActionQValue.Tiles, mStoredBestActionQValue.QValue, value);
#endif
	return true;
}

bool CQSpaceTileCoding::setStoredExplorativeActionQValue(_QPrecision value)
{
#ifdef EXTENDED_FEATURES
	mFATileCoding->setEFValue(mStoredExplorativeActionQValue.Tiles, mStoredExplorativeActionQValue.FeatureValues, mStoredExplorativeActionQValue.QValue, value);
#else
	mFATileCoding->setValue(mStoredExplorativeActionQValue.Tiles, mStoredExplorativeActionQValue.QValue, value);
#endif
	return true;
}

// ***************************************
// ** Trace functions
// ***************************************

void CQSpaceTileCoding::trackVisit(CQValueTileCoding &QTV)
{
	if (mVisits != NULL && mTrackVisits)
		for (int ii=0; ii != mNumTilings; ++ii)
			++mVisits[QTV.Tiles[ii]];
}

_QPrecision CQSpaceTileCoding::getVisits(CQValueTileCoding &QTV)
{
	int visits=0;

	if (mVisits != NULL && mTrackVisits)
		for (int ii=0; ii != mNumTilings; ++ii)
			visits += mVisits[QTV.Tiles[ii]];
	
	return visits/(double)mNumTilings;
}

void CQSpaceTileCoding::resetVisits(void)
{
	if (mVisits != NULL)
		memset(mVisits, 0, getMemorySize()*sizeof(int));
}

void CQSpaceTileCoding::initVisits(void)
{
	if (mVisits != NULL)
	{
		delete[] mVisits;
		mVisits = NULL;
	}
		
	if (mTrackVisits)
	{
		if (getMemorySize() > 0)
		{
			mVisits = new int[getMemorySize()];
			resetVisits();
		}	
	}
}
		
// Trace options
bool CQSpaceTileCoding::traceInit(_QPrecision DiscountFactor, _QPrecision minVal, bool adjustTraceLength)
{
	mTrace.init(DiscountFactor, minVal, mNumTilings, adjustTraceLength);
	return true;
}

_IndexPrecision CQSpaceTileCoding::traceGetLength()
{
	return mTrace.numElements();
}

_QPrecision CQSpaceTileCoding::traceGetDiscountFactor()
{
	return mTrace.getFalloffFactor();
}

// Add a State-Action Pair to the trace (and discount all previous states)
bool CQSpaceTileCoding::traceAdd(CAgentState& state, CAgentAction& action)
{
	CQValueTileCoding QTV;
	// QTV.Reset()// No need to reset?
	if (!getQValueTileCoding(state, action, QTV, TILE_WRITE))
		return false;

#ifdef EXTENDED_FEATURES
	mTrace.addBatchEF(QTV.Tiles, QTV.FeatureValues);
#else
	mTrace.addBatch(QTV.Tiles);
#endif

	trackVisit(QTV);
	return true;
}

bool CQSpaceTileCoding::traceAddStored()
{
#ifdef EXTENDED_FEATURES
	mTrace.addBatchEF(mStoredQValue.Tiles, mStoredQValue.FeatureValues);
#else
	mTrace.addBatch(mStoredQValue.Tiles);
#endif
	trackVisit(mStoredQValue);
	return true;
}

bool CQSpaceTileCoding::traceAddStoredBest()
{
#ifdef EXTENDED_FEATURES
	mTrace.addBatchEF(mStoredBestActionQValue.Tiles, mStoredBestActionQValue.FeatureValues);
#else
	mTrace.addBatch(mStoredBestActionQValue.Tiles);
#endif
	trackVisit(mStoredBestActionQValue);
	return true;
}

bool CQSpaceTileCoding::traceAddStoredExplorative()
{
#ifdef EXTENDED_FEATURES
	mTrace.addBatchEF(mStoredExplorativeActionQValue.Tiles, mStoredExplorativeActionQValue.FeatureValues);
#else
	mTrace.addBatch(mStoredExplorativeActionQValue.Tiles);
#endif
	trackVisit(mStoredExplorativeActionQValue);
	return true;
}

// Clear the trace
bool CQSpaceTileCoding::traceClear()
{
	mTrace.clear();
	return true;
}

bool CQSpaceTileCoding::traceFall()
{
	mTrace.fall();
	return true;
}

// Do a Q space trace update
bool CQSpaceTileCoding::traceUpdateQValues(_QPrecision Delta)
{
	for (_IndexPrecision iTElement = 0; iTElement < mTrace.numElements(); iTElement++)
#ifdef EXTENDED_FEATURES
		mFATileCoding->adjustParameterValue(mTrace[iTElement].index, Delta*mTrace[iTElement].value);
#else
		mFATileCoding->adjustParameterValue(mTrace[iTElement].index, Delta*mTrace[iTElement].value);
#endif
	return true;
}

bool CQSpaceTileCoding::setMemorySize(_IndexPrecision MemorySize)
{
	bool result = mFATileCoding->setMemorySize(MemorySize);
	initVisits();
	
	return result;
}

void CQSpaceTileCoding::resetMemory()
{
	mFATileCoding->reset();
	resetVisits();
}

_IndexPrecision CQSpaceTileCoding::getMemorySize()
{
	return mFATileCoding->getMemorySize();
}

_IndexPrecision CQSpaceTileCoding::getMemoryUsage()
{
	return mFATileCoding->getNumClaims();
}

_IndexPrecision CQSpaceTileCoding::getMemorySizeBytes()
{
	return mFATileCoding->getMemorySize()*mFATileCoding->getNumBytesPerClaim();
}

bool CQSpaceTileCoding::setNumTilings(int numTilings)
{
	if (numTilings > MAX_NUM_TILINGS)
	{
		mLogErrorLn("Number of tilings, " << numTilings << ", larger than MAX_NUM_TILINGS (" << MAX_NUM_TILINGS <<  ") --> increase MAX_NUM_TILINGS!");
		return false;
	}

	if (mNumTilings == numTilings)
		return true;

	mNumTilings = numTilings;
	if (mFATileCoding != NULL)
	{
		delete mFATileCoding;
		mFATileCoding = NULL;
	}
	typedef uint32_t indextype;
	typedef uint16_t hashchecktype;

	mLogNoticeLn("Creating tile coding qspace with " << sizeof(_QPrecision)*8 << "b Q-values, " << sizeof(indextype)*8 << "b feature indices and " << sizeof(hashchecktype)*8 << "b hash checks");

	// Due to templating in tilecoding.h, we need this ugly switch statement.
	// The reward is faster code.
	switch (numTilings)
	{
		case 1:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 1, indextype, hashchecktype>;
			break;
		case 2:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 2, indextype, hashchecktype>;
			break;
		case 4:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 4, indextype, hashchecktype>;
			break;
		case 8:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 8, indextype, hashchecktype>;
			break;
		case 16:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 16, indextype, hashchecktype>;
			break;
		case 32:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 32, indextype, hashchecktype>;
			break;
		case 64:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 64, indextype, hashchecktype>;
			break;
		case 128:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 128, indextype, hashchecktype>;
			break;
		case 256:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 256, indextype, hashchecktype>;
			break;
		case 512:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 512, indextype, hashchecktype>;
			break;
		case 1024:
			mFATileCoding = new CFATileCodingExt<_QPrecision, 1024, indextype, hashchecktype>;
			break;

		default:
		{
			mLogErrorLn("Number of tilings, " << numTilings << ", was either < 2, too large or not a power of 2!");
			return false;
		}
	}
	
	initVisits();

	return true;
}

bool CQSpaceTileCoding::load(std::ifstream& fileInStream)
{
	if (!fileInStream.good())
		return false;

	int numTilings;
	mNumTilings = 0;
	fileInStream.read((char*)&mGeneralizeActions,		sizeof(mGeneralizeActions));
	fileInStream.read((char*)&numTilings,		sizeof(numTilings));

	setNumTilings(numTilings);	// This sets mNumTilings and creates mFATileCoding

	mTrace.load(fileInStream);
	
	return fileInStream.good() && mFATileCoding->load(fileInStream);
}

// Save QSpace to a stream
bool CQSpaceTileCoding::save(std::ofstream& fileOutStream)
{
	if (fileOutStream.bad())
		return false;

	if (mFATileCoding == NULL)
		return false;

	fileOutStream.write((char*)&mGeneralizeActions,	sizeof(mGeneralizeActions));
	fileOutStream.write((char*)&mNumTilings,	sizeof(mNumTilings));
	mTrace.save(fileOutStream);
	mFATileCoding->save(fileOutStream);

	return true;
}
