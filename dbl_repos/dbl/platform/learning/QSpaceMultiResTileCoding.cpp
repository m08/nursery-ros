#include "QSpaceMultiResTileCoding.h"

CQSpaceMultiResTileCoding::CQSpaceMultiResTileCoding():
	CQSpaceTileCoding(),
	mNumResolutions(1)	// We start with the default resolution
{
	mResolutions[0] = 1.0;	// The default resolution. Filling this means a tile coder is created upon calling setNumTilings().
	memset(mTileCoders, 0, sizeof(mTileCoders[0])*CONST_MaxNumTilingResolutions);
}

CQSpaceMultiResTileCoding::~CQSpaceMultiResTileCoding()
{
	for (int iRes=0; iRes<mNumResolutions; iRes++)
		delete mTileCoders[iRes];	// No need to check for NULL since mNumResolutions controls this
}

void CQSpaceMultiResTileCoding::addResolution(double scaleFact)
{
	mResolutions[mNumResolutions] = scaleFact;
	mNumResolutions++;
}

// ********************* inherited from CQSpaceTileCoding ******************** //

void CQSpaceMultiResTileCoding::setInitialQValuePolicy(_QPrecision valMin, _QPrecision valMax)
{
	for (int i=0; i<mNumResolutions; i++)
		mTileCoders[i]->setValueInitPolicy(valMin, valMax);
}

//TODO: add action.store(QTV.Action); yourself, because it was removed!
bool CQSpaceMultiResTileCoding::getQValueTileCodingScaled(const _StatePrecision *scaledState, const int* wrapData, const int numStateVars, const _StatePrecision *scaledAction, const int numActionVars, CQValueTileCoding& QTV, bool bReadOnly)
{
	int err = 0;
	// Concatenate state and action spaces
	_StatePrecision stateActionSpace[AGENT_MAX_NUM_STATEVARS + AGENT_MAX_NUM_ACTIONVARS];	//TODO: We could use numStateVars etc. here (and in a few other similar places), but MSVC probably does not support it
	memcpy(stateActionSpace+numStateVars, scaledAction, sizeof(_StatePrecision)*numActionVars);
	_QPrecision partialQValue=0;
	QTV.QValue = 0;
	for (int iRes=0; iRes < mNumResolutions; iRes++)
	{
		// Copy and scale state space
		for (int iVar=0; iVar<numStateVars; iVar++)
			stateActionSpace[iVar] = mResolutions[iRes]*scaledState[iVar];

		if (wrapData != NULL)
		{
			// Concatenate wrap space by adding zeros
			int	wrapSpace[AGENT_MAX_NUM_STATEVARS + AGENT_MAX_NUM_ACTIONVARS];
			// Copy and scale wrap space
			for (int iVar=0; iVar<numStateVars; iVar++)
				wrapSpace[iVar] = (int)((double)wrapData[iVar]*mResolutions[iRes]);
			memset(wrapSpace+numStateVars, 0, sizeof(int)*numActionVars);
	#ifdef EXTENDED_FEATURES
			todo
	#else
			err = mTileCoders[iRes]->getValue(stateActionSpace, numStateVars + numActionVars, wrapSpace, NULL, 0, QTV.Tiles + iRes*mNumTilings, &partialQValue, bReadOnly);
			QTV.QValue += partialQValue;
	#endif
		}
		else
		{
	#ifdef EXTENDED_FEATURES
			todo
	#else
			err = mTileCoders[iRes]->getValue(stateActionSpace, numStateVars + numActionVars, NULL, 0, QTV.Tiles + iRes*mNumTilings, &partialQValue, bReadOnly);
			QTV.QValue += partialQValue;
	#endif
		}

		if (err != ERR_SUCCESS)
		{
			// TODO: double memory
			mLogWarningLn("Memory should be doubled!");
			return false;
		}
	}
	QTV.QValue /= mNumResolutions;
	return true;
}

bool CQSpaceMultiResTileCoding::getQValueTileCodingScaledIntActions(const _StatePrecision *scaledState, const int* wrapData, const int numStateVars, const int *action, const int numActionVars, CQValueTileCoding& QTV, bool bReadOnly)
{
	int err = 0;
	_StatePrecision stateSpace[AGENT_MAX_NUM_STATEVARS];	//TODO: We could use numStateVars etc. here (and in a few other similar places), but MSVC probably does not support it
	QTV.QValue = 0;
	_QPrecision partialQValue=0;
	for (int iRes=0; iRes < mNumResolutions; iRes++)
	{
		// Copy and scale state space
		for (int iVar=0; iVar<numStateVars; iVar++)
			stateSpace[iVar] = mResolutions[iRes]*scaledState[iVar];

		if (wrapData != NULL)
		{
	#ifdef EXTENDED_FEATURES
			todo
	#else
			// Copy and scale wrap space
			int	wrapSpace[AGENT_MAX_NUM_STATEVARS];
			for (int iVar=0; iVar<numStateVars; iVar++)
				wrapSpace[iVar] = (int)((double)wrapData[iVar]*mResolutions[iRes]);
			err = mTileCoders[iRes]->getValue(stateSpace, numStateVars, wrapSpace, action, numActionVars, QTV.Tiles + iRes*mNumTilings, &partialQValue, bReadOnly);
			QTV.QValue += partialQValue;
	#endif
		}
		else
		{
	#ifdef EXTENDED_FEATURES
			todo
	#else
			err = mTileCoders[iRes]->getValue(stateSpace, numStateVars, action, numActionVars, QTV.Tiles + iRes*mNumTilings, &partialQValue, bReadOnly);
			QTV.QValue += partialQValue;
	#endif
		}

		if (err != ERR_SUCCESS)
		{
			// TODO: double memory
			mLogWarningLn("Memory should be doubled!");
			return false;
		}
	}
	QTV.QValue /= mNumResolutions;
	return true;
}

// Set a specific Q value
bool CQSpaceMultiResTileCoding::setQValue(CAgentState& state, CAgentAction& action, _QPrecision value)
{
	CQValueTileCoding QTV;
	//QTV.Reset(mFATileCoding->getNumTilings(), mAgentAction->NumValues());	// No need to reset?
	if (!getQValueTileCoding(state, action, QTV, TILE_WRITE))
		return false;
#ifdef EXTENDED_FEATURES
	todo
#else
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTileCoders[iRes]->setValue(QTV.Tiles + iRes*mNumTilings, QTV.QValue, value);
#endif
	return true;
}

bool CQSpaceMultiResTileCoding::setStoredQValue(_QPrecision value)
{
#ifdef EXTENDED_FEATURES
	todo
#else
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTileCoders[iRes]->setValue(mStoredQValue.Tiles + iRes*mNumTilings, mStoredQValue.QValue, value);
#endif
	return true;
}

bool CQSpaceMultiResTileCoding::setStoredBestActionQValue(_QPrecision value)
{
#ifdef EXTENDED_FEATURES
	todo
#else
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTileCoders[iRes]->setValue(mStoredBestActionQValue.Tiles + iRes*mNumTilings, mStoredBestActionQValue.QValue, value);
#endif
	return true;
}

bool CQSpaceMultiResTileCoding::setStoredExplorativeActionQValue(_QPrecision value)
{
#ifdef EXTENDED_FEATURES
	todo
#else
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTileCoders[iRes]->setValue(mStoredExplorativeActionQValue.Tiles + iRes*mNumTilings, mStoredExplorativeActionQValue.QValue, value);
#endif
	return true;
}

// ***************************************
// ** Trace functions
// ***************************************

// Visit tracking is not properly implemented!

// Trace options
bool CQSpaceMultiResTileCoding::traceInit(_QPrecision DiscountFactor, _QPrecision minVal, bool adjustTraceLength)
{
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTraces[iRes].init(DiscountFactor, minVal, mNumTilings, adjustTraceLength);
	return true;
}

_IndexPrecision CQSpaceMultiResTileCoding::traceGetLength()
{
	return mTraces[0].numElements();	// Return value of main trace (all traces have equal length)
}

_QPrecision CQSpaceMultiResTileCoding::traceGetDiscountFactor()
{
	return mTraces[0].getFalloffFactor();	// Return value of main trace (all traces have equal length)
}

// Add a State-Action Pair to the trace (and discount all previous states)
bool CQSpaceMultiResTileCoding::traceAdd(CAgentState& state, CAgentAction& action)
{
	CQValueTileCoding QTV;
	// QTV.Reset()// No need to reset?
	if (!getQValueTileCoding(state, action, QTV, TILE_WRITE))
		return false;

#ifdef EXTENDED_FEATURES
	todo
#else
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTraces[iRes].addBatch(QTV.Tiles + iRes*mNumTilings);
#endif

	trackVisit(QTV);
	return true;
}

bool CQSpaceMultiResTileCoding::traceAddStored()
{
#ifdef EXTENDED_FEATURES
	todo
#else
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTraces[iRes].addBatch(mStoredQValue.Tiles + iRes*mNumTilings);
#endif
	trackVisit(mStoredQValue);
	return true;
}

bool CQSpaceMultiResTileCoding::traceAddStoredBest()
{
#ifdef EXTENDED_FEATURES
	todo
#else
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTraces[iRes].addBatch(mStoredBestActionQValue.Tiles + iRes*mNumTilings);
#endif
	trackVisit(mStoredBestActionQValue);
	return true;
}

bool CQSpaceMultiResTileCoding::traceAddStoredExplorative()
{
#ifdef EXTENDED_FEATURES
	todo
#else
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTraces[iRes].addBatch(mStoredExplorativeActionQValue.Tiles + iRes*mNumTilings);
#endif
	trackVisit(mStoredExplorativeActionQValue);
	return true;
}

// Clear the trace
bool CQSpaceMultiResTileCoding::traceClear()
{
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTraces[iRes].clear();
	return true;
}

bool CQSpaceMultiResTileCoding::traceFall()
{
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTraces[iRes].fall();
	return true;
}

// Do a Q space trace update
bool CQSpaceMultiResTileCoding::traceUpdateQValues(_QPrecision Delta)
{
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		for (_IndexPrecision iTElement = 0; iTElement < mTraces[iRes].numElements(); iTElement++)
#ifdef EXTENDED_FEATURES
		todo
#else
			mTileCoders[iRes]->adjustParameterValue(mTraces[iRes][iTElement].index, Delta*mTraces[iRes][iTElement].value);
#endif
	return true;
}

bool CQSpaceMultiResTileCoding::setMemorySize(_IndexPrecision MemorySize)
{
	bool result = true;
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		result &= mTileCoders[iRes]->setMemorySize(MemorySize);	// TODO: scale required memory with resolution to the power numStateDimensions?
	initVisits();

	return result;
}

void CQSpaceMultiResTileCoding::resetMemory()
{
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		mTileCoders[iRes]->reset();
	resetVisits();
}

_IndexPrecision CQSpaceMultiResTileCoding::getMemorySize()
{
	_IndexPrecision result = 0;
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		result +=  mTileCoders[iRes]->getMemorySize();
	return result;
}

_IndexPrecision CQSpaceMultiResTileCoding::getMemoryUsage()
{
	_IndexPrecision result = 0;
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		result +=  mTileCoders[iRes]->getNumClaims();
	return result;
}

_IndexPrecision CQSpaceMultiResTileCoding::getMemorySizeBytes()
{
	_IndexPrecision result = 0;
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		result += mTileCoders[iRes]->getMemorySize()*mTileCoders[iRes]->getNumBytesPerClaim();
	return result;
}

bool CQSpaceMultiResTileCoding::setNumTilings(int numTilings)
{
	if (numTilings*mNumResolutions > MAX_NUM_TILINGS)
	{
		mLogErrorLn("Number of tilings needed for multires, " << mNumResolutions*numTilings << ", larger than MAX_NUM_TILINGS (" << MAX_NUM_TILINGS <<  ") --> increase MAX_NUM_TILINGS!");
		return false;
	}

	if (mNumTilings == numTilings)
		return true;

	mNumTilings = numTilings;
	for (int iRes=0; iRes < mNumResolutions; iRes++)
		if (mTileCoders[iRes] != NULL)
		{
			delete mTileCoders[iRes];
			mTileCoders[iRes] = NULL;
		}
	typedef uint32_t indextype;
	typedef uint16_t hashchecktype;

	mLogNoticeLn("Creating tile coding qspace with " << sizeof(_QPrecision)*8 << "b Q-values, " << sizeof(indextype)*8 << "b feature indices and " << sizeof(hashchecktype)*8 << "b hash checks");

	// Due to templating in tilecoding.h, we need this ugly switch statement.
	// The reward is faster code.
	for (int iRes=0; iRes < mNumResolutions; iRes++)
	{
		switch (numTilings)
		{
			case 1:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 1, indextype, hashchecktype>;
				break;
			case 2:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 2, indextype, hashchecktype>;
				break;
			case 4:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 4, indextype, hashchecktype>;
				break;
			case 8:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 8, indextype, hashchecktype>;
				break;
			case 16:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 16, indextype, hashchecktype>;
				break;
			case 32:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 32, indextype, hashchecktype>;
				break;
			case 64:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 64, indextype, hashchecktype>;
				break;
			case 128:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 128, indextype, hashchecktype>;
				break;
			case 256:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 256, indextype, hashchecktype>;
				break;
			case 512:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 512, indextype, hashchecktype>;
				break;
			case 1024:
				mTileCoders[iRes] = new CFATileCodingExt<_QPrecision, 1024, indextype, hashchecktype>;
				break;

			default:
			{
				mLogErrorLn("Number of tilings, " << numTilings << ", was either < 2, too large or not a power of 2!");
				return false;
			}
		}
	}

	initVisits();

	return true;
}

bool CQSpaceMultiResTileCoding::load(std::ifstream& fileInStream)
{
	mLogErrorLn("TODO: CQSpaceMultiResTileCoding::load()");
	return true;
}

// Save QSpace to a stream
bool CQSpaceMultiResTileCoding::save(std::ofstream& fileOutStream)
{
	mLogErrorLn("TODO: CQSpaceMultiResTileCoding::save()");
	return true;
}
