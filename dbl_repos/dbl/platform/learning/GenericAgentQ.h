/*
 * GenericAgentQ.h
 * Version of CSTGAgentQ that uses the GenericState interface
 *
 * Wouter Caarls <w.caarls@tudelft.nl>
 */

#ifndef __GENERICAGENTQ_H_INCLUDED
#define __GENERICAGENTQ_H_INCLUDED

#include <GenericLearner.h>
#include <STGAgentQ.h>

class CGenericAgentQ : public CSTGAgentQ<GenericState>, public CGenericLearner
{
  private:
    bool mIsObserver;
    GenericState mObservedSTGState;
    uint64_t mObservedTime;

  public:
    CGenericAgentQ(ISTGGenericSensing *sensingInterface, ISTGGenericActuation *actuationInterface, CQSpace* qspace=NULL, CPolicySpace* policySpace=NULL, int stateStartIndex=0, int actionStartIndex=0) :
      CSTGAgentQ<GenericState>(actuationInterface, qspace, policySpace),
      CGenericLearner(sensingInterface, actuationInterface, stateStartIndex, actionStartIndex),
      mIsObserver(false), mObservedTime(UINT64_MAX)
    { }

    virtual bool readConfig(const CConfigSection &configNode);
    virtual bool init();
    virtual void updateState(GenericState* currentSTGState);
    virtual void updateAction(ISTGActuation* actuationInterface);
    virtual int executePolicy(GenericState* currentState, uint64_t absoluteTimeMicroseconds);
    virtual double calculateReward();
    virtual char isTerminalState(uint64_t trialTimeMicroseconds);

    void observe() { setAlgo(laQObserve); mIsObserver = true; }
    bool isObserver() { return mIsObserver; }
};

#endif /* __GENERICAGENTQ_H_INCLUDED */
