/*
 * GenericAgentQ.cpp
 * Version of CSTGAgentQ that uses the GenericState interface
 *
 * Wouter Caarls <w.caarls@tudelft.nl>
 */

#include <GenericAgentQ.h>

bool CGenericAgentQ::readConfig(const CConfigSection &configNode)
{
  bool result = CSTGAgentQ<GenericState>::readConfig(configNode);
  result &= CGenericLearner::readConfig(configNode);

  return result;
}

bool CGenericAgentQ::init()
{
  bool result = true;

  mAgentAction.setNumActionVars(mActionStartIndex + mActionVars.size());
  mAgentState.setNumStateVars(mStateStartIndex + mStateVars.size());
  mAgentState.enableWrapping(true);

  for (unsigned int ii=0; ii != mActionVars.size(); ++ii)
    mAgentAction.mDescriptors[ii].set((_StatePrecision)0.0, (_StatePrecision)1.0, mActionVars[ii].getActions(), (_StatePrecision)mActionVars[ii].getScaling());

  result &= CGenericLearner::init();
  result &= CSTGAgentQ<GenericState>::init();

  return result;
}

void CGenericAgentQ::updateState(GenericState* currentSTGState)
{
  for (unsigned int ii=0; ii != mStateVars.size(); ++ii)
  {
    mAgentState.setStateVar(mStateStartIndex + ii, (_StatePrecision)mStateVars[ii].evaluate(currentSTGState));
    if (mStateVars[ii].getWrapping() != 0.0)
      mAgentState.setStateVarWrap(mStateStartIndex + ii, (int)mStateVars[ii].getWrapping());
  }

  CGenericLearner::updateState(currentSTGState);
}

void CGenericAgentQ::updateAction(ISTGActuation* actuationInterface)
{
  if (!mIsObserver)
  {
    for (unsigned int ii=0; ii != mActionVars.size(); ++ii)
    {
      mActionVars[ii].actuate(mAgentAction[mActionStartIndex + ii], actuationInterface);
    }

    CGenericLearner::updateAction(actuationInterface);
  }
}

int CGenericAgentQ::executePolicy(GenericState* currentState, uint64_t absoluteTimeMicroseconds)
{
	int result = 0;
	if (mIsObserver)
	{
		if (mObservedTime != UINT64_MAX)
		{
			for (unsigned int ii=0; ii != mActionVars.size(); ++ii)
			mAgentAction[mActionStartIndex + ii] = (_StatePrecision)mActionVars[ii].getAction(currentState);

			result = CSTGAgentQ<GenericState>::executePolicy(&mObservedSTGState, mObservedTime);
		}

		mObservedSTGState = *currentState;
		mObservedTime = absoluteTimeMicroseconds;
	}
	else
		result = CSTGAgentQ<GenericState>::executePolicy(currentState, absoluteTimeMicroseconds);

	return result;
}

double CGenericAgentQ::calculateReward()
{
  return CGenericLearner::calculateReward();
}

char CGenericAgentQ::isTerminalState(uint64_t trialTimeMicroseconds)
{
  if (CGenericLearner::isTerminalState(trialTimeMicroseconds))
    return CONST_STATE_TERMINAL | CONST_STATE_ABSORBING;
  else
    return CONST_STATE_NORMAL;
}
