#ifndef _CCMEM_H_
#define _CCMEM_H_

#include <cstring>

// TODO: this shouldn't be included here, its only for a test...
#include "precisions.h"

class CCMem
{
	protected:
		_QPrecision data[2];
	public:
				_QPrecision&	normal()		{ return this->data[0]; } // C-value
		const	_QPrecision&	normal() const	{ return this->data[0]; } // C-value (const version)
				_QPrecision&	pseudo()		{ return this->data[1]; } // C-tilde value incorporating the pseudo reward function
		const	_QPrecision&	pseudo() const	{ return this->data[1]; } // C-tilde value incorporating the pseudo reward function (const version)

//        const _QPrecision& normal() const { return this->data[0]; } // C-value
//        const _QPrecision& pseudo() const { return this->data[1]; } // C-tilde value incorporating the pseudo reward function
//        void set_normal(const _QPrecision& value) { this->data[0] = value; }
//        void set_pseudo(const _QPrecision& value) { this->data[1] = value; }


// ()
		//explicit CCMem(){}
		CCMem(){}
		CCMem(_QPrecision* initData)
		{
			memcpy(data, initData, 2*sizeof(_QPrecision));
		}
		template <class T>
		CCMem(const T& normal, const T& pseudo)
		{
			data[0] = (_QPrecision)normal;
			data[1] = (_QPrecision)pseudo;
		}
		template <class T>
		CCMem(const T& initData)
		{
			data[0] = (_QPrecision)initData;
			data[1] = (_QPrecision)initData;
		}
		CCMem(const CCMem& other)
		{
			*this = other;
		}
// =
		CCMem&			operator=(const CCMem& other)
		{
			data[0] = other.data[0];
			data[1] = other.data[1];
			return *this;
		}
/*		CCMem&			operator=(const _QPrecision& value)
		{
			data[0] = value;
			data[1] = value;
			return *this;
		}
*/
		template<typename T>
		CCMem&			operator=(const T& value)
		{
			//data[0] = (_QPrecision)value;
			data[0] = (_QPrecision)value;
			data[1] = (_QPrecision)value;
			return *this;
		}

		//friend const CCMem	operator+(const CCMem& left, const CCMem& right);
		//friend const CCMem	operator-(const CCMem& left, const CCMem& right);
		//friend const CCMem	operator*(const CCMem& left, const CCMem& right);
		//friend const CCMem	operator/(const CCMem& left, const CCMem& right);
		const CCMem			operator-() const
		{
			CCMem negated;
			negated.data[0] = -data[0];
			negated.data[1] = -data[1];
			return negated;
		}
// +
		const CCMem		operator+(const CCMem& other) const
		{
			CCMem result(data[0] + other.data[0],data[1] + other.data[1]);
			return result;
		}
		template<typename T>
		const CCMem		operator+(const T& val) const
		{
			CCMem result(data[0] + val,data[1] + val);
			return result;
		}

		//template<typename T>
		//friend CCMem	operator+(const CCMem& cmem, const T& value);
		//template<typename T>
		//friend CCMem	operator+(const T& value, const CCMem& cmem);
// -
		const CCMem		operator-(const CCMem& other) const
		{
			CCMem result(data[0] - other.data[0],data[1] - other.data[1]);
			return result;
		}
		template<typename T>
		const CCMem		operator-(const T& val) const
		{
			CCMem result(data[0] - val,data[1] - val);
			return result;
		}
		//template<typename T>
		//friend CCMem	operator-(const CCMem& cmem, const T& value);
		//template<typename T>
		//friend CCMem	operator-(const T& value, const CCMem& cmem);
// *
		const CCMem		operator*(const CCMem& other) const
		{
			CCMem result(data[0] * other.data[0],data[1] * other.data[1]);
			return result;
		}
		template<typename T>
		const CCMem		operator*(const T& val) const
		{
			CCMem result(data[0] * val,data[1] * val);
			return result;
		}
		//template<typename T>
		//friend CCMem	operator*(const CCMem& cmem, const T& value);
		//template<typename T>
		//friend CCMem	operator*(const T& value, const CCMem& cmem);
// /
		const CCMem		operator/(const CCMem& other) const
		{
			CCMem result(data[0] / other.data[0],data[1] / other.data[1]);
			return result;
		}
		template<typename T>
		const CCMem		operator/(const T& val) const
		{
			CCMem result(data[0] / val,data[1] / val);
			return result;
		}
		//template<typename T>
		//friend CCMem	operator/(const CCMem& cmem, const T& value);
		//template<typename T>
		//friend CCMem	operator/(const T& value, const CCMem& cmem);
// +=
		CCMem&			operator+=(const CCMem& other)
		{
			data[0] += other.data[0];
			data[1] += other.data[1];
			return *this;
		}
		template<typename T>
		CCMem&			operator+=(const T& value)
		{
			data[0] += value;
			data[1] += value;
			return *this;
		}
// -=
		CCMem&			operator-=(const CCMem& other)
		{
			data[0] -= other.data[0];
			data[1] -= other.data[1];
			return *this;
		}
		template<typename T>
		CCMem&			operator-=(const T& value)
		{
			data[0] -= value;
			data[1] -= value;
			return *this;
		}
// *=
		CCMem&			operator*=(const CCMem& other)
		{
			data[0] *= other.data[0];
			data[1] *= other.data[1];
			return *this;
		}
		template<typename T>
		CCMem&			operator*=(const T& value)
		{
			data[0] *= value;
			data[1] *= value;
			return *this;
		}
// /=
		CCMem&			operator/=(const CCMem& other)
		{
			data[0] /= other.data[0];
			data[1] /= other.data[1];
			return *this;
		}
		template<typename T>
		CCMem&			operator/=(const T& value)
		{
			data[0] /= value;
			data[1] /= value;
			return *this;
		}
};

/*
istream& operator >>(istream &is,CCMem &obj)
{
      is>>strVal;
      return is;
}
*/

// Inline this function to avoid the need for a .cpp file
inline std::ostream& operator <<(std::ostream &os,const CCMem &obj)
{
      os<< "(n:" << obj.normal() << ",p:" << obj.pseudo() << ")";
      return os;
}

/*
const CCMem operator+(const CCMem& left, const CCMem& right)
{
	CCMem result(left);
	return (result += right);
}
const CCMem operator-(const CCMem& left, const CCMem& right)
{
	CCMem result(left);
	return (result -= right);
}
const CCMem operator*(const CCMem& left, const CCMem& right)
{
	CCMem result(left);
	return (result *= right);
}
const CCMem operator/(const CCMem& left, const CCMem& right)
{
	CCMem result(left);
	return (result /= right);
}
*/

// +
/*
template<typename T>
CCMem			operator+(const CCMem& cmem, const T& value)
{
	CCMem sum;
	sum.data[0] = cmem.data[0] + value;
	sum.data[1] = cmem.data[1] + value;
	return sum;
}
template<typename T>
CCMem			operator+(const T& value, const CCMem& cmem)
{
	CCMem sum;
	sum.data[0] = cmem.data[0] + value;
	sum.data[1] = cmem.data[1] + value;
	return sum;
}

// -
template<typename T>
CCMem			operator-(const CCMem& cmem, const T& value)
{
	CCMem dif;
	dif.data[0] = cmem.data[0] - value ;
	dif.data[1] = cmem.data[1] - value ;
	return dif;
}
template<typename T>
CCMem			operator-(const T& value, const CCMem& cmem)
{
	CCMem dif;
	dif.data[0] = value - cmem.data[0];
	dif.data[1] = value - cmem.data[1];
	return dif;
}

// *
template<typename T>
CCMem			operator*(const CCMem& cmem, const T& value)
{
	CCMem prod;
	prod.data[0] = cmem.data[0] * value ;
	prod.data[1] = cmem.data[1] * value ;
	return prod;
}

template<typename T>
CCMem			operator*(const T& value, const CCMem& cmem)
{
	CCMem prod;
	prod.data[0] = cmem.data[0] * value ;
	prod.data[1] = cmem.data[1] * value ;
	return prod;
}

// /
template<typename T>
CCMem			operator/(const CCMem& cmem, const T& value)
{
	CCMem div;
	div.data[0] = cmem.data[0] / value ;
	div.data[1] = cmem.data[1] / value ;
	return div;
}
template<typename T>
CCMem			operator/(const T& value, const CCMem& cmem)
{
	CCMem div;
	div.data[0] = value / cmem.data[0];
	div.data[1] = value / cmem.data[1];
	return div;
}
*/

#endif
