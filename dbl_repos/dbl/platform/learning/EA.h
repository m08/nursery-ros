#ifndef __EA_H_INCLUDED
#define __EA_H_INCLUDED

#include <iostream>
#include <vector>
#include <randomc.h>
#include <Log2.h>

class CRandPerm
{
  protected:
    std::vector<unsigned int> mNumbers;
    int mSize, mCurrentNumber;
    bool mAutoReplace;

  protected:
    void init()
    {
      mCurrentNumber = 0;

      mNumbers.resize(mSize);
      for (int ii=0; ii != mSize; ++ii)
        mNumbers[ii] = ii;

      for (int ii=0; ii != mSize-1; ++ii)
      {
        int c = (int)(gRanrotB.Random()*(mSize-ii));
        int t = mNumbers[ii]; mNumbers[ii] = mNumbers[ii+c]; mNumbers[ii+c] = t;
      }
    }

  public:
    CRandPerm(int size, bool autoReplace = false) : mSize(size), mAutoReplace(autoReplace)
    {
      init();
    }

    CRandPerm &operator++()
    {
      *this += 1;
      return *this;
    }
    CRandPerm operator++(int)
    {
      CRandPerm res(*this);
      *this += 1;
      return res;
    }
    CRandPerm operator+(int rhs)
    {
      CRandPerm res(*this);
      res += rhs;
      return res;
    }
    CRandPerm &operator+=(int rhs)
    {
      mCurrentNumber += rhs;
      if (mCurrentNumber >= mSize && mAutoReplace)
      {
        init();
        mCurrentNumber %= mSize;
      }
      return *this;
    }

    unsigned int &operator[](int idx)
    {
      return mNumbers[idx];
    }

    const unsigned int &operator[](int idx) const
    {
      return mNumbers[idx];
    }

    operator unsigned int() const
    {
      if (mCurrentNumber < mSize)
        return mNumbers[mCurrentNumber];
      else
        return mSize;
    }
};

template<class Derived>
class IGenotype
{
  public:
    virtual void init() = 0;
    virtual bool mutate(double prob) = 0;
    virtual void crossover(const Derived &rhs, Derived &res) const = 0;

  public:
    virtual ~IGenotype()	{}

    IGenotype<Derived> &operator=(const IGenotype<Derived> &rhs)
    {
      return getGenotype().operator=(rhs.getGenotype());
    }

    Derived *clone() const
    {
      return new Derived(static_cast<const Derived&>(*this));
    }
    Derived &getGenotype() const
    {
      return static_cast<Derived&>(const_cast<IGenotype<Derived>& >(*this));
    }
};

template<class Derived, class T>
class ISegmentedGenotype : public IGenotype<Derived>
{
  public:
    virtual int getNumGenes() = 0;
    virtual void setGenes(const std::vector<T> &values) = 0;
    virtual std::vector<T> getGenes() = 0;
};

template<class T> class CPopulation;

template<class T>
class CIndividual
{
  friend class CPopulation<T>;

  protected:
    IGenotype<T> *mGenotype;
    double mFitness;
    int    mEvaluations;
    bool   mIsParent, mIsChild;

  public:
    CIndividual(IGenotype<T> *genotype) : mGenotype(genotype), mFitness(0), mEvaluations(0), mIsParent(false), mIsChild(false)
    {
      //logNoticeLn(CLog2("individual"), "Constructor");
    }

    CIndividual(const CIndividual<T> &rhs) : mFitness(rhs.mFitness), mEvaluations(rhs.mEvaluations), mIsParent(rhs.mIsParent), mIsChild(rhs.mIsChild)
    {
      //logNoticeLn(CLog2("individual"), "Copy constructor");
      mGenotype = rhs.mGenotype->clone();
    }

    CIndividual<T> &operator=(const CIndividual<T> &rhs)
    {
      //logNoticeLn(CLog2("individual"), "operator=");
      //logNoticeLn(CLog2("individual"), "this: " << getGenotype());
      //logNoticeLn(CLog2("individual"), " rhs: " << rhs.getGenotype());

      mFitness = rhs.mFitness;
      mIsChild = rhs.mIsChild;
      mIsParent = rhs.mIsParent;
      mEvaluations = rhs.mEvaluations;
      *mGenotype = *rhs.mGenotype;

      //logNoticeLn(CLog2("individual"), "this: " << getGenotype());

      return *this;
    }

    virtual ~CIndividual()
    {
      //logNoticeLn(CLog2("individual"), "Destructor");
      delete mGenotype;
    }

    void setFitness(double fitness)
    {
      if (mEvaluations > 0 && fabs(fitness - getFitness()) > 0.1)
        logWarningLn(CLog2("individual"), "Large fitness difference");

      mFitness += fitness;
      mEvaluations++;

      logDebugLn(CLog2("individual"), getGenotype() << " -> " << std::setiosflags(std::ios::fixed) << std::setprecision(3) << getFitness() << " (" << mEvaluations << ")");
    }

    double getFitness() const
    {
      return mFitness/mEvaluations;
    }

    bool requireEvaluation(void) const
    {
      return (mEvaluations < 1);
    }

    T &getGenotype(void) const
    {
      return mGenotype->getGenotype();
    }

    void invalidate(void)
    {
      mFitness = 0;
      mEvaluations = 0;
    }

  public:
    virtual void init()
    {
      mFitness = 0;
      mEvaluations = 0;
      mIsChild = false;
      mIsParent = false;
      mGenotype->init();
    }

    virtual bool mutate(double prob)
    {
      if (mGenotype->mutate(prob))
      {
        invalidate();
        return true;
      }
      else
        return false;
    }

    virtual void crossover(const CIndividual<T> &rhs, CIndividual<T> &res)
    {
      res.invalidate();
      mGenotype->crossover(rhs.getGenotype(), res.getGenotype());
    }

  protected:
    bool isChild(void) const
    {
      return mIsChild;
    }

    void setChild(bool isChild)
    {
      mIsChild = isChild;
    }

    bool isParent(void) const
    {
      return mIsParent;
    }

    void setParent(bool isParent)
    {
      mIsParent = isParent;
    }

    int getEvaluations(void)
    {
      return mEvaluations;
    }
};

template<class T>
class IPopulation
{
  public:
    typedef typename std::vector<CIndividual<T> >::iterator iterator;

  protected:
    CLog2 mLog;

  public:
    IPopulation() : mLog("population") { }
    virtual ~IPopulation() { }

    virtual void reset() = 0;
    virtual void evolve(bool burst) = 0;
    virtual typename IPopulation<T>::iterator begin() = 0;
    virtual typename IPopulation<T>::iterator end() = 0;
};

template<class T>
class CPopulation : public IPopulation<T>
{
  using IPopulation<T>::mLog;

  protected:
    std::vector<CIndividual<T> > mIndividuals;
    double mMutationProb, mCrossoverProb;

    IGenotype<T> *mBase;
    int mSize;

  public:
    CPopulation(const IGenotype<T> &base, int size) : mMutationProb(0.2), mCrossoverProb(0.5), mBase(base.clone()), mSize(size)
    {
      reset();
    }

    virtual ~CPopulation()
    {
      deinit();
      delete mBase;
    }

    void deinit(void)
    {
      mLogNoticeLn("Committing genocide");
      mIndividuals.clear();
    }

    void reset()
    {
      if (!mIndividuals.empty())
        deinit();

      mLogNoticeLn("Plain algorithm building population of " << mSize << " individuals");

      for (int ii=0; ii != mSize; ii++)
      {
        mIndividuals.push_back(CIndividual<T>(mBase->clone()));
        mIndividuals[ii].init();
      }
    }

    typename IPopulation<T>::iterator begin()
    {
      return mIndividuals.begin();
    }

    typename IPopulation<T>::iterator end()
    {
      return mIndividuals.end();
    }

    void setMutationProbability(double prob)
    {
      mMutationProb = prob;
    }

    void setCrossoverProbability(double prob)
    {
      mCrossoverProb = prob;
    }

    void evolve(bool burst)
    {
      mLogInfoLn("Selecting next generation");
      for (CRandPerm pp(mIndividuals.size()); pp != mIndividuals.size(); pp++)
      {
        mIndividuals[pp].setParent(false);
        mIndividuals[pp].setChild(false);
      }

      // Binary deterministic tournament selection
      for (CRandPerm pp(mIndividuals.size()); pp != mIndividuals.size(); pp+=2)
      {
        if (pp+1 == mIndividuals.size())
        {
          mLogDebugLn("Selected " << pp << " " << mIndividuals[pp].getGenotype() << " fitness " << mIndividuals[pp].getFitness());
        }
        else
        {
          if (mIndividuals[pp].getFitness() > mIndividuals[pp+1].getFitness())
          {
            mLogDebugLn("Selected " << pp << " " << mIndividuals[pp].getGenotype() << " fitness " << mIndividuals[pp].getFitness() << " (discarded " << pp+1 << ")");
            mIndividuals[pp].setParent(true);
          }
          else
          {
            mLogDebugLn("Selected " << pp+1 << " " << mIndividuals[pp+1].getGenotype() << " fitness " << mIndividuals[pp+1].getFitness() << " (discarded " << pp << ")");
            mIndividuals[pp+1].setParent(true);
          }
        }
      }

      reproduce(mCrossoverProb);

      if (burst)
      {
        mLogNoticeLn("Burst mutation");
        mutate(1.0);
      }
      else
        mutate(mMutationProb);
    }

  private:
    /// Mutate all child individuals with some probability
    void mutate(double prob)
    {
      for (typename IPopulation<T>::iterator ii = begin(); ii != end(); ++ii)
        if (ii->isChild())
          ii->mutate(prob);
    }

    /// Reproduce parent individuals to invalid spots in population,
    /// applying crossover with some probability.
    void reproduce(double crossoverprob)
    {
      // Replacing random permutation
      CRandPerm pp(mIndividuals.size(), true);

      for (typename IPopulation<T>::iterator ii = begin(); ii != end(); ++ii)
        if (!ii->isParent() && !ii->isChild())
        {
          while (!mIndividuals[pp].isParent()) ++pp;

          mLogDebug("Reproducing " << pp);
          CIndividual<T> &ind = mIndividuals[pp];
          ++pp;

          if (gRanrotB.Random() < crossoverprob)
          {
            while (!mIndividuals[pp].isParent()) ++pp;
            mLogDebugLn(" with " << pp);
            ind.crossover(mIndividuals[pp], *ii);
            ++pp;
          }
          else
          {
            mLogDebugLn("");
            *ii = ind;
          }

          ii->setChild(true);
        }
    }
};


#endif /* __EA_H_INCLUDED */
