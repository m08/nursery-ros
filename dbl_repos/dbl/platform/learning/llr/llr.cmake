# 
# CMake include file for llr library
# Erik Schuitema 2010 (E.Schuitema@tudelft.nl)
# 

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/platform/learning/llr)
INCLUDE(${WORKSPACE_DIR}/dbl/externals/ann/ann.cmake)
INCLUDE(${WORKSPACE_DIR}/dbl/externals/eigen/eigen.cmake)
INCLUDE(${WORKSPACE_DIR}/dbl/platform/io/logging/logging.cmake)
INCLUDE(${WORKSPACE_DIR}/dbl/platform/io/textfile/textfile.cmake)
INCLUDE(${WORKSPACE_DIR}/dbl/platform/threading/threading.cmake)

#TARGET_LINK_LIBRARIES(${TARGET} llr)

#IF (NOT __LLR_CMAKE_INCLUDED)
#  SET(__LLR_CMAKE_INCLUDED 1)
#
#  ADD_SUBDIRECTORY(${WORKSPACE_DIR}/dbl/platform/learning/llr ${WORKSPACE_DIR}/build/dbl/platform/learning/llr/${CMAKE_BUILD_TYPE}${COMPILER_VERSION})
#ENDIF (NOT __LLR_CMAKE_INCLUDED)
