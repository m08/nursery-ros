/*
 * llrtest.cpp
 *
 *  Test program for LLR library, using data set from LEO
 *
 *  Created on: Aug 10, 2010
 *      Author: Erik Schuitema
 */

//#include <DetectNewDelete.h>
//#include <DetectMallocFree.h>

#include "llr.h"
#include <signal.h>
//#include <sys/time.h>
#include <time.h>
#include <Eigen/Core>
#include <Statistics.h>
#include <MemoryPool.h>

#define CONST_Epsilon 1e-6
const int k				= 40;
// Column indices from Matlab, so subtract 1!
int sensorCols[]		= {2, 4, 9, 11, 16, 18, 23, 25, 30, 32, 37, 39, 51, 53};
const int numSensors	= sizeof(sensorCols)/sizeof(int);
int actuatorCols[]		= {8, 15, 22, 29, 36, 43};
const int numActuators	= sizeof(actuatorCols)/sizeof(int);
int transitionVarCols[]	= {55, 57, 59, 61};
//int transitionVarCols[]		= {};
const int numTransitionVars	= sizeof(transitionVarCols)/sizeof(int);
const int numInputs		= numSensors+numActuators;
const int numOutputs	= numSensors+numTransitionVars;

void catch_signal(int sig)
{
}

void reportPointResult(ANNpoint query, ANNpoint prediction, ANNpoint answer, ANNpoint normpredint, int numInputs, int numInputsToReport, int numOutputs)
{
	printf("Search point:\n");
	for (int i=0; i<numInputsToReport; i++)
		printf("%8.5f ", query[i]);
	printf(" + %d voltages\nPrediction:\n", numInputs-numInputsToReport);
	for (int i=0; i<numOutputs; i++)
		printf("%8.5f ", prediction[i]);
	printf("\nActual next state:\n");
	for (int i=0; i<numOutputs; i++)
		printf("%8.5f ", answer[i]);
	printf("\nError:\n");
	for (int i=0; i<numOutputs; i++)
		printf("%8.5f ", answer[i] - prediction[i]);
	printf("\nNormalized prediction interval:\n");
	for (int i=0; i<numOutputs; i++)
		printf("%8.5f ", normpredint[i]);
	printf("\nStudent-t values:\n");
	for (int i=0; i<numOutputs; i++)
		printf("%8.5f ", fabs(answer[i]-prediction[i])/(normpredint[i]+CONST_Epsilon));	// sometimes normpredint is zero
	printf("\n");
}

double getPointDistanceSqr(ANNpoint query, ANNpoint neighbor, ANNcoord* inputWeights, int numCoords)
{
	double result = 0;
	for (int i=0; i<numCoords; i++)
		result += std::pow(inputWeights[i]*(query[i] - neighbor[i]), 2);
	return result;//std::sqrt(result);
}

void benchmarkLLR(CLWR<numSensors, numActuators, numTransitionVars, k>& llr, CLWR<numSensors, numActuators, numTransitionVars, k>& llrVal, SearchDirection direction)
{
	// Timing variables
	timeval tStart, tEnd;
	int	tdifSecs1, tdifUsecs1;

	// Search point
	ANNcoord *searchPoint = new ANNcoord[llr.numDims()];
	ANNcoord *searchPointAns = new ANNcoord[llr.numModelOutputs()];

	double t_dof=0;

	// Create statistics objects for the prediction error: numOutputDims() + one extra for the total error
	CSimpleStat *mErrStat = new CSimpleStat[llr.numModelOutputs()+1];
	int			numConfPredictions=0;
	for (int iStat=0; iStat<llr.numModelOutputs()+1; iStat++)
		mErrStat[iStat].setBufferLength(llrVal.numPoints());

	if (direction == sdForward)
		printf("Starting forward LLR benchmark...\n");
	else
		printf("Starting backward LLR benchmark...\n");
	ANNpoint tempResult		= new ANNcoord[llr.numModelOutputs()];
	ANNpoint tempPredInt	= new ANNcoord[llr.numModelOutputs()];
	// Start timing
	gettimeofday(&tStart, NULL);
	// Define the student-t threshold value at which to reject a prediction
	double t_false = 3.5;//2.57;
	// Loop over all points
	int numPointsInsideNNHull=0;
	for (int iPoint=0; iPoint<llrVal.numPoints(); iPoint++)
	{
		//printf("Point %d: ", iPoint);
		llrVal.getDataPoint(iPoint, searchPoint);
		ANNpoint	searchPointSensIn, searchPointAct;
		if (direction == sdForward)
		{
			searchPointSensIn	= searchPoint;
			searchPointAct	 	= searchPoint + numSensors;
			memcpy(searchPointAns, searchPoint + numSensors + numActuators, llrVal.numModelOutputs()*sizeof(searchPointAns[0]));
		}
		else
		{
			searchPointSensIn	= searchPoint + numSensors + numActuators;
			searchPointAct	 	= searchPoint + numSensors;
			// Copy sensor answer
			memcpy(searchPointAns, searchPoint, llrVal.numSensors()*sizeof(searchPointAns[0]));
			// Copy transition variables answer
			memcpy(searchPointAns+numSensors, searchPoint + numSensors + numActuators + numSensors, llrVal.numTransitionVars()*sizeof(searchPointAns[0]));
		}

		//bool pointInsideNNHull = llr.predict(direction, searchPointSensIn, searchPointAct, tempResult, tempPredInt, &t_dof);
		bool pointInsideNNHull = llr.predict(direction, searchPointSensIn, searchPointAct, tempResult, tempResult+llr.numSensors(), tempPredInt, tempPredInt+llr.numSensors(), &t_dof);
		numPointsInsideNNHull += (int)pointInsideNNHull;
		// Calculate t as the difference between the true T1 data and the predicted data,
		// normalized by its confidence interval. Compare this with a threshold value t_false
		double pointError = 0;
		bool reportPoint=false;
		//if (pointInsideNNHull)
		{
			for (int iDim=0; iDim<numOutputs; iDim++)
			{
				double coordError = fabs(tempResult[iDim] - searchPointAns[iDim]);
				// Divide the residual by the normalized prediction interval to calculate the coordinate's t-value
				double t = coordError/(tempPredInt[iDim] + CONST_Epsilon);
				if (t > t_false)
				{
					reportPoint=true;
					//printf("badsensor %d, ", iDim);
				}
				mErrStat[iDim].addValue(coordError);
				pointError += std::pow(coordError, 2);
			}
			mErrStat[llr.numModelOutputs()].addValue(pointError);
		}
		if (pointError > 10)
			printf("bad prediction for point %d :( (squared error of %.3f)\n", iPoint, pointError);

		if (reportPoint)
		{
			//printf("Full point report: \n");
			//reportPointResult(llrVal.dataPoint(iPoint), tempResult, llrVal.dataPoint(iPoint)+llr.numInputDims(), tempPredInt, llr.numInputDims(), numSensors, llr.numOutputDims());
		}
		else
			// Nothing to report about this point; this was a confident prediction!
			numConfPredictions++;

		//printf("\n");
	}
	gettimeofday(&tEnd, NULL);
	getTimeDiff(&tStart, &tEnd, &tdifSecs1, &tdifUsecs1);
	delete[] searchPoint;
	delete[] searchPointAns;
	delete[] tempPredInt;
	delete[] tempResult;
	printf("Done in %.3fs.\n", (double)tdifSecs1 + ((double)tdifUsecs1)/1E6);
	// Print rank statistics
	if (true)
	{
		int rankbins[numInputs+1];
		llr.rankStat().getHistogram(rankbins, numInputs+1, 0.0, (double)(numInputs+1));
		for (int iRank=0; iRank<numInputs+1; iRank++)
			printf("Queries with rank %d: %d\n", iRank+1, rankbins[iRank]);
	}
	if (true)
	{
		// Print prediction error statistics
		printf("Observed %d of %d predictions within their prediction interval (%.2f%%) using t=%.3f\n", numConfPredictions, llrVal.numPoints(), 100.0*numConfPredictions/llrVal.numPoints(), t_false);
		printf("Observed %d of %d query points inside their nearest neighbor hull.\n", numPointsInsideNNHull, llrVal.numPoints());
		printf("Absolute prediction error per output dimension:\n");
		for (int iDim=0; iDim<llr.numModelOutputs(); iDim++)
			printf("Dim%2d:: Avg: %.5f,\tStdev: %.5f\tMin: %.5f\tMax:%.5f\n", iDim, mErrStat[iDim].getAverage(), mErrStat[iDim].getStdev(), mErrStat[iDim].getMinimum(), mErrStat[iDim].getMaximum());
		printf("Per point Mean Squared Errors (avg %7.4f) were in the range %7.4f - %7.4f:\n", mErrStat[llr.numModelOutputs()].getAverage(), mErrStat[llr.numModelOutputs()].getMinimum(), mErrStat[llr.numModelOutputs()].getMaximum() );
		const int numErrBins=10;
		int errbins[numErrBins];
		double errMin	= 0; // mErrStat.getMinimum();
		double errMax	= 1;// mErrStat.getMaximum();
		double errRange = errMax - errMin;
		mErrStat[llr.numModelOutputs()].getHistogram(errbins, numErrBins, errMin, errMax);
		for (int iBin=0; iBin<numErrBins; iBin++)
			printf("Queries with %7.4f <= error <= %7.4f: %d\n", errMin + iBin*errRange/(double)numErrBins, errMin + (iBin+1)*errRange/(double)numErrBins, errbins[iBin]);
	}

	delete[] mErrStat;
}

// Usage: llrtest [filename] [testpoint index]
// filename defaults to "leotest.txt"
int main(int argc, char** argv)
{
	// Create memory pool
	CMemoryPool	memPool(4*getUnitSize(suMegaByte));

	//signal(SIGTERM, catch_signal);
	//signal(SIGINT, catch_signal);
	double aw = 1.0;	// Angle weight
	double sw = 0.40;	// Angular speed weight
	double vw = 0.1;	// Voltage weight
	double sensorWeights[numSensors] = {aw, sw, aw, sw, aw, sw, aw, sw, aw, sw, aw, sw, aw, sw};
	double actuatorWeights[numActuators] = {vw, vw, vw, vw, vw, vw};
	double inputWeights[numInputs] = {aw, sw, aw, sw, aw, sw, aw, sw, aw, sw, aw, sw, aw, sw, vw, vw, vw, vw, vw, vw};
	double ridgeRegressionFactor = 1.2E-3*std::pow((aw+sw)/2, 2);
//	// Default parameters with unit weights
//	double inputWeights[numInputs] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
//	double ridgeRegressionFactor = 1E-3;

	CLWR<numSensors, numActuators, numTransitionVars, k> llr(&memPool);
	CLWR<numSensors, numActuators, numTransitionVars, k> llrVal(&memPool);	// Validation data
	llr.init(true, true, 18000, 4000, sensorWeights, actuatorWeights);
	llr.setRidgeRegressionFactor(ridgeRegressionFactor);
	llrVal.init(true, true, 18000, 4000, sensorWeights, actuatorWeights);
	llrVal.setRidgeRegressionFactor(ridgeRegressionFactor);
	CLLRFileConfig llrConfig;
	llrConfig.mIDCol = 0;
	llrConfig.mIDStride = 1;
	llrConfig.mNumLinesToSkip	= 0;
	llrConfig.mReadHeader		= 1;
	llrConfig.mActuatorsFromT1	= true;

	// Add columns to config
	printf("sensorCols: %d, actuatorCols: %d\n", sizeof(sensorCols)/sizeof(int), sizeof(actuatorCols)/sizeof(int));
	for (int i=0; i<numSensors; i++)
		llrConfig.mSensorCols.push_back(sensorCols[i]-1);
	for (int i=0; i<numActuators; i++)
		llrConfig.mActuatorCols.push_back(actuatorCols[i]-2);
	for (int i=0; i<numTransitionVars; i++)
		llrConfig.mTransitionVarCols.push_back(transitionVarCols[i]-1);

	// Read the data file using our configuration
	std::string filename("leodata.txt");
	std::string filenameVal("leodata2.txt");
	if (argc>1)
		filename = argv[1];
	if (argc>2)
		filenameVal = argv[2];
	if (!llr.readDataFile(filename, llrConfig))
	{
		printf("Error reading data file \"%s\"! Exiting.\n", filename.c_str());
		return -1;
	}
	if (!llrVal.readDataFile(filenameVal, llrConfig))
	{
		printf("Error reading validation data file \"%s\"! Exiting.\n", filenameVal.c_str());
		return -1;
	}

	// Single point search and report
	llr.memoryReport(suByte);
	ANNidx	nn_idx[k];
	ANNdist	dd[k];
	int pointIndex = 15253;
	if (argc > 3)
		pointIndex = atoi(argv[3]);
	ANNcoord *searchPoint = new ANNcoord[llr.numDims()];	// We sometimes use the inputs only, sometimes the outputs too
	llrVal.getDataPointForwardInputs(pointIndex, searchPoint);
	llr.getNeighbors(sdForward, searchPoint, searchPoint+numSensors, k, nn_idx, dd, 0.1);

	// Print search point
	// Print neighbor distances as well. This is a good test to see if input scaling works correctly.
	printf("Search point: point %d from validation data set:\n", pointIndex);
	for (int i=0; i<llr.numModelInputs(); i++)
		printf("%4.1f ", searchPoint[i]);
	if (true)	// Nearest neighbors report
	{
		printf("\nNearest neighbors in modeling data set:\n");
		// Print search result
		ANNcoord *neighbor = new ANNcoord[llr.numDims()];
		for (int iNN=0; iNN<k; iNN++)
		{
			if (nn_idx[iNN] != ANN_NULL_IDX)
			{
				llr.getDataPoint(nn_idx[iNN], neighbor);
				for (int iDim=0; iDim<llr.numDims(); iDim++)
					printf("%4.1f ", neighbor[iDim]);
			}
			printf("\nDistance=(%6.3f|%6.3f)\n", dd[iNN], getPointDistanceSqr(searchPoint, neighbor, inputWeights, llr.numModelInputs()));
		}
		delete[] neighbor;
	}
	printf("\n");

	// Report a prediction
	//enableReportMallocFree(true);
	ANNcoord prediction[numOutputs];
	ANNcoord predint[numOutputs];	// Prediction interval values (+-, so symmetrical)
	double t_dof=0;
	printf("Test data point on data set:\n");
	llr.getDataPoint(pointIndex, searchPoint);
	llr.predict(sdForward, searchPoint, searchPoint+numSensors, prediction, prediction+llr.numSensors(), predint, predint+llr.numSensors(), &t_dof);
	reportPointResult(searchPoint, prediction, searchPoint+llr.numModelInputs(), predint, llr.numModelInputs(), numSensors, llr.numModelOutputs());
	printf("Test validation point on data set:\n");
	llrVal.getDataPoint(pointIndex, searchPoint);
	llr.predict(sdForward, searchPoint, searchPoint+numSensors, prediction, prediction+llr.numSensors(), predint, predint+llr.numSensors(), &t_dof);
	reportPointResult(searchPoint, prediction, searchPoint+llr.numModelInputs(), predint, llr.numModelInputs(), numSensors, llr.numModelOutputs());
	printf("Test validation point on validation set:\n");
	llrVal.getDataPoint(pointIndex, searchPoint);
	llrVal.predict(sdForward, searchPoint, searchPoint+numSensors, prediction, prediction+llr.numSensors(), predint, predint+llr.numSensors(), &t_dof);
	reportPointResult(searchPoint, prediction, searchPoint+llr.numModelInputs(), predint, llr.numModelInputs(), numSensors, llr.numModelOutputs());
	/*
	// Enter manually point 14762 from llr
	//ANNcoord debugPoint[34] = {0.49451800000000001, 5.0487500000000001, 0.261293, -2.2872599999999998, -0.877664, -5.43201, 0.046344299999999998, -0.072548299999999996, 0.226106, -0.24235999999999999, -0.097606100000000001, 1.2951699999999999, -0.231631, 1.2860100000000001, 11.9467, -12.6869, -11.7883, -0.84602299999999997, -0.56202399999999997, 2.35738, 0.53261800000000004, 5.0941000000000001, 0.24229300000000001, -2.4489200000000002, -0.91616399999999998, -5.3674099999999996, 0.046344299999999998, -0.035859000000000002, 0.226106, -0.31093900000000002, -0.090506100000000006, 1.3033999999999999, -0.22012599999999999, 1.4419};
	ANNcoord debugPoint[llr.numDims()];
	memcpy(debugPoint, llr.dataPoint(14762), llr.numDims()*sizeof(ANNcoord));
	// Enter manually point 93 from llrVal
	ANNcoord debugPointVal[llr.numDims()];
	memcpy(debugPointVal, llrVal.dataPoint(93), llr.numDims()*sizeof(ANNcoord));
	//ANNcoord debugPointVal[34] = {0.50011799999999995, 5.1753400000000003, 0.29169299999999998, -2.1395400000000002, -0.877664, -5.4510100000000001, 0.046344299999999998, -0.0492649, 0.226106, -0.170269, -0.10910599999999999, 1.2428900000000001, -0.242369, 1.1839999999999999, 11.9339, -12.817399999999999, -11.794600000000001, -0.85289199999999998, -0.57563299999999995, 2.1770800000000001, 0.53261800000000004, 5.14832, 0.26709300000000002, -2.3288099999999998, -0.91616399999999998, -5.3832599999999999, 0.046344299999999998, -0.033216299999999997, 0.226106, -0.19244800000000001, -0.097606100000000001, 1.2674000000000001, -0.231631, 1.3184400000000001};
	printf("Point difference:\n");
	for (int i=0; i<llr.numInputDims(); i++)
		printf("%8.5f ", debugPoint[i] - debugPointVal[i]);
	printf("\n");

	printf("Model: llr, testpoint: llr:14762\n");
	llr.predict(debugPoint, prediction, predint, &t_dof);
	reportPointResult(debugPoint, prediction, debugPoint+llr.numInputDims(), predint, llr.numInputDims(), numSensors, llr.numOutputDims());
	printf("Model: llr, testpoint: llrVal:93\n");
	llr.predict(debugPointVal, prediction, predint, &t_dof);
	reportPointResult(debugPointVal, prediction, debugPointVal+llr.numInputDims(), predint, llr.numInputDims(), numSensors, llr.numOutputDims());
	printf("Model: llrVal, testpoint: llr:14762\n");
	llrVal.predict(debugPoint, prediction, predint, &t_dof);
	reportPointResult(debugPoint, prediction, debugPoint+llr.numInputDims(), predint, llr.numInputDims(), numSensors, llr.numOutputDims());
	printf("Model: llrVal, testpoint: llrVal:93\n");
	llrVal.predict(debugPointVal, prediction, predint, &t_dof);
	reportPointResult(debugPointVal, prediction, debugPointVal+llr.numInputDims(), predint, llr.numInputDims(), numSensors, llr.numOutputDims());
	 */

	delete[] searchPoint;

	benchmarkLLR(llr, llrVal, sdForward);
	benchmarkLLR(llr, llrVal, sdBackward);

	//enableReportMallocFree(false);

	// Deallocate 12 bytes of general ANN memory
	annClose();
}

