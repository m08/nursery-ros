/*
 * llrtree.h
 *
 * Tree class that provides a nearest neighbor search for the LLR/LWR classes
 *
 *  Created on: Aug 31, 2010
 *      Author: Erik Schuitema
 */

#ifndef LLRTREE_H_
#define LLRTREE_H_

#include <ANN/ANN.h>
#include <Log2.h>
#ifndef _MSC_VER
#include <sys/time.h>
#endif

enum		SearchDirection {sdForward, sdBackward};

// The tree is NOT owner of the real data
// The tree is buffered //TODO expand
template<int NumSensors, int NumActuators, int NumTransitionVars>
class CLLRTree
{
	protected:
		static const int NumModelInputs	= NumSensors + NumActuators;
		static const int NumPointDims	= NumSensors + NumActuators + NumSensors + NumTransitionVars;
	protected:
		// Log
		CLog2			mLog;

		bool			mForward;	// Indicates whether this class provides a forward search tree	: [s_{t}, a_{t}] -> s_{t+1}
		bool			mBackward;	// Indicates whether this class provides a backward search tree	: [s_{t+1}, a_{t}] -> s_{t}

		// Data
		ANNcoord*		mData;					// *Pointer* to contiguous data block with the data. Points have the following format: sensordata_{t} | actuatordata | sensordata_{t+1} (== S0|A|S1)
		int				mNumPoints;				// Current number of points in the tree
		ANNpointArray	mAnnForwardData;		// Pointer array for forward ANN tree
		ANNpointArray	mAnnBackwardData;		// Pointer array for backward ANN tree
		ANNpointArray	mAnnForwardShadowData;			// Shadow pointer array for ANN trees. Filled while mAnn*Data is in use.
		ANNpointArray	mAnnBackwardShadowData;			// Shadow pointer array for ANN trees. Filled while mAnn*Data is in use.
		int				mMaxNumPoints;			// Maximum number of points in the tree

		// mAnnDataWritePos: Position where a new point *pointer* should be written
		int				mAnnDataWritePos;

		// Tree
		ANNkd_tree*		mForwardTree;
		ANNkd_tree*		mBackwardTree;
		int				mTreeBucketSize;

		// Data point address request without locking. Data format is S0|A|S1.
		// The forward search tree uses S0|A, while the backward tree uses A|S1
		ANNpoint		forwardDataAddr(int index)				{return mData + index*NumPointDims;}
		ANNpoint		backwardDataAddr(int index)				{return mData + index*NumPointDims + NumSensors;}
		// Wrap annData or annShadowData position that is possibly larger than mMaxPointsInTree-1
		inline int		wrapAnnDataPosLarger(int pos)	{return (pos >= mMaxNumPoints)?pos-mMaxNumPoints:pos;}

		void			deleteTree();

	public:
		CLLRTree():
			mLog("LLRtree"),
			mForward(false),
			mBackward(false),
			mData(NULL),
			mNumPoints(0),
			mAnnForwardData(NULL),
			mAnnBackwardData(NULL),
			mAnnForwardShadowData(NULL),
			mAnnBackwardShadowData(NULL),
			mMaxNumPoints(0),
			mAnnDataWritePos(0),
			mForwardTree(NULL),
			mBackwardTree(NULL),
			mTreeBucketSize(10)
		{}

		virtual ~CLLRTree()				{}

		void		updateAndSwapBuffers(int numBufferedPoints);
		void		recreateTree();
		void		clear();
		void		init(ANNcoord* data, int maxNumPoints, bool forward, bool backward);
		void		deinit();

		inline int	numPoints()				{return mNumPoints;}
		inline int  numForwardPoints()       {if (mForwardTree) return mForwardTree->nPoints(); else return 0;}
		inline int  numBackwardPoints()       {if (mBackwardTree) return mBackwardTree->nPoints(); else return 0;}

		// Signal the tree that a new buffered data point has been added to mData (managed outside the tree)
		void		reportBufferedPoint(int newPointIndex);

		void		annkSearch(					// approx k near neighbor search
			SearchDirection dir,
			ANNpoint		q,				// query point
			int				k,				// number of near neighbors to return
			ANNidxArray		nn_idx,			// nearest neighbor array (modified)
			ANNdistArray	dd,				// dist to near neighbors (modified)
			double			eps=0.0);		// error bound

		int			getByteSize();
		double		benchmarkForwardTree(int numTestPoints, int k, double eps);
};

template<int NumSensors, int NumActuators, int NumTransitionVars>
void CLLRTree<NumSensors, NumActuators, NumTransitionVars>::updateAndSwapBuffers(int numBufferedPoints)
{
	// Now make some space for new data:
	// Make content of mAnnForwardData and mAnnForwardShadowData equal by copying the buffered point indices
	// length(mAnnForwardData) == length(mAnnForwardShadowData) == mMaxNumPointsInTree
	if (mAnnDataWritePos >= numBufferedPoints)// No wrap around
	{
		if (mForward)
			memcpy(&mAnnForwardData[mAnnDataWritePos-numBufferedPoints], &mAnnForwardShadowData[mAnnDataWritePos-numBufferedPoints], numBufferedPoints*sizeof(mAnnForwardData[0]));
		if (mBackward)
			memcpy(&mAnnBackwardData[mAnnDataWritePos-numBufferedPoints], &mAnnBackwardShadowData[mAnnDataWritePos-numBufferedPoints], numBufferedPoints*sizeof(mAnnBackwardData[0]));
	}
	else // Wrap around: two memcpy's
	{
		// Copy the last mAnnDataWritePos points
		if (mForward)
			memcpy(&mAnnForwardData[0], &mAnnForwardShadowData[0], mAnnDataWritePos*sizeof(mAnnForwardData[0]));
		if (mBackward)
			memcpy(&mAnnBackwardData[0], &mAnnBackwardShadowData[0], mAnnDataWritePos*sizeof(mAnnBackwardData[0]));
		// Copy the first (numBufferedPoints - mAnnDataWritePos) points
		int startIndex = mMaxNumPoints - (numBufferedPoints - mAnnDataWritePos);
		if (mForward)
			memcpy(&mAnnForwardData[startIndex], &mAnnForwardShadowData[startIndex], (numBufferedPoints-mAnnDataWritePos)*sizeof(mAnnForwardData[0]));
		if (mBackward)
			memcpy(&mAnnBackwardData[startIndex], &mAnnBackwardShadowData[startIndex], (numBufferedPoints-mAnnDataWritePos)*sizeof(mAnnBackwardData[0]));
	}

	// Add the buffered points to the ANN points
	mNumPoints += numBufferedPoints;
	if (mNumPoints > mMaxNumPoints)
		mNumPoints = mMaxNumPoints;

	ANNpointArray	swap;
	// Swap the forward annData pointers
	if (mForward)
	{
		swap					= mAnnForwardData;
		mAnnForwardData			= mAnnForwardShadowData;
		mAnnForwardShadowData	= swap;
	}
	// Swap the backward annData pointers
	if (mBackward)
	{
		swap					= mAnnBackwardData;
		mAnnBackwardData		= mAnnBackwardShadowData;
		mAnnBackwardShadowData	= swap;
	}
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
void CLLRTree<NumSensors, NumActuators, NumTransitionVars>::init(ANNcoord* data, int maxNumPoints, bool forward, bool backward)
{
	mData					= data;
	mMaxNumPoints			= maxNumPoints;
	mForward				= forward;
	mBackward				= backward;
	if (mForward)
	{
		mAnnForwardData			= new ANNpoint[mMaxNumPoints];
		mAnnForwardShadowData	= new ANNpoint[mMaxNumPoints];
	}
	if (mBackward)
	{
		mAnnBackwardData		= new ANNpoint[mMaxNumPoints];
		mAnnBackwardShadowData	= new ANNpoint[mMaxNumPoints];
	}
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
void CLLRTree<NumSensors, NumActuators, NumTransitionVars>::deinit()
{
	if (mAnnForwardData != NULL)
	{
		delete[] mAnnForwardData;
		mAnnForwardData = NULL;
		delete[] mAnnForwardShadowData;
		mAnnForwardShadowData = NULL;
	}
	if (mAnnBackwardData != NULL)
	{
		delete[] mAnnBackwardData;
		mAnnBackwardData = NULL;
		delete[] mAnnBackwardShadowData;
		mAnnBackwardShadowData = NULL;
	}
	deleteTree();
	mNumPoints = 0;
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
void CLLRTree<NumSensors, NumActuators, NumTransitionVars>::clear()
{
	mNumPoints			= 0;
	mAnnDataWritePos	= 0;
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
void CLLRTree<NumSensors, NumActuators, NumTransitionVars>::deleteTree()
{
	if (mForwardTree != NULL)
	{
		delete mForwardTree;
		mForwardTree = NULL;
	}
	if (mBackwardTree != NULL)
	{
		delete mBackwardTree;
		mBackwardTree = NULL;
	}
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
void CLLRTree<NumSensors, NumActuators, NumTransitionVars>::recreateTree()
{
	deleteTree();

	if (mForward)
	{
		mForwardTree	= new ANNkd_tree(mAnnForwardData, mNumPoints, NumModelInputs, mTreeBucketSize);
		mLogInfoLn("Forward search tree built with " << mNumPoints << " points, " << NumModelInputs << " dimensions and bucket size " << mTreeBucketSize);
	}
	if (mBackward)
	{
		mBackwardTree	= new ANNkd_tree(mAnnBackwardData, mNumPoints, NumModelInputs, mTreeBucketSize);
		mLogInfoLn("Backward search tree built with " << mNumPoints << " points, " << NumModelInputs << " dimensions and bucket size " << mTreeBucketSize);
	}
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
void CLLRTree<NumSensors, NumActuators, NumTransitionVars>::reportBufferedPoint(int newPointIndex)
{
	// Add point index to shadow ANN array
	if (mForward)
		mAnnForwardShadowData[mAnnDataWritePos]		= forwardDataAddr(newPointIndex);
	if (mBackward)
		mAnnBackwardShadowData[mAnnDataWritePos]	= backwardDataAddr(newPointIndex);
	// Increment the writing position for shadow data
	mAnnDataWritePos	= wrapAnnDataPosLarger(mAnnDataWritePos+1);
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
void CLLRTree<NumSensors, NumActuators, NumTransitionVars>::annkSearch(
	SearchDirection dir,
	ANNpoint		q,				// query point
	int				k,				// number of near neighbors to return
	ANNidxArray		nn_idx,			// nearest neighbor array (modified)
	ANNdistArray	dd,				// dist to near neighbors (modified)
	double			eps)		// error bound
{
	switch (dir)
	{
		case sdForward:
			{
				if (mForward)
				{
				  ANNkd_tree_copy tree(*mForwardTree);
				  tree.annkSearch(q, k, nn_idx, dd, eps);
				}
				else
					mLogErrorLn("Forward tree was not created!");
			}
			break;
		case sdBackward:
			{
				if (mBackward)
				{
				  ANNkd_tree_copy tree(*mForwardTree);
					tree.annkSearch(q, k, nn_idx, dd, eps);
				}
				else
					mLogErrorLn("Backward tree was not created!");
			}
			break;
	}
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
int CLLRTree<NumSensors, NumActuators, NumTransitionVars>::getByteSize()
{
	int result = 0;
	if (mForwardTree != NULL)
		result += mForwardTree->getByteSize();
	if (mBackwardTree != NULL)
		result += mBackwardTree->getByteSize();

	return result;
}

// getTimeDiff(): helper function
inline void getTimeDiff(timeval* start, timeval* end, int *secs, int *usecs)
{
	int s = end->tv_sec - start->tv_sec;
	int us = end->tv_usec - start->tv_usec;
	if (us < 0)
	{
		s--;
		us += (int)1E6;
	}
	*secs = s;
	*usecs = us;
}

template<int NumSensors, int NumActuators, int NumTransitionVars>
double CLLRTree<NumSensors, NumActuators, NumTransitionVars>::benchmarkForwardTree(int numTestPoints, int k, double eps)
{
	if (!mForward)
	{
		mLogErrorLn("Cannot perform forward tree benchmark: no forward tree!");
		return 0.0;
	}

	// Init random generator
	gRanrotB.RandomInit(1234);
	ANNidxArray		nn_idx = new ANNidx[k];
	ANNdistArray	dd = new ANNdist[k];

	// Start timing
	timeval tStart, tEnd;
	int	tdifSecs1, tdifUsecs1;
	gettimeofday(&tStart, NULL);
	// Do test
	for (int iPoint=0; iPoint<numTestPoints; iPoint++)
	{
		int pointIndex = gRanrotB.IRandom(0, mNumPoints-1);
		mForwardTree->annkSearch(forwardDataAddr(pointIndex), k, nn_idx, dd, eps);
	}
	// End timing
	gettimeofday(&tEnd, NULL);
	getTimeDiff(&tStart, &tEnd, &tdifSecs1, &tdifUsecs1);
	delete[] nn_idx;
	delete[] dd;
	// Report back the timing
	return (double)tdifSecs1 + ((double)tdifUsecs1)/1E6;
}

#endif /* LLRTREE_H_ */
