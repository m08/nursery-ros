/*
 * llr.h
 *
 *  Created on: Aug 10, 2010
 *      Author: Erik Schuitema
 */

#ifndef LLR_H_
#define LLR_H_

#include <ANN/ANN.h>
#include <vector>
#include <Log2.h>
#include <DataFile.hpp>
#include <randomc.h>
#include <Eigen/Core>
#include <Eigen/SVD>
#include <Eigen/QR>
#include <Eigen/Cholesky>
#include <Statistics.h>
#include <WaitEvent.hpp>
#include <Mutex.hpp>
#include <PosixNonRealTimeThread.hpp>
#include <llrtree.h>

/*
 *  ***** Local Linear Modeling class, using a kd-tree (ANN lib) for fast nearest neighbor searches *****
 *
 * There is one main ring buffer <data> with data points.
 * This buffer contains two parts:
 * <[==============][---]>
 *
 * The [===] part is the data currently inside the search tree.
 * The [---] part is being filled with new data by calling addData().
 * As soon as [---] is full, a new tree is built including the [---] data and [===] data,
 * but when [===]+[---] exceeds the model size, the beginning of [===] is truncated (ring buffer idea).
 *
 * During the building of the new tree, all model queries are denied.
 * The new point index array for the tree is quickly generated before the
 * tree is actually being rebuilt, so that addPoint() hardly has to wait.
 *
 * Note that the buffer is not always full:
 * <[=======][----]      >
 * or can be wrapped:
 * <[-][============][--]>
 *
 *
 * Data is stored in the following order: sensordata_t0 | actuatordata | sensordata_t1 | transitiondata.
 *
 * The main reason to template the class in NumSensors and NumActuators is to create
 * matrix types of fixed (maximum) size, which prevents memory allocations in real-time applications.
 *
 * You can use NumTransitionVars to predict additional variables that belong to the whole transition,
 * i.e., variables that belong to the set (S0,A,S1) and can be predicted by both the forward model
 * (from (S0,A)) and the backward model (from (A,S1)).
 * In practice, this is supposedly valid for the rewards in RL.
 * Scaling is not supported for these extra variables.
 *
 *
 */


// What's the unit in which we should report?
// suByte == suKiloByte*1024 == suMegaByte*1024*1024, etc.
enum ESizeUnit{suByte, suKiloByte, suMegaByte};
inline int getUnitSize(ESizeUnit unit)			{return 1 << 10*(int)unit;}
inline std::string getUnitStr(ESizeUnit unit)	{switch (unit){case suByte: return "b"; case suKiloByte: return "kb"; case suMegaByte: return "Mb"; default: return "?";}}

// File data descriptor (config) used when loading LLR data from a file
class CLLRFileConfig
{
	public:
		int 				mIDCol;				// Column that contains state index/ID's
		int					mIDStride;			// Difference between two subsequent state ID's. E.g., if stride==2, state ID's 4221 and 4223 form a set, but 4221 and 4432 indicate a hole in the data.
		std::vector<int>	mSensorCols;		// Input columns from the start state (at 't_0')
		std::vector<int>	mActuatorCols;		// Output columns from the start state (at 't_0')
		std::vector<int>	mTransitionVarCols;	// Variables that belong to a certain transition (s_t, a_t, s_{t+1})
		bool				mActuatorsFromT1;	// If true, actuator signals from t_1 are used (instead of t_0 (default))
		int					mNumLinesToSkip;	// Number of lines to skip when reading the file
		bool				mReadHeader;		// Import the text header line or not (otherwise, if present, add it to mNumLinesToSkip)

		CLLRFileConfig():
			mIDCol(0),
			mIDStride(1),
			mActuatorsFromT1(false),
			mNumLinesToSkip(0),
			mReadHeader(true)
		{
		}
};

// LLR interface that does not require template parameters
// This interface is useful for classes that use the basic
// functionality of LLR, so that you can create one real LLR model
// and share its pointer with other user classes.
class ILLR
{
	public:
		virtual ~ILLR() {}
		virtual int		numSensors()=0;
		virtual int		numActuators()=0;
		virtual int		numTransitionVars()=0;
		virtual int		numModelInputs()=0;
		virtual int		numModelOutputs()=0;
		virtual int		numDims()=0;
		virtual int		numPoints()=0;

		virtual void	init(bool forward, bool backward, int maxNumPointsInTree, int maxNumPointsToBuffer, const ANNcoord* sensorScales=NULL, const ANNcoord* actuatorScales=NULL)=0;

		// Add a single data point. Note that it is not immediately added to the tree (only after adding treeRebuildTriggerNumPoints points).
		// addPoint() is not thread safe in the sense that you cannot call it from different threads simultaneously
		virtual bool	addPoint(ANNpoint sensorDataT0, ANNpoint actuatorData, ANNpoint sensorDataT1, ANNpoint transitionVarData, int maxWaitMilliseconds=CWaitEvent::WAIT_TIME_INFINITE)=0;

		virtual void	clear()=0;

		// triggerTreeRebuild() triggers the worker thread to rebuild the tree
		// to incorporate the buffered points that were added with addPoint().
		virtual void	triggerTreeRebuild()=0;

		// k is the number of nearest neighbors that the model will use.
		// prediction should have space for at least numModelOutputs() values of type ANNcoord.
		// predint, if provided, will be filled with a 'normalized' prediction interval (meaning, for t = 1)
		// It will also return the degrees of freedom of the student-t distribution (k - d_x) in the t_dof var
		// so that you can calculate the correct t value for your prediction interval. Examples:
		// - for k-d_x= 21 :  50%:0.686, 60%:0.859, 70%:1.063, 80%:1.323, 90%:1.721, 95%:2.080, 98%:2.518, 99%:2.831, 99.5%:3.135, 99.8%:3.527, 99.9%:3.819
		// - for k-d_x= INF:  50%:0.674, 60%:0.842, 70%:1.036, 80%:1.282, 90%:1.645, 95%:1.960, 98%:2.326, 99%:2.576
		//
		// For the forward prediction direction, provide SensorData_{t} and ActuatorData_{t}
		// For the backward prediction direction, provide SensorData_{t+1} and ActuatorData_{t}
		virtual bool	predict(SearchDirection direction, ANNpoint sensorData, ANNpoint actuatorData, ANNpoint predictedSensorData, ANNpoint predictedTransVarData, ANNpoint predintSensorData=NULL, ANNpoint predintTransVarData=NULL, double *t_dof=NULL)=0;
		virtual void	setRidgeRegressionFactor(double factor)=0;
		virtual void	setMaxNeighborDistance(double maxDist)=0;
		virtual void	setIntegers(bool *integers)=0;
};


#define RANK_STAT_LENGTH 18000	// Remember rank over the last RANK_STAT_LENGTH points

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
class CLLR: public ILLR
{
	protected:
		static const int NumModelInputs	= NumSensors + NumActuators;
		static const int NumModelOutputs= NumSensors + NumTransitionVars;
		static const int NumPointDims	= NumSensors + NumActuators + NumSensors + NumTransitionVars;
		typedef	double ModelPrecision;
	private:
		// Define a worker thread class that rebuilds the tree when the buffer with new data points is full
		class CLLRTreeWorker: public PosixNonRealTimeThread
		{
			private:
				CLLR*	mLLR;
			public:
				CLLRTreeWorker(CLLR* llr): PosixNonRealTimeThread("LLR-tree-worker", BaseThread::LOW), mLLR(llr)	{}
				void run()
				{
					// Rebuild the tree while it keeps returning true
					do {} while (mLLR->rebuildTree());
				}
		};

		// Internal matrix definitions for solving Ax=b
		// Three rules: A.rows==b.rows, x.cols==b.cols, x.rows==A.cols
		// A has k rows and NumModelInputs+1 cols
		// x has NumModelInputs+1 rows and NumModelOutputs cols
		// b has k rows and NumModelOutputs cols
		//
		// Fixed storage, dynamic size matrices (otherwise, ColPivHouseholderQR can get into trouble with low k)
		typedef Eigen::Matrix<ModelPrecision, Eigen::Dynamic, Eigen::Dynamic, Eigen::AutoAlign, k, NumModelInputs+1>
			CLLR_A;
		typedef Eigen::Matrix<ModelPrecision, Eigen::Dynamic, Eigen::Dynamic, Eigen::AutoAlign, k, NumModelOutputs >
			CLLR_x;
		typedef Eigen::Matrix<ModelPrecision, k				, NumModelOutputs, Eigen::AutoAlign, k, NumModelOutputs >
			CLLR_b;
		typedef Eigen::Matrix<ModelPrecision, Eigen::Dynamic, Eigen::Dynamic, Eigen::AutoAlign, NumModelInputs+1, k>
			CLLR_AT;
		typedef Eigen::Matrix<ModelPrecision, Eigen::Dynamic, Eigen::Dynamic, Eigen::AutoAlign, NumModelInputs+1, NumModelInputs+1>
			CLLR_ATA;
		typedef Eigen::Matrix<ModelPrecision, Eigen::Dynamic, Eigen::Dynamic, Eigen::AutoAlign, NumModelInputs+1, NumModelOutputs>
			CLLR_ATb;

		// Hat matrix diagonal
		typedef Eigen::Array<ModelPrecision, k, 1>
			CLLR_Hdiag;

		typedef CLLR_A
			CLLR_QR;
		typedef Eigen::Matrix<ModelPrecision, Eigen::Dynamic, Eigen::Dynamic, Eigen::AutoAlign, k, NumModelInputs+1>
			CLLR_SVD;

		typedef Eigen::Matrix<ModelPrecision, 1, NumModelOutputs>
			CLLR_y;	// Result vector
		typedef Eigen::Matrix<ModelPrecision, 1, NumModelInputs+1>
			CLLR_q;	// Query vector
		typedef Eigen::Array<ModelPrecision, k, 1>
			CLLR_w;	// Weights vector
		typedef Eigen::Array<size_t, k, 1>
			CLLR_i;	// Integers



		// Fully dynamic matrices:
		/*
		typedef Eigen::MatrixXd
			CLLR_A;
		typedef Eigen::MatrixXd
			CLLR_x;
		typedef Eigen::MatrixXd
			CLLR_b;
		typedef Eigen::MatrixXd
			CLLR_AT;
		typedef Eigen::MatrixXd
			CLLR_ATA;
		typedef Eigen::MatrixXd
			CLLR_ATb;
		typedef CLLR_A
			CLLR_QR;
		typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::AutoAlign, k, NumModelInputs+1>
			CLLR_SVD;
			*/

	protected:
		CLog2			mLog;

		// Data. Data is stored in the following order: sensordata_t0 | actuatordata | sensordata_t1 | transitiondata
		int				mDataTotalLength;
		ANNcoord*		mData;					// Contiguous data block with the data. All data points in mData are weighted by mScales, except for the TransitionVars.
		ANNcoord		mScales[NumPointDims-NumTransitionVars];	// Scaling factors of the sensor and actuator data. Structure is as follows: S0 | A | S1. Scales for S0 and S1 are the same.
		inline ANNcoord	scales(int index) const		{return mScales[index];}
		inline ANNcoord	scalesS(int index) const	{return mScales[index];}
		inline ANNcoord	scalesA(int index) const	{return mScales[index+NumSensors];}
		ModelPrecision	mRidgeRegressionFactor;	// Factor in ridge regression used in solving (ATA + L)x = b, where L = Identity_matrix*mRidgeRegressionFactor.
		ANNdist			mMaxSqrNeighborDist;	// Negative value means unused

		// mDataWritePos: Position where a new point can be written
		int				mDataWritePos;
		// mNumBufferedPoints is increased when calling addPoint(), readDataFile() or addDataFile()
		// When mNumBufferedPoints == mTreeRebuildTriggerNumPoints, the tree will be rebuilt
		int				mNumBufferedPoints;
		// mMaxNumBufferedPoints: Number of points that can be added before the tree is being rebuilt.
		// This should be smaller than the amount of free space in the buffer if you want to
		// continue writing points while the tree is being rebuilt.
		int				mMaxNumBufferedPoints;

		CWaitEvent		mEvtShouldRebuildTree;	// If true, buildTree() is executed and sets this to false. addPoint() (and only addPoint()) sets this to true after filling the last point.
		CWaitEvent		mEvtDataSpaceAvailable;	// If true, there is space for data. addPoint() - and only addPoint() - sets this to false. rebuildTree() sets this to true
		CSharedMutex	mMtxDataRead;	// The mutex for reading mData (fight between predict() and rebuildTree())
		CMutex        mMtxBufferUpdate; // The mutex for updating the buffer (fight between addpoint() and rebuildTree())


		// Tree
		CLLRTree<NumSensors, NumActuators, NumTransitionVars>	mTree;
		double			mKNNEpsilon;	// Approximate NN search tolerance factor

		// Statistics on the rank
		CSimpleStat		mRankStat;
		
		// Integer variables
		CLLR_i			mIntegers, mIntegerFactors;

		// Data point address request without locking
		inline ANNpoint	dataAddr(int pointIndex)		{return mData + pointIndex*(NumPointDims);}
		inline ANNpoint	dataAddrS0(int pointIndex)		{return mData + pointIndex*(NumPointDims);}
		inline ANNpoint	dataAddrA(int pointIndex)		{return mData + pointIndex*(NumPointDims) + NumSensors;}
		inline ANNpoint	dataAddrS1(int pointIndex)		{return mData + pointIndex*(NumPointDims) + NumSensors + NumActuators;}
		inline ANNpoint	dataAddrT(int pointIndex)		{return mData + pointIndex*(NumPointDims) + NumSensors + NumActuators + NumSensors;}

		// Wrap data position that is possibly smaller than 0
		inline int		wrapDataPosSmaller(int pos)		{return (pos < 0)?pos+mDataTotalLength:pos;}
		// Wrap data position that is possibly larger than mDataTotalLength-1
		inline int		wrapDataPosLarger(int pos)		{return (pos >= mDataTotalLength)?pos-mDataTotalLength:pos;}

		void			clearData();	// Clears data
		void			deleteData();	// Deallocates data

		// The worker function that rebuilds the tree
		// Returns whether it succeeded. If it returns false, the worker thread can stop rebuilding.
		bool			rebuildTree(bool forceRebuild=false);
		bool			mStopRebuilding;
		CLLRTreeWorker	mTreeWorkerThread;

		// Different solvers for solving for x in Ax=b. Returns rank of A.
		int				solveQR(CLLR_A& A, CLLR_x& x, CLLR_b& b);
		int				solveQRCol(CLLR_A& A, CLLR_x& x, CLLR_b& b);
		int				solveLS_QRCol(CLLR_ATA& ATA, CLLR_x& x, CLLR_ATb& ATb);
		int				solveQRFull(CLLR_A& A, CLLR_x& x, CLLR_b& b);
		int				solveSVD(CLLR_A& A, CLLR_x& x, CLLR_b& b);

		inline virtual ModelPrecision	getNeighborWeight(double neighborDistSqr, double hSqr)
		{
			return 1.0;	// No weighing in LLR
		}
		// getBackwardModelData() expects SensorData_{t} and ActuatorData_{t}. Returns whether it was successful.
		bool			getForwardModelData(ANNpoint sensorData, ANNpoint actuatorData, CLLR_A& A, CLLR_b& b, CLLR_q& q, CLLR_w& wSqr);
		// getBackwardModelData() expects SensorData_{t+1} and ActuatorData_{t}. Returns whether it was successful.
		bool			getBackwardModelData(ANNpoint sensorData, ANNpoint actuatorData, CLLR_A& A, CLLR_b& b, CLLR_q& q, CLLR_w& wSqr);

	public:
		// Constructor
		CLLR(
#ifdef ANN_REALTIME	// Pass memory pool parameter in real-time apps
			CMemoryPool* mempool
#endif
			):
			mLog("LLR"),
			mDataTotalLength(0),
			mData(NULL),
			mRidgeRegressionFactor(1E-3),
			mMaxSqrNeighborDist(-1),	// Negative value means unused
			mDataWritePos(0),
			mNumBufferedPoints(0),
			mMaxNumBufferedPoints(0),
			mEvtShouldRebuildTree(false, false),
			mEvtDataSpaceAvailable(true, true),
			mKNNEpsilon(0.1),
			mRankStat(RANK_STAT_LENGTH),
			mStopRebuilding(false),
			mTreeWorkerThread(this)
		{
#ifdef ANN_REALTIME
			annSetRealtimeMemoryPool(mempool);
#endif
			// Scale all variables equally by default
			for (int i=0; i<NumPointDims-NumTransitionVars; i++)
				mScales[i] = 1.0;
			
			// No integers by default	
			for (int ii=0; ii < NumModelOutputs; ++ii)
			{
				mIntegers[ii] = 0;
				mIntegerFactors[ii] = 1;
			}
			
			// Start worker thread
			mTreeWorkerThread.start();
		}

		// Destructor
		virtual ~CLLR()
		{
			// Stop tree building worker thread - this way is a little unconvential
			mStopRebuilding = true;
			mEvtShouldRebuildTree = true;	// This wakes up worker thread
			mTreeWorkerThread.awaitTermination();
			// Deinit
			deinit();
		}

		// Data access
		inline int	numSensors()			{return NumSensors;}
		inline int	numActuators()			{return NumActuators;}
		inline int	numTransitionVars()		{return NumTransitionVars;}
		inline int	numModelInputs()		{return NumModelInputs;}
		inline int	numModelOutputs()		{return NumModelOutputs;}
		inline int	numDims()				{return NumPointDims;}
		inline int	numPoints()				{return mTree.numPoints();}
		const CSimpleStat&	rankStat()		{return mRankStat;}

		// Data points are stored as sensordata_{t} | actuatordata | sensordata_{t+1}
		// The interface to these parts has the naming convention S0 | A | S1
		ANNpoint	scaledDataPoint(int index);
		ANNpoint	scaledDataPointS0(int index);
		ANNpoint	scaledDataPointA(int index);
		ANNpoint	scaledDataPointS1(int index);
		// For getDataPoint(), result has to be numDims() long
		void		getDataPoint(int index, ANNpoint result);
		// For getDataPoint*Inputs, result has to be numModelInputs() long
		void		getDataPointForwardInputs(int index, ANNpoint result);		// Returns S0 | A
		void		getDataPointBackwardInputs(int index, ANNpoint result);		// Returns A | S1
		// For getDataPoint*Outputs, result has to be numModelOutputs() long
		void		getDataPointForwardOutputs(int index, ANNpoint result);		// Returns S1
		void		getDataPointBackwardOutputs(int index, ANNpoint result);	// Returns S0
		void		getScaledPoint(ANNpoint sensorDataT0, ANNpoint actuatorData, ANNpoint sensorDataT1, ANNpoint transitionVarData, ANNpoint result);			// Accepts input point of the form S0|A|S1|T
		// createForwardSearchVector() expects SensorData_{t} and ActuatorData_{t} and creates a result vector with format S|A
		void		createForwardSearchVector(ANNpoint sensorData, ANNpoint actuatorData, ANNpoint result);
		// createBackwardSearchVector() expects ActuatorData_{t} and SensorData_{t+1} and creates a result vector with format A|S
		void		createBackwardSearchVector(ANNpoint sensorData, ANNpoint actuatorData, ANNpoint result);


		// init()
		// Set 'forward' to true if you want forward predictions of the form [s_{t}, a_{t}] -> s_{t+1}
		// Set 'backward' to true if you want backward predictions of the form [s_{t+1}, a_{t}] -> s_{t}
		// scales, if provided, should be of proper length (NumSensors and NumActuators, resp.).
		// There's a relation between the scale factors and the ridge regression factor.
		// If you would tune the ridge regression factor while using unit scale factors, and then would
		// multiply all scales with X, you should multiply the ridge regression factor with X*X.
		// Right now, the scale factors cannot be (easily) altered later, because the scaled data is stored, not the original data.
		void		init(bool forward, bool backward, int maxNumPointsInTree, int maxNumPointsToBuffer, const ANNcoord* sensorScales=NULL, const ANNcoord* actuatorScales=NULL);
		// deinit() is done automatically, but you may call it
		void		deinit();

		// Fill model with data from file
		bool		readDataFile(const std::string& filename, const CLLRFileConfig& config);
		// Add data from file to the model
		bool		addDataFile(const std::string& filename, const CLLRFileConfig& config);
		// Add a single data point. Note that it is not immediately added to the tree (only after adding treeRebuildTriggerNumPoints points).
		// addPoint() is not thread safe in the sense that you cannot call it from different threads simultaneously
		bool		addPoint(ANNpoint sensorDataT0, ANNpoint actuatorData, ANNpoint sensorDataT1, ANNpoint transitionVarData, int maxWaitMilliseconds=CWaitEvent::WAIT_TIME_INFINITE);

		// triggerTreeRebuild() triggers the worker thread to rebuild the tree
		// to incorporate the buffered points that were added with addPoint().
		void		triggerTreeRebuild();

		double		benchmarkTree(int numTestPoints, double eps);

		void		memoryReport(ESizeUnit unit);

		//ANNkd_tree*	tree()	{return mTree;}	// No direct tree access (see e.g. getNeighbors())

		// Clear the model: keep the memory allocated, but erase the data (i.e., set numPoints to 0)
		void		clear();

		// getNeighbors() is merely for debugging
		void		getNeighbors(SearchDirection direction, ANNpoint sensorData, ANNpoint actuatorData, int numNeighbors, ANNidx* neighborIndices, ANNdist* neighborDistances, double epsilon);

		// k is the number of nearest neighbors that the model will use.
		// prediction should have space for at least numModelOutputs() values of type ANNcoord.
		// predint, if provided, will be filled with a 'normalized' prediction interval (meaning, for t = 1)
		// It will also return the degrees of freedom of the student-t distribution (k - d_x) in the t_dof var (floating point for some reason..)
		// so that you can calculate the correct t value for your prediction interval. Examples:
		// - for k-d_x= 21 :  50%:0.686, 60%:0.859, 70%:1.063, 80%:1.323, 90%:1.721, 95%:2.080, 98%:2.518, 99%:2.831, 99.5%:3.135, 99.8%:3.527, 99.9%:3.819
		// - for k-d_x= INF:  50%:0.674, 60%:0.842, 70%:1.036, 80%:1.282, 90%:1.645, 95%:1.960, 98%:2.326, 99%:2.576
		//
		// For the forward prediction direction, provide SensorData_{t} and ActuatorData_{t}
		// For the backward prediction direction, provide SensorData_{t+1} and ActuatorData_{t}
		bool		predict(SearchDirection direction, ANNpoint sensorData, ANNpoint actuatorData, ANNpoint predictedSensorData, ANNpoint predictedTransVarData, ANNpoint predintSensorData=NULL, ANNpoint predintTransVarData=NULL, ModelPrecision *t_dof=NULL);
		void		setRidgeRegressionFactor(double factor)	{mRidgeRegressionFactor = factor;}
		// If you want predict() to return false when neighbors are too far away, set a maximum neighbor distance with setMaxNeighborDistance()
		// The distance is calculated using the weighted inputs.
		void		setMaxNeighborDistance(double maxDist)			{mMaxSqrNeighborDist = maxDist*maxDist;}
		void		setIntegers(bool *integers)
		{
			size_t factor = 1;
			for (int ii=0; ii < NumModelOutputs; ++ii)
			{
				mIntegers[ii] = integers[ii];
				mIntegerFactors[ii] = factor;
				
				// NOTE: Should be dependent on integer range
				if (mIntegers[ii])
					factor *= 100;
			}
		}
};

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
class CLWR: public CLLR<NumSensors, NumActuators, NumTransitionVars, k>
{
	protected:
		typedef typename CLLR<NumSensors, NumActuators, NumTransitionVars, k>::ModelPrecision ModelPrecision;

		inline virtual ModelPrecision	getNeighborWeight(double neighborDistSqr, double hSqr)
		{
			// Return sqrt(K(d/h))
			// with Gaussian kernel K(x) = exp(-x^2)
			// Note that neighborDistSqr == d^2 and -(d/h)^2 == -neighborDistSqr/hSqr
			return std::sqrt(std::exp(-neighborDistSqr/hSqr));
		}

	public:
		// Constructor
		CLWR(
		#ifdef ANN_REALTIME	// Pass memory pool parameter in real-time apps
			CMemoryPool* mempool
		#endif
			):
		CLLR<NumSensors, NumActuators, NumTransitionVars, k>(
		#ifdef ANN_REALTIME	// Pass memory pool parameter in real-time apps
			mempool
		#endif
		)	{}

};


template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::clearData()
{
	mTree.clear();
	mDataWritePos		= 0;
	mNumBufferedPoints	= 0;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::deleteData()
{
	if (mData != NULL)
	{
		delete[] mData;
		mData = NULL;
	}
}
// ************* DATA RETRIEVAL *************
template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
ANNpoint CLLR<NumSensors, NumActuators, NumTransitionVars, k>::scaledDataPoint(int index)
{
	return mData + index*(NumPointDims);
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
ANNpoint CLLR<NumSensors, NumActuators, NumTransitionVars, k>::scaledDataPointS0(int index)
{
	return mData + index*(NumPointDims);
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
ANNpoint CLLR<NumSensors, NumActuators, NumTransitionVars, k>::scaledDataPointA(int index)
{
	return mData + index*(NumPointDims) + NumSensors;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
ANNpoint CLLR<NumSensors, NumActuators, NumTransitionVars, k>::scaledDataPointS1(int index)
{
	return mData + index*(NumPointDims) + NumSensors + NumActuators;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getDataPoint(int index, ANNpoint result)
{
	ANNpoint sp = scaledDataPoint(index);
	for (int i=0; i<NumPointDims-NumTransitionVars; i++)
		result[i] = sp[i]/scales(i);
	// The transition vars are not scaled
	for (int i=NumPointDims-NumTransitionVars; i<NumPointDims; i++)
		result[i] = sp[i];
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getDataPointForwardInputs(int index, ANNpoint result)
{
	ANNpoint sp = scaledDataPoint(index);
	for (int i=0; i<NumModelInputs; i++)
		result[i] = sp[i]/scales(i);
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getDataPointForwardOutputs(int index, ANNpoint result)
{
	ANNpoint sp = scaledDataPointS1(index);
	for (int i=0; i<NumModelOutputs-NumTransitionVars; i++)
		result[i] = sp[i]/scalesS(i);
	// The (non-scaled) transition variables are located behind the S1 data
	for (int i=NumModelOutputs-NumTransitionVars; i<NumModelOutputs; i++)
		result[i] = sp[i];
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getDataPointBackwardInputs(int index, ANNpoint result)
{
	ANNpoint sp = scaledDataPointA(index);
	for (int i=0; i<NumModelInputs; i++)
		result[i] = sp[i]/scalesA(i);
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getDataPointBackwardOutputs(int index, ANNpoint result)
{
	ANNpoint sp = scaledDataPointS0(index);
	for (int i=0; i<NumModelOutputs-NumTransitionVars; i++)
		result[i] = sp[i]/scalesS(i);
	ANNpoint spT = dataAddrT(index);
	for (int i=NumModelOutputs-NumTransitionVars, j=0; i<NumModelOutputs; i++, j++)
		result[i] = spT[j];
}

// This function scales a given non-scaled point and puts the result in 'result'
template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getScaledPoint(ANNpoint sensorDataT0, ANNpoint actuatorData, ANNpoint sensorDataT1, ANNpoint transitionVarData, ANNpoint result)
{
	int iRes=0;
	for (int i=0; i<NumSensors; i++)
		result[iRes++] = sensorDataT0[i]*scalesS(i);
	for (int i=0; i<NumActuators; i++)
		result[iRes++] = actuatorData[i]*scalesA(i);
	for (int i=0; i<NumSensors; i++)
		result[iRes++] = sensorDataT1[i]*scalesS(i);
	// The transition variables are not scaled
	for (int i=0; i<NumTransitionVars; i++)
		result[iRes++] = transitionVarData[i];
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::createForwardSearchVector(ANNpoint sensorData, ANNpoint actuatorData, ANNpoint result)
{
	// Output vector with format S|A
	ANNpoint rs = result;
	ANNpoint ra = result + NumSensors;
	for (int i=0; i<NumSensors; i++)
		rs[i] = sensorData[i]*scalesS(i);
	for (int i=0; i<NumActuators; i++)
		ra[i] = actuatorData[i]*scalesA(i);
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::createBackwardSearchVector(ANNpoint sensorData, ANNpoint actuatorData, ANNpoint result)
{
	// Output vector with format A|S
	ANNpoint ra = result;
	ANNpoint rs = result + NumActuators;
	for (int i=0; i<NumActuators; i++)
		ra[i] = actuatorData[i]*scalesA(i);
	for (int i=0; i<NumSensors; i++)
		rs[i] = sensorData[i]*scalesS(i);
}

// ************** INIT/DEINIT ***************
template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::init(bool forward, bool backward, int maxNumPointsInTree, int maxNumPointsToBuffer, const ANNcoord* sensorScales, const ANNcoord* actuatorScales)
{
	deinit();
	mMaxNumBufferedPoints	= maxNumPointsToBuffer;
	if (mMaxNumBufferedPoints > maxNumPointsInTree)
	{
		mMaxNumBufferedPoints = maxNumPointsInTree;
		mLogWarningLn("treeRebuildTriggerNumPoints cannot be larger than maxNumPointsInTree; number was reduced.");
	}
	mDataTotalLength				= maxNumPointsInTree + mMaxNumBufferedPoints;

	mData			= new ANNcoord[mDataTotalLength*NumPointDims];
	mTree.init(mData, maxNumPointsInTree, forward, backward);

	// Set the scale factors if provided. Format of mScales is S0 | A | S1.
	if (sensorScales != NULL)
	{
		memcpy(mScales,							sensorScales, NumSensors*sizeof(ANNcoord));
		memcpy(mScales+NumSensors+NumActuators,	sensorScales, NumSensors*sizeof(ANNcoord));
	}
	if (actuatorScales != NULL)
		memcpy(mScales+NumSensors, actuatorScales, NumActuators*sizeof(ANNcoord));
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::deinit()
{
	mTree.deinit();
	deleteData();
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::clear()
{
	clearData();
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
bool CLLR<NumSensors, NumActuators, NumTransitionVars, k>::readDataFile(const std::string& filename, const CLLRFileConfig& config)
{
	clearData();
	return addDataFile(filename, config);
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
bool CLLR<NumSensors, NumActuators, NumTransitionVars, k>::addDataFile(const std::string& filename, const CLLRFileConfig& config)
{
	//TODO: temporarily set new point buffer to the size of data we are going to add and wait for the tree to be rebuilt
	int oldNumPoints = numPoints();
	// Check if enough data is provided
	if ((config.mSensorCols.size() != NumSensors))
	{
		mLogErrorLn("Config for file \"" << filename << "\" has wrong number of sensor columns: " << config.mSensorCols.size() << " instead of " << NumSensors);
		return false;
	}
	if ((config.mActuatorCols.size() != NumActuators))
	{
		mLogErrorLn("Config for file \"" << filename << "\" has wrong number of actuator columns: " << config.mActuatorCols.size() << " instead of " << NumActuators);
		return false;
	}
	if ((config.mTransitionVarCols.size() != NumTransitionVars))
	{
		mLogErrorLn("Config for file \"" << filename << "\" has wrong number of transition variable columns: " << config.mTransitionVarCols.size() << " instead of " << NumTransitionVars);
		return false;
	}

	CDataFile datafile;
	datafile.read(filename, config.mNumLinesToSkip, config.mReadHeader);
	// Report on data columns to import
	mLogInfo("Sensor columns: ");
	for (uint32_t iS=0; iS<config.mSensorCols.size(); iS++)
		mLogInfo(datafile.colHeader(config.mSensorCols[iS]) << ", ");
	mLogInfoLn("");
	mLogInfo("Actuator columns (taken from time=" << (config.mActuatorsFromT1?"t+1":"t") << "): ");
	for (uint32_t iA=0; iA<config.mActuatorCols.size(); iA++)
		mLogInfo(datafile.colHeader(config.mActuatorCols[iA]) << ", ");
	mLogInfoLn("");
	mLogInfo("Transition variable columns (taken from time=t+1): ");
	for (uint32_t iT=0; iT<config.mTransitionVarCols.size(); iT++)
		mLogInfo(datafile.colHeader(config.mTransitionVarCols[iT]) << ", ");
	mLogInfoLn("");

	// Transfer data from datafile to point array
	uint32_t row=0;
	int actRowShift = (int)config.mActuatorsFromT1;	// Actuator row shift
	while (row < (datafile.data().size() - 1))
	{
		if (datafile.dataValue(config.mIDCol, row+1) - datafile.dataValue(config.mIDCol, row) == config.mIDStride)
		{
			// We found a valid data set! Add an LLR data point if there's space
			//if ((mNumPoints < mDataTotalLength)) //TODO: check and re-enable
			{
				ANNcoord sensorDataT0[NumSensors];
				ANNcoord actuatorData[NumActuators];
				ANNcoord sensorDataT1[NumSensors];
				ANNcoord transitionVarData[NumTransitionVars];
				// Add sensor data for T0
				for (uint32_t iS=0; iS<config.mSensorCols.size(); iS++)
					sensorDataT0[iS] = datafile.dataValue(config.mSensorCols[iS], row);
				// Add actuator data for either T0 or T1, depending on actRowShift
				for (uint32_t iA=0; iA<config.mActuatorCols.size(); iA++)
					actuatorData[iA] = datafile.dataValue(config.mActuatorCols[iA], row + actRowShift);
				// Add sensor data for T1
				for (uint32_t iS=0; iS<config.mSensorCols.size(); iS++)
					sensorDataT1[iS] = datafile.dataValue(config.mSensorCols[iS], row + 1);
				// Add transition variable data for T1
				for (uint32_t iT=0; iT<config.mTransitionVarCols.size(); iT++)
					transitionVarData[iT] = datafile.dataValue(config.mTransitionVarCols[iT], row + 1);

				addPoint(sensorDataT0, actuatorData, sensorDataT1, transitionVarData);
			}
//TODO		else
//			{
//				mLogWarningLn("Data file \"" << filename << "\" contains more data than the maximum of " << mDataTotalLength << " points!");
//				break;
//			}
		}
		else
			mLogInfoLn("Detected data pause between ID's " << datafile.dataValue(config.mIDCol, row) << " and " << datafile.dataValue(config.mIDCol, row+1) << ". Continuing...");
		row++;
	}
	// Force a tree rebuild to include all the read points
	// By calling rebuildTree(true) directly, we know for sure that it ended upon function exit
	// mMtxDataRead indicates whether rebuildTree() was already busy due to us
	mMtxDataRead.wrlock();	// Wait till we get access
	mMtxDataRead.unlock();	// Unlock (nobody else will be calling rebuildTree or query except for us)
	rebuildTree(true);		// Force rebuilding the tree

	// Report newly added points
	mLogInfoLn("Read " << numPoints() - oldNumPoints << " new data points from \"" << filename << "\"");
	return true;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
bool CLLR<NumSensors, NumActuators, NumTransitionVars, k>::rebuildTree(bool forceRebuild)
{
	if (!forceRebuild)
	{
		// Wait till there is a request to rebuild the tree
		mEvtShouldRebuildTree.wait();
	}
	// Check whether we should stop
	if (mStopRebuilding)
		return false;

	// Lock all queries, because we are going to modify the mAnnData array
	mMtxDataRead.wrlock();

	mMtxBufferUpdate.lock();
	mTree.updateAndSwapBuffers(mNumBufferedPoints);
	// Reset mNumBufferedPoints since we processed them all
	mNumBufferedPoints = 0;
	mMtxBufferUpdate.unlock();

	// We've done the book keeping. Signal that there is new space for data.
	mEvtDataSpaceAvailable = true;

	// Rebuild the actual tree
	mTree.recreateTree();

	// Release the query/read lock
	mMtxDataRead.unlock();
	return true;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
bool CLLR<NumSensors, NumActuators, NumTransitionVars, k>::addPoint(ANNpoint sensorDataT0, ANNpoint actuatorData, ANNpoint sensorDataT1, ANNpoint transitionVarData, int maxWaitMilliseconds)
{
	// Wait for space in the data array.
	// Note that we don't have to lock since we write in the buffered area.
	if (!mEvtDataSpaceAvailable.wait(maxWaitMilliseconds))
		return false;

	// Copy new point to the data array in the buffering area; use scaling
	getScaledPoint(sensorDataT0, actuatorData, sensorDataT1, transitionVarData, dataAddr(mDataWritePos));

	mMtxBufferUpdate.lock();
	// Report it to the tree
	mTree.reportBufferedPoint(mDataWritePos);
	// Increment the writing position for mData
	mDataWritePos		= wrapDataPosLarger(mDataWritePos+1);
	// Increment number of added points
	mNumBufferedPoints++;

	// If we added enough points, trigger tree rebuilding
	// to add the buffered points to the tree and to make some data space
	if (mNumBufferedPoints == mMaxNumBufferedPoints)
	{
		mEvtDataSpaceAvailable = false;	// Flag that there is no data left. Only rebuildTree() can solve this.
		mEvtShouldRebuildTree = true;	// Trigger the tree to rebuild so that new space becomes available.
	}
	mMtxBufferUpdate.unlock();

	return true;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::triggerTreeRebuild()
{
	mEvtShouldRebuildTree = true;
}

// Different solvers for solving for x in Ax=b. Returns rank of A.
template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
int CLLR<NumSensors, NumActuators, NumTransitionVars, k>::solveQR(CLLR_A& A, CLLR_x& x, CLLR_b& b)
{
	// Solve with QR
	//Eigen::QR< Eigen::Matrix<double, k, NumInputDims+1> > qrOfA(A);	// Eigen2
	//qrOfA.solve(b, &x);	// Eigen2
	//A.qr().solve(b, &x);// Eigen2
	Eigen::HouseholderQR< CLLR_QR > qrOfA(A);	// Eigen3, Householder
	//Eigen::ColPivHouseholderQR< CLLR_QR > qrOfA(A);	// Eigen3, ColPivHouseholder
	//Eigen::FullPivHouseholderQR< Eigen::Matrix<double, k, NumInputDims+1> > qrOfA(A);	// Eigen3, FullPivHouseholder
	x = qrOfA.solve(b);		// Eigen3
	return NumModelInputs;	// Don't estimate the rank
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
int CLLR<NumSensors, NumActuators, NumTransitionVars, k>::solveQRCol(CLLR_A& A, CLLR_x& x, CLLR_b& b)
{
	Eigen::ColPivHouseholderQR< CLLR_QR > qrOfA(A);	// Eigen3, ColPivHouseholder
	x = qrOfA.solve(b);		// Eigen3
	return qrOfA.rank();
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
int CLLR<NumSensors, NumActuators, NumTransitionVars, k>::solveLS_QRCol(CLLR_ATA& ATA, CLLR_x& x, CLLR_ATb& ATb)
{
	Eigen::ColPivHouseholderQR< CLLR_ATA > qrOfATA(ATA);	// Eigen3, ColPivHouseholder
	x = qrOfATA.solve(ATb);		// Eigen3
	return qrOfATA.rank();
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
int CLLR<NumSensors, NumActuators, NumTransitionVars, k>::solveQRFull(CLLR_A& A, CLLR_x& x, CLLR_b& b)
{
	// Solve with QR
	//Eigen::QR< Eigen::Matrix<double, k, NumInputDims+1> > qrOfA(A);	// Eigen2
	//qrOfA.solve(b, &x);	// Eigen2
	//A.qr().solve(b, &x);// Eigen2
	//Eigen::HouseholderQR< Eigen::Matrix<double, k, NumInputDims+1> > qrOfA(A);	// Eigen3, Householder
	//Eigen::ColPivHouseholderQR< CLLR_QR > qrOfA(A);	// Eigen3, ColPivHouseholder
	Eigen::FullPivHouseholderQR< CLLR_QR > qrOfA(A);	// Eigen3, FullPivHouseholder
	x = qrOfA.solve(b);		// Eigen3
	return qrOfA.rank();
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
int CLLR<NumSensors, NumActuators, NumTransitionVars, k>::solveSVD(CLLR_A& A, CLLR_x& x, CLLR_b& b)
{
	// Solve with SVD
	Eigen::SVD< CLLR_SVD > svdOfA(A);
	//svdOfA.solve(b, &x);	// Eigen2
	//A.svd().solve(b, &x);// Eigen2
	x = svdOfA.solve(b);	// Eigen3
	//x = A.svd().solve(b);// Eigen3
	return NumModelInputs;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getNeighbors(SearchDirection direction, ANNpoint sensorData, ANNpoint actuatorData, int numNeighbors, ANNidx* neighborIndices, ANNdist* neighborDistances, double epsilon)
{
	// Create the scaled search vector
	ANNcoord	wsv[NumModelInputs];
	// We are using the tree and its data --> use the read lock, but don't wait for it
	if (!(mMtxDataRead.tryrdlock() == 0))
		return;

	switch (direction)
	{
    case sdForward:
    {
      createForwardSearchVector(sensorData, actuatorData, wsv);
      mTree.annkSearch(sdForward, wsv, numNeighbors, neighborIndices, neighborDistances, epsilon);
      break;
    }
    case sdBackward:
    {
      createBackwardSearchVector(sensorData, actuatorData, wsv);
      mTree.annkSearch(sdBackward, wsv, numNeighbors, neighborIndices, neighborDistances, epsilon);
      break;
    }
	}
	mMtxDataRead.unlock();
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
bool CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getForwardModelData(ANNpoint sensorData, ANNpoint actuatorData, CLLR_A& A, CLLR_b& b, CLLR_q& q, CLLR_w& wSqr)
{
	// Create the scaled search vector
	ANNcoord	wsv[NumModelInputs];
	createForwardSearchVector(sensorData, actuatorData, wsv);
	// We are using the tree and its data --> acquire the data read lock, but don't wait for it
	if (!(mMtxDataRead.tryrdlock() == 0))
		return false;

	// Check if there are enough neighbors (after locking)
	if (mTree.numForwardPoints() < 2*k)
	{
		// Release the data read lock
		mMtxDataRead.unlock();
		return false;
	}

	ANNidx		nn_idx[k];
	ANNdist		dd[k];

	mTree.annkSearch(sdForward, wsv, k, nn_idx, dd, mKNNEpsilon);
	//printf("dd_max: %.2f, ", dd[k-1]);

	// Check maximum neighbor distance
	if (mMaxSqrNeighborDist > 0)
		if (dd[k-1] > mMaxSqrNeighborDist)
		{
			// Release the data read lock
			mMtxDataRead.unlock();
			return false;
		}

	// We weigh the neighbors with a Gaussian kernel using nearest neighbor bandwidth selection,
	// in which h is set to be the distance to the kth nearest data point
	// In the mean time, we collect the (squared) weights.
	ANNdist hSqr = dd[k-1];
	for (int iNN=0; iNN<k; iNN++)
	{
		ModelPrecision w = getNeighborWeight(dd[iNN], hSqr);
		//double w = 1.0;
		wSqr[iNN] = w*w;
		// TODO: maybe this can be done more efficiently (but probably this memcopy is not the bottleneck)
		// Inputs to A matrix. Append 1.0 to all input vectors
		for (int ii=0; ii<NumModelInputs; ii++)
			A(iNN, ii) = w*dataAddrS0(nn_idx[iNN])[ii];
		A(iNN, NumModelInputs) = w*1.0;
		// Outputs to b matrix. Because the transition variables are located behind S1, we can do this in one loop.
		for (int io=0; io<NumModelOutputs; io++)
			b(iNN, io) = w*dataAddrS1(nn_idx[iNN])[io];
	}

	// We are done with the tree data --> release the data read lock
	mMtxDataRead.unlock();

	// Copy search point data //TODO: can this be done more efficiently?
	for (int ii=0; ii<NumModelInputs; ii++)
		q(0, ii) = wsv[ii];
	q(0, NumModelInputs) = 1.0;

	return true;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
bool CLLR<NumSensors, NumActuators, NumTransitionVars, k>::getBackwardModelData(ANNpoint sensorData, ANNpoint actuatorData, CLLR_A& A, CLLR_b& b, CLLR_q& q, CLLR_w& wSqr)
{
	// Create the scaled search vector
	ANNcoord	wsv[NumModelInputs];
	createBackwardSearchVector(sensorData, actuatorData, wsv);
	// We are using the tree and its data --> acquire the data read lock, but don't wait for it
	if (!(mMtxDataRead.tryrdlock() == 0))
		return false;

	// Check if there are enough neighbors (after locking)
	if (mTree.numBackwardPoints() < k)
	{
		// Release the data read lock
		mMtxDataRead.unlock();
		return false;
	}

	ANNidx		nn_idx[k];
	ANNdist		dd[k];

	mTree.annkSearch(sdBackward, wsv, k, nn_idx, dd, mKNNEpsilon);
	//printf("dd_max: %.2f, ", dd[k-1]);

	// Check maximum neighbor distance
	//mLogInfoLn("Max neighbor distance: " << dd[k-1]);
	if (mMaxSqrNeighborDist > 0)
		if (dd[k-1] > mMaxSqrNeighborDist)
		{
			// Release the data read lock
			mMtxDataRead.unlock();
			return false;
		}

	// We weigh the neighbors with a Gaussian kernel using nearest neighbor bandwidth selection,
	// in which h is set to be the distance to the kth nearest data point
	// In the mean time, we collect the (squared) weights.
	ANNdist hSqr = dd[k-1];
	for (int iNN=0; iNN<k; iNN++)
	{
		ModelPrecision w = getNeighborWeight(dd[iNN], hSqr);
		//double w = 1.0;
		wSqr[iNN] = w*w;
		// TODO: maybe this can be done more efficiently (but probably this memcopy is not the bottleneck)
		// Inputs to A matrix. Append 1.0 to all input vectors
		for (int ii=0; ii<NumModelInputs; ii++)
			A(iNN, ii) = w*dataAddrA(nn_idx[iNN])[ii];
		A(iNN, NumModelInputs) = w*1.0;
		// Outputs to b matrix. Copy transition variables data in separate loop
		for (int io=0; io<NumModelOutputs-NumTransitionVars; io++)
			b(iNN, io) = w*dataAddrS0(nn_idx[iNN])[io];
		for (int io=NumModelOutputs-NumTransitionVars, itt=0; io<NumModelOutputs; io++, itt++)
			b(iNN, io) = w*dataAddrT(nn_idx[iNN])[itt];
	}

	// We are done with the tree data --> release the data read lock
	mMtxDataRead.unlock();

	// Copy search point data //TODO: can this be done more efficiently?
	for (int ii=0; ii<NumModelInputs; ii++)
		q(0, ii) = wsv[ii];
	q(0, NumModelInputs) = 1.0;

	return true;
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
bool CLLR<NumSensors, NumActuators, NumTransitionVars, k>::predict(SearchDirection direction, ANNpoint sensorData, ANNpoint actuatorData, ANNpoint predictedSensorData, ANNpoint predictedTransVarData, ANNpoint predintSensorData, ANNpoint predintTransVarData, ModelPrecision *t_dof)
{
	// Create linear model and make prediction
	CLLR_A	A(k, NumModelInputs+1);
	CLLR_x	x(NumModelInputs+1, (int)NumModelOutputs);	// Cast to (int) to make NumModelOutputs into a temporary
	CLLR_b	b(k, (int)NumModelOutputs);
	CLLR_q	sv;
	CLLR_w	weightsSqr;	// Squared weights vector

	bool modelDataSet=false;
	switch(direction)
	{
		case sdForward:
			modelDataSet = getForwardModelData(sensorData, actuatorData, A, b, sv, weightsSqr);
			break;
		case sdBackward:
			modelDataSet = getBackwardModelData(sensorData, actuatorData, A, b, sv, weightsSqr);
			break;
	}

	if (!modelDataSet)
		return false;
	
	if (mIntegerFactors[NumModelOutputs-1] > 1)
	{
		// Determine integer group of neighbors
		size_t neighbor_groups[k], group_ids[k], total_groups=0;
		ModelPrecision group_weights[k];
		
		for (int ii=0; ii < k; ++ii)
		{
			ModelPrecision weight = A(ii, NumModelInputs);
			weight *= 2;
			
			neighbor_groups[ii] = 0;
			for (int jj=0; jj < NumModelOutputs; ++jj)
			{
				if (mIntegers[jj])
					neighbor_groups[ii] += mIntegerFactors[jj]*round(b(ii, jj)/weight);
			}
			
			size_t jj;
			for (jj=0; jj < total_groups; ++jj)
			if (group_ids[jj] == neighbor_groups[ii])
			{
				group_weights[jj] += weight;
				break;
			}
			    
			if (jj==total_groups)
			{
				group_ids[jj] = neighbor_groups[ii];
				group_weights[jj] = weight;
				total_groups++;
			}
		}

		// Select highest probability group
		size_t group_id = group_ids[0];
		ModelPrecision group_weight=group_weights[0];
		
		for (size_t ii=1; ii < total_groups; ++ii)
		{
			if (group_weights[ii] > group_weight)
			{
				group_weight = group_weights[ii];
				group_id = group_ids[ii];
			}
		}

		// Decrease weights of other groups
		for (int ii=0; ii < k; ++ii)
		{
			if (neighbor_groups[ii] != group_id)
			{
				// NOTE: 100 is a bit arbitrary...
				for (int jj=0; jj < NumModelInputs+1; ++jj)
					A(ii, jj) = A(ii, jj)/100;
				for (int jj=0; jj < NumModelOutputs; ++jj)
					b(ii, jj) = b(ii, jj)/100;
				weightsSqr[ii] = weightsSqr[ii]/(100*100);
			}
		}
	}
		
//	std::cout << "A: " << A << std::endl;
//	std::cout << "b: " << b << std::endl;

	// Solve with QR
	//int rankA = solveQRCol(A, x, b);

	// Lambda matrix with small diagonal elements for ridge regression
	CLLR_ATA	L(Eigen::Matrix<ModelPrecision, NumModelInputs+1, NumModelInputs+1>::Identity()*mRidgeRegressionFactor);
	// Solve with ATA*x = ATb with QR
	CLLR_AT		At	= A.transpose();
	CLLR_ATA	ATAL	= At*A + L;
	//CLLR_ATb	ATb	= At*b;
	//Eigen::ColPivHouseholderQR< CLLR_ATA > decompATAL(ATAL);	// Eigen3, ColPivHouseholder
	//Eigen::FullPivHouseholderQR< CLLR_ATA > decompATAL(ATAL);
	Eigen::LLT<CLLR_ATA> decompATAL(ATAL);
	if (decompATAL.info() != Eigen::Success)
		return false;
	//x = decompATAL.solve(At*b);
	CLLR_AT r = decompATAL.solve(At);
	if (decompATAL.info() != Eigen::Success)
		return false;

	x = r*b;

//	std::cout << "A:\n" << A;
//	std::cout << "x:\n" << x;
//	std::cout << "b:\n" << b;

	//int rankA =  qrOfATAL.rank();
	//int rankA = 21;

	// Solve with SVD
	//int rankA = solveJacobiSVD(A, x, b);

	// Calculate result vector rv and copy it ('de-scaled') to prediction[]
	// Since sensor scalings are equal for T0 and T1, we don't have to distinguish
	// between the forward and backward model here when 'de-scaling'
	CLLR_y rv;

	rv = sv*x;

	for (int i=0; i<NumSensors; i++)
		predictedSensorData[i] = rv.row(0)[i]/scalesS(i);
	// The transition variables are not scaled
	for (int i=0; i<NumTransitionVars; i++)
		predictedTransVarData[i] = rv.row(0)[NumSensors+i];

	// Get the diagonal of the hat matrix
	CLLR_Hdiag Hdiag((A*r).diagonal().array());

	// A measure of the distance between a data point, xi, and the centroid of the X-space is the data point's associated diagonal element hi in the hat matrix.
	// Calculate the largest distance
	//ModelPrecision hdistMax = Hdiag.maxCoeff();

	// How do we find (sv * (A^T * A)^-1 * sv^T) for the confidence bounds and independent variable hull (IVH)?
	// The solvers are of the form Ax=b (solving for x)
	// When A = D^T*D, the A matrix is already square and can directly be inverted
	// So we can write x = (D^T * D)^-1 * b, or in our case p = (A^T * A) * sv^t
	// Then our answer is sv*p and solving p using Cholesky decomposition.

	// With llt()
	//double fact = 1.0 + sv*ATA.llt().solve(sv.transpose());
	// With existing decomposition of ATA or ATAL (ridge regression)
	ModelPrecision hdist = sv*decompATAL.solve(sv.transpose());

	// Prediction interval, if desired (that is, if predint is provided)
	if (predintSensorData != NULL)
	{
		//double p_LWR = (double)rankA;
		ModelPrecision n_LWR = weightsSqr.sum();
		ModelPrecision p_LWR = (weightsSqr * Hdiag).sum();
//		mRankStat.addValue(p_LWR);
		if (n_LWR > p_LWR)
		{
			// We can calculate the prediction interval
			// Calculate the variance ss (or sigma squared). Note: point 2101 gives ss(4)==0
			b -= A*x;	// Now b contains -r
			Eigen::Matrix<ModelPrecision, 1, NumModelOutputs> ss = b.array().square().matrix().colwise().sum();
			ss *= 1.0/(ModelPrecision)(n_LWR-p_LWR);
			//mLog(llInfo) << "Variance: " << ss << endl;
			//Eigen::Matrix<double, NumModelInputs+1, NumModelInputs+1> ATA = A.transpose()*A;
			//mLog(llInfo) << "Search vector: " << sv << endl;

			ModelPrecision fact = 1.0 + hdist;

			//mLog(llInfo) << "Factor: " << fact << endl;
			Eigen::Matrix<ModelPrecision, 1, NumModelOutputs> stdev = (fact*ss).array().sqrt();
			//mLog(llInfo) << "Stdev: " << endl << stdev << endl;

//			mLogNoticeLn("stdev:\n" << stdev);

			// Copy prediction interval output (also scale it for sensors)
			for (int iPi=0; iPi<NumSensors; iPi++)
				predintSensorData[iPi] = stdev(0, iPi)/scalesS(iPi);
			if (predintTransVarData != NULL)
				for (int iPi=0; iPi<NumTransitionVars; iPi++)
					predintTransVarData[iPi] = stdev(0, NumSensors+iPi);
			if (t_dof != NULL)
				*t_dof = n_LWR-p_LWR;
		}
		else
		{
			memset(predintSensorData, 0, NumSensors*sizeof(predintSensorData[0]));
			if (predintTransVarData != NULL)
				memset(predintTransVarData, 0, NumTransitionVars*sizeof(predintTransVarData[0]));
			if (t_dof != NULL)
				*t_dof = n_LWR-p_LWR;
			mLogWarningLn("n_LWR <= p_LWR while calculating prediction intervals");
			return false;
		}
	}
	return true;//hdist <= hdistMax;	// Turn the calculation on around line 1033 if you wanna use this.
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
void CLLR<NumSensors, NumActuators, NumTransitionVars, k>::memoryReport(ESizeUnit unit)
{
	// Collect memory size
	// Raw data
	int memDataMax		= (NumPointDims)*mDataTotalLength*sizeof(ANNcoord);
	int memData			= (NumPointDims)*numPoints()*sizeof(ANNcoord);
	int memPtrData		= numPoints()*sizeof(ANNpoint);
	int memPtrDataMax	= mDataTotalLength*sizeof(ANNpoint);
	mLogInfoLn("Model allocated " << (memDataMax+memPtrDataMax)/getUnitSize(unit) << getUnitStr(unit) << " for " << mDataTotalLength << " points, using " << numPoints() << " (" << (memData + memPtrData)/getUnitSize(unit) << getUnitStr(unit) << ")");

	// Tree data
	mLogInfoLn("Tree uses " << mTree.getByteSize()/getUnitSize(unit) << getUnitStr(unit));
}

template<int NumSensors, int NumActuators, int NumTransitionVars, int k>
double CLLR<NumSensors, NumActuators, NumTransitionVars, k>::benchmarkTree(int numTestPoints, double eps)
{
	// Report back the timing of the forward tree
	return mTree.benchmarkForwardTree();
}

#endif /* LLR_H_ */
