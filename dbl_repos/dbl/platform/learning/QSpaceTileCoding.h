/*
 * QSpaceTileCoding.h
 *
 *  Created on: Jul 28, 2009
 *      Author: Erik Schuitema
 */

#ifndef QSPACETILECODING_H_
#define QSPACETILECODING_H_

#include "QSpace.h"
#include "TileCodingTrace.h"
#include <tilecoding.h>
#include <half.h>

#define TILE_READ	true	// Read-only = true
#define TILE_WRITE	false

// Define this if you want to use extended tile coding features
//#define EXTENDED_FEATURES

class CQValueTileCoding
{
	public:
		_QPrecision		QValue;
		_IndexPrecision Tiles[MAX_NUM_TILINGS];	// The number of tilings might be templated, but this makes things more difficult without extra functionality or performance
		_StatePrecision FeatureValues[MAX_NUM_TILINGS];
		_StatePrecision	Action[AGENT_MAX_NUM_ACTIONVARS];

		CQValueTileCoding()
		{
			QValue = 0;
		};

		CQValueTileCoding(_QPrecision QValueInit)
		{
			QValue = QValueInit;
		};

		void reset(const int NumTiles, const int NumActionVars)
		{
			for (int iTile = 0; iTile < NumTiles; iTile++)
				Tiles[iTile] = 0;
			for (int iActionVar = 0; iActionVar < NumActionVars; iActionVar++)
				Action[iActionVar] = 0;
		};
};

class CQSpaceTileCoding: public CQSpace
{
	protected:
		// The function approximator
		CFATileCoding<_QPrecision, _IndexPrecision>	*mFATileCoding;
		_QPrecision 								bestUnboundedAction;
		int											*mVisits;
		bool										mTrackVisits;
		bool										mGeneralizeActions;
		int											mNumTilings;

		// Store variables
		CQValueTileCoding				mStoredQValue;
		CQValueTileCoding				mStoredBestActionQValue;
		CQValueTileCoding				mStoredExplorativeActionQValue;

		// Trace
		CReplacingTraceTC				mTrace;

		bool			getQValueTileCoding(const CAgentState& state, const CAgentAction& action, CQValueTileCoding& QTV, bool bReadOnly);
		virtual bool	getQValueTileCodingScaled			(const _StatePrecision *scaledState, const int* wrapData, const int numStateVars, const _StatePrecision *scaledAction, const int numActionVars, CQValueTileCoding& QTV, bool bReadOnly);
		virtual bool	getQValueTileCodingScaledIntActions	(const _StatePrecision *scaledState, const int* wrapData, const int numStateVars, const int *action, const int numActionVars, CQValueTileCoding& QTV, bool bReadOnly);
		
		// Visit tracking
		void		initVisits(void);
		void		resetVisits(void);
		void		trackVisit(CQValueTileCoding &QTV);
		_QPrecision	getVisits(CQValueTileCoding &QTV);

	public:
		CQSpaceTileCoding():
			mFATileCoding(NULL),
			mVisits(NULL),
			mTrackVisits(false),
			mGeneralizeActions(true),
			mNumTilings(0)
		{
		}

		virtual ~CQSpaceTileCoding();

		void				generalizeActions(bool enabled);	// When not generalizing in the action space, actions are discretized and fed as ints to tile coding
		void				trackVisits(bool enabled);

		// ***************************************
		// ** Q value and action functions
		// ***************************************
		virtual	void		setInitialQValuePolicy(_QPrecision valMin = 0, _QPrecision valMax = 1);

		// Get a Q value
		virtual _QPrecision probeQValue(CAgentState& state, CAgentAction& action);					// Does nothing other than give you that Q-value. no memory claiming, no caching
		virtual _QPrecision getQValue(CAgentState& state, CAgentAction& action);					//  in current state
		virtual _QPrecision getStoredQValue();														//  stored Q value
		virtual _QPrecision getStoredQValue(CAgentAction& action);									//  also returns the action itself
		_IndexPrecision*	 getStoredQValueTiles();	// For debugging

		// Get the Q value of the best action
		virtual _QPrecision getBestActionQValue(const CAgentState& state, CAgentAction& action, CAgentActionValueList* avList=NULL);	//  in current state
		virtual _QPrecision getBestRelativeActionQValue(const CAgentState& state, CAgentAction& action, const std::vector<int> & relative_bound, const std::vector<int> & under_relative_bound,bool useAbsoluteBounds,CAgentActionValueList* avList=NULL );
		virtual _QPrecision getBestActionQValue_GivenAction(const CAgentState& state, const CAgentAction& action);			//  in current state
		virtual _QPrecision	 getBestActionQValue_Approx(const CAgentState& state, CAgentAction& action, int undersamplingFactor);	// Approximate version of getBestAction using undersampling plus neighborhood examination
		virtual _QPrecision getStoredBestActionQValue();											//  stored best action
		virtual _QPrecision getStoredBestActionQValue(CAgentAction& action);

		//  also returns the best action itself

		// Get the Q value of an explorative action
		virtual _QPrecision getExplorativeActionQValue(const CAgentState& state, const CAgentAction& action);	//  in current state
		virtual _QPrecision getStoredExplorativeActionQValue();										//  stored explorative action
		virtual _QPrecision getStoredExplorativeActionQValue(CAgentAction& action);					//  also returns the action itself

		// Set a specific Q value
		virtual bool		setQValue(CAgentState& state, CAgentAction& action, _QPrecision value);					//  set a specific Q value
		virtual bool		setStoredQValue(_QPrecision value);										//  stored Q value
		virtual bool		setStoredBestActionQValue(_QPrecision value);							//  stored best action
		virtual bool		setStoredExplorativeActionQValue(_QPrecision value);					//  stored explorative action
		virtual bool      	getWithinBounds(int numbSteps, int previousAction, int relative_bound, int under_relative_bound, int Action);	//  get wether action is within bounds
		virtual bool     	getWithinAllBounds(int  *previousActions, int *varNumSteps, int vars, const std::vector<int> &relative_bound, const std::vector<int> &under_relative_bound, int *i, bool useAbsoluteBounds); //get wether action is within all bounds
		virtual bool 		getMaxQwithinBounds()
		{
			return bestUnboundedAction==mStoredBestActionQValue.QValue;
		}
		// ***************************************
		// ** Trace functions
		// ***************************************

		// Trace options
		virtual bool			traceInit(_QPrecision DiscountFactor, _QPrecision minVal, bool adjustTraceLength=true);
		virtual _IndexPrecision	traceGetLength();												//  amount of state-action pairs in trace
		virtual _QPrecision		traceGetDiscountFactor();										//  trace discount factor lambda

		// Discount all previous states
		virtual bool			traceFall();

		// Add a State-Action Pair to the trace (and discount all previous states)
		virtual bool			traceAdd(CAgentState& state, CAgentAction& action);						//  use SetState()
		virtual bool			traceAddStored();														//  stored state action pair
		virtual bool			traceAddStoredBest();													//  stored best action
		virtual bool			traceAddStoredExplorative();											//  stored random action

		// Clear the trace
		virtual bool			traceClear();

		// Do a Q space trace update
		virtual bool			traceUpdateQValues(_QPrecision Delta);

		// ***************************************
		// ** Disk Functions
		// ***************************************

		// Load QSpace from a stream
		virtual bool	load(std::ifstream& fileInStream);	//TODO

		// Save QSpace to a stream
		virtual bool	save(std::ofstream& fileOutStream);	//TODO

		// ***************************************
		// ** Basic memory Functions
		// ***************************************

		virtual bool	setMemorySize(_IndexPrecision MemorySize);
		virtual void	resetMemory();
		virtual _IndexPrecision getMemorySize();
		virtual _IndexPrecision getMemorySizeBytes();
		virtual _IndexPrecision	getMemoryUsage();
		bool			setNumTilings(int numTilings);

};

#endif /* QSPACETILECODING_H_ */
