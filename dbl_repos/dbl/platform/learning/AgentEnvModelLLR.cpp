/*
 * AgentEnvModelLLR.cpp
 *
 *  Created on: Sep 15, 2010
 *      Author: Erik Schuitema
 */

#include <AgentEnvModelLLR.h>
#include <limits>

CAgentEnvModelLLR::CAgentEnvModelLLR(ILLR* llr):
	mLog("AgentEnvModelLLR"),
	mLLR(llr),
	mDoubleCheckPrediction(false)
{
	for (int i=0; i<mLLR->numSensors(); i++)
		mStatePredictionIntervalLimits[i] = std::numeric_limits<double>::max();
	mRewardPredictionIntervalLimit = std::numeric_limits<double>::max();
}

CAgentEnvModelLLR::~CAgentEnvModelLLR()
{
}

bool CAgentEnvModelLLR::predict(SearchDirection direction, const CAgentState& state0, const CAgentAction& action, CAgentState* state1, ComputationalType* reward)
{
	// Convert input betwee floating point types
#ifdef _MSC_VER
	ANNcoord *llrState = new ANNcoord[mLLR->numSensors()];
	ANNcoord *llrAction = new ANNcoord[mLLR->numActuators()];
	ANNcoord *llrResultState = new ANNcoord[mLLR->numSensors()];
	ANNcoord *predintState = new ANNcoord[mLLR->numSensors()];
#else
	ANNcoord llrState[mLLR->numSensors()];
	ANNcoord llrAction[mLLR->numActuators()];
	ANNcoord llrResultState[mLLR->numSensors()];
	ANNcoord predintState[mLLR->numSensors()];
#endif
	ANNcoord llrReward;
	ANNcoord predintReward;

	for (int i=0; i<mLLR->numSensors(); i++)
		llrState[i] = state0[i];
	for (int i=0; i<mLLR->numActuators(); i++)
		llrAction[i] = action[i];
	//int t_dof;
	bool result = mLLR->predict(direction, llrState, llrAction, llrResultState, &llrReward, predintState, &predintReward);

	// Check against prediction interval limits while copying the prediction result to the output variables
	for (int i=0; i<mLLR->numSensors(); i++)
	{
		state1->setStateVar(i, (_StatePrecision)llrResultState[i]);
		if (predintState[i] > mStatePredictionIntervalLimits[i])
			result = false;
	}
	*reward = llrReward;
	if (predintReward > mRewardPredictionIntervalLimit)
		result = false;

#ifdef _MSC_VER
	delete[] llrState;
	delete[] llrAction;
	delete[] llrResultState;
	delete[] predintState;
#endif

	return result;
}

void CAgentEnvModelLLR::setPredictionIntervalLimits(const double* statePredIntLimits, const double rewardPredIntLimits, const double confidenceStudentTValue)
{
	for (int i=0; i<mLLR->numSensors(); i++)
		mStatePredictionIntervalLimits[i] = statePredIntLimits[i]/confidenceStudentTValue;
	mRewardPredictionIntervalLimit = rewardPredIntLimits/confidenceStudentTValue;
}

void CAgentEnvModelLLR::setMaxNeighborDistance(double maxDist)
{
	mLLR->setMaxNeighborDistance(maxDist);
}

void CAgentEnvModelLLR::resetMemory()
{
	mLLR->clear();
}

bool CAgentEnvModelLLR::predictBackward(const CAgentState& stateT1, const CAgentAction& action, CAgentState* stateT0, ComputationalType* reward)
{
	bool result = predict(sdBackward, stateT1, action, stateT0, reward);
	if (result && mDoubleCheckPrediction)
	{
		bool warn = false;
		double tempReward = 0;
		CAgentState tempState(stateT1);
		bool fwdPredictionResult = predict(sdForward, *stateT0, action, &tempState, &tempReward);
		if (!fwdPredictionResult)
		{
//			mLogWarningLn("Could not create reliable forward prediction from the result of a backward prediction");
//			mLogWarningLn(" backward prediction for " << stateT1.toStr());
//			mLogWarningLn(" produced forward state  " << tempState.toStr() << " and reward " << tempReward);
			result = false;
		}
		else
		{
			for (int i=0; i<mLLR->numSensors(); i++)
				if (fabs(tempState[i] - stateT1[i]) > mStatePredictionIntervalLimits[i])
					warn = true;
			if (fabs(tempReward - *reward) > mRewardPredictionIntervalLimit)
				warn = true;
			if (warn)
			{
//				mLogWarningLn("Backward prediction for " << stateT1.toStr());
//				mLogWarningLn(" produced forward state " << tempState.toStr() << " and reward " << tempReward);
//				mLogWarningLn(" for predicted state T0 " << stateT0->toStr() << " and reward " << *reward);
			}
			else
			{
//				mLogNoticeLn("Backward prediction was successfully checked with forward prediction");
			}
			//result = warn;
		}
	}

	return result;
}

bool CAgentEnvModelLLR::predictForward(const CAgentState& stateT0, const CAgentAction& action, CAgentState* stateT1, ComputationalType* reward)
{
	return predict(sdForward, stateT0, action, stateT1, reward);
}

void CAgentEnvModelLLR::reportTransition(double* rawStateT0, double* rawAction, double* rawStateT1, double reward)
{
	mLLR->addPoint(rawStateT0, rawAction, rawStateT1, &reward);
}
