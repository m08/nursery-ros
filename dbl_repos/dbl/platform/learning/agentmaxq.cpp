//#include "StdAfx.h"
#include "agentmaxq.h"
#include <float.h> // needed for FLT_MAX
#include <stdint.h>

// TODO: Execute() and ExecuteBestAction() have not been tested after big changes.

//////////////////////////////////////////
///////////// CMaxqMaxNode ///////////////

CMaxqMaxNode::CMaxqMaxNode(const std::string name):
	CMaxqNode(name), // use default constructor instead
	mExecutorNode(NULL),
	mCurrentSTGState(NULL)
{
//	mName = name;
//	SetLogStream(logger);
	mLearnRate		= 0.25f;
	mExploreRate	= 0.05f;
	mDiscountRate	= 1.0f;
	mTraceFallRate	= 0.9f;
	mTaskRepeatFactor	= 1;
	mTaskToExecute 		= -1;
	mTotalReward	= 0;
	mHrGreedy		= false;
}

void CMaxqMaxNode::AddTask(CMaxqQNode *task)
{
	mQNodes.push_back(task);
	task->SetParent(this);
}

void CMaxqMaxNode::Init()
{
	mLastC = 0;
	mPseudoReward	= 0;
	mHrReward		= 0;
	for (uint32_t iQNode=0; iQNode<mQNodes.size(); iQNode++)
		mQNodes[iQNode]->Init();
}

bool CMaxqMaxNode::Load(std::ifstream& fileInStream)
{
	if (!fileInStream.good())
		return false;

	fileInStream.read((char*)&mReportDebugInfo, sizeof(mReportDebugInfo));
	fileInStream.read((char*)&mHrGreedy, sizeof(mHrGreedy));
	fileInStream.read((char*)&mLearnRate, sizeof(mLearnRate));
	fileInStream.read((char*)&mDiscountRate, sizeof(mDiscountRate));
	fileInStream.read((char*)&mExploreRate, sizeof(mExploreRate));
	fileInStream.read((char*)&mTraceFallRate, sizeof(mTraceFallRate));
	fileInStream.read((char*)&mTaskRepeatFactor, sizeof(mTaskRepeatFactor));
	logDebugLn(mLogStream2,"loaded");
	return true;
}

bool CMaxqMaxNode::Save(std::ofstream& fileOutStream)
{
	if (!fileOutStream.good())
		return false;

	//mName
	fileOutStream.write((char*)&mReportDebugInfo,	sizeof(mReportDebugInfo));
	fileOutStream.write((char*)&mHrGreedy,	sizeof(mHrGreedy));
	//mQNodes // Vector of pointers
	fileOutStream.write((char*)&mLearnRate,	sizeof(mLearnRate));
	fileOutStream.write((char*)&mDiscountRate,	sizeof(mDiscountRate));
	fileOutStream.write((char*)&mExploreRate,	sizeof(mExploreRate));
	fileOutStream.write((char*)&mTraceFallRate,	sizeof(mTraceFallRate));
	fileOutStream.write((char*)&mTaskRepeatFactor,	sizeof(mTaskRepeatFactor));
	logDebugLn(mLogStream2,"saved");
	return true;
}

char CMaxqMaxNode::ShouldTerminate(const bool silent)
{
	char result = IsTerminalState(silent);	// Check locally if we are absorbing or non-absorbing (like timeouts)
	if (result)
		result |= CONST_LOCALLY_TERMINAL;
	if (mExecutorNode != NULL)
		if (mExecutorNode->ShouldTerminate())
			result |= CONST_ANCESTOR_TERMINAL;
	return result;
}

CMaxqMaxNode* CMaxqMaxNode::GetTaskNode(int taskIndex)
{
	return mQNodes[taskIndex]->GetTaskNode();
}

void CMaxqMaxNode::ClearTraces()
{
	for (uint32_t iQNode=0; iQNode<mQNodes.size(); iQNode++)
		mQNodes[iQNode]->mTrace.clear();
}

void CMaxqMaxNode::ClearTreeTraces()
{
	for (uint32_t iQNode=0; iQNode<mQNodes.size(); iQNode++)
	{
		mQNodes[iQNode]->mTrace.clear();
		mQNodes[iQNode]->GetTaskNode()->ClearTreeTraces();
	}
}

void CMaxqMaxNode::SetTreeLearnRate(ComputationalType alpha)
{
	mLearnRate = alpha;
	for (uint32_t i=0; i<mQNodes.size(); i++)
		mQNodes[i]->GetTaskNode()->SetTreeLearnRate(alpha);
}

void CMaxqMaxNode::SetTreeDiscountRate(ComputationalType gamma)
{
	mDiscountRate = gamma;
	for (uint32_t i=0; i<mQNodes.size(); i++)
		mQNodes[i]->GetTaskNode()->SetTreeDiscountRate(gamma);
}

void CMaxqMaxNode::SetTreeExploreRate(ComputationalType epsilon)
{
	mExploreRate = epsilon;
	for (uint32_t i=0; i<mQNodes.size(); i++)
		mQNodes[i]->GetTaskNode()->SetTreeExploreRate(epsilon);
}

void CMaxqMaxNode::SetTreeTraceFallRate(ComputationalType lambda)
{
	mTraceFallRate = lambda;
	for (uint32_t i=0; i<mQNodes.size(); i++)
		mQNodes[i]->GetTaskNode()->SetTreeTraceFallRate(lambda);
}

void CMaxqMaxNode::SetTreeState(CSTGState* currentSTGState)
{
	mCurrentSTGState = currentSTGState;
	for (uint32_t i=0; i<mQNodes.size(); i++)
	{
		mQNodes[i]->UpdateState(currentSTGState);
		mQNodes[i]->GetTaskNode()->SetTreeState(currentSTGState);
	}
}

void CMaxqMaxNode::ShowParameters()
{
	logInfoLn(mLogStream2,"learn rate = " << mLearnRate << "\t discount rate = " << mDiscountRate << "\t explore rate = " << mExploreRate << "\t trace fall rate = " << mTraceFallRate << "\t hr greedy = " << mHrGreedy << "\t task repeat factor = " << mTaskRepeatFactor);
}

void CMaxqMaxNode::ResetTreeMemory()
{
	for (uint32_t i=0; i<mQNodes.size(); i++)
		mQNodes[i]->ResetTreeMemory();
}

/*
void CMaxqMaxNode::ResetTreeTotalReward()
{
	ResetTotalReward();
	for (uint32_t i=0; i<mQNodes.size(); i++)
		mQNodes[i]->GetTaskNode()->ResetTreeTotalReward();
}
*/

int CMaxqMaxNode::Execute(CMaxqMaxNode* executorNode)
{
	int numSteps = 0;
	mExecutorNode = executorNode;	// For ancestor termination.
	// First, process the formal mAction assignment
	ProcessAssignment();

	ClearTraces();

	char terminalStatus = ShouldTerminate();
	if (terminalStatus)
	{
		mTerminatedLocally = false;
		return 0;	// Quit immediately. This node was probably called due to exploration.
	}

	int numChildSteps=0;
	while (!terminalStatus)
	{
		Step(false, numChildSteps);	// TODO: this might go wrong when the ancestor wants to terminate. Is C-value updated anyway??
		// Execute the selected action (after preparing it!!!!)
		mQNodes[mTaskToExecute]->GetTaskNode()->mAction = mQNodes[mTaskToExecute]->PrepareAssignment(mTaskAction);
		// Now, repeat the child task a number of times (usually just once; prim. actions often more than once)
		numChildSteps = 0;
		for (int iActionExec=0; iActionExec < mQNodes[mTaskToExecute]->GetTaskNode()->mTaskRepeatFactor; iActionExec++)
		{
			numChildSteps += mQNodes[mTaskToExecute]->GetTaskNode()->Execute(this);
			terminalStatus = ShouldTerminate();
			if (terminalStatus)
			{
				//printf("[Termination] Last step of \"%s\" took %d steps\n", mName.c_str(), numChildSteps);
				break;
			}
		}
		numSteps += numChildSteps;
		// (..eventually, after cascading, a primitive node executes a simulation step in the line above..)

		// Observe the reward
		CalculateReward();
		mTotalReward += mHrReward;
		//terminalStatus = ShouldTerminate();

		// Perform any post-processing after executing a child task.
		OnChildTaskExecuted(mTaskToExecute);
	}
	// Apparently, we terminated, either because of our own termination predicate or because of ancestors
	// Perform final learning step on one condition:
	//	1) Our invoked module terminated locally - see Dietterich's MAXQ document page 251.
	//
	//
	// FUTURE SUGGESTION: perform a non-absorbing update with Step(false) if not terminating locally
	if (mQNodes[mTaskToExecute]->GetTaskNode()->mTerminatedLocally)
	{
		if (terminalStatus & CONST_TERMINAL_ABSORBING)
			Step(true, numChildSteps);
		else
			Step(false, numChildSteps);
	}
	mTerminatedLocally = (terminalStatus & CONST_LOCALLY_TERMINAL) && (terminalStatus & CONST_TERMINAL_ABSORBING);
	return numSteps;
}

void CMaxqMaxNode::ExecuteBestAction(CMaxqMaxNode* executorNode)
{
	mExecutorNode = executorNode;	// For ancestor termination.
	// First, process the formal mAction assignment
	ProcessAssignment();

	char terminalStatus = ShouldTerminate();
	while (!terminalStatus)
	{
		GetBestAction(true,!mReportDebugInfo);
		mTaskToExecute	= mBestTask;

		// Execute the selected action
		mQNodes[mTaskToExecute]->GetTaskNode()->mAction = mQNodes[mTaskToExecute]->PrepareAssignment(mQNodes[mBestTask]->mBestAction);
		for (int iActionExec=0; iActionExec < mQNodes[mTaskToExecute]->GetTaskNode()->mTaskRepeatFactor; iActionExec++)
		{
			mQNodes[mTaskToExecute]->GetTaskNode()->ExecuteBestAction(this);
			terminalStatus = ShouldTerminate();
			if (terminalStatus)
			{
				//printf("[Termination] Last step of \"%s\" took %d steps\n", mName.c_str(), numChildSteps);
				break;
			}
		}

		// Perform any post-processing after executing a child task.
		OnChildTaskExecuted(mTaskToExecute);
	}
}

int CMaxqMaxNode::ExecuteTimeStep(CMaxqMaxNode* executorNode, char ancestorTerminalStatus)
{
	mExecutorNode = executorNode;	// For debugging

	char terminalStatus = IsTerminalState();	// Check locally if we are absorbing or non-absorbing (like timeouts)
	if (terminalStatus)
		terminalStatus |= CONST_LOCALLY_TERMINAL;
	if (ancestorTerminalStatus)
		terminalStatus |= CONST_ANCESTOR_TERMINAL;
	mTerminatedLocally = (terminalStatus & CONST_LOCALLY_TERMINAL) && (terminalStatus & CONST_TERMINAL_ABSORBING);
	if (mReportDebugInfo)
	{
		logDebug(mLogStream2,"Executing time step, action: " << mAction[0] << " terminal: ");
		//mLogStream << "[DEBUG] Executing time step in node: " << mName.c_str() << " action: " << mAction[0] << " terminal: ";
		if (terminalStatus)
			logDebug(mLogStream2,"yes" << endl);
			//mLogStream << "yes\n";
		else
			logDebug(mLogStream2,"no" << endl);
			//mLogStream << "no\n";
	}
	int numChildSteps = 0;

	if (mTaskToExecute < 0) // This node was just given control by parent node
	{
		/* We assume this does not happen
		if (terminalStatus)
		{
			mTerminatedLocally = false;
			return 0;	// Quit immediately. This node was probably called due to exploration.
		}
		*/
		mNumChildSteps = 0;
		mTaskRepeated = 0;
		// First, process the formal mAction assignment
		ProcessAssignment();
		ClearTraces();
	}
	else
	{
		// Execute the previously chosen child task (either to let it complete learning or to make it perform an action)
		char fakeTerminalStatus = terminalStatus;
		if (mHrGreedy) // If we do hierarchical greedy execution, all max nodes under this node terminate after 1 step
		{
			int numAllowedTasks = 0;
			int allowedTask = -1;
			for (uint32_t iTask=0; iTask<mQNodes.size(); iTask++)
			{
				if (mQNodes[iTask]->GetTaskNode()->IsTaskAllowed())
				{
					numAllowedTasks++;
					if (numAllowedTasks > 1)
					{
						fakeTerminalStatus |= CONST_LOCALLY_TERMINAL; // But only when we actually have a choice
						logInfoLn(mLogStream2,"HrGreedy execution active");
						break;
					}
					allowedTask = iTask;
				}
			}
			if ((numAllowedTasks == 1) && (allowedTask != mTaskToExecute))
			{
				fakeTerminalStatus |= CONST_LOCALLY_TERMINAL; // Or when we can only choose another subtask than the previous one
				logInfoLn(mLogStream2,"HrGreedy execution active");
			}
		}
		numChildSteps = mQNodes[mTaskToExecute]->GetTaskNode()->ExecuteTimeStep(this,fakeTerminalStatus);
		mNumChildSteps += numChildSteps;
		// If child task is done
		if (numChildSteps == 0)
		{
			mTaskRepeated++; // We might need to repeat the same child task
			if (mQNodes[mTaskToExecute]->GetTaskNode()->IsTerminalState()) // Except when it's terminal
				mTaskRepeated = mQNodes[mTaskToExecute]->GetTaskNode()->mTaskRepeatFactor;
		}
	}

	// If node has to terminate OR the subtask is executed mTaskrepeatFactor times
	bool taskRepeatedEnough = false;
	if (mTaskToExecute > -1)
		if (mTaskRepeated >= mQNodes[mTaskToExecute]->GetTaskNode()->mTaskRepeatFactor)
			taskRepeatedEnough = true;
	if ((terminalStatus) || (taskRepeatedEnough))
	{
		// Observe the reward
		CalculateReward();
		mTotalReward += mHrReward;
		// Perform any post-processing after executing a child task.
		OnChildTaskExecuted(mTaskToExecute);
	}

	if (terminalStatus)
	{
		// Apparently, we terminated, either because of our own termination predicate or because of ancestors
		// Perform final learning step on one condition:
		//	1) Our invoked module terminated locally - see Dietterich's MAXQ document page 251.
		//
		// FUTURE SUGGESTION: perform a non-absorbing update with Step(false) if not terminating locally
		if (mTaskToExecute > -1)
		{
			if (mQNodes[mTaskToExecute]->GetTaskNode()->mTerminatedLocally)
			{
				if (terminalStatus & CONST_TERMINAL_ABSORBING)
					Step(true, mNumChildSteps);
				else
					Step(false, mNumChildSteps);
			}
		}
		mTerminatedLocally = (terminalStatus & CONST_LOCALLY_TERMINAL) && (terminalStatus & CONST_TERMINAL_ABSORBING);
		mTaskToExecute = -1;
		return 0;
	}

	// If node just got control from parent OR the child task is executed mTaskrepeatFactor times OR child task is terminal
	if ((mTaskToExecute < 0) || (taskRepeatedEnough))
	{
		// Step() also sets mTaskToExecute and mTaskAction, it is the programmers responsibility to make sure Step() can only choose tasks that are not currently terminal
		Step(false, mNumChildSteps);	// TODO: this might go wrong when the ancestor wants to terminate. Is C-value updated anyway??
		mNumChildSteps = 0;
		mTaskRepeated = 0;
		// Prepare the selected action
		if (mReportDebugInfo)
		{
			CAgentAction setAction = mQNodes[mTaskToExecute]->PrepareAssignment(mTaskAction);
			logCrawl(mLogStream2,"changing action of " << mQNodes[mTaskToExecute]->GetTaskNode()->mName.c_str() << " to [" << setAction[0]);
			//mLogStream << "[DEBUG] " << mName.c_str() << " changing action of " << mQNodes[mTaskToExecute]->GetTaskNode()->mName.c_str() << " to [" << setAction[0];
			for (int i = 1; i < setAction.getNumActionVars(); i++)
				logCrawl(mLogStream2,", " << setAction[i]);
				//mLogStream << ", " << setAction[i];
			logCrawl(mLogStream2,"] in executetimestep" << endl);
			//mLogStream << "] in executetimestep" << endl;
		}
		mQNodes[mTaskToExecute]->GetTaskNode()->mAction = mQNodes[mTaskToExecute]->PrepareAssignment(mTaskAction);
	}

	if (numChildSteps == 0) //ExecuteTimeStep() above was not called or returned 0 (so no action is performed yet)
	{
		// Execute the selected action
		numChildSteps = mQNodes[mTaskToExecute]->GetTaskNode()->ExecuteTimeStep(this,terminalStatus);
		mNumChildSteps += numChildSteps;
		//mNumChildSteps++;
	}

	return numChildSteps;
}


// Step(): Learning step function
//
// Note the following important thing:
// when this function is called for the first time,
// this function does nothing more than GetBestAction(true),
// selecting an action and Executing it. This is for the
// following reason: The traces are empty!
//
void CMaxqMaxNode::Step(bool isTerminalAbsorbingState, const int numChildSteps)
{
	CCMem delta(-mLastC);

	_QPrecision V;
	bool success;
	// This step is very important.
	// According to the definition of a terminal absorbing state,
	// the agent cannot leave the state (it is absorbing)
	// and the reward of any action is zero. This means
	// that the long term expected sum of rewards is: 0!
	// Therefore, the GetBestValues should all be zero
	// in case of a terminal state. This is not found in
	// any pseudo-code by Sutton or Dietterich!
	if (!isTerminalAbsorbingState)
	{
		// Get the best action according to this assignment
		//GetBestAction(true,!mReportDebugInfo);
		GetBestAction(true,false);
		success = mQNodes[mBestTask]->GetBestValues(&mLastC, &V);
		if (!success) // what to do on error?
			logErrorLn(mLogStream2,"QNode::GetBestValues() failed");
			//mLogStream << "\n[ERROR] in CMaxqMaxNode::Step 1\n";
	}
	else
	{
		//mQNodes[mBestTask]->GetBestValues(&mLastC, &mLastCPseudo, &V);
		mLastC = 0;
		//mLastCPseudo = 0;
		V = 0;
	}
	ComputationalType timeDiscount = pow(mDiscountRate, numChildSteps);
	if (mReportDebugInfo)
	{
		logCrawl(mLogStream2,"delta.normal=" << delta.normal() << " + " << timeDiscount << "*(" << mHrReward << " + " << mLastC.normal() << " + " << V << ")");
		//mLogStream << "[DEBUG] " << mName.c_str() << " delta.normal=" << delta.normal() << " + "<< timeDiscount << "*(" << mHrReward << " + " << mLastC.normal() << " + " << V << ")";
		logCrawl(mLogStream2," delta.pseudo=" << delta.pseudo() << " + " << timeDiscount << "*(" << mHrReward << " + " << mPseudoReward << " + " << V << ")" << endl);
		//mLogStream << " delta.pseudo=" << delta.pseudo() << " + " << timeDiscount << "*(" << mHrReward << " + " << mPseudoReward << " + " << V << ")\n";
	}
	delta.normal() += (_QPrecision)timeDiscount*(mHrReward + mLastC.normal() + V);
	delta.pseudo() += (_QPrecision)timeDiscount*(mHrReward + mPseudoReward + mLastC.pseudo() + V);

	// Update elements in all traces. THE ACTUAL LEARNING STEP!
	for (uint32_t iQNode=0; iQNode<mQNodes.size(); iQNode++)
	{
		success = mQNodes[iQNode]->Update(delta*mLearnRate);
		if (!success) // what to do on error?
			logErrorLn(mLogStream2,"QNode::Update() failed");
			//mLogStream << "\n[ERROR] in CMaxqMaxNode::Step 2\n";
	}

	// The best action for mBestTask is now saved in that QNode.

	if ((gRanrotB.Random() < mExploreRate) && (!isTerminalAbsorbingState))
	//if (rg.Random() < mExploreRate)
	{
		if (mReportDebugInfo)
			logDebugLn(mLogStream2,"taking random action");
			//mLogStream << "[DEBUG] " << mName.c_str() << " taking random action\n";
		// pick a random action
		char randomTaskTerminal;
		do
		{
			mTaskToExecute = gRanrotB.IRandom(0, mQNodes.size()-1);
			mTaskAction.copyDescriptors(mQNodes[mTaskToExecute]->mBestAction);	// To copy the descriptors!
			mTaskAction.randomizeUniform(&gRanrotB);

			// Check terminality
			if (mReportDebugInfo)
				logCrawl(mLogStream2,"changing action of " << mQNodes[mTaskToExecute]->GetTaskNode()->mName.c_str() << " to " << (mQNodes[mTaskToExecute]->PrepareAssignment(mTaskAction))[0] << " in step()" << endl);
				//mLogStream << "[DEBUG] " << mName.c_str() << " changing action of " << mQNodes[mTaskToExecute]->GetTaskNode()->mName.c_str() << " to " << (mQNodes[mTaskToExecute]->PrepareAssignment(mTaskAction))[0] << " in step\n";
			mQNodes[mTaskToExecute]->GetTaskNode()->mAction = mQNodes[mTaskToExecute]->PrepareAssignment(mTaskAction);
			mQNodes[mTaskToExecute]->GetTaskNode()->ProcessAssignment();

			// If the mTaskNode is NOT in a terminal state, evaluate its Q-values
			//   Request in silent mode, because the action was not really performed
			randomTaskTerminal = !mQNodes[mTaskToExecute]->GetTaskNode()->IsTaskAllowed();
			randomTaskTerminal |= mQNodes[mTaskToExecute]->GetTaskNode()->IsTerminalState();

		}
		while (randomTaskTerminal);

		// Request new mLastC for the random action
		success = mQNodes[mTaskToExecute]->GetCValues(mTaskAction, &mLastC);
		if (!success) // what to do on error?
			logErrorLn(mLogStream2,"QNode::GetCValues() failed");
			//mLogStream << "\n[ERROR] in CMaxqMaxNode::Step 3\n";

		// Trace is cleared
		ClearTraces();
	}
	else
	{
		mTaskToExecute = mBestTask;
		mTaskAction = mQNodes[mBestTask]->mBestAction;
		// Trace falls
		for (uint32_t iQNode=0; iQNode<mQNodes.size(); iQNode++)
			mQNodes[iQNode]->mTrace.fall((_QPrecision)timeDiscount);	// timeDiscount is the EXTRA falloff factor! Lambda resides in the trace as mFalloff
	}
	success = mQNodes[mTaskToExecute]->AddToTrace(mTaskAction);
	if (!success) // what to do on error?
		logErrorLn(mLogStream2,"QNode::AddToTrace() failed");
		//mLogStream << "\n[ERROR] in CMaxqMaxNode::Step 4\n";
}

_QPrecision CMaxqMaxNode::GetBestAction(bool pseudoC, bool silent)
{
	mBestTask = -1;
	_QPrecision QMax = -_QPrecision_MAX;
	_QPrecision QTask = -_QPrecision_MAX;

	for (uint32_t iTask=0; iTask<mQNodes.size(); iTask++)
	{
		if ((!silent) && (mReportDebugInfo))
			logDebug(mLogStream2,"GetBestAction task :" << iTask << " allowed: ");
			//mLogStream << "[DEBUG] getbestaction " << mName.c_str() << " task: " << iTask << " allowed: ";
		if (mQNodes[iTask]->GetTaskNode()->IsTaskAllowed())
		{
			if ((!silent) && (mReportDebugInfo))
				logDebug(mLogStream2,"yes" << endl);
				//mLogStream << "yes" << endl;
			QTask = mQNodes[iTask]->GetBestAction(pseudoC,silent);
			if (QTask > QMax)
			{
				QMax = QTask;
				mBestTask = iTask;
			}
		}
		else if ((!silent) && (mReportDebugInfo))
			logDebug(mLogStream2,"no" << endl);
			//mLogStream << "no" << endl;
	}
	if (mBestTask < 0)
		logErrorLn(mLogStream2,"GetBestAction: no task allowed " << mBestTask);
	if ((!silent) && (mReportDebugInfo))
		logDebugLn(mLogStream2,"GetBestAction mBestTask: " << mBestTask << " = " << mQNodes[mBestTask]->mName.c_str());
		//mLogStream << "[DEBUG] getbestaction " << mName.c_str() << " mBestTask: " << mBestTask << " = " << mQNodes[mBestTask]->mName.c_str() << endl;
	return QMax;
}



//////////////////////////////////////////
////////////// CMaxqQNode ////////////////

CMaxqQNode::CMaxqQNode(const std::string name):
	CMaxqNode(name), // use default constructor instead
	mParentNode(NULL),
	mTaskNode(NULL),
	mC(NULL),
	mMemsize(0),
	mNumTilings(0),
	mQInitMin(0.0),
	mQInitMax(1.0)
{
//	mName = name;
//	SetLogStream(logger);
}

void CMaxqQNode::SetParent(CMaxqMaxNode* parentNode)
{
	mParentNode = parentNode;
}

void CMaxqQNode::SetTaskNode(CMaxqMaxNode* taskNode)
{
	mTaskNode = taskNode;
	//mBestAction.CopyDescriptors(mTaskNode->mAction);	// TODO: who is going to get the action descriptors from xml?? should be in Max-nodes?
}

bool CMaxqQNode::SetNumTilings(int numTilings)
{
	if (mNumTilings == numTilings)
		return true;

	if (mC != NULL)
	{
		delete mC;
		mC = NULL;
	}
	switch (numTilings)
	{
		case 1:
			mC = new CFATileCodingSafe<CCMem, 1, _IndexPrecision, uint16_t>;
			break;
		case 2:
			mC = new CFATileCodingSafe<CCMem, 2, _IndexPrecision, uint16_t>;
			break;
		case 4:
			mC = new CFATileCodingSafe<CCMem, 4, _IndexPrecision, uint16_t>;
			break;
		case 8:
			mC = new CFATileCodingSafe<CCMem, 8, _IndexPrecision, uint16_t>;
			break;
		case 16:
			mC = new CFATileCodingSafe<CCMem, 16, _IndexPrecision, uint16_t>;
			break;
		case 32:
			mC = new CFATileCodingSafe<CCMem, 32, _IndexPrecision, uint16_t>;
			break;
		case 64:
			mC = new CFATileCodingSafe<CCMem, 64, _IndexPrecision, uint16_t>;
			break;

		default:
		{
			logErrorLn(mLogStream2,"Number of tilings, " << numTilings << ", was either < 2, too large or not a power of 2!");
			//mLogStream << "[ERROR] Number of tilings, " << numTilings << ", was either < 2, too large or not a power of 2!" << endl;
			return false;
		}
	}
	mNumTilings = numTilings;
	//TODO: mTraceFallRate: computationaltype or Qprecision ??
	_QPrecision fallrate = (_QPrecision)GetTraceFallRate();
	mTrace.init(fallrate, _MIN_TRACE_VALUE_MAXQ, numTilings);	// The time discount factor for trace falling is handled in Step()
	return true;
}

bool CMaxqQNode::SetMemsize(_IndexPrecision memsize)
{
	bool result = mC->setMemorySize(memsize);
	if (result)
		mMemsize = memsize;
	return result;
}

void CMaxqQNode::ShowParameters()
{
	logInfoLn(mLogStream2,"Q init = " << mQInitMin << " to " << mQInitMax << "\t action as int = " << mActionAsInt);
}

void CMaxqQNode::RandomizeValues(_QPrecision min, _QPrecision max)
{
	if (mC != NULL)
	{
		logDebugLn(mLogStream2,"randomizing values");
		CCMem cMax(max);
		CCMem cMin(min);
		mC->setValueInitPolicy(cMin,cMax);
		//mC->setValueInitPolicy(min,max);
		mC->reset(); // Called to actually set the random values
	}
}

void CMaxqQNode::Init()
{
	RandomizeValues(mQInitMin, mQInitMax);
	mTrace.init((_QPrecision)GetTraceFallRate(), (_QPrecision)_MIN_TRACE_VALUE_MAXQ, mNumTilings);	// The time discount factor for trace falling is handled in Step()
	mTrace.clear();
	mTaskNode->Init();
}

bool CMaxqQNode::Load(std::ifstream& fileInStream)
{
	if (!fileInStream.good())
		return false;

	fileInStream.read((char*)&mReportDebugInfo,	sizeof(mReportDebugInfo));
	fileInStream.read((char*)&mActionAsInt,	sizeof(mActionAsInt));
	fileInStream.read((char*)&mQInitMin,	sizeof(mQInitMin));
	fileInStream.read((char*)&mQInitMax,	sizeof(mQInitMax));

	int numTilings = 0;
	fileInStream.read((char*)&numTilings,	sizeof(numTilings));
	if (!SetNumTilings(numTilings)) // Also sets mNumTilings
		return false;
	_IndexPrecision memsize = 0;
	fileInStream.read((char*)&memsize,	sizeof(memsize));
	if (!SetMemsize(memsize)) // Also sets mMemsize
		return false;
	if (!mC->load(fileInStream))
		return false;
	if (!mTrace.load(fileInStream,false))
		return false;
	logDebugLn(mLogStream2,"loaded");
	return true;
}

bool CMaxqQNode::Save(std::ofstream& fileOutStream)
{
	if (!fileOutStream.good())
	{
		logErrorLn(mLogStream2,"Bad filestream");
		return false;
	}
	if (mC == NULL)
	{
		logErrorLn(mLogStream2,"mC is null");
		return false;
	}

	//mName
	fileOutStream.write((char*)&mReportDebugInfo,	sizeof(mReportDebugInfo));
	//mParentNode // Pointer
	//mTaskNode // Pointer
	fileOutStream.write((char*)&mActionAsInt,	sizeof(mActionAsInt));
	fileOutStream.write((char*)&mQInitMin,	sizeof(mQInitMin));
	fileOutStream.write((char*)&mQInitMax,	sizeof(mQInitMax));
	fileOutStream.write((char*)&mNumTilings,	sizeof(mNumTilings));
	fileOutStream.write((char*)&mMemsize,	sizeof(mMemsize));
	if (!mC->save(fileOutStream))
		return false;
	if (!mTrace.save(fileOutStream,false))
		return false;
	logDebugLn(mLogStream2,"saved");
	return true;
}

_IndexPrecision CMaxqQNode::GetMemorySize()
{
	if (mC == NULL)
		return 0;
	return mC->getMemorySize();
}

_IndexPrecision	CMaxqQNode::GetMemoryUsage()
{
	if (mC == NULL)
		return 0;
	return mC->getNumClaims();
}

bool CMaxqQNode::GetTileValue(CAgentState& state, CAgentAction& action, _IndexPrecision* tiles, CCMem* value, bool bReadOnly)
{
	int err = 0;
	if (!mActionAsInt)
	{
		_StatePrecision StateActionSpace[AGENT_MAX_NUM_STATEVARS + AGENT_MAX_NUM_ACTIONVARS];
		int	WrapData[AGENT_MAX_NUM_STATEVARS + AGENT_MAX_NUM_ACTIONVARS];

		memcpy(StateActionSpace, state.getState(), sizeof(_StatePrecision)*(state.getNumStateVars()));
		for (int iValue = 0; iValue < action.getNumActionVars(); iValue++)
			StateActionSpace[iValue + state.getNumStateVars()] = action.mDescriptors[iValue].mScale*action[iValue];

		if (state.wrapState())
		{
			memcpy(WrapData, state.getStateWrapData(), sizeof(int)*(state.getNumStateVars()));
			memset(&WrapData[state.getNumStateVars()], 0, sizeof(int)*action.getNumActionVars());
			err = mC->getValue(StateActionSpace, action.getNumActionVars()+state.getNumStateVars(), WrapData, NULL, 0, tiles, value, bReadOnly);
		}
		else
			err = mC->getValue(StateActionSpace, action.getNumActionVars()+state.getNumStateVars(), NULL, 0, tiles, value, bReadOnly);
	}
	else
	{
		int IntActionSpace[AGENT_MAX_NUM_ACTIONVARS];

		for (int iValue = 0; iValue < action.getNumActionVars(); iValue++)
			IntActionSpace[iValue] = (int)(floor(0.5 + action[iValue]/action.mDescriptors[iValue].mStepSize));

		if (state.wrapState())
			err = mC->getValue(state.getState(), state.getNumStateVars(), state.getStateWrapData(), IntActionSpace, action.getNumActionVars(), tiles, value, bReadOnly);
		else
			err = mC->getValue(state.getState(), state.getNumStateVars(), IntActionSpace, action.getNumActionVars(), tiles, value, bReadOnly);
	}

	if (err != ERR_SUCCESS)
	{
		// TODO: double memory
		logWarningLn(mLogStream2,"Memory should be doubled!");
		//mLogStream << "[WARNING] Memory should be doubled!\n";
	}
	return true;
}

bool CMaxqQNode::GetCValues(CAgentAction& action, CCMem* C)
{
	_IndexPrecision tiles[MAX_NUM_TILINGS]; //To avoid memory allocation
	if (!GetTileValue(mCurrentAgentState, action, tiles, C, TILE_READ))
		return false;
	return true;
}

bool CMaxqQNode::GetBestValues(CCMem* C, _QPrecision* V)
{
	_IndexPrecision tiles[MAX_NUM_TILINGS]; //To avoid memory allocation
	if (!GetTileValue(mCurrentAgentState, mBestAction, tiles, C, TILE_READ))
		return false;
	*V		= mBestActionV;
	return true;
}

//bool CMaxqQNode::AddToTrace(const CAgentAction& action)
bool CMaxqQNode::AddToTrace(CAgentAction& action)
{
	_IndexPrecision tiles[MAX_NUM_TILINGS]; //To avoid memory allocation
	CCMem Cval;

	if (mReportDebugInfo)
	{
		logCrawl(mLogStream2,"adding to trace");
		//mLogStream << "[DEBUG] " << mName.c_str() << " adding to trace";
		for (int i=0; i <mCurrentAgentState.getNumStateVars(); i++)
			logCrawl(mLogStream2," mCurrentAgentState[" << i << "]=" << mCurrentAgentState[i]);
			//mLogStream << " mCurrentAgentState[" << i << "]=" << mCurrentAgentState[i];
		for (int i=0; i < action.getNumActionVars(); i++)
			logCrawl(mLogStream2," action[" << i << "]=" << action[i]);
			//mLogStream << " action[" << i << "]=" << action[i];
		logCrawl(mLogStream2,endl);
		//mLogStream << endl;
	}
	if (!GetTileValue(mCurrentAgentState, action, tiles, &Cval, TILE_READ))
		return false;
	mTrace.addBatch(tiles);
	return true;
}

bool CMaxqQNode::Update(CCMem delta)
{
	if ((mReportDebugInfo) && (mTrace.numElements() > 0))
		logDebugLn(mLogStream2,"Updating with delta=" << delta.normal() << " deltapseudo=" << delta.pseudo());
		//mLogStream << "[DEBUG] " << mName.c_str() << " updating with delta=" << delta.normal() << " deltapseudo=" << delta.pseudo() << endl;
	for (_IndexPrecision i=0; i<mTrace.numElements(); i++)
			mC->adjustParameterValue(mTrace[i].index, delta*mTrace[i].value);
	return true;
}

void CMaxqQNode::ResetTreeMemory()
{
	mC->reset();
	mTaskNode->ResetTreeMemory();
}

void CMaxqQNode::PrintValues(CAgentState abst)
{
	logInfo(mLogStream2,"abst=[" << abst[0]);
	//mLogStream << "abst=[";
	//mLogStream << abst[0];
	for (int i=1; i<abst.getNumStateVars(); i++)
		logInfo(mLogStream2,", " << abst[i]);
		//mLogStream << ", " << abst[i];
	logInfo(mLogStream2,"] ");
	//mLogStream << "] ";

	_IndexPrecision tiles[MAX_NUM_TILINGS]; //To avoid memory allocation

	CCMem CVal;
	CAgentAction curAction = mBestAction;

	//keeps up how many times we increased value of Nth action dim
	int stepIndex[AGENT_MAX_NUM_ACTIONVARS];	// Fixed array to avoid allocation of memory in real-time applications

	memset(stepIndex, 0, curAction.getNumActionVars() * sizeof(stepIndex[0])); //fill with zero's: set all values to 0 without loop

	curAction.setStartValues();

	int numSteps = 1;
	for (int iA=0; iA<curAction.getNumActionVars(); iA++)
		numSteps *= curAction.mDescriptors[iA].mNumSteps;

	// Iterate over all values in the action space: all possible combinations
	for (int i=0; i<numSteps; i++)
	{
		if (!GetTileValue(abst,curAction,tiles,&CVal,TILE_READ))
			logErrorLn(mLogStream2,"GetTileValue() failed in PrintValues()");
			//mLogStream << "\n[ERROR] in CMaxqQNode::PrintValues " << mName.c_str() << endl;

		logInfo(mLogStream2," (" << curAction.mDescriptors[0].getValByIndex(stepIndex[0]));
		//mLogStream << " (" << curAction.mDescriptors[0].getValByIndex(stepIndex[0]);
		for (int iA=1; iA<curAction.getNumActionVars(); iA++)
			logInfo(mLogStream2,", " << curAction.mDescriptors[iA].getValByIndex(stepIndex[iA]));
			//mLogStream << ", " << curAction.mDescriptors[iA].getValByIndex(stepIndex[iA]);
		logInfo(mLogStream2,")=" << CVal.pseudo() << " ");
		//mLogStream << ")=" << CVal.pseudo() << " ";

		// Increment value with index 0
		stepIndex[0]++;
		curAction[0] = curAction.mDescriptors[0].getValByIndex(stepIndex[0]);

		// Increment the ones who need incrementing
		for (int iA=0; iA<curAction.getNumActionVars(); iA++)
			// if value of action iA is at max, reset it and increase the next action value
			if (stepIndex[iA] >= curAction.mDescriptors[iA].mNumSteps)
			{
				stepIndex[iA] = 0;
				curAction[iA] = curAction.mDescriptors[iA].mStartVal;
				if (iA < curAction.getNumActionVars()-1)
				{
					stepIndex[iA+1]++;
					curAction[iA+1] = curAction.mDescriptors[iA+1].getValByIndex(stepIndex[iA+1]);
				}
			}
	}
}

_QPrecision CMaxqQNode::GetBestAction(bool pseudoC, bool silent)
{
	_IndexPrecision tiles[MAX_NUM_TILINGS]; //To avoid memory allocation
	mBestActionV	= 0;
	CCMem CVal;
	CCMem maxQVal(-_QPrecision_MAX);
	//CAgentAction oldAction = mTaskNode->mAction;

	//keeps up how many times we increased value of Nth action dim
	int stepIndex[AGENT_MAX_NUM_ACTIONVARS];	// Fixed array to avoid allocation of memory in real-time applications

	_StatePrecision bestAction[AGENT_MAX_NUM_ACTIONVARS];

	memset(stepIndex, 0, mBestAction.getNumActionVars() * sizeof(stepIndex[0])); //fill with zero's: set all values to 0 without loop

	mBestAction.setStartValues();

	int numSteps = 1;
	for (int iA=0; iA<mBestAction.getNumActionVars(); iA++)
		numSteps *= mBestAction.mDescriptors[iA].mNumSteps;

	if ((!silent) && (mReportDebugInfo))
	{
		logCrawl(mLogStream2,"GetBestAction " << mBestAction.mDescriptors[0].mNumSteps);
		//mLogStream << "[DEBUG] GetBestAction " << mName.c_str() << " " << mBestAction.mDescriptors[0].mNumSteps;
		for (int i=0; i<mCurrentAgentState.getNumStateVars(); i++)
			logCrawl(mLogStream2," mCurrentAgentState[" << i << "]=" << mCurrentAgentState[i]);
			//mLogStream << " mCurrentAgentState[" << i << "]=" << mCurrentAgentState[i];
		logCrawl(mLogStream2,endl);
		//mLogStream << endl;
	}

	// Iterate over all values in the action space: all possible combinations
	for (int i=0; i<numSteps; i++)
	{
		// Set the current mBestAction as assignment for the connected task:
		// first prepare the assignment (and put it in the child's mAction) and then let the TaskNode (child) process it
		//mLogStream << mName.c_str() << " changing action of " << mTaskNode->mName.c_str() << " to " << PrepareAssignment(mBestAction)[0] << " in getbestaction\n";

		mTaskNode->mAction = PrepareAssignment(mBestAction);
		mTaskNode->ProcessAssignment();

		// If the mTaskNode is NOT in a terminal state, evaluate its Q-values
		/*
		if ((!silent) && (mReportDebugInfo))
		{
			mLogStream << "[DEBUG] GetBestAction " << mName.c_str() << " step: " << i << " terminal: ";
			if (mTaskNode->IsTerminalState())
				mLogStream << "yes\n";
			else
				mLogStream << "no\n";
		}
		*/
		if (!mTaskNode->IsTerminalState())
		{
			// Evaluate the recursive Q-value : C-tilde + V.
			// But in V, we need the 'uncontaminated' C function, not the pseudo! Hence the boolean in GetBestAction()

			_QPrecision tempV = mTaskNode->GetBestAction(false,silent);	// Depends on our mBestAction via ProcessAssignment()! Without assignment, the mTaskNode cannot know what's best to do.

			if (!GetTileValue(mCurrentAgentState,mBestAction,tiles,&CVal,TILE_READ))
			{
				logErrorLn(mLogStream2,"GetTileValue() failed in GetBestAction()");
				//mLogStream << "\n[ERROR] in CMaxqQNode::GetBestAction " << mName.c_str() << endl;
				//mTaskNode->mAction = oldAction; // Set to original value
				return -_QPrecision_MAX;
			}

			if ((!silent) && (mReportDebugInfo))
			{
				logCrawl(mLogStream2,"GetBestAction (" << mBestAction.mDescriptors[0].getValByIndex(stepIndex[0]));
				//mLogStream << "[DEBUG] GetBestAction " << mName.c_str() << "(" << mBestAction.mDescriptors[0].getValByIndex(stepIndex[0]);
				for (int iA=1; iA<mBestAction.getNumActionVars(); iA++)
					logCrawl(mLogStream2,", " << mBestAction.mDescriptors[iA].getValByIndex(stepIndex[iA]));
					//mLogStream << ", " << mBestAction.mDescriptors[iA].getValByIndex(stepIndex[iA]);
				logCrawl(mLogStream2,")=");
				//mLogStream << ")=";
				if (pseudoC)
					logCrawl(mLogStream2,CVal.pseudo() << " + " << tempV << " = " << CVal.pseudo()+tempV << endl);
					//mLogStream << CVal.pseudo() << " + " << tempV << " = " << CVal.pseudo()+tempV << endl;
				else
					logCrawl(mLogStream2,CVal.normal() << " + " << tempV << " = " << CVal.normal()+tempV << endl);
					//mLogStream << CVal.normal() << " + " << tempV << " = " << CVal.normal()+tempV << endl;
			}
			if ((CVal.pseudo()+tempV) > maxQVal.pseudo())
			{
				maxQVal = CVal + tempV;
				mBestAction.store(bestAction); //store in bestAction
				mBestActionV = tempV;
			}
		}
		// else: just increment mBestAction to try the next option

		// Increment value with index 0
		stepIndex[0]++;
		mBestAction[0] = mBestAction.mDescriptors[0].getValByIndex(stepIndex[0]);

		// Increment the ones who need incrementing
		for (int iA=0; iA<mBestAction.getNumActionVars(); iA++)
			//if value of action iA is at max, reset it and increase the next action value
			if (stepIndex[iA] >= mBestAction.mDescriptors[iA].mNumSteps)
			{
				stepIndex[iA] = 0;
				mBestAction[iA] = mBestAction.mDescriptors[iA].mStartVal;
				if (iA < mBestAction.getNumActionVars()-1)
				{
					stepIndex[iA+1]++;
					mBestAction[iA+1] = mBestAction.mDescriptors[iA+1].getValByIndex(stepIndex[iA+1]);
				}
			}
	}

	_QPrecision result = -_QPrecision_MAX;
	mBestAction.restore(bestAction); //fill mBestAction with the best action

	if (GetTileValue(mCurrentAgentState,mBestAction,tiles,&CVal,TILE_WRITE))
	{
		if (pseudoC)
			result = maxQVal.pseudo();
		else
			result = maxQVal.normal();
	}
	//mTaskNode->mAction = oldAction; // Set to original value
	return result;
}



//////////////////////////////////////////
////////// CMaxqPrimitiveNode ////////////

CMaxqPrimitiveNode::CMaxqPrimitiveNode(ISTGActuation *actuationInterface, const std::string name):
	CMaxqNode(name),
	CMaxqMaxNode(name),
	CMaxqQNode(name),
	mActuationInterface(actuationInterface)
{
}

void CMaxqPrimitiveNode::Init()
{
	//mLastC = 0;
	mPseudoReward	= 0;
	mHrReward		= 0;
	RandomizeValues(mQInitMin, mQInitMax);
}

bool CMaxqPrimitiveNode::Load(std::ifstream& fileInStream)
{
	if (!fileInStream.good())
		return false;

	fileInStream.read((char*)&mReportDebugInfo,	sizeof(mReportDebugInfo));
	fileInStream.read((char*)&mLearnRate,	sizeof(mLearnRate));
	fileInStream.read((char*)&mDiscountRate,	sizeof(mDiscountRate));
	fileInStream.read((char*)&mExploreRate,	sizeof(mExploreRate));
	fileInStream.read((char*)&mTaskRepeatFactor,	sizeof(mTaskRepeatFactor));
	fileInStream.read((char*)&mActionAsInt,	sizeof(mActionAsInt));
	fileInStream.read((char*)&mQInitMin,	sizeof(mQInitMin));
	fileInStream.read((char*)&mQInitMax,	sizeof(mQInitMax));

	_IndexPrecision memsize = 0;
	fileInStream.read((char*)&memsize,	sizeof(memsize));
	if (memsize > 0)
	{
		int numTilings = 0;
		fileInStream.read((char*)&numTilings,	sizeof(numTilings));
		if (!SetNumTilings(numTilings)) // Also sets mNumTilings
			return false;
		if (!SetMemsize(memsize)) // Also sets mMemsize
			return false;
		if (!mC->load(fileInStream))
			return false;
	}
	logDebugLn(mLogStream2,"loaded");
	return true;
}

bool CMaxqPrimitiveNode::Save(std::ofstream& fileOutStream)
{
	if (!fileOutStream.good())
		return false;

	if ((mMemsize > 0) && (mC == NULL))
		return false;

	//mName
	fileOutStream.write((char*)&mReportDebugInfo,	sizeof(mReportDebugInfo));
	fileOutStream.write((char*)&mLearnRate,	sizeof(mLearnRate));
	fileOutStream.write((char*)&mDiscountRate,	sizeof(mDiscountRate));
	fileOutStream.write((char*)&mExploreRate,	sizeof(mExploreRate));
	fileOutStream.write((char*)&mTaskRepeatFactor,	sizeof(mTaskRepeatFactor));
	fileOutStream.write((char*)&mActionAsInt,	sizeof(mActionAsInt));
	fileOutStream.write((char*)&mQInitMin,	sizeof(mQInitMin));
	fileOutStream.write((char*)&mQInitMax,	sizeof(mQInitMax));
	fileOutStream.write((char*)&mMemsize,	sizeof(mMemsize));
	if (mMemsize > 0)
	{
		fileOutStream.write((char*)&mNumTilings,	sizeof(mNumTilings));
		if (!mC->save(fileOutStream))
			return false;
	}
	logDebugLn(mLogStream2,"saved");
	return true;
}

void CMaxqPrimitiveNode::ShowParameters()
{

}

int CMaxqPrimitiveNode::Execute(CMaxqMaxNode* executorNode)
{
	// First, transform the formal mAction assignment
	ProcessAssignment();

	mLastAgentState = mCurrentAgentState;

	// Execute sim step
	ExecutePrimitiveAction();

	// Calculate reward
	CalculateReward();
	mTotalReward += mHrReward;

	bool success = Update(mAction);
	if (!success) // what to do on error??
		logErrorLn(mLogStream2,"Update() failed in Execute()");
		//mLogStream << "\n[ERROR] in CMaxqPrimitiveNode::Execute\n";

	mTerminatedLocally = true; // Primitive node always terminates immediately
	return 1;	// Always a duration of 1
}

void CMaxqPrimitiveNode::ExecuteBestAction(CMaxqMaxNode* executorNode)
{
	// First, transform the formal mAction assignment
	ProcessAssignment();

	// Execute sim step
	ExecutePrimitiveAction();
}

int CMaxqPrimitiveNode::ExecuteTimeStep(CMaxqMaxNode* executorNode, char ancestorTerminalStatus)
{
	if ((mTaskToExecute > -1) || (ancestorTerminalStatus)) // The node only has to make the learning step
	{
		// Calculate reward
		CalculateReward();
		mTotalReward += mHrReward;
		bool success = Update(mAction);
		if (!success) // what to do on error??
			logErrorLn(mLogStream2,"Update() failed in ExecuteTimeStep");
			//mLogStream << "\n[ERROR] in CMaxqPrimitiveNode::ExecuteTimeStep\n";
		mTerminatedLocally = true; // Primitive node always terminates immediately
		mTaskToExecute = -1;
		return 0;
	}
	else // This node has to execute its action
	{
		// First, transform the formal mAction assignment
		ProcessAssignment();
		ExecutePrimitiveAction();
		mTaskToExecute = 0;

		// Fill mLastState, to be used in Update()
		mLastAgentState = mCurrentAgentState;

		return 1;
	}
}

//bool CMaxqPrimitiveNode::Update(const CAgentAction& action)
bool CMaxqPrimitiveNode::Update(CAgentAction& action)
{
	_IndexPrecision tiles[MAX_NUM_TILINGS]; //To avoid memory allocation
	CCMem CVal;
	if (!GetTileValue(mLastAgentState, action, tiles, &CVal, TILE_READ))
		return false;
	CCMem CVal_new;
	CVal_new.pseudo() = 0;
	CVal_new.normal() = (_QPrecision)(mLearnRate*mHrReward + (1-mLearnRate)* CVal.normal());
	if (mReportDebugInfo)
		logDebugLn(mLogStream2,"Updating from " << CVal.normal() << " to " << CVal_new.normal());
		//mLogStream << "[DEBUG] " << mName.c_str() << " updating from " << CVal.normal() << " to " << CVal_new.normal() << endl;
	mC->setValue(tiles,CVal,CVal_new);
	return true;
}

_QPrecision CMaxqPrimitiveNode::GetBestAction(bool pseudoC, bool silent)
{
	_IndexPrecision tiles[MAX_NUM_TILINGS]; //To avoid memory allocation
	CCMem Cval;
	if (!GetTileValue(mCurrentAgentState, mAction, tiles, &Cval, TILE_READ))
		return -_QPrecision_MAX;
	return Cval.normal();
}
