#ifndef __CMA_H_INCLUDED
#define __CMA_H_INCLUDED

#include <EA.h>
#include <cmaes_interface.h>
#include <randomc.h>

template<class T>
class CCMAPopulation : public IPopulation<T>
{
  using IPopulation<T>::mLog;

  protected:
    std::vector<CIndividual<T> > mIndividuals;
    double *mFitness;
    cmaes_t mEvo;

    ISegmentedGenotype<T, double> *mBase;
    int mSize;
    double mStdDev;
    std::ofstream mPopFile;
    int mReset, mGeneration;

  public:
    CCMAPopulation(const ISegmentedGenotype<T, double> &base, int size, double stdDev=0.1) : mFitness(0), mBase(base.clone()), mSize(size), mStdDev(stdDev), mReset(0), mGeneration(0)
    {
      reset();
    }

    ~CCMAPopulation()
    {
      deinit();
      delete mBase;
    }

    void deinit(void)
    {
      mLogNoticeLn("Committing genocide");
      mIndividuals.clear();
      cmaes_exit(&mEvo);

      mPopFile.close();
    }

    void reset()
    {
      if (!mIndividuals.empty())
        deinit();

      std::ostringstream buf;
      buf << "population_" << mReset++ << ".m";
      mPopFile.open(buf.str().c_str());

      mLogNoticeLn("CMAES algorithm building population of " << mSize << " individuals");

      for (int ii=0; ii != mSize; ii++)
      {
        mIndividuals.push_back(mBase->clone());
        mIndividuals[ii].init();
      }

      int dimension = mBase->getNumGenes();
      double *xstart = new double[dimension];
      double *stddev = new double[dimension];
      long int seed = gRanrotB.BRandom();

      // Starting condition
      for (int ii=0; ii != dimension; ++ii)
        stddev[ii] = (xstart[ii] = 0) + mStdDev;

      // Initialize cmaes
      mFitness = cmaes_init(&mEvo, dimension, xstart, stddev, seed, mSize, "non");

      // Generate initial search points
      samplePopulation();
    }

    typename IPopulation<T>::iterator begin()
    {
      return mIndividuals.begin();
    }

    typename IPopulation<T>::iterator end()
    {
      return mIndividuals.end();
    }

    void evolve(bool burst)
    {
      mGeneration++;

      updateDistribution();
      samplePopulation();

      mLogInfoLn("CMAES axis " << cmaes_Get(&mEvo, "minaxislength") << " - " << cmaes_Get(&mEvo, "maxaxislength") << ", stddev " << cmaes_Get(&mEvo, "minstddev") << " - " << cmaes_Get(&mEvo, "maxstddev"));
    }

  private:
    void samplePopulation(void)
    {
      double *const *pop = cmaes_SamplePopulation(&mEvo);

      int cc = 0;
      for (typename IPopulation<T>::iterator ii = begin(); ii != end(); ++ii, ++cc)
      {
        mPopFile << "pop(" << mGeneration+1 << ", " << cc+1 << ", :) = [";

        bool changed = false;
        std::vector<double> values = ii->getGenotype().getGenes();
        for (int jj=0; jj < ii->getGenotype().getNumGenes(); ++jj)
        {
          if (jj) mPopFile << ",";
          mPopFile << pop[cc][jj];

          if (changed)
            values[jj] = pop[cc][jj];
          else if (values[jj] != pop[cc][jj])
          {
            changed = true;
            values[jj] = pop[cc][jj];
          }
        }

        if (changed)
        {
          ii->getGenotype().setGenes(values);
          ii->invalidate();
        }

        mPopFile << "];" << std::endl;
      }
    }

    void updateDistribution(void)
    {
      int cc = 0;
      for (typename IPopulation<T>::iterator ii = begin(); ii != end(); ++ii)
      {
        if (ii->requireEvaluation())
          mLogErrorLn("Tried to evolve without assessing all individuals");
        mFitness[cc++] = -ii->getFitness();
      }

      // Update search distribution
      cmaes_UpdateDistribution(&mEvo, mFitness);

      if (llInfo >= mLog.getLevel())
      {
        const double *xmean = cmaes_GetPtr(&mEvo, "xmean");
        mLogInfo("CMAES mean [");
        for (int ii=0; ii != mBase->getNumGenes(); ii++)
        {
          mLogInfo(xmean[ii]);
          if (ii != mSize-1)
            mLogInfo(", ");
        }
        mLogInfoLn("]");
      }
    }
};

#endif /* __CMA_H_INCLUDED */
