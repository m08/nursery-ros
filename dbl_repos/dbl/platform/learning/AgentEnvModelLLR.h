/*
 * AgentEnvModelLLR.h
 *
 *  Created on: Sep 15, 2010
 *      Author: Erik Schuitema
 */

#ifndef AGENTENVMODELLLR_H_
#define AGENTENVMODELLLR_H_

#include <AgentDyna.h>
#include <llr.h>
#include <Log2.h>

// CAgentEnvModelLLR is a class that uses an existing LLR model.
// The reason for this is that the model most probably has other functions as well.
class CAgentEnvModelLLR: public CAgentEnvModel
{
	protected:
		CLog2			mLog;
		ILLR			*mLLR;
		double			mStatePredictionIntervalLimits[AGENT_MAX_NUM_STATEVARS];
		double			mRewardPredictionIntervalLimit;
		bool			mDoubleCheckPrediction;	// Option to double check backward prediction with forward prediction
		virtual bool	predict(SearchDirection direction, const CAgentState& state0, const CAgentAction& action, CAgentState* state1, ComputationalType* reward);

	public:
		CAgentEnvModelLLR(ILLR* llr);
		virtual ~CAgentEnvModelLLR();

		// predictForward() and predictBackward() return a boolean whether the prediction was successful (i.e., confident)
		virtual void	setPredictionIntervalLimits(const double* statePredIntLimits, const double rewardPredIntLimits, const double confidenceStudentTValue);
		virtual void	setMaxNeighborDistance(double maxDist);
		virtual void	resetMemory();
		virtual bool	predictForward(const CAgentState& stateT0, const CAgentAction& action, CAgentState* stateT1, ComputationalType* reward);
		virtual bool	predictBackward(const CAgentState& stateT1, const CAgentAction& action, CAgentState* stateT0, ComputationalType* reward);
		virtual void	reportTransition(double* rawStateT0, double* rawAction, double* rawStateT1, double reward);
};


#endif /* AGENTENVMODELLLR_H_ */
