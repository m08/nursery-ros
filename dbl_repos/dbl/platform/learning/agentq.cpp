/**
 * @file   agentq.cpp
 * @author E. Kuznetsov, E. Schuitema, S. Troost
 * @date   January 2004
 *
 * @brief  Q learning agent BASE class
 * note: angle notation is usually weird...
 */

//#include <cstdio>
#include <iostream>
#include <algorithm>
//#include <iomanip>
#include <fstream>

#include "agentq.h"
#include <randomc.h>
#include <float.h> // for FLT_MAX ...
#ifdef _MSC_VER
#define strcasecmp _stricmp
#endif

using namespace std;

// ************************************************************* //
// ************************** CAgentQ ************************** //
// ************************************************************* //

CAgentQ::CAgentQ(CQSpace* qspace, CPolicySpace* policySpace) :
		mLog("agentq"), mAlgorithmType(laDefault), mExplorationType(etEGreedy), mLearnRate(
				_LEARN_RATE), mInitialLearnRate(_LEARN_RATE), mLearnRateDecayRate(
				1.0), mExploreRate(_EXPLORE_RATE), mSoftMaxTemperature(1.0), mGamma(
				_GAMMA), mLambda(_LAMBDA), mTotalReward(0), mQSpace(qspace), mPolicySpace(
				policySpace), mQInitMin(0), mQInitMax((_QPrecision) 0.1), mUseActionValueList(
				false), mAgentActionValueList(NULL), mShowQValues(false), mSyncExploration(
				false), mRandom(0), mUseAbsoluteBounds(false) {
}

CAgentQ::~CAgentQ(void) {
	if (mQSpace != NULL) {
		delete mQSpace;
		mQSpace = NULL;
	}

	if (mPolicySpace != NULL) {
		delete mPolicySpace;
		mPolicySpace = NULL;
	}

	if (mAgentActionValueList != NULL) {
		delete mAgentActionValueList;
		mAgentActionValueList = NULL;
	}

	removeSubAgents();

	mLogInfoLn("AgentQ shot");
}


void CAgentQ::setRelativeBound(const std::vector<double> &bound)
{

	for(int i=0; i<bound.size(); i++)
	{
		mRelativeBoundsInt.resize(bound.size());
		int temp_bound=abs(mAgentAction.mDescriptors[i].getIndexByVal(bound.at(i))-mAgentAction.mDescriptors[i].getIndexByVal(0.0));

		if(temp_bound==0)
		{
			temp_bound=1;
		}

		mRelativeBoundsInt.at(i)=temp_bound;


	}


}

void CAgentQ::setUnderRelativeBound(const std::vector<double> &under_bound)
{
	for(int i=0; i<under_bound.size(); i++)
	{
		mUnderRelativeBoundsInt.resize(under_bound.size());
		int temp_bound=abs(mAgentAction.mDescriptors[i].getIndexByVal(under_bound.at(i))-mAgentAction.mDescriptors[i].getIndexByVal(0.0));

		mUnderRelativeBoundsInt.at(i)=temp_bound;
	}

}

void CAgentQ::removeSubAgents() {
	while (mSubAgents.size() > 0) {
		delete mSubAgents.back();
		mSubAgents.pop_back();
	}
}

void CAgentQ::addSubAgent(CQSpace* qspace, CPolicySpace* policySpace) {
	mSubAgents.push_back(
			new CAgentQSubAgent(this, mSubAgents.size(), qspace, policySpace));
	// If we are going to use subagents, we always need the actionvalue list, both for the
	// parent agent as well as for all the sub-agents
	mUseActionValueList = true;
	mSubAgents.back()->enableActionValueList(true);
}

std::string CAgentQ::getAlgoName(ELearningAlgo algo) {
	switch (algo) {
	case laQLearning:
		return "Q";
	case laSA_Qlearning:
		return "SA_Q";
	case laSARSA:
		return "SARSA";
	case laRelative_bounds:
		return "Relative_bounds";
	case laRLearning:
		return "R";
	case laR_SARSA:
		return "R_SARSA";
	case laGM_Q:
		return "GM_Q";
	case laGM_SARSA:
		return "GM_SARSA";
	case laDAS_SARSA:
		return "DAS_SARSA";
	case laDAS_Q:
		return "DAS_Q";
	case laAR_Q:
		return "AR_Q";
	case laAR_SARSA:
		return "AR_SARSA";
	default:
		return "-";
	}
}

std::string CAgentQ::getAlgoName() {
	return getAlgoName(mAlgorithmType);
}

void CAgentQ::setAlgo(ELearningAlgo algo) {
	mAlgorithmType = algo;
	switch (algo) {
	case laGM_Q:
	case laAR_Q:
	case laDAS_Q:
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
			mSubAgents[iAgent]->setAlgo(laQLearning);
		break;
	case laGM_SARSA:
	case laAR_SARSA:
	case laDAS_SARSA:
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
			mSubAgents[iAgent]->setAlgo(laSARSA);
		break;
	default:
		return;
	}
}

bool CAgentQ::setAlgoByName(const char* name) {
	if (strcasecmp(name, "q") == 0)
		mAlgorithmType = laQLearning;
	else if (strcasecmp(name, "sa-q") == 0)
		mAlgorithmType = laSA_Qlearning;
	else if (strcasecmp(name, "sa_q") == 0)
		mAlgorithmType = laSA_Qlearning;
	else if (strcasecmp(name, "r") == 0)
		mAlgorithmType = laRLearning;
	else if (strcasecmp(name, "rlearning") == 0)
		mAlgorithmType = laRLearning;
	else if (strcasecmp(name, "r-learning") == 0)
		mAlgorithmType = laRLearning;
	else if (strcasecmp(name, "r_sarsa") == 0)
		mAlgorithmType = laR_SARSA;
	else if (strcasecmp(name, "rsarsa") == 0)
		mAlgorithmType = laR_SARSA;
	else if (strcasecmp(name, "r-sarsa") == 0)
		mAlgorithmType = laR_SARSA;
	else if (strcasecmp(name, "sarsa") == 0)
		mAlgorithmType = laSARSA;
	else if (strcasecmp(name, "Relative_bounds") == 0)
		mAlgorithmType = laRelative_bounds;

	else if ((strcasecmp(name, "gm_q") == 0) || (strcasecmp(name, "gm-q") == 0)
			|| (strcasecmp(name, "gmq") == 0))
		setAlgo(laGM_Q);
	else if ((strcasecmp(name, "gm_sarsa") == 0)
			|| (strcasecmp(name, "gm-sarsa") == 0)
			|| (strcasecmp(name, "gmsarsa") == 0))
		setAlgo(laGM_SARSA);
	else if ((strcasecmp(name, "ar_q") == 0) || (strcasecmp(name, "ar-q") == 0)
			|| (strcasecmp(name, "arq") == 0))
		setAlgo(laAR_Q);
	else if ((strcasecmp(name, "ar_sarsa") == 0)
			|| (strcasecmp(name, "ar-sarsa") == 0)
			|| (strcasecmp(name, "arsarsa") == 0))
		setAlgo(laAR_SARSA);
	else if ((strcasecmp(name, "das_sarsa") == 0)
			|| (strcasecmp(name, "das-sarsa") == 0)
			|| (strcasecmp(name, "dassarsa") == 0))
		setAlgo(laDAS_SARSA);
	else if ((strcasecmp(name, "das_q") == 0)
			|| (strcasecmp(name, "das-q") == 0)
			|| (strcasecmp(name, "dasq") == 0))
		setAlgo(laDAS_Q);
	else
		return false;

	return true;
}
void CAgentQ::setExplorationType(EExplorationType expType) {
	mExplorationType = expType;
	if (expType == etSoftMax)
		mUseActionValueList = true;

	// TODO: should we do this?
	/*
	 if (mSubAgents.size() > 0)
	 for (uint32_t iAgent=0; iAgent<mSubAgents.size(); iAgent++)
	 mSubAgents[iAgent]->setExplorationType(expType);
	 */
}

bool CAgentQ::setExplorationTypeByName(const char* name) {
	if ((strcasecmp(name, "egreedy") == 0)
			|| (strcasecmp(name, "e-greedy") == 0)
			|| (strcasecmp(name, "e_greedy") == 0))
		setExplorationType(etEGreedy);
	else if ((strcasecmp(name, "softmax") == 0)
			|| (strcasecmp(name, "soft-max") == 0)
			|| (strcasecmp(name, "soft_max") == 0))
		setExplorationType(etSoftMax);
	else
		return false;

	return true;
}

void CAgentQ::enableActionValueList(bool enabled) {
	mUseActionValueList = enabled;
}

double CAgentQ::calculateReward() {
	return 0;
}

void CAgentQ::displayVariables() {
	mLogInfoLn(" ------------------------------------------------- ");
	mLogInfoLn("[*]  Algorithm: " << getAlgoName());
	mLogInfoLn("[*]  learnRate\t" << mLearnRate);
	if (mLearnRateDecayRate < 1.0)
		mLogInfoLn(
				"[*]    (initially: " << mInitialLearnRate << ", decayRate: " << mLearnRateDecayRate << ")");
	if (mExplorationType == etEGreedy)
		mLogInfoLn("[*]  exploreRate \t" << mExploreRate);
	else if (mExplorationType == etSoftMax)
		mLogInfoLn("[*]  softmax temperature\t" << mSoftMaxTemperature);
	else
		mLogInfoLn("[*]  ERROR: no exploration type specified");
	mLogInfoLn("[*]  gamma\t" << mGamma);
	mLogInfoLn("[*]  lambda\t" << mLambda);
	mLogInfoLn(
			"[*]  memsize\t" << ((mQSpace!=NULL)?mQSpace->getMemorySizeBytes():0) << "bytes");
	mLogInfoLn(" ------------------------------------------------- ");

	// Display sub-agents as well
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
		mLogInfoLn(
				" -------- Variables for sub-agent " << iAgent << " -------------- ");
		mSubAgents[iAgent]->displayVariables();
	}
}

/// initialisation with parameters
void CAgentQ::setLearnParameters(const ComputationalType _learnrate,
		const ComputationalType _exploration_param,
		const ComputationalType _gamma, const ComputationalType _lambda) {

	mLearnRate = mInitialLearnRate = _learnrate;
	switch (mExplorationType) {
	case etEGreedy:
		mExploreRate = _exploration_param;
		break;
	case etSoftMax:
		mSoftMaxTemperature = _exploration_param;
		break;
	default:
		break;
	}
	mGamma = _gamma;
	mLambda = _lambda;

	// The length of the trace is not updated because memory allocation is required, which is not acceptable in real-time apps.
	// This is done in init(). Don't use this function during learning and expect the trace length to be adapted.
	if (mQSpace != NULL)
		mQSpace->traceInit((_QPrecision) (mGamma * mLambda), _MIN_TRACE_VALUE,
				false);
	displayVariables();
}

bool CAgentQ::readConfig(const CConfigSection &configNode) {
	bool configresult = true;
	configresult &= mLogAssert(configNode.get("learnRate", &mLearnRate));
	mInitialLearnRate = mLearnRate;
	bool learnRateDecaying = configNode.get("learnRateDecayRate",
			&mLearnRateDecayRate, 1.0);
	if (learnRateDecaying && (sizeof(mLearnRateDecayRate) < sizeof(double)))
		mLogWarningLn(
				"Floating point precision is pretty low for learn rate decaying! Try changing 'ComputationalType' in agentq.h to, e.g., 'double' to avoid problems.");

	configresult &= mLogAssert(configNode.get("exploreRate", &mExploreRate));
	configNode.get("softmaxTemperature", &mSoftMaxTemperature);
	configresult &= mLogAssert(configNode.get("discountRate", &mGamma));
	configresult &= mLogAssert(configNode.get("traceFalloffRate", &mLambda));
	configresult &= mLogAssert(configNode.get("QInitMin", &mQInitMin));
	configresult &= mLogAssert(configNode.get("QInitMax", &mQInitMax));
	configNode.get("syncExploration", &mSyncExploration);
	std::string algoName, expTypeName;
	if (configNode.get("algorithm", &algoName))
		configresult &= mLogAssert(setAlgoByName(algoName.c_str()));
	if (configNode.get("explorationType", &expTypeName))
		configresult &=
				mLogAssert(setExplorationTypeByName(expTypeName.c_str()));
	return configresult;
}

// initialise the agentq
bool CAgentQ::init() {
	// If we don't have our own QSPace and there are no sub-agents, we failed
	if (mQSpace == NULL)
		if (mSubAgents.size() == 0)
			return false;

	//gRanrotB.RandomInit(mRandomSeed);	// Seed moved outside the agent
	mQAction = 0;
	mAverageReward = 0;
	mExecutedActionType = atExplorative;

	// Assume that mAgentAction is initialized
	if (mUseActionValueList) {
		mAgentActionValueList = new CAgentActionValueList;
		mAgentActionValueList->resize(mAgentAction.getNumActions());
	}

	// Initialize QSpace if available
	if (mQSpace != NULL) {
		mQSpace->setInitialQValuePolicy(mQInitMin, mQInitMax);
		mQSpace->resetMemory();
		mQSpace->traceInit((_QPrecision) (mGamma * mLambda), _MIN_TRACE_VALUE);
	}
	// Initialize policy if available
	if (mPolicySpace != NULL) {
		mPolicySpace->resetMemory();
	}

	// Initialize sub-agents if available
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
		// Copy learning parameters
		mSubAgents[iAgent]->mLearnRate = mLearnRate;
		mSubAgents[iAgent]->mInitialLearnRate = mInitialLearnRate;
		mSubAgents[iAgent]->mLearnRateDecayRate = mLearnRateDecayRate;
		mSubAgents[iAgent]->mExploreRate = mExploreRate;
		mSubAgents[iAgent]->mSoftMaxTemperature = mSoftMaxTemperature;
		mSubAgents[iAgent]->mGamma = mGamma;
		mSubAgents[iAgent]->mLambda = mLambda;
		mSubAgents[iAgent]->mQInitMin = mQInitMin;
		mSubAgents[iAgent]->mQInitMax = mQInitMax;
		mSubAgents[iAgent]->mSyncExploration = mSyncExploration;

		// Copy state and action definition
		mSubAgents[iAgent]->mAgentState = mAgentState;
		mSubAgents[iAgent]->mAgentAction = mAgentAction;
		// Init
		mSubAgents[iAgent]->init();
	}

	return true;
}

void CAgentQ::setQSpace(CQSpace* qspace) {
	if (mQSpace == qspace)
		return;
	// We are owner, so remove the old QSpace
	if (mQSpace != NULL)
		delete mQSpace;

	mQSpace = qspace;
}

void CAgentQ::setPolicySpace(CPolicySpace* policySpace) {
	if (mPolicySpace == policySpace)
		return;
	// We are owner, so remove the old policy space
	if (mPolicySpace != NULL)
		delete mPolicySpace;

	mPolicySpace = policySpace;
}

double CAgentQ::learn(char agentInTerminalState) {
	double result;

	switch (mAlgorithmType) {
	case laQLearning:
		result = learn_QLearning(agentInTerminalState);
		break;
	case laQObserve:
		result = learn_QObserve(agentInTerminalState);  // initially observe preprogrammed controller
		break;
	case laSARSA:
	case laRelative_bounds:                           // then use SARSA to learn
		result = learn_SARSA(agentInTerminalState);
		break;
	case laACSARSA:
		result = learn_ACSARSA(agentInTerminalState);
		break;
	case laGM_Q:
	case laGM_SARSA:
	case laAR_Q:
	case laAR_SARSA:
	case laDAS_SARSA:
	case laDAS_Q: {
		result = 0;
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
			result += mSubAgents[iAgent]->learn(agentInTerminalState);
	}
		break;
	default:
		result = -1;
		break;
	}

	// Decay the learn rate
	mLearnRate *= mLearnRateDecayRate;

	return result;
}

bool CAgentQ::consultPolicy() {

	bool result = true;
	result &= consultExplorationPolicy();
	if(!getExplorationStep())
	{
		result &= consultGreedyPolicy();
	}
	return result;
}
bool CAgentQ::consultGreedyPolicy() {
	switch (mAlgorithmType) {
	case laQLearning:
	case laSARSA:
		return consultDefaultGreedyPolicy();
	case laRelative_bounds:
		return consultRelative_boundsGreedyPolicy();
	case laACSARSA:
		return consultACGreedyPolicy();
	case laGM_Q:
	case laGM_SARSA:
		return consultGMGreedyPolicy();
	case laAR_Q:
	case laAR_SARSA:
		return consultARGreedyPolicy();
	case laDAS_Q:
	case laDAS_SARSA:
		return consultDASGreedyPolicy();
	case laQObserve:
		// No policy consultation! mAgentAction is externally provided, i.e. set outside CAgentQ
		return true;
	default:
		return false;
	}
}

bool CAgentQ::consultExplorationPolicy() {
	switch (mAlgorithmType) {
	case laQLearning:
	case laSARSA:
		return consultDefaultExplorationPolicy();// Exploration policy may overwrite mAgentAction
	case laRelative_bounds:
		return consultRelativeExplorationPolicy();
	case laACSARSA:
		return consultACExplorationPolicy();
	case laGM_Q:
	case laGM_SARSA:
		return consultGMExplorationPolicy();// Exploration policy may overwrite mAgentAction
	case laAR_Q:
	case laAR_SARSA:
		return consultARExplorationPolicy();// Exploration policy may overwrite mAgentAction
	case laDAS_Q:
	case laDAS_SARSA:
		return consultDASExplorationPolicy();// Exploration policy may overwrite mAgentAction
	case laQObserve:
		// No policy consultation! mAgentAction is externally provided, i.e. set outside CAgentQ
		return true;
	default:
		return false;
	}
}

bool CAgentQ::consultDefaultGreedyPolicy() {
	mExecutedActionType = atGreedy;
	return (mQSpace->getBestActionQValue(mAgentState, mAgentAction,
			mAgentActionValueList) != CONST_QVALUE_INVALID);
	// DEBUG TEST
	/*
	 _QPrecision approxMaxQ	= mQSpace->getBestActionQValue_Approx(mAgentState, mAgentAction, 2);
	 _QPrecision realMaxQ	= mQSpace->getBestActionQValue(mAgentState, mAgentAction);
	 if (approxMaxQ == realMaxQ)
	 mLogInfoLn("Approximation of getBestAction() was correct!");
	 else
	 mLogWarningLn("Approximation of getBestAction() returned " << approxMaxQ << " instead of " << realMaxQ);
	 */
}

bool CAgentQ::consultRelative_boundsGreedyPolicy() {
	mExecutedActionType = atGreedy;
	//added by H Meijdam
	//std::cout<<"begin \n";
	//int previousAction = mAgentAction.mDescriptors[0].getIndexByVal(mAgentAction.values()[0]);
	//std::cout<<"previousAction: "<< previousAction<< "\n";
	//added by H Meijdam
	//std::cout<<mQSpace->probeQValue(mAgentState, mAgentAction);


	return (mQSpace->getBestRelativeActionQValue(mAgentState, mAgentAction,
			mRelativeBoundsInt, mUnderRelativeBoundsInt, mUseAbsoluteBounds,
			mAgentActionValueList) != CONST_QVALUE_INVALID);

}

bool CAgentQ::consultDefaultExplorationPolicy() {
	bool result = true;
	switch (mExplorationType) {
	case etEGreedy: {
		// choose random action
		double random;
		if (mSyncExploration)
			random = mRandom;
		else
			random = gRanrotB.Random();

		if (random < mExploreRate) {
			mExecutedActionType = atExplorative;
			mAgentAction.randomizeUniform(&gRanrotB);
			result &= (mQSpace->getExplorativeActionQValue(mAgentState,
					mAgentAction) != CONST_QVALUE_INVALID);
		}
	}
		break;
	case etSoftMax:
		// Softmax always explores
	{
		mExecutedActionType = atExplorative;
		int softmaxAction = mAgentAction.randomizeGibbs(mAgentActionValueList,
				mSoftMaxTemperature, &gRanrotB);
		result &= (mQSpace->getExplorativeActionQValue(mAgentState,
				mAgentAction) != CONST_QVALUE_INVALID);

		// Debug information
		mLogInfo(
				"Took softmax action, Q=" << (*mAgentActionValueList)[softmaxAction].mValue);
		if (softmaxAction == mAgentActionValueList->mBestAction)
			mLogInfoLn(", greedy");
		else
			mLogInfoLn(
					", greedy Q=" << (*mAgentActionValueList)[mAgentActionValueList->mBestAction].mValue);
	}
		break;
	default:
		break;
	}

	return result;
}


bool CAgentQ::consultRelativeExplorationPolicy() {

	bool result = true;


	switch (mExplorationType) {
	case etEGreedy: {
		// choose random action
		double random;
		if (mSyncExploration)
			random = mRandom;
		else
			random = gRanrotB.Random();

		if (random < mExploreRate)
		{
			explorationStep=true;
			//	while (first|| !leave)
				//			{
					//			first = false;
					//			mExecutedActionType = atExplorative;
					//			mAgentAction.randomizeUniform(&gRanrotB);
					//			if(mQSpace->getWithinAllBounds(previousAction, mRelativeBound, mUnderRelativeBound, mAgentAction., mUseAbsoluteBounds))
					//		}
			int vars = mAgentAction.getNumActionVars();
			//std::cout<<mAgentAction.mDescriptors[0].mIntervalSize<<"  "<<mAgentAction.mDescriptors[1].mIntervalSize;
			int previousActions[vars];
			int varNumSteps[vars];
			for (int i = 0; i<vars; i++)
			{
				previousActions[i] = mAgentAction.mDescriptors[i].getIndexByVal(mAgentAction.values()[i]);
				varNumSteps[i]=mAgentAction.mDescriptors[i].mNumSteps;
			}
			int candidateActions[vars];
			mExecutedActionType = atExplorative;
			mAgentAction.randomizeUniform(&gRanrotB);
			for (int k = 0; k<vars; k++)
			{
				candidateActions[k] = mAgentAction.mDescriptors[k].getIndexByVal(mAgentAction.getActionVar(k));
			}

			while (!mQSpace->getWithinAllBounds(previousActions, varNumSteps, vars, mRelativeBoundsInt,  mUnderRelativeBoundsInt, candidateActions ,mUseAbsoluteBounds))
			{
				mExecutedActionType = atExplorative;
				mAgentAction.randomizeUniform(&gRanrotB);
				for (int k = 0; k<vars; k++)
				{
					candidateActions[k] = mAgentAction.mDescriptors[k].getIndexByVal(mAgentAction.getActionVar(k));
				}


			}
			result &= (mQSpace->getExplorativeActionQValue(mAgentState,	mAgentAction) != CONST_QVALUE_INVALID);

		}
		else
		{
			explorationStep=false;
		}
	}
		break;
	case etSoftMax:
		// Softmax always explores
	{
		mExecutedActionType = atExplorative;
		int softmaxAction = mAgentAction.randomizeGibbs(mAgentActionValueList,
				mSoftMaxTemperature, &gRanrotB);
		result &= (mQSpace->getExplorativeActionQValue(mAgentState,
				mAgentAction) != CONST_QVALUE_INVALID);

		// Debug information
		mLogInfo(
				"Took softmax action, Q=" << (*mAgentActionValueList)[softmaxAction].mValue);
		if (softmaxAction == mAgentActionValueList->mBestAction)
			mLogInfoLn(", greedy");
		else
			mLogInfoLn(
					", greedy Q=" << (*mAgentActionValueList)[mAgentActionValueList->mBestAction].mValue);
	}
		break;
	}

	return result;
}

void CAgentQ::useAbsoluteBounds() {
	mUseAbsoluteBounds = true;
}
bool CAgentQ::consultACGreedyPolicy() {
	mExecutedActionType = atGreedy;
	return mPolicySpace->consultPolicy(mAgentState, mAgentAction);
	// Optional: discretize action
	//mAgentAction.discretize();
}

bool CAgentQ::consultACExplorationPolicy() {
	bool result = true;
	switch (mExplorationType) {
	case etEGreedy: {
		// choose random action
		double random;
		if (mSyncExploration)
			random = mRandom;
		else
			random = gRanrotB.Random();

		if (random < mExploreRate) {
			mAgentAction.randomizeUniform(&gRanrotB);
			mExecutedActionType = atExplorative;
			result &= mPolicySpace->consultExporationPolicy(mAgentState,
					mAgentAction);
		}
	}
		break;
	case etSoftMax: {
		mAgentAction.randomizeGibbs(mAgentActionValueList, mSoftMaxTemperature,
				&gRanrotB);
		mExecutedActionType = atExplorative;
		result &= mPolicySpace->consultExporationPolicy(mAgentState,
				mAgentAction);
	}
		break;
	default:
		break;
	}
	return result;
}

bool CAgentQ::consultGMGreedyPolicy() {
	bool result = true;
	mExecutedActionType = atGreedy;
	// Request action value list of all agents
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
		result &= mSubAgents[iAgent]->consultGreedyPolicy();
	// Sum all agent lists to have a useful list for the greatest mass algorithm; copy action as well from agent 0
	_QPrecision bestQValue = -_QPrecision_MAX;
	for (uint32_t iAction = 0; iAction < mAgentActionValueList->size();
			iAction++) {
		// Init mValue to zero
		(*mAgentActionValueList)[iAction].mValue = 0;
		// Copy action details to our own list entry
		memcpy((*mAgentActionValueList)[iAction].mAction,
				(*mSubAgents[0]->getAgentActionValueList())[iAction].mAction,
				mAgentAction.getNumActionVars() * sizeof(_StatePrecision));
		// Sum agent mValue's to own mValue
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
			(*mAgentActionValueList)[iAction].mValue +=
					(*mSubAgents[iAgent]->getAgentActionValueList())[iAction].mValue;
		// Keep track of best action in the summed list
		if ((*mAgentActionValueList)[iAction].mValue > bestQValue) {
			bestQValue = (*mAgentActionValueList)[iAction].mValue;
			mAgentActionValueList->mBestAction = iAction;
		}
	}
	// fill mAgentAction
	mAgentAction.restore(mAgentActionValueList->bestAction());

	// Communicate our decision to the sub-agents
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
		// Request the Q-value of this greedy action and store it in each agent's QSpace
		result &=
				(mSubAgents[iAgent]->getQSpace()->getBestActionQValue_GivenAction(
						mSubAgents[iAgent]->getAgentState(), mAgentAction)
						!= CONST_QVALUE_INVALID);
	}
	return result;
}

bool CAgentQ::consultGMExplorationPolicy() {
	bool result = true;
	// Decide whether we want to explore or not
	switch (mExplorationType) {
	case etEGreedy: {
		// choose random action
		double random;
		if (mSyncExploration)
			random = mRandom;
		else
			random = gRanrotB.Random();

		if (random < mExploreRate) {
			mExecutedActionType = atExplorative;
			mAgentAction.randomizeUniform(&gRanrotB);
			//mQSpace->getExplorativeActionQValue(mAgentState, mAgentAction);
		}
	}
		break;
	case etSoftMax:
		// Softmax always explores
	{
		mExecutedActionType = atExplorative;
		int softmaxAction = mAgentAction.randomizeGibbs(mAgentActionValueList,
				mSoftMaxTemperature, &gRanrotB);
		//mQSpace->getExplorativeActionQValue(mAgentState, mAgentAction);

		// Debug information
		mLogInfo(
				"Took softmax action, Q=" << (*mAgentActionValueList)[softmaxAction].mValue);
		if (softmaxAction == mAgentActionValueList->mBestAction)
			mLogInfoLn(", greedy");
		else
			mLogInfoLn(
					", greedy Q=" << (*mAgentActionValueList)[mAgentActionValueList->mBestAction].mValue);
	}
		break;
	default:
		break;
	}

	// Communicate our decision to the sub-agents
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
		// Set the type of action we will take
		mSubAgents[iAgent]->setExecutedActionType(mExecutedActionType);
		// If explorative, make sure to store the correct Q-value and feature indices through the QSpace of each agent
		if (mExecutedActionType == atExplorative) {
			// Request the Q-value of this exploratory action and store it in each agent's QSpace
			result &=
					(mSubAgents[iAgent]->getQSpace()->getExplorativeActionQValue(
							mSubAgents[iAgent]->getAgentState(), mAgentAction)
							!= CONST_QVALUE_INVALID);
		}
	}
	return result;
}

#define MAX_NUM_SUBAGENTS 16

bool CAgentQ::consultARGreedyPolicy() {
	_QPrecision visits[MAX_NUM_SUBAGENTS];

	bool result = true;
	mExecutedActionType = atGreedy;
	// Request action value list of all agents
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
		result &= mSubAgents[iAgent]->consultGreedyPolicy();
	// Sum all agent lists to have a useful list for the greatest mass algorithm; copy action as well from agent 0
	_QPrecision bestQValue = -_QPrecision_MAX;

	for (uint32_t iAction = 0; iAction < mAgentActionValueList->size();
			iAction++) {
		// Init mValue to zero
		(*mAgentActionValueList)[iAction].mValue = 0;
		// Copy action details to our own list entry
		memcpy((*mAgentActionValueList)[iAction].mAction,
				(*mSubAgents[0]->getAgentActionValueList())[iAction].mAction,
				mAgentAction.getNumActionVars() * sizeof(_StatePrecision));

		// Preload number of visits for all subagents
		memset(visits, 0, mSubAgents.size() * sizeof(_QPrecision));
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
			visits[iAgent] =
					(*mSubAgents[iAgent]->getAgentActionValueList())[iAction].mVisits;

		// Sum agent mValue's to own mValue
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
			double weight = getSubAgentWeight(iAgent, visits);
			(*mAgentActionValueList)[iAction].mValue +=
					weight
							* (*mSubAgents[iAgent]->getAgentActionValueList())[iAction].mValue;
		}
		// Keep track of best action in the summed list
		if ((*mAgentActionValueList)[iAction].mValue > bestQValue) {
			bestQValue = (*mAgentActionValueList)[iAction].mValue;
			mAgentActionValueList->mBestAction = iAction;
		}
	}
	// fill mAgentAction
	mAgentAction.restore(mAgentActionValueList->bestAction());

	// Communicate our decision to the sub-agents
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
		// Request the Q-value of this greedy action and store it in each agent's QSpace
		result &=
				(mSubAgents[iAgent]->getQSpace()->getBestActionQValue_GivenAction(
						mSubAgents[iAgent]->getAgentState(), mAgentAction)
						!= CONST_QVALUE_INVALID);
	}
	return result;
}

bool CAgentQ::consultARExplorationPolicy() {
	bool result = true;
	// Decide whether we want to explore or not
	switch (mExplorationType) {
	case etEGreedy: {
		// choose random action
		double random;
		if (mSyncExploration)
			random = mRandom;
		else
			random = gRanrotB.Random();

		if (random < mExploreRate) {
			mExecutedActionType = atExplorative;
			mAgentAction.randomizeUniform(&gRanrotB);
			//result &= (mQSpace->getExplorativeActionQValue(mAgentState, mAgentAction) != CONST_QVALUE_INVALID);
		}
	}
		break;
	case etSoftMax:
		// Softmax always explores
	{
		mExecutedActionType = atExplorative;
		int softmaxAction = mAgentAction.randomizeGibbs(mAgentActionValueList,
				mSoftMaxTemperature, &gRanrotB);
		//result &= (mQSpace->getExplorativeActionQValue(mAgentState, mAgentAction) != CONST_QVALUE_INVALID);

		// Debug information
		mLogInfo(
				"Took softmax action, Q=" << (*mAgentActionValueList)[softmaxAction].mValue);
		if (softmaxAction == mAgentActionValueList->mBestAction)
			mLogInfoLn(", greedy");
		else
			mLogInfoLn(
					", greedy Q=" << (*mAgentActionValueList)[mAgentActionValueList->mBestAction].mValue);
	}
		break;
	default:
		break;
	}

	// Communicate our decision to the sub-agents
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
		// Set the type of action we will take
		mSubAgents[iAgent]->setExecutedActionType(mExecutedActionType);
		// If explorative, make sure to store the correct Q-value and feature indices through the QSpace of each agent
		if (mExecutedActionType == atExplorative) {
			// Request the Q-value of this exploratory action and store it in each agent's QSpace
			result &=
					(mSubAgents[iAgent]->getQSpace()->getExplorativeActionQValue(
							mSubAgents[iAgent]->getAgentState(), mAgentAction)
							!= CONST_QVALUE_INVALID);
		}
	}
	return result;
}

bool CAgentQ::consultDASGreedyPolicy() {
	bool result = true;
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
		// Consult sub-agent's greedy policy
		result &= mSubAgents[iAgent]->consultGreedyPolicy();
		// Copy its first action to the parent mAgentAction
		mAgentAction[iAgent] =
				mSubAgents[iAgent]->getAgentAction().getActionVar(0);
	}
	return result;
}

bool CAgentQ::consultDASExplorationPolicy() {
	bool result = true;
	// Synchronized exploration
	if (mSyncExploration) {
		// Draw one random number for all agents
		mRandom = gRanrotB.Random();
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
			mSubAgents[iAgent]->mRandom = mRandom;
	}

	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++) {
		// Consult sub-agent's exploration policy
		result &= mSubAgents[iAgent]->consultExplorationPolicy();
		// Copy its first action to the parent mAgentAction
		mAgentAction[iAgent] =
				mSubAgents[iAgent]->getAgentAction().getActionVar(0);
	}
	return result;
}

double CAgentQ::learn_QLearning(char agentInTerminalState) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
		mQAction = 0;
		//mAgentQLogger << "[DEBUG] Terminal state processed; mQAction = 0" << endl;
	} else {
		mQAction = mQSpace->getStoredBestActionQValue();// Stored during consultPolicy()
		delta += mGamma * mQAction;	// new Q_action
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
	}

	if (mExecutedActionType == atGreedy) {
		mQSpace->traceFall();
		mQSpace->traceAddStoredBest();
	} else if (mExecutedActionType == atExplorative) {
		mQAction = mQSpace->getStoredExplorativeActionQValue();	// Stored during consultPolicy()
		mQSpace->traceClear();
		mQSpace->traceAddStoredExplorative();
	} else if (mExecutedActionType == atEffective) {
		mQAction = mQSpace->getQValue(mAgentState, mAgentEffectiveAction);
		mQSpace->traceFall();
		mQSpace->traceAddStored();
	}

	if (mShowQValues) {
		if (mExecutedActionType == atExplorative)
			mLogDebugLn("QAction: " << mQAction << " (explorative step)");
		else
			mLogDebugLn("QAction: " << mQAction);
	}

	return mLearnRate * delta;
}

int CAgentQ::QStep(char agentInTerminalState) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
		mQAction = 0;
		//mAgentQLogger << "[DEBUG] Terminal state processed; mQAction = 0" << endl;
	} else {
		mQAction = mQSpace->getBestActionQValue(mAgentState, mAgentAction);
		delta += mGamma * mQAction;	// new Q_action
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
	}

	// choose random action
	double random;
	if (mSyncExploration)
		random = mRandom;
	else
		random = gRanrotB.Random();

	if (random < mExploreRate) {
		mAgentAction.randomizeUniform(&gRanrotB);
		mQAction = mQSpace->getExplorativeActionQValue(mAgentState,
				mAgentAction);
		mQSpace->traceClear();
		mQSpace->traceAddStoredExplorative();
	} else {
		mQSpace->traceFall();
		mQSpace->traceAddStoredBest();
	}

	if (mShowQValues)
		mLogDebugLn("QAction: " << mQAction);

	return 0;
}

double CAgentQ::learn_QObserve(char agentInTerminalState) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
		mQAction = 0;
		//mAgentQLogger << "[DEBUG] Terminal state processed; mQAction = 0" << endl;
	} else {
		// We assume that mAgentAction has been filled with the action that will be executed
		// We request the Q-value that belongs to the provided action.
		mQAction = mQSpace->getQValue(mAgentState, mAgentAction);
		delta += mGamma * mQAction;	// new Q_action
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
	}

	mQSpace->traceFall();
	mQSpace->traceAddStored();

	if (mShowQValues)
		mLogDebugLn("QAction: " << mQAction);

	return mLearnRate * delta;
}

int CAgentQ::QObserveStep(char agentInTerminalState) {
	// Just don't forget to turn off consultPolicy!
	learn_QObserve(agentInTerminalState);

	return 0;
}

// ------------------------------------------------ //
// ------------- CAgentQ::SARSAStep --------------- //
// ------------------------------------------------ //

double CAgentQ::learn_SARSA(char agentInTerminalState) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQAction = 0;
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
	} else {
		switch (mExecutedActionType) {
		case atGreedy: {
			// fetch best action
			mQAction = mQSpace->getStoredBestActionQValue();// Stored during consultPolicy()
			delta += mGamma * mQAction;	// new Q_action
			mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
			mQSpace->traceFall();
			mQSpace->traceAddStoredBest();
		}
			break;
		case atExplorative: {
			mQAction = mQSpace->getStoredExplorativeActionQValue();	// Stored during consultPolicy()
			delta += mGamma * mQAction;	// new Q_action
			mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
			mQSpace->traceFall();
			mQSpace->traceAddStoredExplorative();
		}
			break;
		case atEffective: {
			mQAction = mQSpace->getQValue(mAgentState, mAgentEffectiveAction);
			delta += mGamma * mQAction;	// new Q_action
			mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
			mQSpace->traceFall();
			mQSpace->traceAddStored();
		}
			break;
		default:
			break;
		}
	}

	if (mShowQValues) {
		if (mExecutedActionType == atExplorative)
			mLogInfoLn("QAction: " << mQAction << " (explorative step)");
		else
			mLogInfoLn("QAction: " << mQAction);
	}

//	if (mQAction > 150)
//		mLogWarningLn("QAction has too large value: " << mQAction);

	return mLearnRate * delta;
}

double CAgentQ::learn_ACSARSA(char agentInTerminalState) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQAction = 0;
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
	} else {
		// Get Q-value belonging to the action provided by mPolicySpace (inside mAgentAction)
		mQAction = mQSpace->getQValue(mAgentState, mAgentAction);
		delta += mGamma * mQAction;	// new Q_action
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
		mQSpace->traceFall();
		mQSpace->traceAddStored();
	}

	if (mShowQValues)
		mLogDebugLn("QAction: " << mQAction);

	// Update the separately stored policy with the greedy action in 4 steps:
	// 1) backup chosen action
	_StatePrecision chosenActionValues[AGENT_MAX_NUM_ACTIONVARS];
	mAgentAction.store(chosenActionValues);
	// 2) get best action
	mQSpace->getBestActionQValue(mAgentState, mAgentAction);
	// 3) update policy
	mPolicySpace->updateConsultedPolicy(mAgentAction, 0.25);
	//mPolicySpace->updatePolicy(mAgentState, mAgentAction, 0.9);
	// 4) restore chosen action
	mAgentAction.restore(chosenActionValues);
	return mLearnRate * delta;
}

// -------- SARSA learn step --------
int CAgentQ::SARSAStep(char agentInTerminalState) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQAction = 0;
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
	} else {
		// choose random action
		double random;

		if (mSyncExploration)
			random = mRandom;
		else
			random = gRanrotB.Random();

		if (random < mExploreRate) {
			mAgentAction.randomizeUniform(&gRanrotB);
			mQAction = mQSpace->getExplorativeActionQValue(mAgentState,
					mAgentAction);
			delta += mGamma * mQAction;	// new Q_action
			mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
			mQSpace->traceFall();
			mQSpace->traceAddStoredExplorative();
		} else {
			// fetch best action
			mQAction = mQSpace->getBestActionQValue(mAgentState, mAgentAction);
			delta += mGamma * mQAction;	// new Q_action
			mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
			mQSpace->traceFall();
			mQSpace->traceAddStoredBest();
		}
	}

	if (mShowQValues)
		mLogDebugLn("QAction: " << mQAction);

	return 0;
}

// ------------------------------------------------ //
// ------------- CAgentQ::SA-QStep --------------- //
// ------------------------------------------------ //

// -------- Q learn step with policy based on Metropolis Criterion --------
int CAgentQ::SA_QStep(char agentInTerminalState) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQAction = 0;
		//memset(tileIndices, 0, sizeof(int)*mNumTilings);	// tileIndices will be added to the trace -> cannot be uninitialized memory (actually bugs)
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
	} else {
		// fetch best action
		mQAction = mQSpace->getBestActionQValue(mAgentState, mAgentAction);
		delta += mGamma * mQAction;	// new Q_action
		mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));

		// check if random action is the one
		mAgentAction.randomizeUniform(&gRanrotB);
		if (gRanrotB.Random()
				< exp(
						(mQSpace->getExplorativeActionQValue(mAgentState,
								mAgentAction) - mQAction) / mExploreRate)) {
			mQSpace->traceClear();
			mQSpace->traceAddStoredExplorative();
			mQAction = mQSpace->getStoredExplorativeActionQValue();
		} else {
			mQSpace->traceFall();
			mQSpace->traceAddStoredBest();
			mQSpace->getStoredBestActionQValue(mAgentAction); // restore best action
		}
	}

	if (mShowQValues)
		mLogDebugLn("QAction: " << mQAction);
	return 0;
}

// ------------------------------------------------ //
// ---------------- CAgentQ::RStep ---------------- //
// ------------------------------------------------ //

int CAgentQ::RStep(char agentInTerminalState) {
	assert(false);
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	_QPrecision oldQaction = mQAction;

	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQAction = 0;
		//memset(tileIndices, 0, sizeof(int)*mNumTilings);	// tileIndices will be added to the trace -> cannot be uninitialized memory (actually bugs in the memory doubling procedure)

	} else {
		// fetch best action ("not so smart" approach)
		// it calls GetState internally, for all possible actions
		//GetBestAction(action);

		// get state tile indeces
		//GetTileIndices(action, tileIndices);

		//mQAction = SumQValues(tileIndices);
		//	logbook << HDR_DEBUG <<  "[agentQ] mQAction:" << mQAction << endl;

		mQAction = mQSpace->getBestActionQValue(mAgentState, mAgentAction);

	}

	delta += mQAction - mAverageReward;	// new Q_action

	// Update mAverageReward if we performed the greedy action
	// If the trace is empty, this is the first update of the run (oldQaction is invalid)
	if ((mExecutedActionType == atGreedy)
			&& (mQSpace->traceGetLength()/*mTrace.NumElements() */> 0))
		mAverageReward += 0.05 * mLearnRate
				* (reward - mAverageReward + mQAction - oldQaction);

	mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
	//for (unsigned int i=0; i<mTrace.NumElements(); i++)
	//{
	//	Q[mTrace[i].index] += mLearnRate*delta*mTrace[i].value;
	//}

	// choose random action
	if (gRanrotB.Random() < mExploreRate) {
		mExecutedActionType = atExplorative;

		//GetTileIndices(action, tileIndices);
		//mQAction = SumQValues(tileIndices);
		mAgentAction.randomizeUniform(&gRanrotB);
		mQAction = mQSpace->getExplorativeActionQValue(mAgentState,
				mAgentAction);

		//mTrace.Clear();
		mQSpace->traceClear();
		mQSpace->traceAddStoredExplorative();
	} else {
		mExecutedActionType = atGreedy;
		//mTrace.Fall();
		mQSpace->traceFall();
		mQSpace->traceAddStoredBest();
	}

	if (mShowQValues)
		mLogDebugLn("QAction: " << mQAction);

	//mTrace.AddBatch(tileIndices);

//	delete[] tileIndices;
	return 0;
}

// ------------------------------------------------ //
// ------------ CAgentQ::R_SARSAStep -------------- //
// ------------------------------------------------ //

int CAgentQ::R_SARSAStep(char agentInTerminalState) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;
	_QPrecision oldQaction = mQAction;

	ComputationalType delta = reward - mQAction;	// old Q_action

	// If the state is terminal AND absorbing, the expected mQAction is 0
	if ((agentInTerminalState & CONST_STATE_TERMINAL)
			&& (agentInTerminalState & CONST_STATE_ABSORBING)) {
		mQAction = 0;
		//memset(tileIndices, 0, sizeof(int)*mNumTilings);	// tileIndices will be added to the trace -> cannot be uninitialized memory (actually bugs)
	} else {
		if (gRanrotB.Random() < mExploreRate) {
			// choose random action
			mAgentAction.randomizeUniform(&gRanrotB);
			mQAction = mQSpace->getExplorativeActionQValue(mAgentState,
					mAgentAction);
			delta += mQAction - mAverageReward;	// new Q_action
			if ((mQSpace->traceGetLength()/*mTrace.NumElements()*/> 0))
				mAverageReward += 0.1 * mLearnRate
						* (reward - mAverageReward + mQAction
								- (oldQaction + mLearnRate * delta));

			mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
			mQSpace->traceFall();
			mQSpace->traceAddStoredExplorative();
		} else {
			// fetch best action
			mQAction = mQSpace->getBestActionQValue(mAgentState, mAgentAction);
			//GetBestAction(action);
			delta += mQAction - mAverageReward;	// new Q_action
			if ((mQSpace->traceGetLength()/*mTrace.NumElements()*/> 0))
				mAverageReward += 0.1 * mLearnRate
						* (reward - mAverageReward + mQAction
								- (oldQaction + mLearnRate * delta));

			mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));
			mQSpace->traceFall();
			mQSpace->traceAddStoredBest();
		}
		// get state tile indeces
		//GetTileIndices(action, tileIndices);

		//mQAction = SumQValues(tileIndices);
		//	logbook << HDR_DEBUG <<  "[agentQ] mQAction:" << mQAction << endl;

		// Update mAverageReward if we performed the greedy action
		// If the trace is empty, this is the first update of the run (oldQaction is invalid)
		//if ((mQSpace->TraceGetLength()/*mTrace.NumElements()*/ > 0))
		//	mAverageReward += 0.1*mLearnRate*(reward - mAverageReward + mQAction - (oldQaction + mLearnRate*delta));

	}

	//for (unsigned int i=0; i<mTrace.NumElements(); i++)
	//{
	//Q[mTrace[i].index] += mLearnRate*delta*mTrace[i].value;
	//}

	//mTrace.Fall();

	if (mShowQValues)
		mLogDebugLn("QAction: " << mQAction);

	//mTrace.AddBatch(tileIndices);

	//delete[] tileIndices;
	return 0;
}

// SarsaLearnStep: Experimental function used in ModularSarsa
// ********************* => does this belong here????
void CAgentQ::sarsaLearnStep(bool clearTrace) {
	ComputationalType reward = calculateReward();
	mTotalReward += reward;

	ComputationalType delta = reward - mQAction;

	// get state tile indeces
	//GetTileIndices(action, tileIndices);

	//mQAction = SumQValues(tileIndices);

	mQAction = mQSpace->getQValue(mAgentState, mAgentAction);

//	logbook << HDR_DEBUG <<  "[agentQ] mQAction:" << mQAction << endl;

	delta += mGamma * mQAction;

//	logbook << HDR_DEBUG << "[agentQ] reward:" << reward << " delta:" << int((delta/mQAction)*100) << "%" << endl;

	//for (unsigned int i=0; i<mTrace.NumElements(); i++)
	//{
	//Q[mTrace[i].index] += mLearnRate*delta*mTrace[i].value;
	//}
	mQSpace->traceUpdateQValues((_QPrecision) (mLearnRate * delta));

	// choose random action
	if (clearTrace) {
//		logbook << HDR_DEBUG << "[agentQ] Random action was chosen" << endl;
//		mTrace.Clear();
		mQSpace->traceClear();
	} else {
		//mTrace.Fall();
		mQSpace->traceFall();
	}

	// Update eligibility trace (a replacing trace).
	mQSpace->traceAddStored();
	//mTrace.AddBatch(tileIndices);

	//delete[] tileIndices;
	// return action and reset conditions
//	logbook << HDR_DEBUG <<"[agentQ] done   in----------------- [ " <<stopwatch.GetDeltaMs() << "] ms-----------" << endl;
}

bool CAgentQ::save(const std::string &fileName) {
	std::ofstream fileStream(fileName.c_str(),
			std::ios::out | std::ios::binary);
	if (fileStream.bad())
		return false;

	bool result = save(fileStream);
	fileStream.close();
	return result;
}

bool CAgentQ::save(std::ofstream &fileOutStream) {
	if (fileOutStream.bad())
		return false;

	// Save agent version
	int agentVersion = _AGENTQ_VERSION;
	fileOutStream.write((char*) &agentVersion, sizeof(agentVersion));

	// Save the learn parameters
	fileOutStream.write((char*) &mAlgorithmType, sizeof(mAlgorithmType));
	fileOutStream.write((char*) &mLearnRate, sizeof(mLearnRate));
	fileOutStream.write((char*) &mInitialLearnRate, sizeof(mInitialLearnRate));
	fileOutStream.write((char*) &mLearnRateDecayRate,
			sizeof(mLearnRateDecayRate));
	fileOutStream.write((char*) &mExploreRate, sizeof(mExploreRate));
	fileOutStream.write((char*) &mGamma, sizeof(mGamma));
	fileOutStream.write((char*) &mLambda, sizeof(mLambda));
	fileOutStream.write((char*) &mQInitMin, sizeof(mQInitMin));
	fileOutStream.write((char*) &mQInitMax, sizeof(mQInitMax));

	// Save the QSpace
	bool result = true;
	result &= mQSpace->save(fileOutStream);

	result &= mAgentState.save(fileOutStream);
	result &= mAgentAction.save(fileOutStream);

	fileOutStream.write((char*) &mShowQValues, sizeof(mShowQValues));
	fileOutStream.write((char*) &mSyncExploration, sizeof(mSyncExploration));
	return result;
}

bool CAgentQ::init(const std::string &fileName) {
	std::ifstream fileStream(fileName.c_str(), std::ios::in | std::ios::binary);
	if (fileStream.bad())
		return false;

	bool result = init(fileStream);
	fileStream.close();
	return result;
}

bool CAgentQ::init(std::ifstream &fileInStream) {
	if (fileInStream.bad())
		return false;

	// Load agent version
	int agentVersion = -1;
	fileInStream.read((char*) &agentVersion, sizeof(agentVersion));
	if (agentVersion != _AGENTQ_VERSION) {
		mLogErrorLn(
				"CAgentQ::init(file) read file with version " << agentVersion << ", but version " << _AGENTQ_VERSION << " needed!");
		return false;
	}

	// Load the learn parameters
	fileInStream.read((char*) &mAlgorithmType, sizeof(mAlgorithmType));
	fileInStream.read((char*) &mLearnRate, sizeof(mLearnRate));
	fileInStream.read((char*) &mInitialLearnRate, sizeof(mInitialLearnRate));
	fileInStream.read((char*) &mLearnRateDecayRate,
			sizeof(mLearnRateDecayRate));
	fileInStream.read((char*) &mExploreRate, sizeof(mExploreRate));
	fileInStream.read((char*) &mGamma, sizeof(mGamma));
	fileInStream.read((char*) &mLambda, sizeof(mLambda));
	mAverageReward = 0;
	mExecutedActionType = atExplorative;
	fileInStream.read((char*) &mQInitMin, sizeof(mQInitMin));
	fileInStream.read((char*) &mQInitMax, sizeof(mQInitMax));
	mQAction = 0;

	// Load the QSpace
	bool result = true;
	result &= mQSpace->load(fileInStream);

	result &= mAgentState.load(fileInStream);
	result &= mAgentAction.load(fileInStream);
	//reset actions to start at zero
	//mAgentAction.setStartZeroValues();
	mLogInfoLn(
			"CAgentQ::init(stream) found " << mAgentAction.getNumActionVars() << " action vars");

	fileInStream.read((char*) &mShowQValues, sizeof(mShowQValues));
	fileInStream.read((char*) &mSyncExploration, sizeof(mSyncExploration));
	mRandom = 0;
	return result;
}

double CAgentQ::getMinimumEpisodeTime() {
	return 1;
}

void CAgentQ::resetTrial() {
	// Own QSpace
	if (mQSpace != NULL)
		mQSpace->traceClear();

	// Total reward
	resetTotalReward();
	mAgentAction.setStartZeroValues();
	//mAgentAction.setStartValues();
	//set previous actions to zero


	// Sub-agents
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
		mSubAgents[iAgent]->resetTrial();

	//DEBUG: this shouldn't make any difference since the trace is cleared!
	/*
	 mQAction = 0;
	 mExecutedActionType = atExplorative;
	 */
}

void CAgentQ::resetLife() {
	// Reset learn rate
	mLearnRate = mInitialLearnRate;

	// Own QSpace
	if (mQSpace != NULL)
		mQSpace->resetMemory();

	// Own Policy space
	if (mPolicySpace != NULL)
		mPolicySpace->resetMemory();

	// Average reward
	mAverageReward = 0;

	// Sub-agents
	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
		mSubAgents[iAgent]->resetLife();
}

void CAgentQ::updateTotalReward() {
	if (mSubAgents.size() > 0) {
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
			mSubAgents[iAgent]->updateTotalReward();
	} else
		mTotalReward += calculateReward();
}

double CAgentQ::getTotalReward() {
	if (mSubAgents.size() > 0) {
		double result = 0;
		for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
			result += mSubAgents[iAgent]->getTotalReward();
		return result;
	} else
		return mTotalReward;
}

void CAgentQ::resetTotalReward() {
	mTotalReward = 0;

	for (uint32_t iAgent = 0; iAgent < mSubAgents.size(); iAgent++)
		mSubAgents[iAgent]->resetTotalReward();
}

// ************************************************************* //
// ********************** CAgentQSubAgent ********************** //
// ************************************************************* //

CAgentQSubAgent::CAgentQSubAgent(CAgentQ* parent, const int agentIndex,
		CQSpace* qspace, CPolicySpace* policySpace) :
		CAgentQ(qspace, policySpace), mParent(parent), mAgentIndex(agentIndex) {

}

double CAgentQSubAgent::calculateReward() {
	return mParent->calculateSubAgentReward(mAgentIndex);
}

