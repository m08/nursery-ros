/*
 * QSpaceTileCoding.h
 *
 *  Created on: 2008?
 *      Author: Sebastiaan Troost, Erik Schuitema
 */

#include <QSpaceTileCoding.h>
#include <randomc.h>


class CQSpaceLenientTileCoding: public CQSpaceTileCoding
{
	protected:
		_QPrecision		*mLenienceTemp;	// lenience temperature per feature (tau) in the range [0,1], starting at 1.0
		double			mKappa;			// lenience temperature coefficient
		double			mBeta;			// lenience temperature decay rate
		// In the original paper [Panait, Sullivan, Luke, 2006], 'MaxTemp' is used, i.e., an initial temperature.
		// However, this is a redundant parameter if mKappa is also used.
		// Therefore, all temperatures start at 1.0.

		void			resetLenienceTemp();

	public:
		CQSpaceLenientTileCoding();
		~CQSpaceLenientTileCoding();

	// Do a Q space trace update
	virtual bool	traceUpdateQValues(_QPrecision Delta);

	virtual bool	setMemorySize(_IndexPrecision MemorySize);
	virtual void	resetMemory();
	//__int64 m_iUpdates;

	void			setLenienceParams(double kappa, double beta);
};
