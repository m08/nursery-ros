/**
 * @file   agentq.h
 * @author Jev Kuznetsov  <jev@ph.tn.tudelft.nl>, Erik Schuitema, Sebastiaan Troost
 * @date   January 2004 - Februari 2008
 *
 * @brief  Q learning agent header files.
 */


#ifndef __AGENTQ_HPP
#define __AGENTQ_HPP

#include <string>
#include <randomc.h>
//#include <platform/math/half/half.h>
#include <QSpace.h>
#include <PolicySpace.h>
#include <Log.h>

#include <float.h>
#include <Configuration.h>



/// general rl parameters

#define _MINIMUM_MEMSIZE	268435456/8    // old tile coding memsize 32768*2
#define _LEARN_RATE			0.3f           // (initial) learning rate
#define _EXPLORE_RATE		0.0f            // (initial) explore rate
#define _GAMMA				0.95f            // discounting factor
#define _LAMBDA				0.80f             // elegibility factor (learning speed)
#define _MIN_TRACE_VALUE	0.0001f		// minimum trace value. below this value, the state is not accounted for anymore in the trace
#define _AGENTQ_VERSION		4

// Should be bitwise OR-able, e.g. (CONST_STATE_TERMINAL | CONST_STATE_ABSORBING)
#define CONST_STATE_NORMAL		0x00
#define CONST_STATE_TERMINAL	0x01
#define CONST_STATE_ABSORBING	0x02

#define ComputationalType	double	// This type is used in all computations before eventually saving values in _QPrecision precision

enum ELearningAlgo
{
	laQLearning,
	laRelative_bounds,//SARSA learning with action bounds
	laQObserve,
	laSARSA,
	laRLearning,
	laR_SARSA,
	laSA_Qlearning,
	laACSARSA,
	// Multi-agent options
	laGM_Q,		// greatest-mass Q-learning
	laGM_SARSA,	// greatest-mass SARSA
	laAR_Q,		// adaptive resolution Q-learning
	laAR_SARSA,	// adaptive resolution SARSA
	laDAS_SARSA,	// decomposed action space SARSA
	laDAS_Q,		// decomposed action space Q-learning
	// Default option
	laDefault = laQLearning,
	laNone
};

enum EExplorationType
{
	etEGreedy,
	etSoftMax,
	etNone
};

enum EActionType
{
	atGreedy,		// The agent took the greedy action
	atExplorative,	// The agent took an explorative action
	atEffective		// The agent took another action than we thought (the effective action)
};


/** @brief Leaning Class
*
*	This class is the implementation of the following learning algorithms:
*	Q, SARSA, R, R_Q, SA_Q
*	Its main function Learnstep does a learnstep with the appropriate algorithm.
*/
class CAgentQSubAgent;

class CAgentQ
{
	protected:

		CLog2               mLog;
		ELearningAlgo       mAlgorithmType;       // Q, R, SARSA or R_SARSA
		EExplorationType    mExplorationType;

		// The main learning parameters
		ComputationalType   mLearnRate;           // these are initialised from defines
		ComputationalType   mInitialLearnRate;		// Backup of the initial learn rate (used when decaying the learn rate)
		ComputationalType		mLearnRateDecayRate;	// Decay rate for the learn rate, per step
		ComputationalType		mExploreRate;
		ComputationalType		mSoftMaxTemperature;	// Temperature of soft-max policy
		ComputationalType		mGamma;
		ComputationalType		mLambda;
		//std::vector<double>		mRelativeBounds;
		std::vector<int>		mRelativeBoundsInt;
		std::vector<int>		mUnderRelativeBoundsInt;

		int                 mUnderRelativeBound;
		bool                mUseAbsoluteBounds;   // Whether to use absolute or relative bounds

		// variables used in R-learning
		ComputationalType   mAverageReward;       // Used for R-learning
		EActionType         mExecutedActionType;  // Type of the action that was executed (see EActionType)

		ComputationalType   mTotalReward;         // total sum of rewards since last reset

		// Q-space and policy space
		CQSpace*            mQSpace;
		CPolicySpace*       mPolicySpace;         // Can be NULL. Only used for actor-critic algorithms

		// Q-value initialization
		_QPrecision         mQInitMin;
		_QPrecision         mQInitMax;

		_QPrecision         mQAction;

	//	int				reset;             // if reset !=0 use trace. otherwise not.

		// Agent state and action. It is the responsibility of
		// the user of the agent to fill mAgentState with sensible data
		// as well as to put mAgentAction into effect (i.e. executing the action)
		CAgentState         mAgentState;
		CAgentAction        mAgentAction;

		CAgentAction        mAgentEffectiveAction;      // Can be used when it turns out that not mAgentAction was executed, but a different one: the effective action
		//int					mRandomSeed;		// seed moved outside the agent
		bool                mUseActionValueList;        // Indicates whether we need to fill the action-value list every time
		CAgentActionValueList*  mAgentActionValueList;  // action-value list used during exploration

		// Sub-agent functionality for, e.g., greatest-mass Q-learning and SARSA
		// std::vector<CAgentNQSubAgent*> ?
		std::vector<CAgentQSubAgent*>	mSubAgents;
		bool              mSyncExploration;	// Exploration synchronization between sub-agents
		bool              explorationStep;
		void              removeSubAgents();



		/// get the state-action checksum (for verifying behavior on different machines)
		virtual float     checkSum()  { return 0;}

		// Learn steps (without policy consultation)
		// Partial steps (require policy consulting prior to the learn step)
		double            learn_QLearning(char agentInTerminalState);
		double            learn_QObserve(char agentInTerminalState);
		double            learn_SARSA(char agentInTerminalState);
		double            learn_ACSARSA(char agentInTerminalState);
		double            learn_AR(char agentInTerminalState);

		// Learn step wrapping function (without policy consultation)
		// Should be combined with consultPolicy().
		// learn() returns the size of the temporal difference adjustment that it applied
		// NOTE: This function also updates the total reward sum!
		virtual double    learn(char agentInTerminalState);

		// The two static parts of consultPolicy()
		virtual bool      consultRelative_boundsGreedyPolicy();
		virtual bool      consultDefaultGreedyPolicy();
		virtual bool      consultDefaultExplorationPolicy();
		// The actor-critic variants
		virtual bool		consultACGreedyPolicy();
		virtual bool		consultACExplorationPolicy();
		// The greatest-mass variants
		virtual bool		consultGMGreedyPolicy();	// Greatest-mass greedy policy over all agents
		virtual bool		consultGMExplorationPolicy();
		virtual bool		consultARGreedyPolicy();	// Adaptive resolution greedy policy over all agents
		virtual bool		consultARExplorationPolicy();
		virtual bool		consultDASGreedyPolicy();	// Decomposed action space
		virtual bool		consultDASExplorationPolicy();

		//defining the bound


		// Full steps
		virtual int		QStep(char agentInTerminalState);
		int					QObserveStep(char agentInTerminalState);
		int					SA_QStep(char agentInTerminalState);
		int					RStep(char agentInTerminalState);
		int					SARSAStep(char agentInTerminalState);
		int					R_SARSAStep(char agentInTerminalState);

		// Full step wrapping function
		virtual int		step(char agentInTerminalState)
		{
			switch (mAlgorithmType)
			{
				case laQLearning:
					return QStep(agentInTerminalState);
				case laQObserve:
					return QObserveStep(agentInTerminalState);
				case laSARSA:
					return SARSAStep(agentInTerminalState);
				case laRelative_bounds:
					return SARSAStep(agentInTerminalState);
				case laRLearning:
					return RStep(agentInTerminalState);
				case laR_SARSA:
					return R_SARSAStep(agentInTerminalState);
				case laSA_Qlearning:
					return SA_QStep(agentInTerminalState);
				default:
					return -1;
			}
		}

		// Supervised sarsa step: no selection of action (e.g. no GetBestAction),
		// but it does learn from the action parameter
		virtual void	sarsaLearnStep(bool clearTrace); // sarsa supervised learn step .. ?

	public:

		bool			mShowQValues;
		// For MultiAgent Random syncing! (QStep only)
		double			mRandom;

		CAgentQ(CQSpace* qspace=NULL, CPolicySpace* policySpace=NULL);
		virtual ~CAgentQ();

		// Policy consulting functions - a return value of 'true' indicates success.
		// Learning happens in two steps: 1) consult the policy 2) do the learning
		virtual bool		consultPolicy();
		// Algorithm-'independent' version of consulting the greedy policy only
		virtual bool		consultGreedyPolicy();
		// Algorithm-'independent' version of consulting the exploration policy only
		virtual bool		consultExplorationPolicy();
		virtual bool      	consultRelativeExplorationPolicy();
		virtual bool 		getExplorationStep()
		{
			return explorationStep;
		}


		/// set custom factors
		// _exploration_param is the exploration rate for e-greedy and the temperature for softmax.
		void				setLearnParameters(	const ComputationalType _learnrate,
											const ComputationalType _exploration_param,
											const ComputationalType _gamma,
											const ComputationalType _lambda);
		inline ComputationalType    getTraceParameter() {return mLambda;}
		inline ComputationalType    getExplorationRate() {return mExploreRate;}
		inline ComputationalType	getLearningRate()	{return mLearnRate;}
		inline ComputationalType	getDiscountRate()	{return mGamma;}
		std::string			getAlgoName();
		std::string			getAlgoName(ELearningAlgo algo);
		virtual void		setAlgo(ELearningAlgo algo);
		virtual bool		setAlgoByName(const char* name);


		virtual void		setRelativeBound(const std::vector<double> &bound);
		virtual void 	 	setUnderRelativeBound(int under_bound){mUnderRelativeBound=under_bound;}
		virtual void 		setUnderRelativeBound(const std::vector<double> &under_bound);
		virtual void      	useAbsoluteBounds();


		virtual std::vector<int>	getRelativeBound() { return mRelativeBoundsInt;}

		// Set the exploration type, e.g., e-greedy or softmax
		virtual void		setExplorationType(EExplorationType expType);
		bool				setExplorationTypeByName(const char* name);

		// function to enable the generation of an action-value list every time the greedy policy is consulted
		void				enableActionValueList(bool enabled);

		// Sets the qspace for this agent (will become OWNER and will delete it in its constructor).
		// 'qspace' should be constructed and initialized before being passed to setQSpace().
		virtual void		setQSpace(CQSpace* qspace);
		CQSpace*			getQSpace()			{return mQSpace;}
		CPolicySpace*		getPolicySpace()	{return mPolicySpace;}
		const CAgentState&	getAgentState()		{return mAgentState;}
		const CAgentAction&	getAgentAction()	{return mAgentAction;}	// Use this to copy its format
		const CAgentActionValueList*	getAgentActionValueList()	{return mAgentActionValueList;}

		// Sub-agent functionality
		void					addSubAgent(CQSpace* qspace=NULL, CPolicySpace* policySpace=NULL);
		inline CAgentQSubAgent*	getSubAgent(const int agentIndex)	{return mSubAgents[agentIndex];}
		int						getNumSubAgents()	{return mSubAgents.size();}


		// Example: setExecutedActionType(atEffective) signals the agent that mAgentEffectiveAction was filled with the actually executed action
		void				setExecutedActionType(EActionType actionType)	{mExecutedActionType = actionType;}

		// Sets the policy space for this agent (will become OWNER). The policy space should be constructed and initialized before passing.
		void				setPolicySpace(CPolicySpace* policySpace);

		// init() the agent, possibly from a file(stream) created with save()
		virtual bool		readConfig(const CConfigSection &configNode);
		virtual bool		init();	/// initialise the agentq
		virtual bool		init(const std::string &fileName);
		virtual bool		init(std::ifstream &fileInStream);
		virtual void		deinit()	{}	// deinitialize the agentq
		virtual bool		save(const	std::string &fileName);
		virtual bool		save(std::ofstream &fileOutStream);

		// calculareReward() should return the reward that the agent earned in the current state (mAgentState)
		virtual double		calculateReward();
		// Version of calculateReward() that calculates this result for any state.
		virtual double		calculateReward(const CAgentAction& action, const CAgentState& resultState)	{return 0.0;}

		// Sub-agent functionality:
		//   override calculateSubAgentReward() to specify the reward function for each agent
		virtual double		calculateSubAgentReward(const int agentIndex)	{return 0.0;}
		// Version of calculateSubAgentReward() that calculates this result for any state.
		virtual double		calculateSubAgentReward(const int agentIndex, const CAgentAction& action, const CAgentState& resultState)	{return 0.0;}

		// Override to provide visits-dependent subagent weight for adaptive resolution
		virtual double		getSubAgentWeight(const int agentIndex, _QPrecision *visits) {return 1;}

		// reset() should be called at the beginning of a new learning trial
		virtual void		resetTrial();
		virtual void		resetLife();

		virtual void		updateTotalReward();		// Updates the total reward sum of the agent
		virtual double		getTotalReward();			// Returns the total reward sum of the agent
		virtual void		resetTotalReward();			// Resets  the total reward sum of the agent
		virtual char		isTerminalState(uint64_t trialTimeMicroseconds)		{ return CONST_STATE_NORMAL; }	// Returns whether the agent is in a terminal state
		// Version of isTerminalState() that calculates this result for any state. Ultimately, replace the above version with the version below.
		virtual char		isTerminalState(const CAgentState& state, uint64_t trialTimeMicroseconds)		{ return CONST_STATE_NORMAL; }	// Returns whether the agent is in a terminal state

		virtual void		displayVariables();		/// dislpays internal variables
		//void			DisplayVariables(const std::string &filename);

		virtual double		getMinimumEpisodeTime();

};

// Sub-agent to be used within CAgentQ
class CAgentQSubAgent: public CAgentQ
{
	protected:
		CAgentQ*		mParent;
		int				mAgentIndex;	// Index of the agent in the parent array
	public:
		CAgentState&	getAgentState()		{return mAgentState;}
		CAgentAction&	getAgentAction()	{return mAgentAction;}
		CAgentQSubAgent(CAgentQ* parent, const int agentIndex, CQSpace* qspace=NULL, CPolicySpace* policySpace=NULL);
		double		calculateReward();
};

#endif // __AGENTQ_HPP included.
