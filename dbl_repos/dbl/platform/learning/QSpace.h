/**
 * @file   QSpace.h
 * @author Sebastiaan Troost <s.troost@student.tudelft.nl>
 * @date   October 2007
 *
 * @brief  QSpace class
 */

#ifndef QSPACE_H_
#define QSPACE_H_
#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <stdint.h>

#include <randomc.h>
#include <half.h>
#include <Log2.h>
#include "AgentInterface.h"

#include "precisions.h"

#define CONST_QVALUE_INVALID	(-_QPrecision_MAX)

class CQSpace
{
	protected:
		CLog2				mLog;

	public:
		CQSpace():
			mLog("qspace")
		{ }

		virtual ~CQSpace() {};

		// Set the initial value policy: values will be initialized with a random value between valMin and valMax.
		// Set valMin == valMax to initialize to a constant value.
		// This function can be implemented e.g. by pre-setting all pre-allocated memory values,
		// or by configuring an "initNewMemValue()" function that is called when creating new memory on the spot.
		virtual	void setInitialQValuePolicy(_QPrecision valMin = 0, _QPrecision valMax = 1) {};

	public:
		// ***************************************
		// ** Q value and action functions
		// ***************************************

		// A floating point return value of CONST_QVALUE_INVALID indicates (memory) failure

		// Get a Q value
		virtual _QPrecision probeQValue(CAgentState& state, CAgentAction& action) { return 0; }					// Does nothing other than give you that Q-value. no memory claiming, no caching
		virtual _QPrecision getQValue(CAgentState& state, CAgentAction& action) { return 0; };					//  in current state
		virtual _QPrecision getStoredQValue() { return 0; };														//  stored Q value
		virtual _QPrecision getStoredQValue(CAgentAction& action) { return 0; };									//  also returns the action itself

		// Get the Q value of the best action and return the best action as well
		virtual _QPrecision getBestActionQValue(const CAgentState& state, CAgentAction& action, CAgentActionValueList* avList=NULL) { return 0; };			//  in current state
		virtual _QPrecision getBestRelativeActionQValue(const CAgentState& state, CAgentAction& action, const std::vector<int> &relative_bound, const std::vector<int> &under_relative_bound,bool useAbsoluteBounds, CAgentActionValueList* avList=NULL) { return 0; };			//  in current state

		// Get the Q value of the best action, assuming that the best action is already known
		virtual _QPrecision getBestActionQValue_GivenAction(const CAgentState& state, const CAgentAction& action) { return 0; };			//  in current state
		virtual	_QPrecision	 getBestActionQValue_Approx(const CAgentState& state, CAgentAction& action, int undersamplingFactor) { return 0; };	// Approximate version of getBestAction using undersampling plus neighborhood examination
		virtual _QPrecision getStoredBestActionQValue() { return 0; };											//  stored best action
		virtual _QPrecision getStoredBestActionQValue(CAgentAction& action) { return 0; };						//  also returns the best action itself

		// Get the Q value of an explorative action. Make sure you fill the action manually before calling this function!
		virtual _QPrecision getExplorativeActionQValue(const CAgentState& state, const CAgentAction& action) { return 0; };	//  in current state
		virtual _QPrecision getStoredExplorativeActionQValue() { return 0; };									//  stored explorative action
		virtual _QPrecision getStoredExplorativeActionQValue(CAgentAction& action) { return 0; };				//  also returns the action itself

		// Set a specific Q value
		virtual bool		setQValue(CAgentState& state, CAgentAction& action, _QPrecision value) { return false; };	//  set a specific Q value
		virtual bool		setStoredQValue(_QPrecision value) { return false; };										//  stored Q value
		virtual bool		setStoredBestActionQValue(_QPrecision value) { return false; };								//  stored best action
		virtual bool		setStoredExplorativeActionQValue(_QPrecision value) { return false; };						//  stored random action
        virtual bool      	getWithinBounds(int numbSteps, int previousAction, int relative_bound, int under_relative_bound, int Action){return false;};
        virtual bool      	getWithinAllBounds(int  *previousActions, int *varNumSteps, int vars, const std::vector<int> &relative_bound, const std::vector<int> & under_relative_bound, int *i, bool useAbsoluteBounds){return false;};
        virtual bool 		getMaxQwithinBounds()
        		{
        			return false;
        		}
        // ***************************************
		// ** Trace functions
		// ***************************************

		// Trace options
		// TraceInit should clear the trace as well!
		virtual bool			traceInit(_QPrecision DiscountFactor, _QPrecision minVal, bool adjustTraceLength=true) { return false; };
		virtual _IndexPrecision	traceGetLength() { return 0; };												//  amount of state-action pairs in trace
		virtual _QPrecision		traceGetDiscountFactor() { return 0; };										//  trace discount factor lambda

		// Discount all previous states
		virtual bool			traceFall() { return false; };

		// Add a State-Action Pair to the trace (and discount all previous states)
		virtual bool			traceAdd(CAgentState& state, CAgentAction& action) { return false; };			//  use SetState()
		virtual bool			traceAddStored() { return false; };												//  stored state action pair
		virtual bool			traceAddStoredBest() { return false; };											//  stored best action
		virtual bool			traceAddStoredExplorative() { return false; };									//  stored explorative action

		// Clear the trace
		virtual bool			traceClear() { return false; };

		// Do a Q space trace update
		virtual bool			traceUpdateQValues(_QPrecision Delta) { return false; };

		// ***************************************
		// ** Disk Functions
		// ***************************************

		// Load QSpace from a stream
		virtual bool load(std::ifstream& FileInStream) { return false; };
		//virtual bool LoadQSpace(std::string& FileName) { return false; };

		// Save QSpace to a stream
		virtual bool save(std::ofstream& FileOutStream) { return false; };
		//virtual bool SaveQSpace(std::string& FileName) { return false; };

		// ***************************************
		// ** Basic memory Functions
		// ***************************************

		virtual bool	setMemorySize(_IndexPrecision MemorySize) { return false; };
		virtual void	resetMemory() {};
		virtual _IndexPrecision getMemorySize()			{return 0;}	// Should return 0 at construction of the class by definition
		virtual _IndexPrecision	getMemoryUsage()		{return 0;}
		virtual _IndexPrecision getMemorySizeBytes()	{return 0;}

};
#endif
