#include "QSpaceLenientTileCoding.h"

CQSpaceLenientTileCoding::CQSpaceLenientTileCoding():
	CQSpaceTileCoding(),
	mLenienceTemp(NULL),
	mKappa(1.0),
	mBeta(0.99)
{
}

CQSpaceLenientTileCoding::~CQSpaceLenientTileCoding()
{
	if (mLenienceTemp != NULL)
		delete[] mLenienceTemp;
}

void CQSpaceLenientTileCoding::resetLenienceTemp()
{
	//memset(mLenienceTemp, 0, getMemorySize()*sizeof(mLenienceTemp[0]));
	for (unsigned int i=0; i<getMemorySize(); i++)
		mLenienceTemp[i] = 1.0;
}

void CQSpaceLenientTileCoding::resetMemory()
{
//	m_iUpdates = 0;
	resetLenienceTemp();
	CQSpaceTileCoding::resetMemory();
}

// Do a Q space trace update
bool CQSpaceLenientTileCoding::traceUpdateQValues(_QPrecision Delta)
{
/*	m_iUpdates++;
	bool bUpdate = true;
	float fDelta = Delta;
	if (Delta < 0.0)
	{
		double dChance = exp(-3.0*pow(.9982,(0.05*m_iUpdates)));
		float random = rg.Random();
		if (random > dChance)
			bUpdate = false;
	}
*/
	
	double dAvgLenienceTemperature = 0;
	if (mTrace.numElements() >= (_IndexPrecision)mNumTilings)
	{
		for (_IndexPrecision iTElement = mTrace.numElements() - mNumTilings; iTElement < mTrace.numElements(); iTElement++)
		{
			//mLenienceTemp[mTrace[iTElement].index] = 1.0 - mBeta*(1.0 - mLenienceTemp[mTrace[iTElement].index]);
			mLenienceTemp[mTrace[iTElement].index] *= mBeta;
			dAvgLenienceTemperature += mLenienceTemp[mTrace[iTElement].index];
		}
	}
	dAvgLenienceTemperature /= mNumTilings;
	double dInvLenience = exp(-mKappa*dAvgLenienceTemperature);	// Inverted lenience = 1 - lenience, for convenience ;)

	bool bUpdate = true;
	if (Delta < 0.0)
		if (gRanrotB.Random() > dInvLenience)
			bUpdate = false;

	if (bUpdate)
		return CQSpaceTileCoding::traceUpdateQValues(Delta);
	else
		return true;
}

bool CQSpaceLenientTileCoding::setMemorySize(_IndexPrecision MemorySize)
{
	bool shouldResize = (MemorySize != getMemorySize());	// Check this *before* resizing the inherited QSpace!
	bool result = CQSpaceTileCoding::setMemorySize(MemorySize);
	// If resizing the inherited QSpace succeeded, also resize the lenience temperature array
	if (result)
	{
		if (shouldResize)
		{
			if (mLenienceTemp != NULL)
				delete[] mLenienceTemp;

			mLenienceTemp = new _QPrecision[MemorySize];
		}
		// Always reset the values
		resetLenienceTemp();
	}
	return result;
}

void CQSpaceLenientTileCoding::setLenienceParams(double kappa, double beta)
{
	mKappa	= kappa;
	mBeta	= beta;
}
