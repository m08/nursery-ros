#ifndef TILECODINGTRACE_H_
#define TILECODINGTRACE_H_

#define DEFAULT_TRACE_LENGTH 1000
#ifndef MAX_NUM_TILINGS
	#define MAX_NUM_TILINGS	64	// Default maximum number of tilings
#endif

#include <Log2.h>
#include <assert.h>


// TODO: this shouldn't be included here
#include "precisions.h"

// Trace class designed to deal with a tile coding function approximator
struct STraceElementTC
{
	_IndexPrecision	index;
	_QPrecision		value;
};

class CEligibilityTraceTC
{
	protected:
		CLog2					mLog;
		STraceElementTC*		mElements;
		_IndexPrecision			mCurrentPos;
		_QPrecision				mFalloff;
		_IndexPrecision			mNumFilled;
		_IndexPrecision			mNumElements;
		_IndexPrecision			mNumTilings;

	public:
		CEligibilityTraceTC();
		virtual ~CEligibilityTraceTC();
		void					fall(const _QPrecision extraFalloff=1.0);	// let trace fall
		_QPrecision				getFalloffFactor()	{ return mFalloff; }
		_IndexPrecision			numElements();
		void					add(const _IndexPrecision index);
		virtual void			addBatch(const _IndexPrecision *indices);
		// addBatchEF(): version of addBatch() for extended (non-binary) features
		virtual void			addBatchEF(const _IndexPrecision *indices, const _QPrecision *featureValues);
		void					clear();
		virtual void			init(const _QPrecision falloffFactor, const _QPrecision minimumValue, const _IndexPrecision numTilings, bool adjustTraceLength=true);
		inline STraceElementTC&	operator[](_IndexPrecision iIterator)		{ assert(iIterator < mNumElements); return mElements[iIterator];}

		virtual bool			load(std::ifstream& FileStream, bool loadElements=false);
		virtual bool			save(std::ofstream& FileStream, bool saveElements=false);

};

typedef CEligibilityTraceTC CCumulativeTrace;

class CReplacingTraceTC: public CEligibilityTraceTC
{
	protected:
		void	resetExistingFeatures(const _IndexPrecision *indices);

	public:
		void	addBatch(const _IndexPrecision *indices);
		// addBatchEF(): version of addBatch() for extended (non-binary) features
		void	addBatchEF(const _IndexPrecision *indices, const _QPrecision *featureValues);
};

#endif /* TILECODINGTRACE_H_ */
