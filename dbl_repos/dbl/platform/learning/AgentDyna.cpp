/*
 * agentdyna.cpp
 *
 *  Created on: Sep 3, 2010
 *      Author: Erik Schuitema
 */

#include <AgentDyna.h>

bool CAgentDyna::init(CAgentQ* agentQ, CAgentEnvModel* model)
{
	mAgentQ			= agentQ;
	mAgentState		= agentQ->getAgentState();
	mResultState	= mAgentState;
	mAgentAction	= agentQ->getAgentAction();
	mTempAction		= mAgentAction;

	mModel			= model;
	return true;
}

CAgentDynaRandom::CAgentDynaRandom():
	mLog("DynaRandom"),
	mStateAsInts(false)
{
}

void CAgentDynaRandom::stateAsInts(bool enabled)
{
	mStateAsInts = enabled;
}

void CAgentDynaRandom::setStateBounds(CAgentState& stateBoundsMin, CAgentState& stateBoundsMax, int numStateVars)
{
	memcpy(mStateBoundsMin, stateBoundsMin.getState(), numStateVars*sizeof(_StatePrecision));
	memcpy(mStateBoundsMax, stateBoundsMax.getState(), numStateVars*sizeof(_StatePrecision));
}

void CAgentDynaRandom::randomize(CAgentState& state, CAgentAction& action)
{
	// Randomize state
	if (mStateAsInts)
	{
		for (int i=0; i<state.getNumStateVars(); i++)
			state.setStateVar(i, (_StatePrecision)gRanrotB.IRandom((int)mStateBoundsMin[i], (int)mStateBoundsMax[i]));
	}
	else
	{
		for (int i=0; i<state.getNumStateVars(); i++)
			state.setStateVar(i, mStateBoundsMin[i] + (mStateBoundsMax[i]-mStateBoundsMin[i])*(_StatePrecision)gRanrotB.Random());
	}

	// Randomize action
	action.randomizeUniform(&gRanrotB);
}

int CAgentDynaRandom::singleBackup(double learningRate)
{
	randomize(mAgentState, mAgentAction);
	// Check if the generated state is terminal. If so, quit.
	if (mAgentQ->isTerminalState(mAgentState, 0) & CONST_STATE_TERMINAL)
		return 0;

	ComputationalType reward;
	if (!mModel->predictForward(mAgentState, mAgentAction, &mResultState, &reward))
		return 0;

	// Overwrite reward with calculated one
	if (!mPredictReward)
	{
	  ComputationalType calculatedReward = mAgentQ->calculateReward(mAgentAction, mResultState);
	  reward = calculatedReward;
	}

	if (reward > 110)
	{
		mLogWarningLn("Reward was " << reward << " for " << mAgentState.toStr() << "." << mAgentAction.toStr() << " -> " << mResultState.toStr());
		return -1;
	}

	if (mStateAsInts)
		// Round result
		for (int i=0; i<mResultState.getNumStateVars(); i++)
			mResultState[i] = floor(mResultState[i] + 0.5f);

	// Learning update

	// Call this before calling getQValue(mAgentState, mAgentAction), otherwise the internally stored result of getQValue() is lost
	ComputationalType Qnext;
	char nextStateIsTerminal = mAgentQ->isTerminalState(mResultState, 0);	// Assume time 0 as if it was the first state of the trial
	if ((nextStateIsTerminal & CONST_STATE_TERMINAL) && (nextStateIsTerminal & CONST_STATE_ABSORBING))
	{
		Qnext = 0;
		//mLogNoticeLn("Terminal absorbing state detected");
	}
	else
		Qnext = mAgentQ->getQSpace()->getBestActionQValue(mResultState, mTempAction);	// We don't need the resulting best action for this state

	if (fabs(Qnext) > 110)
	{
		mLogWarningLn("Qnext was " << Qnext << " for s:" << mResultState.toStr());
		return -1;
	}

	// The call to getQValue() stores the tiles of mAgentState, which we can use directly afterwards in setStoredQValue()
	ComputationalType Qcurr = mAgentQ->getQSpace()->getQValue(mAgentState, mAgentAction);

	// The update rule
	ComputationalType Qnew = Qcurr + learningRate*(reward + mAgentQ->getDiscountRate()*Qnext - Qcurr);

	// Update the Q-value
	mAgentQ->getQSpace()->setStoredQValue((_QPrecision)Qnew);

	// Residual gradient update
	if (mResidualWeight > 0 && (!(nextStateIsTerminal & CONST_STATE_TERMINAL) || !(nextStateIsTerminal & CONST_STATE_ABSORBING)))
	{
	  ComputationalType Qres = Qnext - mResidualWeight*learningRate*mAgentQ->getDiscountRate()*(reward + mAgentQ->getDiscountRate()*Qnext - Qcurr);
	  mAgentQ->getQSpace()->setStoredBestActionQValue((_QPrecision)Qres);
	}

	return 1;
}

// CAgentDynaSequential

CAgentDynaSequential::CAgentDynaSequential():
	mLog("DynaSequential"),
	mStateAsInts(false)
{
}

void CAgentDynaSequential::stateAsInts(bool enabled)
{
	mStateAsInts = enabled;
}

void CAgentDynaSequential::setStateBounds(CAgentState& stateBoundsMin, CAgentState& stateBoundsMax, int numStateVars)
{
	memcpy(mStateBoundsMin, stateBoundsMin.getState(), numStateVars*sizeof(_StatePrecision));
	memcpy(mStateBoundsMax, stateBoundsMax.getState(), numStateVars*sizeof(_StatePrecision));
}

void CAgentDynaSequential::randomize(CAgentState& state, CAgentAction& action)
{
	// Randomize state
	if (mStateAsInts)
	{
		for (int i=0; i<state.getNumStateVars(); i++)
			state.setStateVar(i, (_StatePrecision)gRanrotB.IRandom((int)mStateBoundsMin[i], (int)mStateBoundsMax[i]));
	}
	else
	{
		for (int i=0; i<state.getNumStateVars(); i++)
			state.setStateVar(i, mStateBoundsMin[i] + (mStateBoundsMax[i]-mStateBoundsMin[i])*(_StatePrecision)gRanrotB.Random());
	}

	// Randomize action
	action.randomizeUniform(&gRanrotB);
}

void CAgentDynaSequential::reset()
{
  randomize(mAgentState, mAgentAction);
}

int CAgentDynaSequential::singleBackup(double learningRate)
{
	if (mAgentQ->isTerminalState(mAgentState, 0) & CONST_STATE_TERMINAL)
	  randomize(mAgentState, mAgentAction);

	if (mAgentQ->isTerminalState(mAgentState, 0) & CONST_STATE_TERMINAL)
	{
	  mLogNoticeLn("Randomized to a terminal state");
	  return 0;
	}

	ComputationalType reward;
	if (!mModel->predictForward(mAgentState, mAgentAction, &mResultState, &reward))
	{
	  randomize(mAgentState, mAgentAction);
	  return 0;
	}

	// Overwrite reward with calculated one
	if (!mPredictReward)
	{
	  ComputationalType calculatedReward = mAgentQ->calculateReward(mAgentAction, mResultState);
	  reward = calculatedReward;
	}

	if (reward > 110)
		mLogWarningLn("Reward was " << reward << " for " << mAgentState.toStr() << "." << mAgentAction.toStr() << " -> " << mResultState.toStr());

	if (mStateAsInts)
		// Round result
		for (int i=0; i<mResultState.getNumStateVars(); i++)
			mResultState[i] = floor(mResultState[i] + 0.5f);

	// Learning update

	// Call this before calling getQValue(mAgentState, mAgentAction), otherwise the internally stored result of getQValue() is lost
	ComputationalType Qnext;
	char nextStateIsTerminal = mAgentQ->isTerminalState(mResultState, 0);	// Assume time 0 as if it was the first state of the trial
	if ((nextStateIsTerminal & CONST_STATE_TERMINAL) && (nextStateIsTerminal & CONST_STATE_ABSORBING))
	{
		Qnext = 0;
		//mLogNoticeLn("Terminal absorbing state detected");
	}
	else
		Qnext = mAgentQ->getQSpace()->getBestActionQValue(mResultState, mTempAction);

	if (Qnext > 110)
		mLogWarningLn("Qnext was " << Qnext << " for s:" << mResultState.toStr());

	// The call to getQValue() stores the tiles of mAgentState, which we can use directly afterwards in setStoredQValue()
	ComputationalType Qcurr = mAgentQ->getQSpace()->getQValue(mAgentState, mAgentAction);

	// The update rule
	ComputationalType Qnew = Qcurr + learningRate*(reward + mAgentQ->getDiscountRate()*Qnext - Qcurr);

	if (Qnew > 110)
		mLogWarningLn("Updating Qnew to " << Qcurr << "+" << learningRate << "*(" << reward << "+" << mAgentQ->getDiscountRate() << "*" << Qnext << "-" << Qcurr << "=" << Qnew);

	// Update the Q-value
	mAgentQ->getQSpace()->setStoredQValue((_QPrecision)Qnew);

	// Residual gradient update
	if (mResidualWeight > 0 && (!(nextStateIsTerminal & CONST_STATE_TERMINAL) || !(nextStateIsTerminal & CONST_STATE_ABSORBING)))
	{
	  ComputationalType Qres = Qnext - mResidualWeight*learningRate*mAgentQ->getDiscountRate()*(reward + mAgentQ->getDiscountRate()*Qnext - Qcurr);
	  mAgentQ->getQSpace()->setStoredBestActionQValue((_QPrecision)Qres);
	}

	mAgentState = mResultState;
	mAgentAction = mTempAction;

	return 1;
}

