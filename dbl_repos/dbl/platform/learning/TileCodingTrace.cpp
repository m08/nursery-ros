#include "TileCodingTrace.h"

#include <math.h>

CEligibilityTraceTC::CEligibilityTraceTC():
	mLog("el.trace")
{
	mElements = NULL;
	mNumElements = 0;
	mNumTilings = 1;
	mFalloff = 0;
	clear();
}

void CEligibilityTraceTC::init(const _QPrecision falloffFactor, const _QPrecision minimumValue, const _IndexPrecision numTilings, bool adjustTraceLength)
{
	mFalloff = falloffFactor;
	mNumTilings = numTilings;
	_IndexPrecision	oldNumElements = mNumElements;

	// Calculate N from: power(falloffFactor, N) == minimumValue
	// --> N = log(minimumValue) / log(falloffFactor);
	if (minimumValue > 0)
		mNumElements = (_IndexPrecision)ceil(log(minimumValue) / log(falloffFactor));
	else
		mNumElements = DEFAULT_TRACE_LENGTH;

	if (falloffFactor <= 0.0)
		//mNumElements = 0;	// old way, no solution for zero trace length
		mNumElements = 1;

	// We need a multiple of numTilings
	mNumElements *= numTilings;

	if (adjustTraceLength)
	{
		if (mElements != NULL)
		{
			delete[] mElements;
			mElements = NULL;
		}
		if (mNumElements > 0)
			mElements = new STraceElementTC[mNumElements];
		mLogDebugLn("Constructing eligibility trace with " << mNumElements << " elements.");
	}
	else
	{
		if (mNumElements > oldNumElements)
		{
			mLogWarningLn("New trace parameters require a lengthier trace, but reallocation not allowed! Num required elements is " << mNumElements);
			while (mNumElements > oldNumElements)
				mNumElements -= numTilings;
			mLogWarningLn("Clipping to " << mNumElements << "elements!");
		}
		else
			mLogInfoLn("Adjusted trace parameters without reallocating memory; num elements is now " << mNumElements << " (was " << oldNumElements << ")");
	}

	clear();
}

CEligibilityTraceTC::~CEligibilityTraceTC()
{
	if (mElements != NULL)
		delete[] mElements;
}

void CEligibilityTraceTC::fall(const _QPrecision extraFalloff)	// let trace fall
{
	_QPrecision falloff = mFalloff*extraFalloff;
	for (_IndexPrecision i=0; i<mNumFilled; i++)
		mElements[i].value *= falloff;
}

_IndexPrecision CEligibilityTraceTC::numElements()
{
	return mNumFilled;
}

void CEligibilityTraceTC::add(const _IndexPrecision index)
{
	mElements[mCurrentPos].index = index;
	mElements[mCurrentPos].value = 1.0f;

	mCurrentPos++;
	if (mCurrentPos >= mNumElements)
		mCurrentPos = 0;

	if (mNumFilled < mNumElements)
		mNumFilled++;
}

void CEligibilityTraceTC::addBatch(const _IndexPrecision *indices)
{
	// Assume that there's always (just) enough room for the complete batch
	for (_IndexPrecision i=0; i<mNumTilings; i++)
	{
 		mElements[mCurrentPos].index = indices[i];
		mElements[mCurrentPos].value = (_QPrecision)1.0;
		mCurrentPos++;
	}

	if (mCurrentPos >= mNumElements)
		mCurrentPos = 0;

	if (mNumFilled < mNumElements)
		mNumFilled += mNumTilings;
}


void CEligibilityTraceTC::addBatchEF(const _IndexPrecision *indices, const _QPrecision *featureValues)
{
	// Assume that there's always (just) enough room for the complete batch
	for (_IndexPrecision i=0; i<mNumTilings; i++)
	{
 		mElements[mCurrentPos].index = indices[i];
		mElements[mCurrentPos].value = featureValues[i];
		mCurrentPos++;
	}

	if (mCurrentPos >= mNumElements)
		mCurrentPos = 0;

	if (mNumFilled < mNumElements)
		mNumFilled += mNumTilings;
}

void CEligibilityTraceTC::clear()
{
	mCurrentPos	= 0;
	mNumFilled	= 0;
}

bool CEligibilityTraceTC::load(std::ifstream& FileStream, bool loadElements)
{
	if (FileStream.bad()) return false;

	FileStream.read((char*)&mNumTilings,			sizeof(mNumTilings));
	FileStream.read((char*)&mFalloff,				sizeof(mFalloff));
	FileStream.read((char*)&mNumElements,			sizeof(mNumElements));
	FileStream.read((char*)&mNumFilled,				sizeof(mNumFilled));
	FileStream.read((char*)&mCurrentPos,			sizeof(mCurrentPos));

	if (mElements)
	{
		delete[] mElements;
		mElements = NULL;
	}
	mElements = new STraceElementTC[mNumElements];
	// Usually there's no need to load the elements of the trace, since the agent will not start in the exact same state when it is revived again.
	if (loadElements)
		FileStream.read((char*)&mElements[0],			mNumElements*sizeof(mElements[0]));
	else
		clear();

	return !FileStream.bad();
}

bool CEligibilityTraceTC::save(std::ofstream& FileStream, bool saveElements)
{
	if (FileStream.bad()) return false;

	FileStream.write((char*)&mNumTilings,			sizeof(mNumTilings));
	FileStream.write((char*)&mFalloff,				sizeof(mFalloff));
	FileStream.write((char*)&mNumElements,			sizeof(mNumElements));
	FileStream.write((char*)&mNumFilled,			sizeof(mNumFilled));
	FileStream.write((char*)&mCurrentPos,			sizeof(mCurrentPos));

	// Usually there's no need to store the elements of the trace, since the agent will not start in the exact same state when it is revived again.
	if (saveElements)
		FileStream.write((char*)&mElements[0],			mNumElements*sizeof(mElements[0]));

	return !FileStream.bad();
}

// ===== Replacing Trace ======

void CReplacingTraceTC::resetExistingFeatures(const _IndexPrecision *indices)
{
	// If a feature that is to be added is already in the trace, reset its value to 0
	for (_IndexPrecision i=0; i<mNumFilled; i++)
	{
		for (_IndexPrecision iNew=0; iNew<mNumTilings; iNew++)
			if (mElements[i].index == indices[iNew])
				mElements[i].value = 0;
	}
}

void CReplacingTraceTC::addBatch(const _IndexPrecision *indices)
{
	resetExistingFeatures(indices);
	// Now add all features
	CEligibilityTraceTC::addBatch(indices);
}

void CReplacingTraceTC::addBatchEF(const _IndexPrecision *indices, const _QPrecision *featureValues)
{
	resetExistingFeatures(indices);
	// Now add all features
	CEligibilityTraceTC::addBatchEF(indices, featureValues);
}
