/*
 * agentgps.h
 *
 *  Created on: Sep 10, 2010
 *      Author: Erik Schuitema
 *
 * GPS is the family of General Prioritized Solvers, such as Prioritized Sweeping.
 *
 */

#ifndef AGENTLLRDYNA_H_
#define AGENTLLRDYNA_H_

#include <AgentDyna.h>
#include <algorithm>
#include <Log2.h>

// Priority queue of bounded size. When a new element is pushed to a full queue,
// the element with the smallest priority will be popped from the queue.
class CAgentPriorityQueue
{
	public:
		class QueueElement
		{
			public:
				CAgentState	state;
				double		priority;
		};

		static inline bool	compareQueueElements(const QueueElement* x, const QueueElement* y)
		{
			return x->priority < y->priority;
		}

	protected:
		int							mMaxLength;
		// The QueueElements can be quite large. Therefore, we don't want to swap them around in the priority heap.
		// So we have a separate vector with the QueueElements and an extra vector with free spots.
		std::vector<QueueElement*>	mLocations;		// The pointers to the elements. The first part is the used heap, the last part the free places.

		std::vector<QueueElement>	mElements;		// The queue elements (not all are valid)
		std::vector<QueueElement*>::iterator	mHeapBegin;
		std::vector<QueueElement*>::iterator	mHeapEnd;

	public:
		CAgentPriorityQueue();
		CAgentPriorityQueue(int maxQueueLength);

		// setMaxLength() sets the maximum queue length by reserving enough memory
		void				setMaxLength(const int maxLength);
		// clear() empties the queue
		void				clear();
		// push() adds a new state to the queue with given priority
		void				push(const CAgentState& state, const double priority);
		// pop() removes the element with the largest priority.  It invalidates top(). If you need its data, first use top() before you pop().
		void				pop();
		// size() returns the number of elements in the queue
		unsigned int		size();
		// top() returns the element with the largest priority. It can return an invalid reference if size() == 0.
		const QueueElement&	top();
		// back() returns the element with the smallest priority. Mainly interesting for debugging
		const QueueElement& back();

		const QueueElement&	heapElement(int index);	// For debugging only!
};

// MG-PS: Prioritized Sweeping according to McMahan and Gordon (2005)
class CAgentMGPS: public CAgentDyna
{
	protected:
		CLog2					mLog;
		_StatePrecision	mStateBoundsMin[AGENT_MAX_NUM_STATEVARS];
		_StatePrecision	mStateBoundsMax[AGENT_MAX_NUM_STATEVARS];
		int						mNumStateVars;
		CAgentPriorityQueue		mQueue;
		bool					mStateAsInts;	// For gridworlds
		bool					mRandomizeQueue;// Init queue with random element when empty
		void			randomize(CAgentState& state);

	public:
		CAgentMGPS(int maxQueueLength);
		virtual ~CAgentMGPS();
		void			stateAsInts(bool enabled);
		virtual void	resetMemory();
		virtual void	reportUpdate(CAgentState& state, double priority);
		void			setStateBounds(CAgentState& stateBoundsMin, CAgentState& stateBoundsMax, int numStateVars);
		bool			stateInsideBounds(const CAgentState& state);
		virtual int		singleBackup(double learningRate);
		int				currentQueueSize();
		void			enableQueueRandomization(bool enabled);
};


#endif /* AGENTLLRDYNA_H_ */
