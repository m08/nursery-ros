/*
 * STGListener.h
 *
 *  Created on: Nov 25, 2008
 *      Author: Erik Schuitema
 */

#ifndef STGLISTENER_H_
#define STGLISTENER_H_

#include <mqueue.h>
#include <STG.h>

/** Class that is able to receive messages from a State Transition Generator
*  When deriving your own listener class from this class, be sure to make use of
*  the functions startListening() and stopListening(), otherwise waitForNewState() has no effect!
*/
template<class STGStateType>
class CSTGListener
{
	private:
		CSTGInQueue<STGStateType>					        mInQueue;
		CStateTransitionGenerator<STGStateType>		*mSTG;

	public:
		CSTGListener(CStateTransitionGenerator<STGStateType>* stg)
		{
			mSTG = stg;
		}
		virtual ~CSTGListener()
		{

		}

		inline bool waitForNewState()
		{
			return mInQueue.waitForNewState();
		}

		inline CStateTransitionGenerator<STGStateType> *getSTG()
		{
			return mSTG;
		}

		inline STGStateType* getState()
		{
			return mInQueue.getState();
		}

		inline bool startListening(ESTGReceiveMode receiveMode, int frequencyDivider, int queueLength, const char *nameExtension)
		{
			std::string stgQueueName = mSTG->createSubscription(receiveMode, frequencyDivider, queueLength, nameExtension);
			if (stgQueueName.empty())
				return false;

			return mInQueue.open(stgQueueName);
		}

		inline bool stopListening()
		{
			int retval = mInQueue.close();
			mSTG->stopSubscription(mInQueue.getName());
			return retval;
		}

};



#endif /* STGLISTENER_H_ */
