/*
 * STGTiming.h
 *
 *  Created on: May 19, 2010
 *      Author: erik
 */

#ifndef STGTIMING_H_
#define STGTIMING_H_

//#include <pthread.h>	// How does this work with Xenomai?
#include <Statistics.h>
#include <Log2.h>
#include <Mutex.hpp>

// CRtFifo: First-in, first-out queue with fixed size (no memory reallocation allowed in real-time apps).
// Items (templated) are added to the back of the buffer and removed from the front,
// so the front contains the oldest element.
// Internally, it uses an std::vector as a ring buffer with read and write position.
// NOTE: This implementation is minimalistic and merely sufficient for use in CSTGTiming
template <class T>
class CRtFifo
{
/*
	private:
		// Maybe such a structure can prevent the user from needing mutexes?
		class E
		{
			public:
				T			element;
				bool		occupied;
				E(): occupied(false)	{}
		};
*/
	protected:
		std::vector<T>	mData;			// Fifo buffer
		int32_t			mSize;			// Maximum number of elements in the Fifo
		int32_t			mNumElements;	// Current number of elements in the Fifo
		int32_t			mReadPos;
		int32_t			mWritePos;
		bool			mOverrun;		// True if the user tried to add an element to a full Fifo

		int32_t			frontPos() const	{return mReadPos;}
		int32_t			backPos() const		{return (mWritePos-1 >= 0)?mWritePos:(mSize-1);}

	public:
		// Iterator. Does not change the Fifo, hence the const *mFifo.
		class iterator
		{
			private:
				const CRtFifo<T>* mFifo;
				int32_t	mPos;

				// Constructor. Should not be called outside CRtFifo. 'pos' should point to an occupied Fifo element
				friend class CRtFifo;
				iterator(const CRtFifo<T>* fifo, int32_t pos): mFifo(fifo), mPos(pos)	{}

			public:
				// The default constructor is not necessary, unless you plan to misuse the iterator
				//iterator(): mFifo(NULL), mPos(-1)	{}

				// Since we assume that the constructor receives a valid pos,
				// the operator++ is the only one that changes pos and invalidates it,
				// operator bool() only has to check for a valid pos and a non-empty Fifo.
				operator bool()	const				{return valid();}
				bool	valid() const				{return (mPos >= 0) && (!mFifo->empty());}
				//operator T*()						{if (mPos >= 0) return &mFifo->mData[mPos].element; else return NULL;}
				const T* operator->()				{if (mPos >= 0) return &mFifo->mData[mPos]; else return NULL;}
				iterator& operator++(int unused)	// postfix ++ operator needs the 'int unused' variable
				{
					// Only increment valid positions
					if (mPos != -1)
					{
						// Trying to increment the backPos invalidates the iterator (end of buffer)
						if (mPos == mFifo->backPos())
							mPos = -1;
						else
						{
							// Increment
							mPos++;
							// Check for wrap around
							if (mPos >= mFifo->size())
								mPos = 0;
						}
					}
					return *this;
				}
		};

		CRtFifo(uint32_t size):
			mSize(size),
			mNumElements(0),
			mReadPos(0),
			mWritePos(0),
			mOverrun(false)
		{
			mData.resize(size);	// Create a vector with size elements
		}

		void		push_back(const T& x)
		{
			// If the Fifo is full, we fail and hope that next time mWritePos is not occupied anymore.
			// (in this way, we don't invalidate front())
			if (full())
				mOverrun = true;
			else
			{
				// Occupy the element at mWritePos
				mNumElements++;
				mData[mWritePos] = x;
				mWritePos++;
				if (mWritePos >= mSize)
					mWritePos = 0;
			}
		}
		void		pop_front()
		{
			mReadPos++;
			if (mReadPos >= mSize)
				mReadPos = 0;
			mNumElements--;
		}
		//void		clear()			{mData.clear();}
		bool		overrun() const	{return mOverrun;}
		int32_t		size()	const	{return mSize;}
		bool		empty()	const	{return mNumElements <= 0;}
		bool		full()	const	{return mNumElements >= mSize;}
		iterator	front()	const	{return iterator(this, frontPos());}
		iterator	back()	const	{return iterator(this, backPos());}
};

class CSTGStateTime
{
	public:
		CSTGStateTime(): ID(0), time(0) {}
		CSTGStateTime(uint64_t __ID, uint64_t __time): ID(__ID), time(__time) {}
		uint64_t	ID;
		uint64_t	time;
};

#define TIME_INVALID ((uint64_t)-1)

// CSTGTiming keeps track of (statistics of) all the timing characteristics inside the STG
class CSTGTiming
{
	protected:
		CLog2						mLog;
		// One thread calls reportState(), while another thread calls reportAction(),
		// so that should be thread safe.
		// The fifo is only safe if reportAction(someID) is always called after reportState(someID)
		CRtFifo<CSTGStateTime>		mStates;
		CMutex						mReportLock;			// Protects mStates fifo
		CSimpleStat					mMeasurementTime;		// Time it takes to collect state information
		CSimpleStat					mMeasurementPeriod;		// Time between state measurements
		CSimpleStat					mBroadcastPeriod;		// Time between broadcasts
		CSimpleStat					mActuationPeriod;		// Time between actions
		CSimpleStat					mBroadcastDelay;		// Time between state measurement and broadcast
		CSimpleStat					mActuationDelay;		// Time between state measurement and actuation
		uint64_t					mLastMeasurementTime;
		uint64_t					mLastBroadcastTime;
		uint64_t					mLastActuationTime;

	public:
		// maxDelaySteps is the maximum actuation delay the timing can detect, expressed in measurement steps
		// statBufferLEngth is the number of samples the statistics buffers can hold
		CSTGTiming(uint32_t maxDelaySteps, uint32_t statBufferLength):
			mLog("STG-timing"),
			mStates(maxDelaySteps),
			mMeasurementTime(statBufferLength),
			mMeasurementPeriod(statBufferLength),
			mBroadcastPeriod(statBufferLength),
			mActuationPeriod(statBufferLength),
			mBroadcastDelay(statBufferLength),
			mActuationDelay(statBufferLength),
			mLastMeasurementTime(TIME_INVALID),
			mLastBroadcastTime(TIME_INVALID),
			mLastActuationTime(TIME_INVALID)
		{
		}

		~CSTGTiming()
		{
		}

		// Statistics interface
		const CSimpleStat& measurementTime() const		{return mMeasurementTime;}
		const CSimpleStat& measurementPeriod() const	{return mMeasurementPeriod;}
		const CSimpleStat& broadcastPeriod() const		{return mBroadcastPeriod;}
		const CSimpleStat& actuationPeriod() const		{return mActuationPeriod;}
		const CSimpleStat& broadcastDelay() const		{return mBroadcastDelay;}
		const CSimpleStat& actuationDelay() const		{return mActuationDelay;}

		// Function to clear the statistics. Useful when switching to another policy with different timing properties.
		// This function is *not* thread safe, so don't call clearStats() while calling measurementPeriod(), actuationDelay() etc. from another thread.
		void	clearStats()
		{
			// Don't clear mStates; we don't know if reportAction(stateID) will be called for already cleared states.
			mMeasurementTime.clear();
			mMeasurementPeriod.clear();
			mBroadcastPeriod.clear();
			mActuationPeriod.clear();
			mBroadcastDelay.clear();
			mActuationDelay.clear();
		}

		// Reporting functions
		// reportState() reports a new state measurement to this class.
		// stateID					: the unique identified of this state
		// measurementStartTimeUs	: time stamp when the state measurement started
		// measurementEndTimeUs		: time stamp when the state measurement ended
		// broadcastTimeUs			: time stamp when the state information was broadcast to the listeners
		void	reportState(uint64_t stateID, uint64_t measurementStartTimeUs, uint64_t measurementEndTimeUs, uint64_t broadcastTimeUs)
		{
			// Lock while modifying the Fifo
			mReportLock.lock();
			// Make room if necessary. This easily happens when no actions are reported (no policy is active)
			if (mStates.full())
			{
				mLogCrawlLn("Timing buffer too small for current actuation delay!");
				mStates.pop_front();
			}
			// Add latest state. Take the end time stamp of the measurement as the time stamp of the state information
			mStates.push_back(CSTGStateTime(stateID, measurementEndTimeUs));

			// Unlock
			mReportLock.unlock();

			// Add measurement time
			mMeasurementTime.addValue(measurementEndTimeUs - measurementStartTimeUs);
			// Add broadcast delay
			mBroadcastDelay.addValue(broadcastTimeUs - measurementEndTimeUs);
			// Add broadcast period
			if (mLastBroadcastTime != TIME_INVALID)
				mBroadcastPeriod.addValue(broadcastTimeUs - mLastBroadcastTime);
			// Add measurement period
			if (mLastMeasurementTime != TIME_INVALID)
				mMeasurementPeriod.addValue(measurementEndTimeUs - mLastMeasurementTime);

			// Save timing
			mLastMeasurementTime	= measurementEndTimeUs;
			mLastBroadcastTime		= broadcastTimeUs;
		}

		// Report a new action. Returns the main timing result if desired: its actuation delay
		void	reportAction(uint64_t stateID, uint64_t actuationTimeUs, uint64_t *resultActuationDelay=NULL)
		{
			// Lock while modifying the Fifo
			mReportLock.lock();
			// Find the state for which the action is reported and delete it and everything before it
			// The iterator automatically becomes invalid if it runs over the last element
			bool found=false;
			uint64_t actuationDelay = 0;
			for (CRtFifo<CSTGStateTime>::iterator it = mStates.front(); it.valid(); it++)
			{
				if (it->ID == stateID)
				{
					// We found the state to which this action is a response
					found = true;
					actuationDelay = actuationTimeUs - it->time;
					mActuationDelay.addValue(actuationDelay);
					mStates.pop_front();
					break;
				}
				else
					mStates.pop_front();
			}
			// Unlock
			mReportLock.unlock();
			if (!found)
			{
				// The state to which the action responded is not in the buffer (anymore)
				// We were probably too late! The buffer length should be increased.
				// Lock while modifying the Fifo
				mReportLock.lock();
				uint64_t minStateID = (uint64_t)-1;
				uint64_t maxStateID = 0;
				for (CRtFifo<CSTGStateTime>::iterator it = mStates.front(); it.valid(); it++)
				{
					if (it->ID > maxStateID)
						maxStateID = it->ID;
					if (it->ID < minStateID)
						minStateID = it->ID;
				}
				// Unlock
				mReportLock.unlock();
				mLogErrorLn("Action responded to an already purged state (ID=" << stateID <<  "); available states in the range " << minStateID << "-" << maxStateID << ". Try increasing BufferLength.");
			}
				// Add actuation period
			if (mLastActuationTime != TIME_INVALID)
				mActuationPeriod.addValue(actuationTimeUs - mLastActuationTime);
			// Return the main result if desired: the actuation delay
			if (resultActuationDelay != NULL)
				*resultActuationDelay = actuationDelay;

			// Save timing
			mLastActuationTime = actuationTimeUs;
		}
};


#endif /* STGTIMING_H_ */
