/*
 * STGTimelinePolicy.h
 *
 *  Created on: Dec 22, 2008
 *      Author: Erik Schuitema
 */

#ifndef STGTIMELINEPOLICY_H_
#define STGTIMELINEPOLICY_H_

#include <vector>

#include <STGPolicy.h>
#include <Configuration.h>
#include <Log2.h>

// Forward declaration of CSTGTimelineSection and CSTGTimelinePeriod
class CSTGTimelineSection;
class CSTGTimelinePeriod;

class CSTGTimelineAction
{
  protected:
    CLog2                mLog;
    ISTGActuation        *mActuationInterface;
    CSTGTimelinePeriod  *mParent;

  public:
    CSTGTimelineAction():
      mLog("timeline-action")
    {
        mActuationInterface = NULL;
        mParent              = NULL;
    }
    virtual ~CSTGTimelineAction() {}

    virtual void setAction(CSTGJointTransformFunc jointTransformFunc)=0;
    virtual bool readConfig(const CConfigSection &configNode)=0;

    void setTimelinePeriod(CSTGTimelinePeriod *parentPeriod)
    {
      mParent = parentPeriod;
    }

    void setActuationInterface(ISTGActuation  *interface)
    {
      mActuationInterface = interface;
    }
};

class CSTGTimelineActionOff: public CSTGTimelineAction
{
  public:
    CSTGTimelineActionOff() {}

    virtual void  setAction(CSTGJointTransformFunc jointTransformFunc) {}
    virtual bool  readConfig(const CConfigSection &configNode) {return true;}
};

class CSTGTimelineActionPosition: public CSTGTimelineAction
{
  protected:
    int        mJointIndex;
    double    mPosition;
    double    mSpeed;
  public:
    CSTGTimelineActionPosition(int jointIndex, double position, double speed):
      mJointIndex(jointIndex), mPosition(position), mSpeed(speed) {}

    virtual void setAction(CSTGJointTransformFunc jointTransformFunc)
    {
      if (jointTransformFunc == NULL)
        mActuationInterface->setJointPosition(mJointIndex, mPosition, mSpeed);
      else
        mActuationInterface->setJointPosition((*jointTransformFunc)(mJointIndex), mPosition, mSpeed);
    }

    virtual bool readConfig(const CConfigSection &configNode)
    {
      bool configresult = true;
      std::string jointName;
      configresult &= mLogAssert(configNode.get("jointname", &jointName));
      mJointIndex = mActuationInterface->getJointIndexByName(jointName);
      configresult &= mLogAssert(configNode.get("position", &mPosition, 0.0));
      configresult &= mLogAssert(configNode.get("speed", &mSpeed, 0.0));
      return configresult;
    }
};

class CSTGTimelineActionSpeed: public CSTGTimelineAction
{
  protected:
    int      mJointIndex;
    double    mSpeed;
  public:
    CSTGTimelineActionSpeed(int jointIndex, double speed):
      mJointIndex(jointIndex), mSpeed(speed) {}

    virtual void  setAction(CSTGJointTransformFunc jointTransformFunc)
    {
      if (jointTransformFunc == NULL)
        mActuationInterface->setJointSpeed(mJointIndex, mSpeed);
      else
        mActuationInterface->setJointSpeed((*jointTransformFunc)(mJointIndex), mSpeed);
    }

    virtual bool  readConfig(const CConfigSection &configNode)
    {
      bool configresult=true;
      std::string jointName;
      configresult &= mLogAssert(configNode.get("jointname", &jointName));
      mJointIndex = mActuationInterface->getJointIndexByName(jointName);
      configresult &= mLogAssert(configNode.get("speed", &mSpeed, 0.0));
      return configresult;
    }
};

class CSTGTimelineActionVoltage: public CSTGTimelineAction
{
  protected:
    int      mJointIndex;
    double    mVoltage;
  public:
    CSTGTimelineActionVoltage(int jointIndex, double voltage):
      mJointIndex(jointIndex), mVoltage(voltage) {}

    virtual void  setAction(CSTGJointTransformFunc jointTransformFunc)
    {
      if (jointTransformFunc == NULL)
        mActuationInterface->setJointVoltage(mJointIndex, mVoltage);
      else
        mActuationInterface->setJointVoltage((*jointTransformFunc)(mJointIndex), mVoltage);
    }

    virtual bool  readConfig(const CConfigSection &configNode)
    {
      bool configresult=true;
      std::string jointName;
      configresult &= mLogAssert(configNode.get("jointname", &jointName));
      mJointIndex = mActuationInterface->getJointIndexByName(jointName);
      configresult &= mLogAssert(configNode.get("voltage", &mVoltage, 0.0));
      return configresult;
    }
};

class CSTGTimelineActionTorque: public CSTGTimelineAction
{
  protected:
    int      mJointIndex;
    double    mTorque;
    bool    mLockSet;  //did we read a lock parameter from the xml?
    bool    mLockState;//state of the lock
  public:
    CSTGTimelineActionTorque(int jointIndex, double torque, bool lockSet, bool lockState):
      mJointIndex(jointIndex), mTorque(torque), mLockSet(lockSet), mLockState(lockState) {}

    virtual void  setAction(CSTGJointTransformFunc jointTransformFunc)
    {
      if (jointTransformFunc == NULL)
      {
        if(mLockSet)
          mActuationInterface->setJointLock(mJointIndex, mLockState);
        mActuationInterface->setJointTorque(mJointIndex, mTorque);
      }
      else
      {
        if(mLockSet)
          mActuationInterface->setJointLock((*jointTransformFunc)(mJointIndex), mLockState);
        mActuationInterface->setJointTorque((*jointTransformFunc)(mJointIndex), mTorque);
      }
    }

    virtual bool  readConfig(const CConfigSection &configNode)
    {
      bool configresult=true;
      std::string jointName;
      configresult &= mLogAssert(configNode.get("jointname", &jointName));
      mJointIndex = mActuationInterface->getJointIndexByName(jointName);
      if(configNode.get("lock", &mLockState, false));
      mLockState = true;
      return configresult;
    }
};

class CSTGTimelinePeriod
{
protected:
  std::string                        mName;
  CLog2                              mLog;
  // TODO: make mActions vector of objects, not pointers?
  std::vector<CSTGTimelineAction*>  mActions;  // Array of action pointers.
  ISTGActuation                      *mActuationInterface;
  bool                              mIsParent;
  uint64_t                          mPeriod;
  ESTGActuationMode                  mActuationMode;
public:
  CSTGTimelinePeriod(uint64_t periodMicroseconds, bool isParent):  // Is this object parent of its added actions? If so, it will delete them automatically
    mLog("timeline-policy"),
    mActuationInterface(NULL),
    mIsParent(isParent),
    mPeriod(periodMicroseconds),
    mActuationMode(amOff)
  {}

  ~CSTGTimelinePeriod()
  {
    if (mIsParent)
    {
      for (unsigned int i=0; i<mActions.size(); i++)
        delete mActions[i];
    }
  }

  const std::string& name() { return mName; }

  void setActuationInterface(ISTGActuation  *interface) { mActuationInterface = interface; }

  void setActuationMode(ESTGActuationMode actuationMode) { mActuationMode = actuationMode; }

  uint64_t getPeriod() { return mPeriod; } // Microseconds

  int getNumActions() { return mActions.size(); }

  CSTGTimelineAction* addAction(CSTGTimelineAction *newAction)  // Action will be deleted automatically if mIsParent == true
  {
    newAction->setActuationInterface(mActuationInterface);
    newAction->setTimelinePeriod(this);
    mActions.push_back(newAction);
    return newAction;
  }

  void setActions(CSTGJointTransformFunc jointTransformFunc)
  {
    for (unsigned int i=0; i<mActions.size(); i++)
    {
      mActions[i]->setAction(jointTransformFunc);
    }
  }

  bool readConfig(const CConfigSection &configNode)
  {
    bool configresult=true;
    double dPeriod;
    configNode.get("name", &mName);  // Name is optional
    configresult &= mLogAssert(configNode.get("duration", &dPeriod, 0.0));  // in microseconds!!
    mPeriod = (uint64_t)dPeriod;

    // Read all actions
    int numActions=0;
    for (CConfigSection node = configNode.section("action"); !node.isNull(); node = node.nextSimilarSection())
    {
      // Add action. The type is determined by the section type
      CSTGTimelineAction *newAction = NULL;
      switch (mActuationMode)
      {
      case amOff:
        newAction = new CSTGTimelineActionOff();
        break;
      case amPosition:
        newAction = new CSTGTimelineActionPosition(0, 0.0, 0.0);
        break;
      case amSpeed:
        newAction = new CSTGTimelineActionSpeed(0, 0.0);
        break;
      case amVoltage:
        newAction = new CSTGTimelineActionVoltage(0, 0.0);
        break;
      case amTorque:
      case amTorqueInit:
        newAction = new CSTGTimelineActionTorque(0, 0.0, false, false);
        break;
      default:
        newAction = NULL;
        break;
      }

      if (newAction == NULL)
      {
        mLogErrorLn("While reading timeline policy period: unknown type " << (int)mActuationMode);
        configresult = false;
      }
      else
      {
        // Read action config
        configresult &= addAction(newAction)->readConfig(node);
        numActions++;
      }
    }
    mLogDebugLn("Read " << numActions << " actions");
    return configresult;
  }
};

// Every section has its actuation mode
class CSTGTimelineSection
{
protected:
  CLog2                mLog;
  std::vector<CSTGTimelinePeriod*>  mPeriods;  // Array of time line periods.
  int                  mCurrentPeriod;
  int                  mRepeatCount;
  int                  mNumTimesExecuted;
  ISTGActuation        *mActuationInterface;
  ESTGActuationMode    mActuationMode;
  bool                mStarted;
  bool                mDone;
  uint64_t            mPeriodStartTime;

  void printCurrentPeriodName()
  {
    mLogDebugLn("Starting period " << mCurrentPeriod << ": \"" << mPeriods[mCurrentPeriod]->name() << "\"");
  }

public:
  // repeatCount: 0 = infinite
  CSTGTimelineSection(ISTGActuation *actuationInterface, ESTGActuationMode actuationMode, int repeatCount):
    mLog("timeline-policy")
{
    mActuationInterface  = actuationInterface;
    mActuationMode    = actuationMode;
    mRepeatCount    = repeatCount;
    reset();
}
  virtual ~CSTGTimelineSection()
  {
    // Delete all pointers in mPeriods
    for (unsigned int i=0; i<mPeriods.size(); i++)
      delete mPeriods[i];
  }

  CSTGTimelinePeriod*  getPeriod(int periodIndex) { return mPeriods.at(periodIndex); }

  ESTGActuationMode  getActuationMode() { return mActuationMode; }

  void reset()
  {
    mNumTimesExecuted  = 0;
    mStarted      = false;
    mDone        = false;
    mCurrentPeriod    = 0;
  }

  void prepare()
  {
    //printf("Preparing for actuation mode %d...\n", mActuationMode);
    mActuationInterface->setActuationMode(mActuationMode);
    //printf("Actuation mode prepared!\n");
  }

  CSTGTimelinePeriod*  addPeriod(uint64_t periodMicroseconds)
  {
    // Add a new period object and tell the object that it is owner of its actions
    CSTGTimelinePeriod* newPeriod = new CSTGTimelinePeriod(periodMicroseconds, true);
    newPeriod->setActuationMode(mActuationMode);
    newPeriod->setActuationInterface(mActuationInterface);
    mPeriods.push_back(newPeriod);
    return newPeriod;
  }

  // Version of addPeriod that accepts doubles. This facilitates scientific notation (e.g., 1E6) without warnings etc.
  inline CSTGTimelinePeriod* addPeriod(double periodMicroseconds)
  {
    return addPeriod((uint64_t)periodMicroseconds);
  }

  bool isDone() { return mDone; }

  int executePolicy(uint64_t absoluteTimeMicroseconds, CSTGJointTransformFunc jointTransformFunc)
  {
    // Store very first starting time and check for content
    if (!mStarted)
    {
      mStarted      = true;
      mPeriodStartTime  = absoluteTimeMicroseconds;
      // Do we have content? If not, we are instantly done!
      if (mPeriods.size() == 0)
        mDone = true;
      else
        printCurrentPeriodName();
    }
    // Advance a period when a period's run time is over.
    if (!mDone)
    {
      if (absoluteTimeMicroseconds >= mPeriodStartTime + mPeriods[mCurrentPeriod]->getPeriod())
      {
        // Advance only when there is a next period
        if (mCurrentPeriod+1 < (int)mPeriods.size())
        {
          mPeriodStartTime  = absoluteTimeMicroseconds;
          mCurrentPeriod++;
          // Plot current period as mCurrentPeriod+1 to start counting at 1.
          printCurrentPeriodName();
          //printf("[POLICY] Advancing to period %d (total %d), which has %d actions\n", mCurrentPeriod+1, mPeriods.size(), mPeriods[mCurrentPeriod]->getNumActions());
        }
        else
          // All periods of this section are executed. Either reset to the beginning or signal that we are done.
        {
          mNumTimesExecuted++;
          //printf("[POLICY] Last period (%d) finished (%dth of %d times).\n", mCurrentPeriod, mNumTimesExecuted, mRepeatCount);
          // Check if we have finished yet. mRepeatCount == 0 means: repeat infinitely, so mDone is never set to true.
          if ((mNumTimesExecuted >= mRepeatCount) && (mRepeatCount != 0))
          {
            mDone = true;
          }
          else
          {
            reset();
            mStarted      = true;
            mPeriodStartTime  = absoluteTimeMicroseconds;
            printCurrentPeriodName();
          }
        }
      }
    }

    // As long as we are not done, keep sending action commands
    if (!mDone)
    {
      // Set current actions according to the action objects in our current period
      mPeriods[mCurrentPeriod]->setActions(jointTransformFunc);
    }
    return 0;
  }

  bool readConfig(const CConfigSection &configNode)
  {
    bool configresult=true;
    std::string actuationModeName;
    configresult &= mLogAssert(configNode.get("actuationmode", &actuationModeName, ""));
    mActuationMode = mActuationInterface->getActuationModeByName(actuationModeName);
    configNode.get("repeatcount", &mRepeatCount, 1);

    int numPeriods=0;
    for (CConfigSection node = configNode.section("period"); !node.isNull(); node = node.nextSimilarSection())
    {
      // Read timeline period
      configresult &= addPeriod((uint64_t)0)->readConfig(node);  // period time (now 0) will be read by readConfig
      numPeriods++;
    }
    mLogDebugLn("Read " << numPeriods << " periods");
    return configresult;
  }
};

template<class STGStateType>
class CSTGTimelinePolicy: public CSTGPolicy<STGStateType>
{
protected:
  CLog2                               mLog;
  std::vector<CSTGTimelineSection*>   mSections;
  int                                 mCurrentSection;
  CSTGJointTransformFunc              mJointTransformFunc;

public:
  CSTGTimelinePolicy(ISTGActuation *actuationInterface):
    CSTGPolicy<STGStateType>(actuationInterface),
    mLog("timeline-policy"),
    mCurrentSection(0),
    mJointTransformFunc(NULL)
    {
    }
  virtual ~CSTGTimelinePolicy() { clear(); }

  virtual void clear()
  {
    // Delete all pointers in mPeriods
    for (unsigned int i=0; i<mSections.size(); i++)
      delete mSections[i];
    mSections.clear();
    mCurrentSection = 0;
  }

  virtual void reset(uint64_t absoluteTimeMicroseconds)
  {
    CSTGPolicy<STGStateType>::reset(absoluteTimeMicroseconds);
    mCurrentSection = 0;
    for (unsigned int i=0; i<mSections.size(); i++)
      mSections[i]->reset();

    // Prepare the first section
    if (mSections.size() > 0)
      mSections[mCurrentSection]->prepare();
  }

  CSTGTimelineSection* getSection(int periodIndex) { return mSections.at(periodIndex); }

  int getCurrentSection() { return mCurrentSection; }

  CSTGTimelineSection* addSection(ESTGActuationMode actuationMode, int timesToRepeat=1)
  {
    // Add a new section object
    CSTGTimelineSection* newSection = new CSTGTimelineSection(this->getActuationInterface(), actuationMode, timesToRepeat);
    mSections.push_back(newSection);
    return newSection;
  }

  virtual bool isDone(STGStateType* currentState, uint64_t absoluteTimeMicroseconds)
  {
    if (mSections.size() > 0)
      return mSections.back()->isDone();  // If the last section is done, we're done!
    else
      return true;  // No sections -> we're done!
  }

  int executePolicy(STGStateType* currentState, uint64_t absoluteTimeMicroseconds)
  {
    int result = 0;
    if (mCurrentSection < (int)mSections.size())
    {
      // Check whether we are done with the current section and if so, advance to the next section
      if (mSections[mCurrentSection]->isDone())
      {
        mCurrentSection++;
        // Prepare the next section if there is one
        if (mCurrentSection < (int)mSections.size())
        {
          mSections[mCurrentSection]->prepare();
          printf("Section %d done. Advancing to the next section.\n", mCurrentSection-1);
        }
        else
        {
          printf("All sections completed\n");
        }
      }

      if (mCurrentSection < (int)mSections.size())
      {
        // Execute current section's policy
        result = mSections[mCurrentSection]->executePolicy(absoluteTimeMicroseconds, mJointTransformFunc);
      }
    }
    return result;
  }

  void setJointTransformFunction(CSTGJointTransformFunc func)
  {
    mJointTransformFunc = func;
  }

  virtual bool readConfig(const CConfigSection &configNode, bool append=false)
  {
    if (!append)
      clear();

    bool configresult=true;
    int numSections=0;
    for (CConfigSection node = configNode.section("section"); !node.isNull(); node = node.nextSimilarSection())
    {
      // Read timeline section
      configresult &= addSection(amOff)->readConfig(node);  // actuation mode (now amOff) will be read by readConfig
      if (!configresult)
        mLogErrorLn("Error in section " << numSections);
      numSections++;
    }
    mLogDebugLn("Read " << numSections << " timeline policy sections");
    // Check for empty config node
    if (mSections.size() == 0)
    {
      mLogErrorLn("No sections found in config node");
      configresult = false;
    }

    return configresult;
  }
};

#endif /* STGTIMELINEPOLICY_H_ */
