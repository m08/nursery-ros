/*
 * DataFile.hpp
 *
 *  Provides table-like (col,row) access to data files.
 *
 *  Created on: Aug 10, 2010
 *      Author: Erik Schuitema
 */

#ifndef DATAFILE_HPP_
#define DATAFILE_HPP_

#include <TextFile.hpp>
#include <vector>
#include <string>
#include <math.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <iostream>

typedef std::vector<double>			CDoubleVector;

// Data file in table format
class CDataFile
{
	protected:
		std::vector<std::string>	mHeader;
		std::vector<CDoubleVector*>	mData;

		std::string::size_type	findWhiteSpace(const std::string& string, int pos)
		{
			// Just detect tab separation for now
			return string.find('\t', pos);
		}

	public:
		CDataFile()		{}
		~CDataFile()	{clear();}

		void clear()
		{
			for (unsigned int i=0; i<mData.size(); i++)
				delete mData[i];
			mData.clear();
		}

		// The read() function assumes that the file contains some lines to skip,
		// followed by possibly one header line, then data lines in matrix form.
		void read(const std::string& filename, int numLinesToSkip, bool header)
		{
			TextFile fIn(filename, readText);	// Automatically closed in destructor
			std::string line;

			// Skip first lines
			for (int i=0; i<numLinesToSkip; i++)
				fIn.readLine(line);

			// Read header if existent
			if (header)
			{
				fIn.readLine(line);
				// (I ripped the following lines from the internet)
			    std::istringstream iss(line);
			    copy(std::istream_iterator<std::string>(iss),
			    		std::istream_iterator<std::string>(),
			    		std::back_inserter<std::vector<std::string> >(mHeader));
			}

			// Start reading data
			while (fIn.readLine(line))
			{
				if (line.size() <= 0)
					continue;

				CDoubleVector *data = new CDoubleVector;
				// Parse data line and put fields into data
				unsigned int searchPos=0;

				while (searchPos < line.size())
				{
					std::string::size_type whitePos = findWhiteSpace(line, searchPos);
					if (whitePos == std::string::npos)	// no occurrence means last item
						whitePos = line.size();
					if (whitePos-searchPos > 0)
					{
						double value = atof(line.substr(searchPos, whitePos-searchPos).c_str());
						data->push_back(value);
					}
					searchPos = whitePos+1;
				}
				mData.push_back(data);	// Deleted in clear()
			}
		}

		// data() returns a vector of data rows.
		const std::vector<CDoubleVector*>& data() {return mData;}

		// dataValue(col, row) == data()[row]->at(col)
		// With range checking
		//inline double dataValue(int col, int row)	{return mData[row]->at(col);}
		// Without range checking
		inline double		dataValue(int col, int row)	{return (*mData[row])[col];}
		inline std::string	colHeader(int col)			{if (col < (int)mHeader.size()) return mHeader[col]; else return "";}
		inline int 			getSize()					{return (int) mData.size();}

		// Search feature:
		// getRowIndex() returns the row index where the column value is close (but smaller than) the supplied value parameter.
		int getRowIndex(double value, int column, int startSearchAt)
		{
			int index = startSearchAt;
			if (index >= (int)mData.size())
				index = mData.size()-1;
			if (index < 0)
				index = 0;


			if (dataValue(column, index) >= value)
			{
				while ((index > 0) && (dataValue(column, index) > value))
					index--;
			}
			else	// < time
			{
				while ((index < (int)mData.size()) && (dataValue(column, index) < value))
					index++;

				// Take the index just before we passed the wanted time
				if (index > 0)
					index--;
			}

			return index;
		}
};

#endif /* DATAFILE_HPP_ */
