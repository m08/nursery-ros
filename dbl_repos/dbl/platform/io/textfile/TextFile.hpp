#ifndef TEXTFILE_H_
#define TEXTFILE_H_

#include <string>
#include <iostream>
#include <fstream>


/**
 * FileMode is an enum for storing the mode for the access-mode of a file.
 */
enum FileMode {
	readText,       /*!< Read-only text */
	writeText,      /*!< Write text */
	appendText,     /*!< Append text */
	readBinary,     /*!< Read-only binary */
	writeBinary,    /*!< Write binary */
	appendBinary,   /*!< Append binary */
	numOfFileModes  /*!< The number of file-modes */
};

class TextFile
{
public:
	TextFile(): _flushInterval(1){}
	TextFile(int FlushInterval): _flushInterval(FlushInterval) {}
	/**
	 * Constructor which opens a stream directly
	 *
	 * @param filename The name of the file
	 * @param mode The mode of opening
	 */
	TextFile(const std::string& filename, FileMode fileMode){
		openFile(filename, fileMode);
	}

	/**
	 * Closes the streams
	 */
	virtual ~TextFile(){
		closeFile();
	}



	/**
	* Reads a line from the file.
	*
	* @param line The line
	* @return true if reading was successful
	*/
	bool readLine(std::string& line) {
		if (_mode == writeText) {
			return false;
		}
		if (!_inputFile.is_open()) {
			return false;
		}
		if (_inputFile.eof()) {
			return false;
		}

		std::getline(_inputFile, line);
		return true;
	}

	/**
	* Writes a line to the file.
	*
	* @param line The line
	* @return true if writing was successful
	*/
	bool writeLine(const std::string& line){
		static int count = 0;
		if (_mode == readText) {
			return false;
		}
		if (!_outputFile.is_open()) {
			return false;
		}
		_outputFile.write(line.c_str(), line.length());
		_outputFile.write("\n", 1);

		if (++count >= _flushInterval) {
			_outputFile.flush();
			count = 0;
		}
	//	_outputFile.flush();
		return true;
	}

	/**
	 * Checks if the file exists
	 *
	 * @param filename The name of the file
	 * @return true if the file exists
	 */
	bool fileExists(const std::string& filename){
		bool retValue = true;
		std::ifstream inp;
		inp.open(filename.c_str(), std::ios::in);
		inp.close();
		if(inp.fail()) {
			retValue  = false;
		}
		return retValue;
	}

	/**
	 * Open a file in a specific mode
	 *
	 * @param filename The name of the file
	 * @param mode The mode of opening
	 * @return true if the file could be opened
	 */
	bool openFile(const std::string& filename, FileMode fileMode){
		_mode = fileMode;

		if (_mode == readText)
		{
			if (!fileExists(filename)) {
				printf("File '%s' doesn't exist for input.\n", filename.c_str());
				return false;
			}
			_inputFile.open(filename.c_str(),std::ios::in);
		}
		else if (_mode == appendText)
		{
			// Appends if file exists.
			_outputFile.open(filename.c_str(), std::ios::out | std::ios::app);
		}
		else if (_mode == writeText)
		{
			// Do not append.
			_outputFile.open(filename.c_str(), std::ios_base::out | std::ios_base::trunc);
		}
		else
		{
			// Mode not known
			printf("Unknown mode given.\n");
			return false;
		};
		return true;
	}

	/**
	 * Closes the file
	 */
	void closeFile(){
		if (_outputFile.is_open()) {
			_outputFile.close();
		}
		if (_inputFile.is_open()) {
			_inputFile.close();
		}
	}
protected:
	std::ifstream _inputFile;
	std::ofstream _outputFile;
	FileMode _mode;
	int _flushInterval;
};

#endif /*TEXTFILE_H_*/
