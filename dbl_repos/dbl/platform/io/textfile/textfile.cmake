#
# CMake include file for textfile library
# Wouter Caarls <w.caarls@tudelft.nl>
#
# 29-03-2010 (wcaarls): Initial revision
#

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/platform/io/textfile)

IF (NOT __TEXTFILE_CMAKE_INCLUDED)
  SET(__TEXTFILE_CMAKE_INCLUDED 1)

ENDIF (NOT __TEXTFILE_CMAKE_INCLUDED)
