/*
 * RtLog.h
 *
 * Real-time implementation of CLog for logging to the console and to file using rt_printf() and rt_fprintf().
 *
 *  Created on: Jul 20, 2009
 *      Author: Erik Schuitema
 */

#ifndef RTLOG_H_
#define RTLOG_H_

#include <iostream>
#include <sstream>
#include <fstream>
#include <streambuf>
#include <iomanip>
#include <rtdk.h>
#include <sys/time.h>
#include <native/timer.h>
#include <Log.h>

#define RTPRINTBUF_BUFLEN  2048

// rt_printf has problems with printing long strings.
// When reaching the end of its ring buffer, long strings may be truncated.
// The minimum available space for a new string is (RT_PRINT_LINE_BREAK + 1 - 2*sizeof(entry_head))==239.
// If the previous rt_printf would result in less space, the buffer automatically wraps around.
// Therefore, printing strings of 239 chars or less is safe.
// I subtracted a few in case I made a miscalculation :-)
#define RTPRINTF_MAX_STRING_SIZE  236

class CRtprintfStringbuf: public std::stringbuf
{
  protected:
    int      mInitialCapacity;
    bool    mOutputToConsole;
    bool    mOutputToFile;
    bool    mTimeStamping;
    std::string  mHeaderText;
    std::string mSystemHeader;
    int      mHeaderColor;
    int      mMessageColor;
    FILE    *mFileOut;
    FILE    *mOut;

    // sync(): synchronize the content of the buffer with the designated output
    virtual int sync()
    {
      // See basic_stringbuf::str() for similar code. However, in that code, I couldn't understand the use of egptr():
      // "The current egptr() may not be the actual string end."

      if (this->pptr())
      {
        int len = (int)(this->pptr() - this->pbase());
        if (len > 0)
        {
          // Null-terminate the string before outputting it (this is necessary)
          if (len > (int)_M_string.capacity() - 1)
            len = _M_string.capacity() - 1;
          // Writing in the 'unused' space of the buffer shouldn't do any harm
          // so we don't have to back it up
          this->pbase()[len] = '\0';

          // Call rt_printf and/or rt_fprintf to output the temp buffer, preceded by the header text
          if (mOutputToConsole)
          {  rt_fprintf(mOut,"\033[%dm", mHeaderColor);
            rt_fprintf(mOut,"%s", mHeaderText.c_str());
            // write time of sending if desired
            if (mTimeStamping)
              rt_fprintf(mOut,"[TS:%llu] ", rt_timer_read());
            rt_fprintf(mOut,"\033[%dm", mMessageColor);
            rt_fprintf(mOut,"%s", this->mSystemHeader.c_str());
            // Write main string in chunks
            int numChunks = 1 + (strlen(this->pbase()) / RTPRINTF_MAX_STRING_SIZE);
            for (int i=0; i<numChunks; i++)
              rt_fprintf(mOut,"%.*s", RTPRINTF_MAX_STRING_SIZE, this->pbase() + i*RTPRINTF_MAX_STRING_SIZE);
            rt_fprintf(mOut,"\033[0m");  // reset all attributes

          }
          if (mOutputToFile)
          {
            rt_fprintf(mFileOut, "%s", mHeaderText.c_str());
            rt_fprintf(mFileOut, "%s", this->mSystemHeader.c_str());
            // Write main string in chunks
            int numChunks = 1 + (strlen(this->pbase()) / RTPRINTF_MAX_STRING_SIZE);
            for (int i=0; i<numChunks; i++)
              rt_fprintf(mFileOut, "%.*s", RTPRINTF_MAX_STRING_SIZE, this->pbase() + i*RTPRINTF_MAX_STRING_SIZE);
          }

          // Adjust the write buffer pointers. This effectively empties the buffer.
          //setp(pbase(), epptr());
          _M_sync(const_cast<char_type*>(_M_string.data()), 0, 0);
        }
      }

      return std::stringbuf::sync();
    }

  public:
    CRtprintfStringbuf():
      mOutputToConsole(true),
      mOutputToFile(false),
      mTimeStamping(false),
      mHeaderColor(0),
      mMessageColor(0),
      mFileOut(NULL),
      mOut(stdout)
    {
      // Reserve a chunck of memory to avoid reallocation
      _M_string.reserve(RTPRINTBUF_BUFLEN);
      _M_sync(const_cast<char_type*>(_M_string.data()), 0, 0);
      mInitialCapacity = _M_string.capacity();  // For some reason, _M_string.capacity() is sometimes larger than what we asked for!
    }
    ~CRtprintfStringbuf()
    {
      // Force a last sync
      sync();
      // Force file closure
      enableFileOutput(false);
    }

    int   hasReallocated()                        {return _M_string.capacity() - mInitialCapacity;}
    void  enableConsoleOutput(bool bEnabled)      {mOutputToConsole = bEnabled; mOut = stdout;}
    void  redirectConsoleOutput(FILE* file)       {mOut = file;}

    // enableFileOutput returns the FILE pointer that it wants the logFactory to close upon destruction.
    FILE*  enableFileOutput(bool bEnabled, const std::string& filename="")
    {
      if (bEnabled)
      {
        // Round up old file if open
        if (mOutputToFile)
        {
          // First, flush the current content of the internal buffer to the old file
          sync();
          // We cannot call fclose(), because we cannot know how long rt_fprintf needs the open file.
        }
        // Overwrite file pointer
        mFileOut = fopen(filename.c_str(), "wt");
        // Check if file was successfully opened
        if (mFileOut == NULL)
        {
          rt_printf("[ERROR] Unable to enable file output for file %s!\n", filename.c_str());
          mOutputToFile  = false;
        }
        else
          mOutputToFile  = true;
      }
      else
      {
        // Stop file output
        if (mOutputToFile)
        {
          // Flush the content of the internal buffer to the file
          sync();
          // We cannot call fclose(), because we cannot know how long rt_fprintf needs the open file.
          mFileOut = NULL;
          mOutputToFile  = false;
        }
      }
      return mFileOut;
    }
    void  enableTimeStamping(bool enabled)              {mTimeStamping  = enabled;}
    void  setHeaderText(const std::string &headerText)  {mHeaderText  = headerText;}
    std::string& headerText()                           {return mHeaderText;}
    void  setHeaderColor(int headerColor)               {mHeaderColor  = headerColor;}
    void  setMessageColor(int messageColor)             {mMessageColor  = messageColor;}
    void  setSystemHeader(const std::string &text)      {mSystemHeader = text;}
};

class CRtLogStream : public CLogStream
{
  protected:
    CRtprintfStringbuf mBuffer;

  public:
    CRtLogStream(): CLogStream(&mBuffer) {
    }
    // hasReallocated() should return 0. If not, you put too much into the stream before flushing!
    int   hasReallocated()                             {return mBuffer.hasReallocated();}
    void  enableConsoleOutput(bool bEnabled)            {return mBuffer.enableConsoleOutput(bEnabled);}
    void  redirectConsoleOutput(FILE* file)             {mBuffer.redirectConsoleOutput(file);}
    FILE  *enableFileOutput(bool bEnabled, const std::string& filename="") {
      return mBuffer.enableFileOutput(bEnabled, filename);}
    void  enableTimeStamping(bool enabled)              {mBuffer.enableTimeStamping(enabled);}
    void  setHeaderText(const std::string &headerText)  {mBuffer.setHeaderText(headerText);}
    std::string& headerText()                           {return mBuffer.headerText();}
    void  setHeaderColor(int headerColor)               {mBuffer.setHeaderColor(headerColor);}
    void  setMessageColor(int messageColor)             {mBuffer.setMessageColor(messageColor);}
    void  setSystemHeader(const std::string &text)      {mBuffer.setSystemHeader(text); }
};

class CRtLog: public CLog
{
  protected:
    CRtLogStream  mCout;
    CRtLogStream  mCerr;
    CRtLogStream  mCcustom[LOG_MAX_CUSTOMLOGS];

  public:
    CLogStream&  cout()                     {return mCout;}
    CLogStream&  cerr()                     {return mCerr;}
    CLogStream&  ccustom(const int index)   {return mCcustom[index];}
};

#endif /* RTLOG_H_ */
