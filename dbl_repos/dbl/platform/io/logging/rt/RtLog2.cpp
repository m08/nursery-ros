/*
 * Realtime implementation of the log factory.
 * Wouter Caarls <w.caarls@tudelft.nl>
 */

#include <RtLog.h>
#include <Log2.h>



class CRtLog2Factory : public CLog2Factory
{
  public:
    CRtLog2Factory()
    {
        // Enable auto buffer initialization of realtime printing library.
        // If you want a buffer size (thread specific!) that is different from the default size,
        // call rt_print_init(desired_buffer_size, NULL) in the thread constructor of the thread
        // with special buffer needs.
        rt_print_auto_init(1);
    }

    virtual CLogStream &getLog(const std::string &name)
    {
      if (mLogs.find(name) == mLogs.end())
      {
        CRtLogStream *log = new CRtLogStream();
        log->setHeaderText(LOG2HDRDELIMLEFT + name + LOG2HDRDELIMRIGHT);
        log->setLevel(mLevel);
        log->enableTimeStamping(mTimeStamping);
        mLogs[name] = log;

        equalizeHeaderTexts();

        return *log;
      }
      else
        return *mLogs[name];
    }
};

// We use lazy initialization for the log factory
// Since static local objects are constructed the first time control flows over their declaration (only),
// the new logfactory will only be created once: the first time gLogFactory() is called.
CLog2Factory& gLogFactory()
{
  // WARNING: to avoid a static *de*initialization disaster, one should not use gLogFactory() in the destructor of static objects. However, this is unlikely. If so, initialize a static pointer with 'new' and return *logfact.
  static CRtLog2Factory logfact;
  return logfact;
}
