#
# CMake include file for realtime logging library
# Wouter Caarls <w.caarls@tudelft.nl>
#
# 16-04-2010 (wcaarls): Initial revision
#

INCLUDE (${WORKSPACE_DIR}/dbl/platform/io/logging/logging.cmake)

TARGET_LINK_LIBRARIES(${TARGET} rtlogging)

IF (NOT __RTLOGGING_CMAKE_INCLUDED)
  SET(__RTLOGGING_CMAKE_INCLUDED 1)

# ivan: workaround for CMP0002 cmake error - move to root CMakeLists.txt
#  ADD_SUBDIRECTORY(${WORKSPACE_DIR}/dbl/platform/io/logging/rt ${WORKSPACE_DIR}/build/rtlogging/${CMAKE_BUILD_TYPE}${COMPILER_VERSION})
ENDIF (NOT __RTLOGGING_CMAKE_INCLUDED)
