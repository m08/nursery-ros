/*
 * STGODESim.h
 *
 *  Created on: Apr 9, 2009
 *      Author: Erik Schuitema
 */

#ifndef STGODESIM_H_
#define STGODESIM_H_

#include "ODESim.h"
#include <STG.h>
#include <Configuration.h>
#include <sstream>

template<class STGStateType>	// STGStateType must be derived from CSTGState
class CSTGODESim: public CStateTransitionGenerator<STGStateType>
{
	protected:
		CODESim				mSim;	// If desired, template the type of sim (now CODESim)
	public:
		// CSTGODESim():
		// The STG name is extended with the process ID to create a unique name per sim instance.
		CSTGODESim(const std::string& name):
			CStateTransitionGenerator<STGStateType>(name)
		{
		}

		uint64_t		getAbsTime()
		{
			return (uint64_t)(mSim.getTime()*1E6); // Absolute time in microseconds
		}

		CODESim*		getSim()
		{
			return &mSim;
		}

		virtual bool	init()
		{
			return mSim.init();
		}

		virtual bool	deinit()
		{
			return mSim.deinit();
		}

		virtual bool	isInitialized()
		{
			return mSim.isInitialized();
		}

		// Set noObjects to true if you don't want to read just all objects from XML but instead
		// want to explicitly configurate pre-created objects.
		virtual bool	readConfig(const CConfigSection &configSection, bool noObjects=false)
		{
			return mSim.readConfig(configSection, noObjects);
		}
};

#endif /* STGODESIM_H_ */
