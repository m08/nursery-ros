/*
 * TestODE.h
 *
 *  Created on: Apr 8, 2009
 *      Author: Erik Schuitema
 */

#ifndef TESTODE_H_
#define TESTODE_H_

#include "ui_testode.h"
#include <ODESim.h>

class CODETestSim: public QObject, public CODESim
{
	Q_OBJECT

	protected:
		bool	mGraphicsEnabled;

	public:
		CODETestSim();
		void	onPostStep();
		void	enableGraphics(bool enabled)	{mGraphicsEnabled = enabled;}

	signals:
		void	singleStepDone();

};

class CTestODEApp : public QDialog, private Ui::DlgTestODE
{
    Q_OBJECT

	protected:
		CODETestSim*	mSim;
		void		closeEvent(QCloseEvent *event);

	public:
		CTestODEApp(QWidget *parent = 0);
		void		setSim(CODETestSim* sim);
		GLWidget*	getGLWidget();

	public slots:
		void		enableGraphics(bool enabled);
		void		enableShadows(bool enabled);
		void		enableTextures(bool enabled);

		void		startSim();
		void		pauseSim(bool enabled);
		void		stopSim();
		void		singleSimStep();

		void		startVideoRecording();
		void		stopVideoRecording();

		void		drawGLWidget();
		void		addLogItem(const QString& logItem);

};



#endif /* TESTODE_H_ */
