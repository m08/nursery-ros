/*
 * TestODEApp.cpp
 *
 *  Created on: Apr 8, 2009
 *      Author: Erik Schuitema
 */

// If we include <QtGui> there is no need to include every class used: <QString>, <QFileDialog>,...
#include <QtGui>
#include "TestODEApp.h"

//***************** CODETestSim - the simulator derived from CODESim ************//

CODETestSim::CODETestSim():
	mGraphicsEnabled(true)
{
}

void CODETestSim::onPostStep()
{
	if (mGraphicsEnabled)
	{
		emit singleStepDone();
		usleep(50);	// To give other threads some time as well
	}
}

//**************** CTestODEApp ******************//

CTestODEApp::CTestODEApp(QWidget *parent)
{
	setupUi(this); // this sets up GUI
	mGLWidget->init();	// Do not forget to initialize the GLWidget
	setWindowFlags(Qt::WindowMaximizeButtonHint);

	// signals/slots mechanism
	connect(mCloseButtonBox, SIGNAL(rejected()), this, SLOT(close()));
	connect(mCbxGraphics, SIGNAL(toggled(bool)), this, SLOT(enableGraphics(bool)));
	connect(mCbxShadows, SIGNAL(toggled(bool)), this, SLOT(enableShadows(bool)));
	connect(mCbxTextures, SIGNAL(toggled(bool)), this, SLOT(enableTextures(bool)));
	connect(mCbxPauseSim, SIGNAL(toggled(bool)), this, SLOT(pauseSim(bool)));

	connect(mBtnStartVideoRecording, SIGNAL(clicked()), this, SLOT(startVideoRecording()));
	connect(mBtnStopVideoRecording, SIGNAL(clicked()), this, SLOT(stopVideoRecording()));

	connect(mBtnStartSim, SIGNAL(clicked()), this, SLOT(startSim()));
	connect(mBtnStopSim, SIGNAL(clicked()), this, SLOT(stopSim()));
	connect(mBtnSingleStep, SIGNAL(clicked()), this, SLOT(singleSimStep()));
	addLogItem(
			"Welcome to the ODE simulation test!\n"
			"Change the viewpoint by clicking + dragging in the window:\n"
			"   Left button  : pan and tilt.\n"
			"   Right button : forward and sideways.\n"
			"   Left + right : sideways and up.\n"
			"Other features:"
			"   Ctrl-T : toggle textures.\n"
			"   Ctrl-S : toggle shadows.\n"
			"   Ctrl+M : Switch between standard-view mode and center-view mode."
			);
}

GLWidget* CTestODEApp::getGLWidget()
{
	return mGLWidget;
}

void CTestODEApp::setSim(CODETestSim* sim)
{
	mSim = sim;
	mGLWidget->setSim(mSim);
    connect(mSim, SIGNAL(singleStepDone()), this, SLOT(drawGLWidget()));
}

void CTestODEApp::drawGLWidget()
{
	mGLWidget->update();
}

void CTestODEApp::closeEvent(QCloseEvent *event)
{
	// Do something before closing
	stopSim();
	event->accept();
}

void CTestODEApp::enableGraphics(bool enabled)
{
	mSim->enableGraphics(enabled);
	mGLWidget->enableGraphics(enabled);
	mGLWidget->update();
}

void CTestODEApp::enableShadows(bool enabled)
{
	mGLWidget->dsSetShadows(enabled?1:0);
	mGLWidget->update();
}

void CTestODEApp::enableTextures(bool enabled)
{
	mGLWidget->dsSetTextures(enabled?1:0);
	mGLWidget->update();
}

void CTestODEApp::startSim()
{
	if (mSim->start())
	{
		mBtnStartSim->setEnabled(false);
		mBtnSingleStep->setEnabled(true);
		mBtnStopSim->setEnabled(true);
		addLogItem("Simulation started");
	}
	else
		addLogItem("Could not start simulation!");
}

void CTestODEApp::stopSim()
{
	if (mSim->stop())
	{
		mBtnStartSim->setEnabled(true);
		mBtnSingleStep->setEnabled(false);
		mBtnStopSim->setEnabled(false);
		addLogItem("Simulation stopped");
	}
	else
		addLogItem("Could not stop simulation!");
}

void CTestODEApp::pauseSim(bool enabled)
{
	mSim->pause(enabled);
}

void CTestODEApp::addLogItem(const QString& logItem)
{
	mLbxLog->insertItem(0, logItem);
}

void CTestODEApp::singleSimStep()
{
	//for (int i=0; i<100; i++)
		mSim->singleStep();
	//printf("Sim time: %.2f\n", mSim->getTime());
	QString textLine;
	textLine.sprintf("Single step done. Sim time: %.5f", mSim->getTime());
	addLogItem(textLine);
}

void CTestODEApp::startVideoRecording()
{
	QString fileName = mGLWidget->selectVideoFileAndFormat();
	if(!fileName.isNull())
	{
		int fps					= 30;
		int bitrate				= 1000000;
		int keyframeInterval	= 20;
		bool multithreaded		= false;
		if (mGLWidget->startVideoEncoding(fileName, bitrate, keyframeInterval, fps, multithreaded))
		{
			// Enable the right buttons
			mBtnStartVideoRecording->setEnabled(false);
			mBtnStopVideoRecording->setEnabled(true);

		}
		else
		{
			printf("Error: Could not start recording video!\n");
			addLogItem("Error: Could not start recording video!");
		}
	}
}

void CTestODEApp::stopVideoRecording()
{
	mGLWidget->stopVideoEncoding();

	// Enable the right buttons
	mBtnStartVideoRecording->setEnabled(true);
	mBtnStopVideoRecording->setEnabled(false);
}
