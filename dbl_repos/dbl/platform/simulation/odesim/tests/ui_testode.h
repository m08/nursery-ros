/********************************************************************************
** Form generated from reading UI file 'testode.ui'
**
** Created: Thu Apr 7 14:29:26 2011
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTODE_H
#define UI_TESTODE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <SimVisWidget.h>

QT_BEGIN_NAMESPACE

class Ui_DlgTestODE
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_4;
    QPushButton *mBtnStartSim;
    QSpacerItem *verticalSpacer;
    QPushButton *mBtnStopSim;
    QPushButton *mBtnSingleStep;
    QCheckBox *mCbxPauseSim;
    CSimVisWidget *mGLWidget;
    QGridLayout *gridLayout_5;
    QDialogButtonBox *mCloseButtonBox;
    QCheckBox *mCbxGraphics;
    QSpacerItem *verticalSpacer_2;
    QCheckBox *mCbxShadows;
    QCheckBox *mCbxTextures;
    QPushButton *mBtnStopVideoRecording;
    QPushButton *mBtnStartVideoRecording;
    QListWidget *mLbxLog;

    void setupUi(QDialog *DlgTestODE)
    {
        if (DlgTestODE->objectName().isEmpty())
            DlgTestODE->setObjectName(QString::fromUtf8("DlgTestODE"));
        DlgTestODE->resize(519, 493);
        gridLayout_2 = new QGridLayout(DlgTestODE);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        mBtnStartSim = new QPushButton(DlgTestODE);
        mBtnStartSim->setObjectName(QString::fromUtf8("mBtnStartSim"));
        mBtnStartSim->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mBtnStartSim->sizePolicy().hasHeightForWidth());
        mBtnStartSim->setSizePolicy(sizePolicy);

        gridLayout_4->addWidget(mBtnStartSim, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 5, 0, 1, 1);

        mBtnStopSim = new QPushButton(DlgTestODE);
        mBtnStopSim->setObjectName(QString::fromUtf8("mBtnStopSim"));
        mBtnStopSim->setEnabled(false);
        sizePolicy.setHeightForWidth(mBtnStopSim->sizePolicy().hasHeightForWidth());
        mBtnStopSim->setSizePolicy(sizePolicy);

        gridLayout_4->addWidget(mBtnStopSim, 3, 0, 1, 1);

        mBtnSingleStep = new QPushButton(DlgTestODE);
        mBtnSingleStep->setObjectName(QString::fromUtf8("mBtnSingleStep"));
        mBtnSingleStep->setEnabled(false);

        gridLayout_4->addWidget(mBtnSingleStep, 2, 0, 1, 1);

        mCbxPauseSim = new QCheckBox(DlgTestODE);
        mCbxPauseSim->setObjectName(QString::fromUtf8("mCbxPauseSim"));
        mCbxPauseSim->setChecked(true);

        gridLayout_4->addWidget(mCbxPauseSim, 1, 0, 1, 1);


        gridLayout->addLayout(gridLayout_4, 0, 1, 1, 1);

        mGLWidget = new CSimVisWidget(DlgTestODE);
        mGLWidget->setObjectName(QString::fromUtf8("mGLWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(mGLWidget->sizePolicy().hasHeightForWidth());
        mGLWidget->setSizePolicy(sizePolicy1);
        mGLWidget->setMinimumSize(QSize(400, 300));

        gridLayout->addWidget(mGLWidget, 0, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        mCloseButtonBox = new QDialogButtonBox(DlgTestODE);
        mCloseButtonBox->setObjectName(QString::fromUtf8("mCloseButtonBox"));
        sizePolicy.setHeightForWidth(mCloseButtonBox->sizePolicy().hasHeightForWidth());
        mCloseButtonBox->setSizePolicy(sizePolicy);
        mCloseButtonBox->setStandardButtons(QDialogButtonBox::Close);

        gridLayout_5->addWidget(mCloseButtonBox, 6, 0, 1, 1);

        mCbxGraphics = new QCheckBox(DlgTestODE);
        mCbxGraphics->setObjectName(QString::fromUtf8("mCbxGraphics"));
        mCbxGraphics->setChecked(true);

        gridLayout_5->addWidget(mCbxGraphics, 0, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_5->addItem(verticalSpacer_2, 3, 0, 1, 1);

        mCbxShadows = new QCheckBox(DlgTestODE);
        mCbxShadows->setObjectName(QString::fromUtf8("mCbxShadows"));
        mCbxShadows->setChecked(true);

        gridLayout_5->addWidget(mCbxShadows, 2, 0, 1, 1);

        mCbxTextures = new QCheckBox(DlgTestODE);
        mCbxTextures->setObjectName(QString::fromUtf8("mCbxTextures"));
        mCbxTextures->setChecked(true);

        gridLayout_5->addWidget(mCbxTextures, 1, 0, 1, 1);

        mBtnStopVideoRecording = new QPushButton(DlgTestODE);
        mBtnStopVideoRecording->setObjectName(QString::fromUtf8("mBtnStopVideoRecording"));

        gridLayout_5->addWidget(mBtnStopVideoRecording, 5, 0, 1, 1);

        mBtnStartVideoRecording = new QPushButton(DlgTestODE);
        mBtnStartVideoRecording->setObjectName(QString::fromUtf8("mBtnStartVideoRecording"));

        gridLayout_5->addWidget(mBtnStartVideoRecording, 4, 0, 1, 1);


        gridLayout->addLayout(gridLayout_5, 1, 1, 1, 1);

        mLbxLog = new QListWidget(DlgTestODE);
        mLbxLog->setObjectName(QString::fromUtf8("mLbxLog"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(mLbxLog->sizePolicy().hasHeightForWidth());
        mLbxLog->setSizePolicy(sizePolicy2);
        mLbxLog->setMaximumSize(QSize(16777215, 170));

        gridLayout->addWidget(mLbxLog, 1, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(DlgTestODE);

        QMetaObject::connectSlotsByName(DlgTestODE);
    } // setupUi

    void retranslateUi(QDialog *DlgTestODE)
    {
        DlgTestODE->setWindowTitle(QApplication::translate("DlgTestODE", "TestODE", 0, QApplication::UnicodeUTF8));
        mBtnStartSim->setText(QApplication::translate("DlgTestODE", "Start", 0, QApplication::UnicodeUTF8));
        mBtnStopSim->setText(QApplication::translate("DlgTestODE", "Stop", 0, QApplication::UnicodeUTF8));
        mBtnSingleStep->setText(QApplication::translate("DlgTestODE", "Single step", 0, QApplication::UnicodeUTF8));
        mCbxPauseSim->setText(QApplication::translate("DlgTestODE", "Pause", 0, QApplication::UnicodeUTF8));
        mCbxGraphics->setText(QApplication::translate("DlgTestODE", "Graphics", 0, QApplication::UnicodeUTF8));
        mCbxShadows->setText(QApplication::translate("DlgTestODE", "Shadows", 0, QApplication::UnicodeUTF8));
        mCbxTextures->setText(QApplication::translate("DlgTestODE", "Textures", 0, QApplication::UnicodeUTF8));
        mBtnStopVideoRecording->setText(QApplication::translate("DlgTestODE", "Stop video rec", 0, QApplication::UnicodeUTF8));
        mBtnStartVideoRecording->setText(QApplication::translate("DlgTestODE", "Start video rec", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DlgTestODE: public Ui_DlgTestODE {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTODE_H
