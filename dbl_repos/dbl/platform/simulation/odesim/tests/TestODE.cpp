/*
 * LeoSim.cpp
 *
 *  Created on: Apr 8, 2009
 *      Author: Erik Schuitema
 */

#include "TestODEApp.h"
//#include <simulation/ODESim.h>
#include <XMLConfiguration.h>

int main(int argc, char** argv)
{
	// Read xml config
	std::string xmlFilename;	//"/home/erik/workspace/simulation/odesim/tests/testode.xml";
	if (argc > 1)
		xmlFilename = argv[1];
	else
	{
		printf("Please provide an XML configuration file as the first parameter, e.g.:\n\tTestODE ../testode.xml\n\n");
		return -1;
	}

	CXMLConfiguration	xmlConfig;
	if (!xmlConfig.loadFile(xmlFilename))
	{
		printf("Couldn't load XML configuration file \"%s\"!\nPlease check that the file exists and that it is sound (error: %s).\n", xmlFilename.c_str(), xmlConfig.errorStr().c_str());
		return -1;
	}
	else
	{
		int numResolvedExpressions = xmlConfig.resolveExpressions();
		printf("XML configuration file \"%s\" successfully read. Resolved %d expressions.\n", xmlFilename.c_str(), numResolvedExpressions);
	}

    QApplication app(argc, argv);
	CTestODEApp *dialog = new CTestODEApp;
	CODETestSim sim;//sim(dialog->getGLWidget());

	// Feed xml config to leoSim
	if (!sim.readConfig(xmlConfig.root().section("ode")))
	{
		printf("[ERROR] Loading leoSim XML configuration failed!\n");
	}

	// Init sim
	if (!sim.init())
	{
		printf("[ERROR] Could not init LeoSim! Aborting!\n");
		return -1;
	}

	dialog->setSim(&sim);
	dialog->show();
    // Run app
	int appResult = app.exec();
    // Deinit sim
	sim.deinit();
	return appResult;
}
