/*
 * ODEDebug.h
 *
 *  Created on: Apr 14, 2010
 *      Author: Erik Schuitema
 *
 *
 *	==== DEPRECATED, PLEASE USE CLog2 FOR EASY LOGGING ====
 *
 */

#ifndef ODEDEBUG_H_
#define ODEDEBUG_H_

//#define __DEBUG__

#ifdef __DEBUG__
//TODO: If this does not work for your platform, please consult the following URL:
// http://www.redhat.com/docs/manuals/enterprise/RHEL-4-Manual/gcc/variadic-macros.html
#define dbgprintf(format, ...) fprintf (stderr, format, ## __VA_ARGS__)
#else
#define dbgprintf(format, ...) do {} while(0)
#endif


#endif /* ODEDEBUG_H_ */
