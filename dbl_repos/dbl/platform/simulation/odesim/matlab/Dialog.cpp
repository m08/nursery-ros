/*
 * Dialog.cpp
 *
 *  Created on: Jan 11, 2010
 *      Author: wcaarls
 */

#include <QtGui>

#include "Dialog.h"

Dialog::Dialog(Simulator *sim) :
  mSim(sim)
{
  mLogDebugLn("Dialog constructor");

  setupUi(this); // this sets up GUI
  mGLWidget->enableTimeDisplay(false);
  mGLWidget->init();  // Do not forget to initialize the GLWidget
  setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

  // Set sim
  mGLWidget->setSim(sim->getSim());
  mGLWidget->dsSetShadows(0);

  // signals/slots mechanism
  connect(sim, SIGNAL(drawFrame()), this, SLOT(drawGLWidget()));
  connect(mCbxEnableGraphics, SIGNAL(toggled(bool)), this, SLOT(enableGraphics(bool)));
}

Dialog::~Dialog(void)
{
  mLogDebugLn("Dialog destructor");
}

void Dialog::drawGLWidget()
{
  char timeStr[20];
  sprintf(timeStr, "Time: %.4f", mSim->getSim()->getTime());
  mStatusBar->showMessage(timeStr);

  mGLWidget->update();
}

void Dialog::enableGraphics(bool enabled)
{
  mGLWidget->enableGraphics(enabled);
  mGLWidget->update();
}
