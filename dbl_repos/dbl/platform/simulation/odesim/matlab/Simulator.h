/*
 * Simulator.h
 *
 *  Created on: Jan 11, 2010
 *      Author: wcaarls
 */

#ifndef SIMULATOR_H_
#define SIMULATOR_H_

#ifdef WITH_GUI
#include <QtCore/qobject.h>
#endif
#include <GenericODESim.h>

#include "MatlabODE.h"

class Simulator:
#ifdef WITH_GUI
public QObject,
#endif
public CGenericODESim, private PosixNonRealTimeThread, protected SimulationLog
{
#ifdef WITH_GUI
    Q_OBJECT
#endif

  protected:
    uint64_t    mStepCounter;
    CWaitEvent  mEvtActuation;

    //void      resetBindData();
    // Main function from AbstractXenomaiTask
    // that has to be overridden in order to
    // implement actual task functionality.
    void      run();
    bool      shouldStep(); // Waits for an actuation signal and returns true when a new step should be made
    void      fillState();

  public:
    Simulator();

    bool      start();
    bool      stop();

    virtual void setInitialCondition(rg_uint32 seed=0);

    virtual bool readConfig(const CConfigSection &configSection, bool noObjects=false);

    // Call activateActions() at a moment of your choosing to put the new control actions into effect
    virtual int activateActions(const uint64_t& stateID);
#ifdef WITH_GUI
  signals:
    void      drawFrame();  // signal to emit to the GUI to initiate drawing of the screen
#endif 
};

#endif /* SIMULATOR_H_ */
