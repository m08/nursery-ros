/*
 * Dialog.h
 *
 *  Created on: Jan 11, 2010
 *      Author: wcaarls
 */

#ifndef DIALOG_H_
#define DIALOG_H_

#include <QtGui/QDialog>

#include "MatlabODE.h"
#include "Simulator.h"
#include "DialogUI.h"

class Dialog : public QMainWindow, public Ui::Dialog, protected MainLog
{
  Q_OBJECT

protected:
  Simulator *mSim;

public:
  Dialog(Simulator *sim);
  ~Dialog();

public slots:
  void enableGraphics(bool);
  void drawGLWidget();
};

#endif /* DIALOG_H_ */
