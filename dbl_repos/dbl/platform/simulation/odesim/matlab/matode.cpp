#include <stdio.h>
#include <mex.h>

// From MatlabODE.cpp
void __mexFunction(int nlhs, mxArray *plhs[ ], int nrhs, const mxArray *prhs[ ]);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  __mexFunction(nlhs, plhs, nrhs, prhs);
}
