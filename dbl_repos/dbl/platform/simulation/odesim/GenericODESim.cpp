/*
 * GenericODESim.cpp
 * Version of CSTGODESim that uses the GenericState interface.
 *
 * Wouter Caarls <w.caarls@tudelft.nl>
 *
 * Known sensors:
 *   <object>.<joint>.<jointsensor>
 *   <object>.<body>.<bodysensor>.<axis>
 *   <object>.<body>@<object2>.<body2>.<axis|polar>
 *
 * Known actuators:
 *   <object>.<joint>.torque (for motor type 'torque')
 *   <object>.<joint>.force (for motor type 'force')
 *   <object>.<joint>.voltage (for motor type 'servomotor')
 *
 * Where:
 *   <jointsensor> := "angle" | "anglerate"
 *   <bodysensor>  := "position" | "velocity"
 *   <axis>        := "x" | "y" | "z"
 *   <polar>       := "azimuth" | "elevation" | "distance"
 */

#include <GenericODESim.h>

using std::cout;
using std::endl;

void ODEMessageFunction(int errnum, const char *msg, va_list ap)
{
  char buf[1024];
  vsnprintf(buf, 1024, msg, ap);

  if (errnum)
    logErrorLn(CLog2("ode"), "Error " << errnum << ": " << buf);
  else
    logErrorLn(CLog2("ode"), buf);

  throw CODEException(buf);
}

bool CGenericODESim::init()
{
  dSetErrorHandler(ODEMessageFunction);
  dSetDebugHandler(ODEMessageFunction);
  dSetMessageHandler(ODEMessageFunction);

  mSensors.clear();
  mActuators.clear();
  return CSTGODESim<GenericState>::init();
}

void CGenericODESim::fillState()
{
  for (unsigned int ii=0; ii != mSensors.size(); ++ii)
  {
    mLogCrawlLn("state[" << mStateStartIndex+ii << "] = " << mSensors[ii]->getValue());
    mState.var[mStateStartIndex+ii] = mSensors[ii]->getValue();
  }
  for (unsigned int ii=0; ii != mActuators.size(); ++ii)
  {
    mState.prevAction[mActionStartIndex+ii] = mActuators[ii]->getAction();
  }
}

int CGenericODESim::activateActions(const uint64_t& stateID)
{
  for (unsigned int ii=0; ii != mActuators.size(); ++ii)
    mActuators[ii]->activate();
  return 0;	// Success
}

void CGenericODESim::resetActuationValues()
{
  for (unsigned int ii=0; ii != mActuators.size(); ++ii)
  {
    mActuators[ii]->setValue(0);
    mActuators[ii]->setAction(0);
  }
}

void CGenericODESim::setJointValue(int idx, double value)
{
  if (idx >= mActionStartIndex && idx < ((int)mActuators.size())-mActionStartIndex)
  {
    mLogCrawlLn("action[" << idx << "] = " << value);
    mActuators[idx-mActionStartIndex]->setValue(value);
  }
  else
    mLogErrorLn("Action index out of bounds");
}

void CGenericODESim::setJointTorque(int idx, double value)
{
	setJointValue(idx, value);
}

void CGenericODESim::setJointForce(int idx, double value)
{
	setJointValue(idx, value);
}

void CGenericODESim::setJointVoltage(int idx, double value)
{
	setJointValue(idx, value);
}

void CGenericODESim::setJointAction(int idx, double value)
{
  if (idx >= mActionStartIndex && idx < ((int)mActuators.size())-mActionStartIndex)
  {
    mActuators[idx-mActionStartIndex]->setAction(value);
  }
  else
    mLogErrorLn("Action index out of bounds");
}

int CGenericODESim::getActionIndex(const std::string &name)
{
  // Check previously defined actuators
  for (unsigned int ii=0; ii != mActuators.size(); ++ii)
    if (mActuators[ii]->getName() == name)
      return mActionStartIndex + ii;

  // Acquire sim access
  CODESimAccess simAccess(&mSim);

  std::string temp = name;

  std::string objectName = temp.substr(0, temp.find('.'));
  temp = temp.substr(temp.find('.')+1, temp.npos);
  std::string jointName  = temp.substr(0, temp.find('.'));
  temp = temp.substr(temp.find('.')+1, temp.npos);
  std::string motorName  = temp.substr(0, temp.find('.'));
  if (temp.find('.') != temp.npos)
    temp = temp.substr(temp.find('.')+1, temp.npos);
  else
    temp = "";
  std::string modeName = temp;

  if (modeName == "")
  {
    modeName = motorName;
    motorName = "";
  }

  CODEObject *object = simAccess.resolveObject(objectName);
  if (!object)
  {
    mLogErrorLn("Couldn't find object " << objectName << " while resolving action " << name);
    return -1;
  }

  CODEJoint  *joint  = object->resolveJoint(jointName);
  if (!joint)
  {
    mLogErrorLn("Couldn't find joint " << jointName << " of object " << objectName << " while resolving action " << name);
    return -1;
  }

  int motorAxis = 0;

  if (motorName == "axis2")
    motorAxis = 1;
  else if (motorName == "axis3")
    motorAxis = 2;

  CGenericJointActuator *actuator =
      new CGenericJointActuator(name, joint->getMotor(motorAxis), getActuationModeByName(modeName));
  mActuators.push_back(actuator);

  return mActionStartIndex + mActuators.size()-1;
}

int CGenericODESim::getStateIndex(const std::string &name)
{
  // Check previously defined sensors
  for (unsigned int ii=0; ii != mSensors.size(); ++ii)
    if (mSensors[ii]->getName() == name)
      return mStateStartIndex + ii;

  // Acquire sim access
  CODESimAccess simAccess(&mSim);

  std::string temp = name;

  std::string objectName = temp.substr(0, temp.find('.'));
  temp = temp.substr(temp.find('.')+1, temp.npos);
  std::string memberName  = temp.substr(0, temp.find('.'));
  temp = temp.substr(temp.find('.')+1, temp.npos);

  CODEObject *object = simAccess.resolveObject(objectName);
  if (!object)
  {
    mLogErrorLn("Couldn't find object " << objectName << " while resolving state " << name);
    return -1;
  }

  if (temp.find('@') != temp.npos)
  {
    // Relative position
    std::string axisName  = temp.substr(0, temp.find('@'));
    temp = temp.substr(temp.find('@')+1, temp.npos);
    std::string relObjectName  = temp.substr(0, temp.find('.'));
    temp = temp.substr(temp.find('.')+1, temp.npos);
    std::string relBodyName  = temp;

    CODEBody *body = object->resolveBody(memberName);
    if (!body)
    {
      mLogErrorLn("Couldn't find body " << objectName << "." << memberName << " while resolving state " << name);
      return -1;
    }

    CODEObject *relObject = simAccess.resolveObject(relObjectName);
    if (!relObject)
    {
      mLogErrorLn("Couldn't find object " << relObjectName << " while resolving state " << name);
      return -1;
    }

    CODEBody *relBody = relObject->resolveBody(relBodyName);
    if (!relBody)
    {
      mLogErrorLn("Couldn't find body " << relObjectName << "." << relBodyName << " while resolving state " << name);
      return -1;
    }

    CGenericDistanceSensor *sensor = new CGenericDistanceSensor(name, body, relBody, axisName);
    mSensors.push_back(sensor);
    return mStateStartIndex + mSensors.size()-1;
  }
  else
  {
    std::string subMemberName  = temp.substr(0, temp.find('.'));
    if (temp.find('.') != temp.npos)
      temp = temp.substr(temp.find('.')+1, temp.npos);
    else
      temp = "";
    std::string subSubMemberName = temp;

    CODEObject *object = simAccess.resolveObject(objectName);
    if (!object)
    {
      mLogErrorLn("Couldn't find object " << objectName << " while resolving state " << name);
      return -1;
    }

    CODEJoint *joint = object->resolveJoint(memberName);
    if (joint)
    {
      // Joint position
      CGenericJointSensor *sensor = new CGenericJointSensor(name, joint, subMemberName);
      mSensors.push_back(sensor);
      return mStateStartIndex + mSensors.size()-1;
    }

    CODEBody *body = object->resolveBody(memberName);
    if (body)
    {
      // Body position
      if (subSubMemberName == "")
      {
        subSubMemberName = subMemberName;
        subMemberName = "position";
      }

      CGenericBodySensor *sensor = new CGenericBodySensor(name, body, subMemberName, subSubMemberName);
      mSensors.push_back(sensor);
      return mStateStartIndex + mSensors.size()-1;
    }

    mLogErrorLn("Couldn't find member " << memberName << " of object " << objectName << " while resolving state " << name);
    return -1;
  }
}
