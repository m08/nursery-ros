/*
 * ODELogging.h
 *
 *  Created on: Apr 19, 2010
 *      Author: Erik Schuitema
 */

#ifndef ODELOGGING_H_
#define ODELOGGING_H_

#include <Log2.h>

class CODELoggable
{
	protected:
		CLog2	mLog;
	public:
		CODELoggable():	mLog("ode")	{}
};

#endif /* ODELOGGING_H_ */
