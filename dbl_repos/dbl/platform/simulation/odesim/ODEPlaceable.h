/*
 * ODEPlaceable.h
 *
 *  Created on: Jun 11, 2009
 *      Author: Erik Schuitema
 */

#ifndef ODEPLACEABLE_H_
#define ODEPLACEABLE_H_

// Base class for placeable objects
class CODEPlaceable
{
	public:
		virtual ~CODEPlaceable() { }
	
		// Get the position of this object. If you want (like for drawing objects), provide an additional displacement (dx, dy, dz) within the body's reference frame
		//template<class floattype>
		//virtual void	getPosition(floattype *pos, double dx=0, double dy=0, double dz=0)	{}
		virtual void	getPosition(double *pos, double dx=0, double dy=0, double dz=0)=0;
		void			getPositionF(float *pos, double dx=0, double dy=0, double dz=0)
		{
			double dPos[3];
			getPosition(dPos, dx, dy, dz);
			for (int i=0; i<3; i++)
				pos[i] = (float)dPos[i];
		}
		// Get the rotation matrix of this object.
		//template<class floattype>
		//virtual void	getRotation(floattype *rot)	{}
		virtual void	getRotation(double *rot)=0;
		void			getRotationF(float *rot)
		{
			double dRot[12];
			getRotation(dRot);
			for (int i=0; i<12; i++)
				rot[i] = (float)dRot[i];
		}
};

#endif /* ODEPLACEABLE_H_ */
