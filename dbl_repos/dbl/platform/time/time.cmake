#
# CMake include file for time library
# Wouter Caarls <w.caarls@tudelft.nl>
#
# 29-03-2010 (wcaarls): Initial revision
#

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/platform/time)

IF (NOT __TIME_CMAKE_INCLUDED)
  SET(__TIME_CMAKE_INCLUDED 1)

ENDIF (NOT __TIME_CMAKE_INCLUDED)
