/*
 * Neural-network agent, to be used with STGPolicyPlayerNN
 *
 * Wouter Caarls <w.caarls.@tudelft.nl>
 */

#ifndef __STGAGENTNN_H_INCLUDED
#define __STGAGENTNN_H_INCLUDED

#include <Log2.h>
#include <randomc.h>
#include <STGPolicy.h>
#include <GenericState.h>
#include <EA.h>
#include <GenericLearner.h>

#ifdef WIN32
#include <win32_compat.h>
#endif

enum EActivationFunction {afLinear, afSigmoid, afTanH};

class ANN : public ISegmentedGenotype<ANN, double>
{
  friend std::ostream& operator<<(std::ostream &os, const ANN &obj);

  protected:
    std::vector<double> mWeights, mInputs, mHiddens, mOutputs;
    bool mBias;
    EActivationFunction mActivationFunction;
    double mActivationSteepness;

  public:

    double RandomNormal() const;
    double Activate(double x) const;
    double Normalize(double x) const;
    double InvNormalize(double x) const;

  public:
    ANN() : mActivationFunction(afSigmoid), mActivationSteepness(1)
    { }

    ANN(int inputs, int hiddens, int outputs, bool bias) :
      mActivationFunction(afSigmoid), mActivationSteepness(1)
    {
      setSize(inputs, hiddens, outputs, bias);
    }
    ANN &operator=(const ANN &rhs);
    virtual ~ANN() { }

    virtual void init();
    virtual bool mutate(double prob);
    virtual void crossover(const ANN &rhs, ANN &res) const;
    void run();
    void backPropagate(std::vector<double> &desired);

    void setSize(int inputs, int hiddens, int outputs, bool bias=true);
    void setActivationFunction(EActivationFunction act);
    EActivationFunction getActivationFunction();
    void setActivationSteepness(double steepness);
    double getActivationSteepness();
    void setInputs(std::vector<double> &inputs);
    void setInput(int idx, double input);
    double getInput(int idx);
    std::vector<double> getOutputs();
    double getOutput(int idx);

    bool exportPhenotype(std::ofstream& fileOutStream);

    int getNumGenes();
    void setGenes(const std::vector<double> &values);
    std::vector<double> getGenes();
};

class CSTGAgentNN : public CSTGPolicy<GenericState>, public CGenericLearner
{
  protected:
    std::string mActivationFunction;
    int mHiddenNodes, mActionNodes;
    bool mBias;
    double mReward;
    ANN mAnn;
    std::vector<double> mActions;

  public:
    CSTGAgentNN(ISTGGenericSensing *sensingInterface, ISTGGenericActuation *actuationInterface, int stateStartIndex=0, int actionStartIndex=0) :
      CSTGPolicy<GenericState>(actuationInterface),
      CGenericLearner(sensingInterface, actuationInterface, stateStartIndex, actionStartIndex),
      mActivationFunction("sigmoid"), mHiddenNodes(-1), mActionNodes(0), mBias(true)
    {
    }

    virtual bool readConfig(const CConfigSection &configNode);
    virtual bool init();
    virtual int executePolicy(GenericState* currentState, uint64_t absoluteTimeMicroseconds);
    virtual void reset(uint64_t absoluteTimeMicroseconds);
    virtual double calculateReward();
    virtual void updateState(GenericState* currentSTGState);
    virtual void updateAction(ISTGActuation* actuationInterface);

    ANN& getController();
    void setController(const ANN &ann);
};

#endif /* __STGAGENTNN_H_INCLUDED */
