/**
 * @file common/math/VectorAlgorithm.h
 * Definition of class common::math::VectorAlgorithm.
 */

#ifndef __common_math_VectorAlgorithm_h_
#define __common_math_VectorAlgorithm_h_

#include "MathFunctions.h"

#include <assert.h>
#include <sstream>
#include <string>

namespace common {
namespace math {

/**
 * VectorAlgorithm is a class for implementing algorithms for vector-operations.
 */
class VectorAlgorithm
{

protected:

  /**
   * Default constructor.
   */
  VectorAlgorithm()
  {}

public:

  /**
   * @name Compare functions.
   * @{
   */

  /**
   * Compares two vectors for equality.
   *
   * @param first       The first vector
   * @param second      The second vector
   * @param sizeFirst   The number of entries of the first matrix
   * @param sizeSecond  The number of entries of the second matrix
   */
  template<typename First, typename Second>
  static bool equal(const First& first, const Second& second, unsigned int sizeFirst, unsigned int sizeSecond)
  {
    if (sizeFirst != sizeSecond) {
      return false;
    }
    return VectorAlgorithm::equal(first, second, sizeFirst);
  }

  /**
   * Compares two vectors with same dimensions for equality.
   *
   * @param first   The first matrix
   * @param second  The second matrix
   * @param size    The number of entries
   */
  template<typename First, typename Second>
  static bool equal(const First& first, const Second& second, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      if (first(index) != second(index)) {
        return false;
      }
    }
    return true;
  }

  /**
   * @}
   */

  /**
   * @name String functions.
   * @{
   */

  /**
   * Gets the string-representation of a vector.
   *
   * @param vector  The vector
   * @param size    The number of entries
   */
  template<typename T, typename Vector>
  static std::string toString(const Vector& vector, unsigned int size)
  {
    std::stringstream strStream;
    strStream << "[";
    for (unsigned int index = 0; index < size; index++) {
      if (index != 0) {
        strStream << "; ";
      }
      strStream << vector(index);
    }
    strStream << "]";
    return strStream.str();
  }

  /**
   * @}
   */

  /**
   * @name Assignment functions.
   * @{
   */

  /**
   * Assigns a vector.
   *
   * @param source  The source vector
   * @param target  The target vector
   * @param size    The number of entries
   */
  template<typename Source, typename Target>
  static void assign(const Source& source, Target& target, unsigned int size)
  {
    assert(source.getSize() == target.getSize());
    for (unsigned int index = 0; index < size; index++) {
      target(index) = source(index);
    }
  }

  /**
   * Sets all entries of the vector to zero.
   *
   * @param vector  The vector
   * @param size    The number of entries
   */
  template<typename Vector>
  static void setZero(Vector& vector, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      vector(index) = 0;
    }
  }

  /**
   * @}
   */

  /**
   * @name Addition functions.
   * @{
   */

  /**
   * Adds two vectors without modifing the summands.
   *
   * @param summand1  The first summand vector
   * @param summand2  The second summand vector
   * @param sum       The sum vector
   * @param size      The number of entries
   */
  template<typename Summand1, typename Summand2, typename Sum>
  static void vectorAddition(const Summand1& summand1, const Summand2& summand2, Sum& sum, unsigned int size)
  {
    assert(summand1.getSize() == summand2.getSize() && summand2.getSize() == sum.getSize());
    for (unsigned int index = 0; index < size; index++) {
      sum(index) = summand1(index) + summand2(index);
    }
  }

  /**
   * Adds a scalar to the vector without modifing the summand.
   *
   * @param summand1  The summand vector
   * @param summand2  The scalar value
   * @param sum       The sum vector
   * @param size      The number of entries
   */
  template<typename Summand1, typename Summand2, typename Sum>
  static void scalarAddition(const Summand1& summand1, const Summand2& summand2, Sum& sum, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      sum(index) = summand1(index) + summand2;
    }
  }

  /**
   * Adds two vectors.
   *
   * @param summand1  The first summand vector
   * @param summand2  The second summand vector
   * @param size      The number of entries
   */
  template<typename Summand1, typename Summand2>
  static void vectorAddition(Summand1& summand1, const Summand2& summand2, unsigned int size)
  {
    assert(summand1.getSize() == summand2.getSize());
    for (unsigned int index = 0; index < size; index++) {
      summand1(index) += summand2(index);
    }
  }

  /**
   * Adds a scalar to the vector.
   *
   * @param summand1  The summand vector
   * @param summand2  The scalar value
   * @param size      The number of entries
   */
  template<typename Summand1, typename Summand2>
  static void scalarAddition(Summand1& summand1, const Summand2& summand2, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      summand1(index) += summand2;
    }
  }

  /**
   * @}
   */

  /**
   * @name Subtraction functions.
   * @{
   */

  /**
   * Subtracts two vectors without modifing the minuend and subtrahend.
   *
   * @param minuend     The minuend vector
   * @param subtrahend  The subtrahend vector
   * @param difference  The difference vector
   * @param size        The number of entries
   */
  template<typename Minuend, typename Subtrahend, typename Difference>
  static void vectorSubtraction(const Minuend& minuend, const Subtrahend& subtrahend, Difference& difference, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      difference(index) = minuend(index) - subtrahend(index);
    }
  }

  /**
   * Subtracts a scalar from the vector without modifing the minuend and subtrahend.
   *
   * @param minuend     The minuend vector
   * @param subtrahend  The subtrahend value
   * @param difference  The difference vector
   * @param size        The number of entries
   */
  template<typename Minuend, typename Subtrahend, typename Difference>
  static void scalarSubtraction(const Minuend& minuend, const Subtrahend& subtrahend, Difference& difference, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      difference(index) = minuend(index) - subtrahend;
    }
  }

  /**
   * Subtracts two vectors.
   *
   * @param minuend     The minuend vector
   * @param subtrahend  The subtrahend vector
   * @param size        The number of entries
   */
  template<typename Minuend, typename Subtrahend>
  static void vectorSubtraction(Minuend& minuend, const Subtrahend& subtrahend, unsigned int size)
  {
    assert(minuend.getSize() == subtrahend.getSize());
    for (unsigned int index = 0; index < size; index++) {
      minuend(index) -= subtrahend(index);
    }
  }

  /**
   * Subtracts a scalar from the vector.
   *
   * @param minuend     The minuend vector
   * @param subtrahend  The scalar value
   * @param size        The number of entries
   */
  template<typename Minuend, typename Subtrahend>
  static void scalarSubtraction(Minuend& minuend, const Subtrahend& subtrahend, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      minuend(index) -= subtrahend;
    }
  }

  /**
   * Negates the vector.
   *
   * @param vector   The original vector
   * @param negated  The negated vector
   * @param size        The number of entries
   */
  template<typename Vector, typename Negation>
  static void vectorNegation(const Vector& vector, Negation& negated, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      negated(index) = -vector(index);
    }
  }

  /**
   * @}
   */

  /**
   * @name Multiplication functions.
   * @{
   */

  /**
   * Multiplies a vector with another vector without modifing the factors.
   *
   * @param factor1  The first factor vector
   * @param factor2  The second factor vector
   * @param size     The number of entries
   * @return         The product scalar
   */
  template<typename T, typename Factor1, typename Factor2>
  static T vectorMultiplication(const Factor1& factor1, const Factor2& factor2, unsigned int size)
  {
    T product = 0;
    for (unsigned int index = 0; index < size; index++) {
      product += factor1(index) * factor2(index);
    }
    return product;
  }

  /**
   * Inner product of two vectors without modifing the factors.
   *
   * @param factor1  The first factor vector
   * @param factor2  The second factor vector
   * @param size     The number of entries
   * @return         The product scalar
   */
  template<typename T, typename Factor1, typename Factor2>
  static T innerProduct(const Factor1& factor1, const Factor2& factor2, unsigned int size)
  {
    T product = 0;
    for (unsigned int index = 0; index < size; index++) {
      product += factor1(index) * factor2(index);
    }
    return product;
  }

  /**
   * Calculates the cross product of two 3D vectors without modifing the factors.
   *
   * @param factor1  The first factor vector3
   * @param factor2  The second factor vector3
   * @param product  The cross product vector
   */
  template<typename Factor1, typename Factor2, typename Product>
  static void crossProduct(const Factor1& factor1, const Factor2& factor2, Product& product)
  {
    product(0) = factor1(1) * factor2(2) - factor1(2) * factor2(1);
    product(1) = factor1(2) * factor2(0) - factor1(0) * factor2(2);
    product(2) = factor1(0) * factor2(1) - factor1(1) * factor2(0);
  }

  /**
   * Outer product of two vectors without modifing the factors.
   *
   * @param factor1  The first factor vector
   * @param factor2  The second factor vector
   * @param product  The product vector
   * @param size1    The number of entries
   * @param size2    The number of entries
   */
  template<typename Factor1, typename Factor2, typename Product>
  static void outerProduct(const Factor1& factor1, const Factor2& factor2, Product& product, unsigned int size1, unsigned int size2)
  {
    for (unsigned int index1 = 0; index1 < size1; index1++) {
      for (unsigned int index2 = 0; index2 < size2; index2++) {
        product(index1, index2) = factor1(index1) * factor2(index2);
      }
    }
  }

  /**
   * Multiplies a scalar to the vector without modifing the factors.
   *
   * @param factor1  The factor vector
   * @param factor2  The scalar value
   * @param product  The product vector
   * @param size     The number of entries
   */
  template<typename Factor1, typename Factor2, typename Product>
  static void scalarMultiplication(const Factor1& factor1, const Factor2& factor2, Product& product, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      product(index) = factor1(index) * factor2;
    }
  }

  /**
   * Multiplies a scalar to the vector.
   *
   * @param factor1  The factor vector
   * @param factor2  The scalar value
   * @param size     The number of entries
   */
  template<typename Factor1, typename Factor2>
  static void scalarMultiplication(Factor1& factor1, const Factor2& factor2, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      factor1(index) *= factor2;
    }
  }

  /**
   * @}
   */

  /**
   * @name Division functions.
   * @{
   */

  /**
   * Divides a vector by a scalar without modifing the dividend and the divisor.
   *
   * @param dividend  The dividend vector
   * @param divisor   The scalar value
   * @param quotient  The quotient vector
   * @param size      The number of entries
   */
  template<typename Dividend, typename Divisor, typename Quotient>
  static void scalarDivision(const Dividend& dividend, const Divisor& divisor, Quotient& quotient, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      quotient(index) = dividend(index) / divisor;
    }
  }

  /**
   * Divides a vector by a scalar.
   *
   * @param dividend  The dividend vector
   * @param divisor   The quotient value
   * @param size      The number of entries
   */
  template<typename Dividend, typename Divisor>
  static void scalarDivision(Dividend& dividend, const Divisor& divisor, unsigned int size)
  {
    for (unsigned int index = 0; index < size; index++) {
      dividend(index) /= divisor;
    }
  }

  /**
   * @}
   */

  /**
   * @name Rotate functions.
   * @{
   */

  /**
   * Rotates a vector by an angle.
   *
   * @param vector  The vector
   * @param angle   The angle in radians
   * @param size    The number of entries
   */
  template<typename T, typename Vector>
  static void rotateVector2(Vector& vector, double angle, unsigned int size)
  {
    assert(size == 2);
    T x = vector(0);
    T y = vector(1);
    double sinAngle = common::math::Math::sin(angle);
    double cosAngle = common::math::Math::cos(angle);
    vector(0) = cosAngle * x - sinAngle * y;
    vector(1) = sinAngle * x + cosAngle * y;
  }

  /**
   * Rotates a vector by an angle.
   *
   * @param vector  The vector
   * @param angle   The angle in radians
   * @param size    The number of entries
   */
  template<typename T, typename Vector>
  static void rotateVector2(Vector& vector, float angle, unsigned int size)
  {
    assert(size == 2);
    T x = vector(0);
    T y = vector(1);
    float sinAngle = common::math::Math::sin(angle);
    float cosAngle = common::math::Math::cos(angle);
    vector(0) = cosAngle * x - sinAngle * y;
    vector(1) = sinAngle * x + cosAngle * y;
  }

  /**
   * @}
   */

  /**
   * @name Aggregate functions.
   * @{
   */

  /**
   * Returns the length of a vector.
   *
   * @param vector  The vector
   * @param size    The number of entries
   * @return        The length of the vector
   */
  template<typename T, typename Vector>
  static T vectorLength(const Vector& vector, unsigned int size)
  {
    T length = 0;
    for (unsigned int index = 0; index < size; index++) {
      length += Math::sqr(vector(index));
    }
    return Math::sqrt(length);
  }

  /**
   * Returns the L1-norm of a vector.
   *
   * @param vector  The vector
   * @param size    The number of entries
   * @return        The L1-norm of the vector
   */
  template<typename T, typename Vector>
  static T vectorL1Norm(const Vector& vector, unsigned int size)
  {
    T length = 0;
    for (unsigned int index = 0; index < size; index++) {
      length += Math::abs(vector(index));
    }
    return length;
  }

  /**
   * Normalizes a vector.
   *
   * @param vector  The vector
   * @param size    The number of entries
   */
  template<typename T, typename Vector>
  static void vectorNormalize(Vector& vector, unsigned int size)
  {
    T length = vectorLength<T>(vector, size);
    if (length != 0) {
      for (unsigned int index = 0; index < size; index++) {
        vector(index) /= length;
      }
    }
  }

  /**
   * Permute a vector.
   *
   * @param vector   The vector
   * @param perm     The permutation
   * @param[out] res The permuted vector
   * @param size     size of the vector
   */
  template<typename Vector, typename Perm>
  static void permuteVector(const Vector& vector, const Perm& perm, Vector& res, unsigned int size)
  {
    for (unsigned i = 0; i < size; i++) {
      res(i) = vector(perm(i));
    }
  }

  /**
   * Clamps a vector.
   * Clamping is done for each element.
   *
   * @param vector   The vector
   * @param minimum  The minimum vector
   * @param maximum  The maximum vector
   * @param size     The number of entries
   */
  template<typename Vector>
  static void clampVector(Vector& vector, const Vector& minimum, const Vector& maximum, unsigned int size)
  {
    for (unsigned int i = 0; i < size; i++) {
      vector(i) = Math::clamp(vector(i), minimum(i), maximum(i));
    }
  }

  /**
   * @}
   */

};

} // namespace math
} // namespace common

#endif // __common_math_VectorAlgorithm_h_
