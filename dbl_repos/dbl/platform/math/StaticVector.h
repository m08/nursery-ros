/**
 * @file common/math/StaticVector.h
 * Definition of class common::math::StaticVector.
 */

#ifndef __common_math_StaticVector_h_
#define __common_math_StaticVector_h_

#ifdef _DEBUG
//#include "IVector.h"
#endif

#include "VectorAlgorithm.h"


#include <assert.h>
#include <string>

namespace common {
namespace math {

template<unsigned int>
class StaticPermutation;


/**
 * StaticVector is the class for vectors with static size.
 */
template<typename T, unsigned int Size>
class StaticVector
#ifdef _DEBUG
//  : public IVector<T>
#endif
{

public:

  /**
   * Default constructor.
   */
  StaticVector()
  {
    assert(Size > 0);
  }

  /**
   * Data init constructor.
   */
  StaticVector(T* initData)
  {
	memcpy(data, initData, Size*sizeof(T));
  }

  /**
   * Scalar data init constructor
   */
  StaticVector(const T& initData)
  {
	*this = initData;
  }

  /**
   * Copy constructor.
   *
   * @param other  The other vector
   */
  StaticVector(const StaticVector& other)
  {
    VectorAlgorithm::assign(other, *this, Size);
  }

  /**
   * Assignment operator.
   *
   * @param other  The other vector
   * @return       This vector
   */
  StaticVector& operator=(const StaticVector& other)
  {
    VectorAlgorithm::assign(other, *this, other.getSize());
    return *this;
  }

  /**
   * Assignment operator for scalars.
   *
   * @param other  The scalar value
   * @return       This vector
   */
  StaticVector& operator=(const T& value)
  {
	for (unsigned int i=0; i<Size; i++)
		data[i] = value;
	return *this;
  }
  /**
   * Gets the number of entries.
   *
   * @return  The number of entries
   */
  unsigned int getSize() const
  {
    return Size;
  }

  /**
   * Gets the const-entry for the given index.
   *
   * @param index  The index [0..m-1]
   * @return       The const-entry
   */
  const T& operator()(unsigned int index) const
  {
    assert(index < Size);
    return this->data[index];
  }

  /**
   * Gets the entry for the given index.
   *
   * @param index  The index [0..m-1]
   * @return       The entry
   */
  T& operator()(unsigned int index)
  {
    assert(index < Size);
    return this->data[index];
  }

protected:

  /** The vector-entries */
  T data[Size];

public:

  /**
   * @name Compare methods.
   * @{
   */

  /**
   * Equality operator.
   *
   * @param other  The other vector
   * @return       true if the vectors are equal
   */
  bool operator==(const StaticVector& other) const
  {
    return VectorAlgorithm::equal(*this, other, Size);
  }

  /**
   * Inequality operator.
   *
   * @param other  The other vector
   * @return       true if the vectorsare not equal
   */
  bool operator!=(const StaticVector& other) const
  {
    return !VectorAlgorithm::equal(*this, other, Size);
  }

  /**
   * @}
   */

  /**
   * @name String methods.
   * @{
   */

  /**
   * Gets the string-representation of the vector.
   *
   * @return  The string
   */
  std::string toString() const
  {
    return VectorAlgorithm::toString<T>(*this, Size);
  }

  /**
   * @}
   */

  /**
   * @name Addition methods.
   * @{
   */

  /**
   * Adds another static vector.
   *
   * @param other  The other vector
   * @return       The sum vector
   */
  StaticVector<T, Size> operator+(const StaticVector<T, Size>& other) const
  {
    StaticVector<T, Size> sum;
    VectorAlgorithm::vectorAddition(*this, other, sum, Size);
    return sum;
  }

  /**
   * Adds another static vector and stores the result in this vector.
   *
   * @param other  The other vector
   * @return       Reference to this vector
   */
  StaticVector<T, Size>& operator+=(const StaticVector<T, Size>& other)
  {
    VectorAlgorithm::vectorAddition(*this, other, Size);
    return *this;
  }

  /**
   * @}
   */

  /**
   * @name Subtraction methods.
   * @{
   */

  /**
   * Subtracts another static vector.
   *
   * @param other  The other vector
   * @return       The difference vector
   */
  StaticVector<T, Size> operator-(const StaticVector<T, Size>& other) const
  {
    StaticVector<T, Size> difference;
    VectorAlgorithm::vectorSubtraction(*this, other, difference, Size);
    return difference;
  }

  /**
   * Negates the vector.
   *
   * @return  The negated vector
   */
  StaticVector<T, Size> operator-() const
  {
    StaticVector<T, Size> negated;
    VectorAlgorithm::vectorNegation(*this, negated, Size);
    return negated;
  }

  /**
   * @}
   */

  /**
   * @name Multiplication methods.
   * @{
   */

  /**
   * Multiplies with another vector.
   *
   * @param other  The other vector
   * @return       This vector
   */
  T operator*(const StaticVector<T, Size>& other) const
  {
    return VectorAlgorithm::vectorMultiplication<T>(*this, other, Size);
  }

  /**
   * Multiplies with a scalar.
   *
   * @param scalar  The scalar
   * @return        The product vector
   */
  StaticVector<T, Size> operator*(const T& scalar) const
  {
    StaticVector<T, Size> product;
    VectorAlgorithm::scalarMultiplication(*this, scalar, product, Size);
    return product;
  }

  /**
   * Multiplies with a scalar.
   *
   * @param scalar  The scalar
   * @return        This vector
   */
  StaticVector<T, Size>& operator*=(const T& scalar)
  {
    VectorAlgorithm::scalarMultiplication(*this, scalar, Size);
    return *this;
  }

  /**
   * calculates the inner product (scalar-product) with another vector
   *
   * @param other  the other vector
   * @return       the inner product
   */
  T inner(const StaticVector<T, Size>& other) const
  {
    return VectorAlgorithm::innerProduct<T>(*this, other, Size);
  }

  /**
   * @}
   */

  /**
   * @name Divison methods.
   * @{
   */

  /**
   * Divides by a scalar.
   *
   * @param scalar  The scalar
   * @return        The quotient vector
   */
  StaticVector<T, Size> operator/(const T& scalar) const
  {
    StaticVector<T, Size> quotient;
    VectorAlgorithm::scalarDivision(*this, scalar, quotient, Size);
    return quotient;
  }

  /**
   * Divides by a scalar.
   *
   * @param scalar  The scalar
   * @return        This vector
   */
  StaticVector<T, Size>& operator/=(const T& scalar)
  {
    VectorAlgorithm::scalarDivision(*this, scalar, Size);
    return *this;
  }

  /**
   * @}
   */

  /**
   * @name Aggregate methods.
   * @{
   */

  /**
   * Returns the length of this vector.
   *
   * @return  The length
   */
  T getLength() const
  {
    return VectorAlgorithm::vectorLength<T>(*this, Size);
  }

  /**
   * Returns the L1-norm of this vector.
   *
   * @return  The L1-norm
   */
  T getL1Norm() const
  {
    return VectorAlgorithm::vectorL1Norm<T>(*this, Size);
  }

  /**
   * Sets all entries to zero.
   */
  void setZero()
  {
    return VectorAlgorithm::setZero(*this, Size);
  }

  /**
   * Normalizes this vector.
   */
  void normalize()
  {
    VectorAlgorithm::vectorNormalize<T>(*this, Size);
  }

  /**
   * swap two elements
   */
  void swap(unsigned int index1, unsigned int index2)
  {
    T tmp = data[index1];
    data[index1] = data[index2];
    data[index2] = tmp;
  }

  /**
   * Permute the vector.
   *
   * @param perm Permutation.
   * @return     permuted vector.
   */
  StaticVector<T, Size> permute(const StaticPermutation<Size>& perm) const
  {
    StaticVector<T, Size> res;
    VectorAlgorithm::permuteVector(*this, perm, res, Size);
    return res;
  }

  /**
   * Clamps this vector.
   * Clamping is done for each element.
   *
   * @param minimum  The minimum values.
   * @param maximum  The maximum values.
   */
  void clamp(const StaticVector<T, Size>& minimum, const StaticVector<T, Size>& maximum)
  {
    VectorAlgorithm::clampVector(*this, minimum, maximum, Size);
  }

  /**
   * @}
   */

};

} // namespace math
} // namespace common

#endif // __common_math_StaticVector_h_
