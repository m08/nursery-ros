/*
 * QuinticSplineTrajectory.hpp
 *
 *  Created on: Jan 17, 2011
 *  Last changes on: Feb 2, 2011
 *      Author: jgdkarssen
 */

#ifndef QuinticSplineTrajectory_H_
#define QuinticSplineTrajectory_H_


class CQuinticSplineTrajectory
{
	protected:
		double mA, mB, mC, mD, mE, mF;// polynomial coefficients
		double mStartTime;
		double mDuration;
		bool done;

		double y1, y2, y3;	// intermediate values

	public:
		CQuinticSplineTrajectory()
		{
			// default settings
			mA=0; mB=0; mC=0; mD=0; mE=0; mF=0;
			mStartTime = 0;
			mDuration = 1;
			done = false;
		}

		void setTrajectory
		(
			double startTime, double duration,
			double startPosition, double startSpeed, double startAcceleration,
			double endPosition,   double endSpeed,   double endAcceleration
		)
		{
			//double y1, y2, y3;	// intermediate values

			mStartTime = startTime;
			mDuration = duration;
			done = false;

			// scale speeds and accelerations with mDuration
			startSpeed = startSpeed*mDuration;
			endSpeed = endSpeed*mDuration;
			startAcceleration = startAcceleration*(mDuration*mDuration);
			endAcceleration = endAcceleration*(mDuration*mDuration);

			// determine coefficients
			mF = startPosition;
			mE = startSpeed;
			mD = 1/2*startAcceleration;
			mC = 10*endPosition-4*endSpeed+1/2*endAcceleration-3*mD-6*mE-10*mF;
			mB = -15*endPosition+7*endSpeed-endAcceleration+3*mD+8*mE+15*mF;
			mA = 6*endPosition-3*endSpeed+1/2*endAcceleration-mD-3*mE-6*mF;
		}
		double getPosition(double currentTime)
		{
			double t; // scaled time between 0 and 1
			double t2, t3, t4, t5; // polynomial coefficients

			t = (currentTime-mStartTime) /mDuration;

			if ( t < 0 )
				t = 0;
			else if ( t > 1 )
			{
				t = 1;
				done = true;
			}

			t2 = t*t;
			t3 = t2*t;
			t4 = t3*t;
			t5 = t4*t;

			return ( mA*t5 + mB*t4 + mC*t3 + mD*t2 + mE*t + mF );
		}
		double getSpeed(double currentTime)
		{
			double t; // scaled time between 0 and 1
			double t2, t3, t4; // polynomial coefficients

			t = (currentTime-mStartTime)/mDuration;

			if ( t < 0 )
				t = 0;
			else if ( t > 1 )
			{
				t = 1;
				done = true;
			}

			t2 = t*t;
			t3 = t2*t;
			t4 = t3*t;

			return ( (5*mA*t4 + 4*mB*t3 + 3*mC*t2 + 2*mD*t + mE) / mDuration);
		}
		double getAcceleration(double currentTime)
		{
			double t; // scaled time between 0 and 1
			double t2, t3; // polynomial coefficients

			t = (currentTime-mStartTime)/mDuration;

			if ( t < 0 )
				t = 0;
			else if ( t > 1 )
			{
				t = 1;
				done = true;
			}

			t2 = t*t;
			t3 = t2*t;

			return ( (20*mA*t3 + 12*mB*t2 + 6*mC*t + 2*mD) / (mDuration*mDuration));
		}
		bool isDone()
		{
			return done;
		}
};
#endif //QuinticSplineTrajectory_H_
