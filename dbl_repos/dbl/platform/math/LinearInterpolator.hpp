#ifndef LINEARINTERPOLATOR_HPP_
#define LINEARINTERPOLATOR_HPP_


#include <cmath>


/**
 * This is just a copy of Flame code. 
 * 
 * @todo make this nicer
 */
class LinearInterpolator
{
protected:
	double			*_mY;
	double			_mXStart;
	double			_mXEnd;
	double			_mXStepsize;
	double			_mX1OverStepsize;
	unsigned int	_size;


public:
	LinearInterpolator
	(
		const double t_start,
		const double t_end,
		double* const values, 
		const unsigned int size
	) 
	:
		_mY(values), 
		_mXStart(t_start), 
		_mXEnd(t_end), 
		_mXStepsize((t_end - t_start) / (size - 1)), 
		_mX1OverStepsize((size - 1) / (t_end - t_start)), 
		_size(size)
	{
		// empty
	}


	virtual ~LinearInterpolator()
	{
		// empty
	}


	double calculate(const double t)
	{
		int x0 = (int) floor( (t - _mXStart) * _mX1OverStepsize );

		// Hold first y-value (don't extrapolate)
		if (t < 0)
			return _mY[0];

		// Hold last y-value (don't extrapolate)
		if (t >= _size - 1)
			return _mY[_size - 1];

		// Interpolate
		return _mY[x0] + ((t - _mXStart - x0 * _mXStepsize) * _mX1OverStepsize) * (_mY[x0 + 1] - _mY[x0]);
	}
};


#endif /*LINEARINTERPOLATOR_HPP_*/
