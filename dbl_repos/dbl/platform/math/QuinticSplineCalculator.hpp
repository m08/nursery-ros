#ifndef QUINTICSPLINECALCULATOR_HPP_
#define QUINTICSPLINECALCULATOR_HPP_


class QuinticSplineCalculator
{
public:
	static double compute
	(
		double t, 
		const double x0, const double v0, const double a0, 
		const double x1, const double v1, const double a1
	)
	{
		double t2, t3, t4, t5;          // powers of time variable
		double a, b, c, d, e, f;        // polynomial coefficients
		double y1, y2, y3;              // intermediate values

		if ( t < 0 ) 
			t = 0; 
		else if ( t > 1 ) 
			t = 1;

		t2 = t*t;
		t3 = t2*t;
		t4 = t3*t;
		t5 = t4*t;

		f = x0;
		e = v0;
		d = 0.5 * a0;

		// compute the rightmost column of the solution for [a b ]
		y1 = x1 -   d - e - f;
		y2 = v1 - 2*d - e;
		y3 = a1 - 2*d;

		// multiply out the solution matrix to find [a b c]
		a =   6*y1 - 3*y2 + 0.5f*y3;
		b = -15*y1 + 7*y2 -      y3;
		c =  10*y1 - 4*y2 + 0.5f*y3;

		// compute the polynomial
		return ( a*t5 + b*t4 + c*t3 + d*t2 + e*t + f );
	}
};

#endif /*QUINTICSPLINECALCULATOR_HPP_*/
