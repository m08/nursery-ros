#ifndef LINEARSPLINECALCULATOR_HPP_
#define LINEARSPLINECALCULATOR_HPP_


class LinearSplineCalculator
{
public:
	static double compute
	(
		double t, 
		const double x0,  
		const double x1
	)
	{
		if ( t < 0 ) 
			t = 0; 
		else if ( t > 1 ) 
			t = 1;

		
		return ((1.0-t) * x0 + t * x1);
	}
};

#endif /*LINEARSPLINECALCULATOR_HPP_*/
