/**
 * @file common/math/MathFunctions.h
 * Definition of class common::math::MathFunctions.
 */

#ifndef __common_math_MathFunctions_h_
#define __common_math_MathFunctions_h_

#include "MathConstants.h"

#include <assert.h>
#include <math.h>

namespace common {
namespace math {

/**
 * Forward declaration of class MathFunctions.
 */
class MathFunctions;

/**
 * The typedef Math is provided for convenience only.
 */
typedef MathFunctions Math;

/**
 * MathFunctions is a class for providing mathematical functions.
 */
class MathFunctions
{

private:

  /**
   * Constructor.
   */
  MathFunctions();

  static MathFunctions instance;

public:

  static void init();

  static const int cordicIterations = 10; // Number of times to run the algorithm
  static float powTable[cordicIterations]; // in Radians
  static float arcTanTable[cordicIterations]; // in Radians

  /**
   * @name Aggregate functions.
   * @{
   */

  /**
   * Gets the const minimum of two values.
   *
   * @param first   The first value
   * @param second  The second value
   * @return        The const minimum
   */
  template<typename T>
  static const T& minimum(const T& first, const T& second)
  {
    return (first < second) ? first : second;
  }

  /**
   * Gets the minimum of two values.
   *
   * @param first   The first value
   * @param second  The second value
   * @return        The minimum
   */
  template<typename T>
  static T& minimum(T& first, T& second)
  {
    return (first < second) ? first : second;
  }

  /**
   * Gets the const maximum of two values.
   *
   * @param first   The first value
   * @param second  The second value
   * @return        The const maximum
   */
  template<typename T>
  static const T& maximum(const T& first, const T& second)
  {
    return (first < second) ? second : first;
  }

  /**
   * Gets the maximum of two values.
   *
   * @param first   The first value
   * @param second  The second value
   * @return        The maximum
   */
  template<typename T>
  static T& maximum(T& first, T& second)
  {
    return (first < second) ? second : first;
  }

  /**
   * Gets the value clamped into the given interval between min and max.
   *
   * @param value   The value
   * @param min     The minimum
   * @param max     The maximum
   * @return        The clamped value
   */
  template<typename T>
  static T clamp(const T& value, const T& min, const T& max)
  {
    if (value < min) {
      return min;
    }
    if (value > max) {
      return max;
    }
    return value;
  }

  /**
   * Gets the absolute value.
   *
   * @param value   The value
   * @return        The absolute value
   */
  template<typename T>
  static T abs(const T& value)
  {
    if (value < 0) {
      return -value;
    }
    return value;
  }

  /**
   * Gets the square of the value.
   *
   * @param value   The value
   * @return        The square of the value
   */
  template<typename T>
  static T sqr(const T& value)
  {
    return value * value;
  }

  /**
   * Gets the squareroot of the value of type double.
   *
   * @param value   The value
   * @return        The squareroot of the value
   */
  static double sqrt(const double& value);

  /**
   * Gets the squareroot of the value of type float.
   *
   * @param value   The value
   * @return        The squareroot of the value
   */
  static float sqrt(const float& value);

  /**
   * Gets the squareroot of the value of type float.
   * The accuracy is about 10^-3.
   *
   * @param value   The value
   * @return        The squareroot of the value
   */
  static float sqrtFast(const float& value);

  /**
   * Gets the squareroot of the value of type unsigned long.
   *
   * @param value   The value
   * @return        The integer squareroot of the value
   */
  static unsigned short sqrt(unsigned long value);

  /**
   * Gets the squareroot of the sum of the argument squares of type double.
   *
   * @param x   The first value
   * @param y   The second value
   * @return    The hypotenuse
   */
  static double hypot(const double& x, const double& y);

  /**
   * Gets the squareroot of the sum of the argument squares of type float.
   *
   * @param x   The first value
   * @param y   The second value
   * @return    The hypotenuse
   */
  static float hypot(const float& x, const float& y);

  /**
   * Modulos the value into the range between min and max.
   * The value should be near the desired range to allow fast computation.
   *
   * @param value  The value
   * @param min    The minimum value of the range
   * @param max    The maximum value of the range
   * @return       The modulo-value
   */
  template<typename T>
  static T modulo(const T& value, const T& min, const T& max)
  {
    T t = value;
    while (t < min) {
      t += (max - min);
    }
    while (t > max) {
      t -= (max - min);
    }
    return t;
  }

  /**
   * Normalizes the float angle into the range [-pi, pi).
   * The value should be near the desired range to allow fast computation.
   *
   * @param angle  The angle
   * @return       The normalized angle
   */
  static float normalizeAngle(const float& angle);

  /**
   * Normalizes the double angle into the range [-pi, pi).
   * The value should be near the desired range to allow fast computation.
   *
   * @param angle  The angle
   * @return       The normalized angle
   */
  static double normalizeAngle(const double& angle);

  /**
   * Distributes the argument in a Gaussian way.
   * @param d A value that should be distributed.
   * @return A transformation of d according to a Gaussian curve.
   */
  static float gaussian(const float d);

  /**
   * @}
   */

  /**
   * @name Property functions.
   * @{
   */

  /**
   * Returns true if the value of type integer is even.
   *
   * @param value   The value
   * @return        true if the value is even
   */
  static bool even(const int& value);

  /**
   * Returns true if the value of type unsigned integer is even.
   *
   * @param value   The value
   * @return        true if the value is even
   */
  static bool even(const unsigned int& value);

  /**
   * Returns true if the value of type integer is odd.
   *
   * @param value   The value
   * @return        true if the value is odd
   */
  static bool odd(const int& value);

  /**
   * Returns true if the value of type unsigned integer is odd.
   *
   * @param value   The value
   * @return        true if the value is odd
   */
  static bool odd(const unsigned int& value);

  /**
   * Gets the sign of the value.
   *
   * @param value   The value
   * @return        -1 if the value is negativ, 1 if the value is positiv and 0 if the value is zero
   */
  template<typename T>
  static T signum(const T& value)
  {
    if (value < 0) {
      return -1;
    }
    if (value > 0) {
      return 1;
    }
    return 0;
  }

  /**
   * @}
   */

  /**
   * @name Round functions.
   * @{
   */

  /**
   * Gets the ceil value of type double.
   *
   * @param value   The value
   * @return        The ceil value
   */
  static double ceil(const double& value);

  /**
   * Gets the ceil value of type float.
   *
   * @param value   The value
   * @return        The ceil value
   */
  static float ceil(const float& value);

  /**
   * Gets the ceil value of type double as an integer.
   *
   * @param value   The value
   * @return        The ceil value
   */
  static int ceilToInt(const double& value);

  /**
   * Gets the ceil value of type float as an integer.
   *
   * @param value   The value
   * @return        The ceil value
   */
  static int ceilToInt(const float& value);

  /**
   * Gets the floor value of type double.
   *
   * @param value   The value
   * @return        The floor value
   */
  static double floor(const double& value);

  /**
   * Gets the floor value of type float.
   *
   * @param value   The value
   * @return        The floor value
   */
  static float floor(const float& value);

  /**
   * Gets the floor value of type double as an integer.
   *
   * @param value   The value
   * @return        The floor value
   */
  static int floorToInt(const double& value);

  /**
   * Gets the floor value of type float as an integer.
   *
   * @param value   The value
   * @return        The floor value
   */
  static int floorToInt(const float& value);

  /**
   * Gets the rounded value of type double.
   *
   * @param value   The value
   * @return        The rounded value
   */
  static double round(const double& value);

  /**
   * Gets the rounded value of type float.
   *
   * @param value   The value
   * @return        The rounded value
   */
  static float round(const float& value);

  /**
   * Gets the rounded value of type double as an integer.
   *
   * @param value   The value
   * @return        The rounded value
   */
  static int roundToInt(const double& value);

  /**
   * Gets the rounded value of type float as an integer.
   *
   * @param value   The value
   * @return        The rounded value
   */
  static int roundToInt(const float& value);

  /**
   * @}
   */

  /**
   * @name Convert functions.
   * @{
   */

  /**
   * Converts the double value to int.
   *
   * @param value  The double value
   * @return       The integer value
   */
  static int toInt(const double& value);

  /**
   * Converts the float value to int.
   *
   * @param value  The float value
   * @return       The integer value
   */
  static int toInt(const float& value);

  /**
   * Converts the double value to float.
   *
   * @param value  The double value
   * @return       The float value
   */
  static float toFloat(const double& value);

  /**
   * Converts the float value to float.
   *
   * @param value  The float value
   * @return       The float value
   */
  static float toFloat(const float& value);

  /**
   * Converts the int value to float.
   *
   * @param value  The int value
   * @return       The float value
   */
  static float toFloat(const int& value);

  /**
   * Converts an angle of type double measured in radians to an approximately equivalent angle measured in degrees.
   *
   * @param rad  The angle in radians
   * @return     The angle in degrees
   */
  static double toDegrees(const double& rad);

  /**
   * Converts an angle of type float measured in radians to an approximately equivalent angle measured in degrees.
   *
   * @param rad  The angle in radians
   * @return     The angle in degrees
   */
  static float toDegrees(const float& rad);

  /**
   * Converts an angle of type double measured in degrees to an approximately equivalent angle measured in radians.
   *
   * @param deg  The angle in degrees
   * @return     The angle in radians
   */
  static double toRadians(const double& deg);

  /**
   * Converts an angle of type float measured in degrees to an approximately equivalent angle measured in radians.
   *
   * @param deg  The angle in degrees
   * @return     The angle in radians
   */
  static float toRadians(const float& deg);

  /**
   * Converts an angle of type int measured in degrees to an approximately equivalent angle measured in radians.
   *
   * @param deg  The angle in degrees
   * @return     The angle in radians
   */
  static float toRadians(int deg);

  /**
   * @}
   */

  /**
   * @name Trigonometric functions.
   * @{
   */

  /**
   * Returns the sine of an angle of type double.
   *
   * @param angle  The angle in radians
   * @return       The sine of the angle
   */
  static double sin(const double& angle);

  /**
   * Returns the sine of an angle of type float.
   *
   * @param angle  The angle in radians
   * @return       The sine of the angle
   */
  static float sin(const float& angle);

  /**
   * Returns the cosine of an angle of type double.
   *
   * @param angle  The angle in radians
   * @return       The cosine of the angle
   */
  static double cos(const double& angle);

  /**
   * Returns the cosine of an angle of type float.
   *
   * @param angle  The angle in radians
   * @return       The cosine of the angle
   */
  static float cos(const float& angle);

  /**
   * Returns the sine and the cosine of a normalized angle of type float.
   * The accuracy is about 10^-3.
   *
   * @param angle  The normalized angle in radians
   * @param sin    The sine of the angle
   * @param cos    The cosine of the angle
   */
  static void sinCosFast(const float& angle, float& sin, float& cos);

  /**
   * Returns the tangent of an angle of type double.
   *
   * @param angle  The angle in radians
   * @return       The tangent of the angle
   */
  static double tan(const double& angle);

  /**
   * Returns the tangent of an angle of type float.
   *
   * @param angle  The angle in radians
   * @return       The tangent of the angle
   */
  static float tan(const float& angle);

  /**
   * Returns the arc sine of an angle of type double, in the range of -pi/2 through pi/2.
   *
   * @param angle  The angle in radians
   * @return       The arc sine of the angle
   */
  static double asin(const double& angle);

  /**
   * Returns the arc sine of an angle of type float, in the range of -pi/2 through pi/2.
   *
   * @param angle  The angle in radians
   * @return       The arc sine of the angle
   */
  static float asin(const float& angle);

  /**
   * Returns the arc cosine of an angle of type double, in the range of 0.0 through pi.
   *
   * @param angle  The angle in radians
   * @return       The arc cosine of the angle
   */
  static double acos(const double& angle);

  /**
   * Returns the arc cosine of an angle of type float, in the range of 0.0 through pi.
   *
   * @param angle  The angle in radians
   * @return       The arc cosine of the angle
   */
  static float acos(const float& angle);

  /**
   * Returns the arc tangent of an angle of type double, in the range of -pi/2 through pi/2.
   *
   * @param angle  The angle in radians
   * @return       The arc tangent of the angle
   */
  static double atan(const double& angle);

  /**
   * Returns the arc tangent of an angle of type float, in the range of -pi/2 through pi/2.
   *
   * @param angle  The angle in radians
   * @return       The arc tangent of the angle
   */
  static float atan(const float& angle);

  /**
   * Returns the arc tangent of the rectangular coordinates of type double, in the range of -pi through pi.
   *
   * @param ordinate  The ordinate coordinate
   * @param abscissa  The abscissa coordinate
   * @return          The arc tangent of the rectangular coordinates
   */
  static double atan2(const double& ordinate, const double& abscissa);

  /**
   * Returns the arc tangent of the rectangular coordinates of type float, in the range of -pi through pi.
   *
   * @param ordinate  The ordinate coordinate
   * @param abscissa  The abscissa coordinate
   * @return          The arc tangent of the rectangular coordinates
   */
  static float atan2(const float& ordinate, const float& abscissa);

  /**
   * Returns Euler's number e raised to the power of a value of type double.
   *
   * @param value  The value
   * @return       The number e raised to the power of the value
   */
  static double exp(const double& value);

  /**
   * Returns Euler's number e raised to the power of a value of type float.
   *
   * @param value  The value
   * @return       The number e raised to the power of the value
   */
  static float exp(const float& value);

  /**
   * Returns the natural logarithm (base e) of a value of type double.
   *
   * @param value  The value
   * @return       The natural logorithm of the value
   */
  static double log(const double& value);

  /**
   * Returns the natural logarithm (base e) of a value of type float.
   *
   * @param value  The value
   * @return       The natural logorithm of the value
   */
  static float log(const float& value);

  /**
   * Returns the base 10 logarithm of a value of type double.
   *
   * @param value  The value
   * @return       The base 10 logorithm of the value
   */
  static double log10(const double& value);

  /**
   * Returns the base 10 logarithm of a value of type float.
   *
   * @param value  The value
   * @return       The base 10 logorithm of the value
   */
  static float log10(const float& value);

  /**
   * @}
   */

  /**
   * @name Compare functions based on epsilon.
   * @{
   */

  /**
   * Checks if the given values of type double are nearly equal (less than epsilon).
   *
   * @param first    The first value
   * @param second   The second value
   * @param epsilon  The epsilon
   * @return         true if the values is nearly equal
   */
  static bool equal(const double& first, const double& second, const double& epsilon = EPSILON_DOUBLE);

  /**
   * Checks if the given values of type float are nearly equal (less than epsilon).
   *
   * @param first    The first value
   * @param second   The second value
   * @param epsilon  The epsilon
   * @return         true if the values is nearly equal
   */
  static bool equal(const float& first, const float& second, const float& epsilon = EPSILON_FLOAT);

  /**
   * Checks if the given value of type double is nearly zero (less than epsilon).
   *
   * @param value    The value
   * @param epsilon  The epsilon
   * @return         true if the value is nearly zero
   */
  static bool zero(const double& value, const double& epsilon = EPSILON_DOUBLE);

  /**
   * Checks if the given value of type float is nearly zero (less than epsilon).
   *
   * @param value    The value
   * @param epsilon  The epsilon
   * @return         true if the value is nearly zero
   */
  static bool zero(const float& value, const float& epsilon = EPSILON_FLOAT);

  /**
   * Returns the number of decimal places behind the dot of the given number (the function rounds after the first digit found after the dot - .9 up, .1 down)
   *
   * @param value   The number
   * @return        The number of decimal places
   */
  static int numberOfDecimalPlaces(const double& value);

  /**
   * @}
   */

};

} // namespace math
} // namespace common

#endif // __common_math_MathFunctions_h_
