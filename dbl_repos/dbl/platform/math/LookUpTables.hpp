#ifndef LOOKUPTABLES_HPP_
#define LOOKUPTABLES_HPP_


class LookUpTables
{
private:
	static void init
	(
		const double  t_start,  
		const double  t_end,
		const double* values
	)
	{
		int  numElements = sizeof(values)/(sizeof(double);
		
		
		// Goal init: Initialize the whole LOT. Calculate all interpolated values from t_start to t_end for the given stepsize 
	
		
	}
public:
	static double compute
	(
		const double t
	)
	{
		// Calc. index of the matrix
		int  index       = (int)floor( (t - t_start) * ( (numElements - 1) / (t_end - t_start) ) );
		
		// Out of range conditions:
		if (index < 0)
		{
			return values[0];
		}
		
		if (index >= mNumElements-1)
		{
			return values[mNumElements-1];
		}
		
		// Return interpolated value	
		return 
		
		// something like : mY[x0] + ((xValue - mXStart - x0*mXStepsize)* mX1OverStepsize)*(mY[x0+1] - mY[x0]);;
		
	}
	
};

#endif /*LOOKUPTABLES_HPP_*/

//__________________________________________________________________________________________
// // FLAME CODE to run CLUT

//ff_swing_interleg_AngleControl[] = {-0.5, -1.5, -2.5, -1.5, 0.0, 2.5, 3.0, -2.5, -2.5, -1.8, -1.8};


//ff_swing_interleg.Init(0.0, 0.9, ff_swing_interleg_AngleControl, sizeof(ff_swing_interleg_AngleControl)/sizeof(float));

//joints.interleg.ff = ff_swing_interleg.GetValue( Controller()->TimeElapsed() );

//__________________________________________________________________________________________
// FLAME IMPLEMENTATION:

/*
#include "LookupTables.h"

#include <math.h>
#include <stdlib.h>

CLUTLinear::CLUTLinear()
{
	mY = NULL;
	mNumElements = 0;
}

void CLUTLinear::Init(float xStart, float xEnd, float* yVec, int numElements)
{
	mY	= yVec;
	mXStart	= xStart;
	mXEnd	= xEnd;
	mXStepsize		= (xEnd - xStart)/(numElements-1);
	mX1OverStepsize	= (numElements-1)/(xEnd - xStart);	// Multiplying is faster than dividing
	mNumElements = numElements;
}


float CLUTLinear::GetValue(float xValue)
{
	int x0 = (int)floor( (xValue - mXStart) * mX1OverStepsize );

	// Hold first y-value (don't extrapolate)
	if (x0 < 0)
		return mY[0];
	
	// Hold last y-value (don't extrapolate)
	if (x0 >= mNumElements-1)
		return mY[mNumElements-1];

	// Interpolate
	return mY[x0] + ((xValue - mXStart - x0*mXStepsize)* mX1OverStepsize)*(mY[x0+1] - mY[x0]);
}

*/
