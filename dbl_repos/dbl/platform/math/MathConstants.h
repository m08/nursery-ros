/**
 * @file common/math/MathConstants.h
 * Definition of mathematical constants.
 */

#ifndef __common_math_MathConstants_h_
#define __common_math_MathConstants_h_

namespace common {
namespace math {

/**
 * @name Constants.
 * @{
 */

/**
 * The constant PI of type double.
 */
const double PI_DOUBLE = 3.1415926535897932;
const double HALF_PI_D = 1.5707963267948966;
/**
 * The constant PI of type float.
 */
const float PI_FLOAT = 3.1415926535f;
const float HALF_PI_F = 1.57079633f;

/**
 * The constant e of type double.
 */
const double E_DOUBLE = 2.718281828;

/**
 * The constant e of type float.
 */
const float E_FLOAT = 2.718281828f;

/**
 * The epsilon for type double.
 */
const double EPSILON_DOUBLE = 2.2204460492503131e-016;

/**
 * The epsilon for type float.
 */
const float EPSILON_FLOAT = 1.192092896e-07f;

/**
 * @}
 */

/**
 * @name Constants for the minimum- and the maximum-value of a specific type.
 * @{
 */

/**
 * The constant for the minimum-value of the type char.
 */
const char MIN_CHAR = -128;

/**
 * The constant for the maximum-value of the type char.
 */
const char MAX_CHAR = 127;

/**
 * The constant for the maximum-value of the type unsigned char.
 */
const unsigned char MAX_UNSIGNED_CHAR = 255;

/**
 * The constant for the minimum-value of the type double.
 */
const double MIN_DOUBLE = 2.2250738585072014e-308;

/**
 * The constant for the maximum-value of the type double.
 */
const double MAX_DOUBLE = 1.7976931348623158e+308;

/**
 * The constant for the minimum-value of the type float.
 */
const float MIN_FLOAT = 1.175494351e-38f;

/**
 * The constant for the maximum-value of the type float.
 */
const float MAX_FLOAT = 3.402823466e+38f;

/**
 * The constant for the minimum-value of the type int.
 */
const int MIN_INT = (-2147483647 - 1);

/**
 * The constant for the maximum-value of the type int.
 */
const int MAX_INT = 2147483647;

/**
 * The constant for the maximum-value of the type unsigned int.
 */
const unsigned int MAX_UNSIGNED_INT = 0xffffffff; //4294967295;

/**
 * The constant for the minimum-value of the type long.
 */
const long MIN_LONG = (-2147483647 - 1);

/**
 * The constant for the maximum-value of the type long.
 */
const long MAX_LONG = 2147483647;

/**
 * The constant for the maximum-value of the type unsigned long.
 */
const unsigned long MAX_UNSIGNED_LONG = 0xffffffff; //4294967295;

/**
 * The constant for the minimum-value of the type short.
 */
const short MIN_SHORT = -32768;

/**
 * The constant for the maximum-value of the type short.
 */
const short MAX_SHORT = 32767;

/**
 * The constant for the maximum-value of the type unsigned short.
 */
const unsigned short MAX_UNSIGNED_SHORT = 65535;

/**
 * @}
 */

} // namespace math
} // namespace common

#endif // __common_math_MathConstants_h_
