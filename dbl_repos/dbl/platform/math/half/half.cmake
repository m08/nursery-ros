#
# CMake include file for half library
# Wouter Caarls <w.caarls@tudelft.nl>
#
# 29-03-2010 (wcaarls): Initial revision
#

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/platform/math/half)
TARGET_LINK_LIBRARIES(${TARGET} half)

IF (NOT __HALF_CMAKE_INCLUDED)
  SET(__HALF_CMAKE_INCLUDED 1)

# ivan: workaround for CMP0002 cmake error - move to root CMakeLists.txt
#  ADD_SUBDIRECTORY(${WORKSPACE_DIR}/dbl/platform/math/half ${WORKSPACE_DIR}/build/half/${CMAKE_BUILD_TYPE}${COMPILER_VERSION})
ENDIF (NOT __HALF_CMAKE_INCLUDED)
