// Min-max heap implementation -*- C++ -*-

// This is an extension of the STL heap implementation (stl_heap.h)
// that provides extraction of both the smallest and the largest
// element in constant time.
// Author: Erik Schuitema <e.schuitema@tudelft.nl>
// Copyright (c) 2010 Delft University of Technology, The Netherlands.
// http://www.dbl.tudelft.nl/

// Copyright (C) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009
// Free Software Foundation, Inc.
//
// This file is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3,
// or (at your option) any later version.

// This file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// Under Section 7 of GPL version 3, you are granted additional
// permissions described in the GCC Runtime Library Exception, version
// 3.1, as published by the Free Software Foundation.

// You should have received a copy of the GNU General Public License and
// a copy of the GCC Runtime Library Exception along with this program;
// see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
// <http://www.gnu.org/licenses/>.

/*
 *
 * Copyright (c) 1994
 * Hewlett-Packard Company
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.  Hewlett-Packard Company makes no
 * representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied warranty.
 *
 * Copyright (c) 1997
 * Silicon Graphics Computer Systems, Inc.
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.  Silicon Graphics makes no
 * representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied warranty.
 */

/** @file stl_minmaxheap.h
 *
 */

#ifndef _STL_MINMAXHEAP_H
#define _STL_MINMAXHEAP_H 1

//#include <debug/debug.h>
#include <ilog2.h>
#define _GLIBCXX_MOVE(_Tp) (_Tp)

namespace std {

  /**
   * @defgroup minmaxheap_algorithms Heap Algorithms
   * @ingroup sorting_algorithms
   */

  // Heap-manipulation functions: push_heap, pop_heap, make_heap, sort_heap,
  // + is_heap and is_heap_until in C++0x.

#define __MINMAXHEAP_PARENT(X)		((X-1)/2)
#define __MINMAXHEAP_GRANDPARENT(X)	((X-3)/4)
#define __MINMAXHEAP_LEVEL(X)		(ilog2_x86(X+1)) // Very slow alternative: ((int)log2(X+1))

  template<typename _RandomAccessIterator, typename _Distance, typename _Tp>
    void
    __push_minmaxheap(_RandomAccessIterator __first,
		_Distance __holeIndex, _Distance __topIndex, _Tp ___value)
    {
	  // The hole has no children (so it is on one of the lowest levels).
	  // First, check with its direct parent whether it should start on a min or max level.
	  // Then, sift up according to the new level
	  int level = __MINMAXHEAP_LEVEL(__holeIndex);
	  _Distance __parent = (__holeIndex - 1) / 2;
	  if ((level &1) == 0)	// Initial max level; direct parent is min-level
	  {
		  if (__holeIndex > __topIndex && *(__first + __parent) > ___value)
		{
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __parent));
		  __holeIndex = __parent;
		}
	  }
	  else	// Initial min level
	  {
		  if (__holeIndex > __topIndex && *(__first + __parent) < ___value)
		{
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __parent));
		  __holeIndex = __parent;
		}
	  }

	  // Now, do the real sift-up
	  level = __MINMAXHEAP_LEVEL(__holeIndex);
      _Distance __grandparent = __MINMAXHEAP_GRANDPARENT(__holeIndex);
	  if ((level &1) == 0)	// Max level
	  {
		  // Check against grandparents.
		  // (__MINMAXHEAP_PARENT(__holeIndex) > __topIndex) guarantees that there is a grandparent
		  while ((__MINMAXHEAP_PARENT(__holeIndex) > __topIndex) && *(__first + __grandparent) < ___value)
		{
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __grandparent));
		  __holeIndex = __grandparent;
		  __grandparent = __MINMAXHEAP_GRANDPARENT(__holeIndex);
		}
	  }
	  else	 // Min level; reverse operator
	  {
		  // Check against grandparents.
		  // (__MINMAXHEAP_PARENT(__holeIndex) > __topIndex) guarantees that there is a grandparent
		  while ((__MINMAXHEAP_PARENT(__holeIndex) > __topIndex) && *(__first + __grandparent) > ___value)
		{
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __grandparent));
		  __holeIndex = __grandparent;
		  __grandparent = __MINMAXHEAP_GRANDPARENT(__holeIndex);
		}
	  }
	  *(__first + __holeIndex) = _GLIBCXX_MOVE(___value);
    }

  /**
   *  @brief  Push an element onto a minmaxheap.
   *  @param  first  Start of minmaxheap.
   *  @param  last   End of minmaxheap + element.
   *  @ingroup minmaxheap_algorithms
   *
   *  This operation pushes the element at last-1 onto the valid minmaxheap over the
   *  range [first,last-1).  After completion, [first,last) is a valid minmaxheap.
  */
  template<typename _RandomAccessIterator>
    inline void
    push_minmaxheap(_RandomAccessIterator __first, _RandomAccessIterator __last)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	  _ValueType;
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
	  _DistanceType;

      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_function_requires(_LessThanComparableConcept<_ValueType>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap(__first, __last - 1);

      _ValueType ___value = _GLIBCXX_MOVE(*(__last - 1));
      std::__push_minmaxheap(__first, _DistanceType((__last - __first) - 1),
		       _DistanceType(0), _GLIBCXX_MOVE(___value));
    }

  template<typename _RandomAccessIterator, typename _Distance, typename _Tp,
	   typename _Compare>
    void
    __push_minmaxheap(_RandomAccessIterator __first, _Distance __holeIndex,
		_Distance __topIndex, _Tp ___value, _Compare __comp)
    {
	  // See the copy of the non __comp version for comments
	  int level = __MINMAXHEAP_LEVEL(__holeIndex);
	  _Distance __parent = (__holeIndex - 1) / 2;
	  if ((level &1) == 0)
	  {
		  if (__holeIndex > __topIndex && !__comp(*(__first + __parent), ___value))
		{
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __parent));
		  __holeIndex = __parent;
		}
	  }
	  else
	  {
		  if (__holeIndex > __topIndex && __comp(*(__first + __parent), ___value))
		{
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __parent));
		  __holeIndex = __parent;
		}
	  }

	  level = __MINMAXHEAP_LEVEL(__holeIndex);
      _Distance __grandparent = __MINMAXHEAP_GRANDPARENT(__holeIndex);
	  if ((level &1) == 0)
	  {
		  while ((__MINMAXHEAP_PARENT(__holeIndex) > __topIndex) && __comp(*(__first + __grandparent), ___value))
		{
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __grandparent));
		  __holeIndex = __grandparent;
		  __grandparent = __MINMAXHEAP_GRANDPARENT(__holeIndex);
		}
	  }
	  else
	  {
		  while ((__MINMAXHEAP_PARENT(__holeIndex) > __topIndex) && !__comp(*(__first + __grandparent), ___value))
		{
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __grandparent));
		  __holeIndex = __grandparent;
		  __grandparent = __MINMAXHEAP_GRANDPARENT(__holeIndex);
		}
	  }
	  *(__first + __holeIndex) = _GLIBCXX_MOVE(___value);
    }

  /**
   *  @brief  Push an element onto a minmaxheap using comparison functor.
   *  @param  first  Start of minmaxheap.
   *  @param  last   End of minmaxheap + element.
   *  @param  comp   Comparison functor.
   *  @ingroup minmaxheap_algorithms
   *
   *  This operation pushes the element at last-1 onto the valid minmaxheap over the
   *  range [first,last-1).  After completion, [first,last) is a valid minmaxheap.
   *  Compare operations are performed using comp.
  */
  template<typename _RandomAccessIterator, typename _Compare>
    inline void
    push_minmaxheap(_RandomAccessIterator __first, _RandomAccessIterator __last,
	      _Compare __comp)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	  _ValueType;
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
	  _DistanceType;

      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap_pred(__first, __last - 1, __comp);

      _ValueType ___value = _GLIBCXX_MOVE(*(__last - 1));
      std::__push_minmaxheap(__first, _DistanceType((__last - __first) - 1),
		       _DistanceType(0), _GLIBCXX_MOVE(___value), __comp);
    }

#define __MINMAXHEAP_SECONDCHILD(X)			(2*(X+1))
#define __MINMAXHEAP_FIRSTGRANDCHILD(X)		(4*X+3)
#define __MINMAXHEAP_LARGESTCHILD(X, op)	(*(__first+X) op *(__first+X-1)?X:X-1) // Compares child at position X with child at position X-1
#define __MINMAXHEAP_LARGESTNODE(X,Y, op)	(*(__first+X) op *(__first+Y)?X:Y)		// Compares node at position X with node at position Y
#define __MINMAXHEAP_FULLGRANDFAMLENGTH(L)	((L-3)/4)			// Returns the length of the heap for which all nodes are guaranteed to have 4 grand children

  template<typename _RandomAccessIterator, typename _Distance, typename _Tp>
    void
    __adjust_minmaxheap(_RandomAccessIterator __first, _Distance __holeIndex,
		  _Distance __len, _Tp ___value)
    {
	  _Distance fullGrandFamLength = __MINMAXHEAP_FULLGRANDFAMLENGTH(__len);
	  _Distance level = __MINMAXHEAP_LEVEL(__holeIndex);
	  if ((level &1) == 0)	// Max level
	  {
		  const _Distance __topIndex = __holeIndex;

		  _Distance __secondChild = __holeIndex;
		  while (__secondChild < fullGrandFamLength)
		{
			  // Determine largest child
			  // First, determine second child
		  __secondChild = __MINMAXHEAP_SECONDCHILD(__secondChild);
			  // Then, check all 4 grand children of __secondChild and (__secondChild-1)
		  __secondChild = __MINMAXHEAP_LARGESTNODE(__MINMAXHEAP_LARGESTCHILD(__MINMAXHEAP_SECONDCHILD(__secondChild), >), __MINMAXHEAP_LARGESTCHILD(__MINMAXHEAP_SECONDCHILD(__secondChild-1), >), >);
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
		  __holeIndex = __secondChild;
		}
		  // From this point on, we are sure that there aren't 4 grand children (so 3 or less).

		  // Take care of the ordering of the last layers
		  // Move the hole to the largest from the children plus grand children
		  // If the hole went to one of the grand children, check with its direct parent
		  // The *largest* of the children and grand children will be put in __secondChild.
		  __secondChild = __MINMAXHEAP_SECONDCHILD(__holeIndex);
		  _Distance numChildren = __len - __secondChild + 1;	// Interpret as value between 0 and 2
		  // Do nothing if there are zero children; the hole ended on a final level
		  if (numChildren == 1)	// If there's only one child, it's simple: swap it with the hole
		  {
			  __secondChild--;
			  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
			  __holeIndex = __secondChild;
		  }
		  else if (numChildren >= 2)
		  {
			  _Distance numGrandChildren = (__len - __MINMAXHEAP_FIRSTGRANDCHILD(__holeIndex)); // Interpret between 0 and 4
			  __secondChild = __MINMAXHEAP_LARGESTNODE(__secondChild, __secondChild-1, >);
			  if (numGrandChildren > 0)
			  {
				  // Determine largest between children and grand child and swap hole
				  if (numGrandChildren == 1)
					  __secondChild = __MINMAXHEAP_LARGESTNODE(__secondChild, __len-1, >);	// The last and only end grand child could be the largest
				  else
				  {
					  _Distance largestFirstGrandFam = __MINMAXHEAP_LARGESTNODE(__len-numGrandChildren, __len-numGrandChildren+1, >);
					  if (numGrandChildren == 2)
						  __secondChild = __MINMAXHEAP_LARGESTNODE(__secondChild, largestFirstGrandFam, >);
					  else	// 3 nodes
						  __secondChild = __MINMAXHEAP_LARGESTNODE(__secondChild, __MINMAXHEAP_LARGESTNODE(largestFirstGrandFam, __len-1, >), >);
				  }
			  }
			  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
			  __holeIndex = __secondChild;
		  }

		  std::__push_minmaxheap(__first, __holeIndex, __topIndex,
				   _GLIBCXX_MOVE(___value));
	  }
	  else	// Min-level
	  {
		  const _Distance __topIndex = __holeIndex;

		  _Distance __secondChild = __holeIndex;
		  while (__secondChild < fullGrandFamLength)
		{
			  // Determine largest child
			  // First, determine second child
		  __secondChild = __MINMAXHEAP_SECONDCHILD(__secondChild);
			  // Then, check all 4 grand children of __secondChild and (__secondChild-1)
		  __secondChild = __MINMAXHEAP_LARGESTNODE(__MINMAXHEAP_LARGESTCHILD(__MINMAXHEAP_SECONDCHILD(__secondChild), <), __MINMAXHEAP_LARGESTCHILD(__MINMAXHEAP_SECONDCHILD(__secondChild-1), <), <);
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
		  __holeIndex = __secondChild;
		}
		  // Take care of the ordering of the last layers
		  // Move the hole to the largest from the children plus grand children
		  // If the hole went to one of the grand children, check with its direct parent
		  // The *largest* of the children and grand children will be put in __secondChild.
		  __secondChild = __MINMAXHEAP_SECONDCHILD(__holeIndex);
		  _Distance numChildren = __len - __secondChild + 1;	// Interpret as value between 0 and 2
		  // Do nothing if there are zero children; the hole ended on a final level
		  if (numChildren == 1)	// If there's only one child, it's simple: swap it with the hole
		  {
			  __secondChild--;
			  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
			  __holeIndex = __secondChild;
		  }
		  else if (numChildren >= 2)
		  {
			  _Distance numGrandChildren = (__len - __MINMAXHEAP_FIRSTGRANDCHILD(__holeIndex)); // Interpret between 0 and 4
			  __secondChild = __MINMAXHEAP_LARGESTNODE(__secondChild, __secondChild-1, <);
			  if (numGrandChildren > 0)
			  {
				  // Determine largest between children and grand child and swap hole
				  if (numGrandChildren == 1)
					  __secondChild = __MINMAXHEAP_LARGESTNODE(__secondChild, __len-1, <);	// The last and only end grand child could be the largest
				  else
				  {
					  _Distance largestFirstGrandFam = __MINMAXHEAP_LARGESTNODE(__len-numGrandChildren, __len-numGrandChildren+1, <);
					  if (numGrandChildren == 2)
						  __secondChild = __MINMAXHEAP_LARGESTNODE(__secondChild, largestFirstGrandFam, <);
					  else	// 3 nodes
						  __secondChild = __MINMAXHEAP_LARGESTNODE(__secondChild, __MINMAXHEAP_LARGESTNODE(largestFirstGrandFam, __len-1, <), <);
				  }
			  }
			  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
			  __holeIndex = __secondChild;
		  }

		  std::__push_minmaxheap(__first, __holeIndex, __topIndex,
				   _GLIBCXX_MOVE(___value));	  }
    }

  template<typename _RandomAccessIterator>
    inline void
    __pop_minmaxheap_max(_RandomAccessIterator __first, _RandomAccessIterator __last,
	       _RandomAccessIterator __result)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	_ValueType;
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
	_DistanceType;

      _ValueType ___value = _GLIBCXX_MOVE(*__result);
      *__result = _GLIBCXX_MOVE(*__first);
      std::__adjust_minmaxheap(__first, _DistanceType(0),
			 _DistanceType(__last - __first),
			 _GLIBCXX_MOVE(___value));
    }

  template<typename _RandomAccessIterator>
    inline void
    __pop_minmaxheap_min(_RandomAccessIterator __first, _RandomAccessIterator __last,
	       _RandomAccessIterator __result)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	_ValueType;
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
	_DistanceType;

      _ValueType ___value = _GLIBCXX_MOVE(*__result);	// Copy the value where the hole will be moved to
      // Determine min node
      const _DistanceType __oldlen = __last - __first + 1;
      _DistanceType __holeIndex(0);	// By default, e.g., if __oldlen == 1
      if (__oldlen == 2)
    	  __holeIndex = 1;	// Only one child on the first min level
      else if (__oldlen > 2)
    	  __holeIndex = __MINMAXHEAP_LARGESTNODE(1, 2, <);	// Determine the smallest child

      *__result = _GLIBCXX_MOVE(*(__first + __holeIndex));
      std::__adjust_minmaxheap(__first, __holeIndex,
			 _DistanceType(__last - __first),
			 _GLIBCXX_MOVE(___value));
    }

  /**
   *  @brief  Pop an element off a minmaxheap.
   *  @param  first  Start of minmaxheap.
   *  @param  last   End of minmaxheap.
   *  @ingroup minmaxheap_algorithms
   *
   *  This operation pops the top of the minmaxheap.  The elements first and last-1
   *  are swapped and [first,last-1) is made into a minmaxheap.
  */
  template<typename _RandomAccessIterator>
    inline void
    pop_minmaxheap_max(_RandomAccessIterator __first, _RandomAccessIterator __last)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	_ValueType;

      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_function_requires(_LessThanComparableConcept<_ValueType>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap(__first, __last);

      --__last;
      std::__pop_minmaxheap_max(__first, __last, __last);
    }

  template<typename _RandomAccessIterator>
    inline void
    pop_minmaxheap_min(_RandomAccessIterator __first, _RandomAccessIterator __last)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	_ValueType;

      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_function_requires(_LessThanComparableConcept<_ValueType>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap(__first, __last);

      --__last;
      std::__pop_minmaxheap_min(__first, __last, __last);
    }

#define __MINMAXHEAP_LARGESTCHILD_COMPG(X)	(__comp(*(__first+X), *(__first+X-1))?X-1:X)
#define __MINMAXHEAP_LARGESTCHILD_COMPL(X)	(__comp(*(__first+X), *(__first+X-1))?X:X-1)
#define __MINMAXHEAP_LARGESTNODE_COMPG(X,Y)	(__comp(*(__first+X), *(__first+Y))?Y:X)
#define __MINMAXHEAP_LARGESTNODE_COMPL(X,Y)	(__comp(*(__first+X), *(__first+Y))?X:Y)

  // Returns the index of the smallest element
  template<typename _RandomAccessIterator>
    inline _RandomAccessIterator
    minmaxheap_min(_RandomAccessIterator __first, _RandomAccessIterator __last)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
      _DistanceType;

      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_function_requires(_LessThanComparableConcept<_ValueType>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap(__first, __last);

      // Determine min node
      const _DistanceType __len = __last - __first;
      if (__len == 2)
    	  return (__first + 1);	// Only one child on the first min level
      else if (__len > 2)
    	  return __first + __MINMAXHEAP_LARGESTNODE(1, 2, <);	// Determine the smallest child

      return __first;	// If __len <= 1
   }

  // Returns the index of the smallest element
  template<typename _RandomAccessIterator, typename _Compare>
    inline _RandomAccessIterator
    minmaxheap_min(_RandomAccessIterator __first, _RandomAccessIterator __last, _Compare __comp)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
      _DistanceType;

      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_function_requires(_LessThanComparableConcept<_ValueType>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap(__first, __last);

      // Determine min node
      const _DistanceType __len = __last - __first;
      if (__len == 2)
    	  return (__first + 1);	// Only one child on the first min level
      else if (__len > 2)
    	  return __first + __MINMAXHEAP_LARGESTNODE_COMPL(1, 2);	// Determine the smallest child

      return __first;	// If __len <= 1
   }

  template<typename _RandomAccessIterator, typename _Distance,
	   typename _Tp, typename _Compare>
    void
    __adjust_minmaxheap(_RandomAccessIterator __first, _Distance __holeIndex,
		  _Distance __len, _Tp ___value, _Compare __comp)
    {
	  _Distance fullGrandFamLength = __MINMAXHEAP_FULLGRANDFAMLENGTH(__len);
	  _Distance level = __MINMAXHEAP_LEVEL(__holeIndex);
	  if ((level &1) == 0)	// Max level
	  {
		  const _Distance __topIndex = __holeIndex;

		  _Distance __secondChild = __holeIndex;
		  while (__secondChild < fullGrandFamLength)
		{
		  __secondChild = __MINMAXHEAP_SECONDCHILD(__secondChild);
		  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPG(__MINMAXHEAP_LARGESTCHILD_COMPG(__MINMAXHEAP_SECONDCHILD(__secondChild)), __MINMAXHEAP_LARGESTCHILD_COMPG(__MINMAXHEAP_SECONDCHILD(__secondChild-1)));
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
		  __holeIndex = __secondChild;
		}
		  __secondChild = __MINMAXHEAP_SECONDCHILD(__holeIndex);
		  _Distance numChildren = __len - __secondChild + 1;
		  if (numChildren == 1)
		  {
			  __secondChild--;
			  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
			  __holeIndex = __secondChild;
		  }
		  else if (numChildren >= 2)
		  {
			  _Distance numGrandChildren = (__len - __MINMAXHEAP_FIRSTGRANDCHILD(__holeIndex));
			  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPG(__secondChild, __secondChild-1);
			  if (numGrandChildren > 0)
			  {
				  if (numGrandChildren == 1)
					  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPG(__secondChild, __len-1);
				  else
				  {
					  _Distance largestFirstGrandFam = __MINMAXHEAP_LARGESTNODE_COMPG(__len-numGrandChildren, __len-numGrandChildren+1);
					  if (numGrandChildren == 2)
						  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPG(__secondChild, largestFirstGrandFam);
					  else
						  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPG(__secondChild, __MINMAXHEAP_LARGESTNODE_COMPG(largestFirstGrandFam, __len-1));
				  }
			  }
			  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
			  __holeIndex = __secondChild;
		  }

		  std::__push_minmaxheap(__first, __holeIndex, __topIndex,
				   _GLIBCXX_MOVE(___value), __comp);
	  }
	  else
	  {
		  const _Distance __topIndex = __holeIndex;

		  _Distance __secondChild = __holeIndex;
		  while (__secondChild < fullGrandFamLength)
		{
		  __secondChild = __MINMAXHEAP_SECONDCHILD(__secondChild);
		  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPL(__MINMAXHEAP_LARGESTCHILD_COMPL(__MINMAXHEAP_SECONDCHILD(__secondChild)), __MINMAXHEAP_LARGESTCHILD_COMPL(__MINMAXHEAP_SECONDCHILD(__secondChild-1)));
		  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
		  __holeIndex = __secondChild;
		}
		  __secondChild = __MINMAXHEAP_SECONDCHILD(__holeIndex);
		  _Distance numChildren = __len - __secondChild + 1;
		  if (numChildren == 1)
		  {
			  __secondChild--;
			  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
			  __holeIndex = __secondChild;
		  }
		  else if (numChildren >= 2)
		  {
			  _Distance numGrandChildren = (__len - __MINMAXHEAP_FIRSTGRANDCHILD(__holeIndex));
			  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPL(__secondChild, __secondChild-1);
			  if (numGrandChildren > 0)
			  {
				  if (numGrandChildren == 1)
					  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPL(__secondChild, __len-1);
				  else
				  {
					  _Distance largestFirstGrandFam = __MINMAXHEAP_LARGESTNODE_COMPL(__len-numGrandChildren, __len-numGrandChildren+1);
					  if (numGrandChildren == 2)
						  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPL(__secondChild, largestFirstGrandFam);
					  else
						  __secondChild = __MINMAXHEAP_LARGESTNODE_COMPL(__secondChild, __MINMAXHEAP_LARGESTNODE_COMPL(largestFirstGrandFam, __len-1));
				  }
			  }
			  *(__first + __holeIndex) = _GLIBCXX_MOVE(*(__first + __secondChild));
			  __holeIndex = __secondChild;
		  }

		  std::__push_minmaxheap(__first, __holeIndex, __topIndex,
				   _GLIBCXX_MOVE(___value), __comp);
	  }
    }

  template<typename _RandomAccessIterator, typename _Compare>
    inline void
    __pop_minmaxheap_max(_RandomAccessIterator __first, _RandomAccessIterator __last,
	       _RandomAccessIterator __result, _Compare __comp)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	_ValueType;
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
	_DistanceType;

      _ValueType ___value = _GLIBCXX_MOVE(*__result);
      *__result = _GLIBCXX_MOVE(*__first);
      std::__adjust_minmaxheap(__first, _DistanceType(0),
			 _DistanceType(__last - __first),
			 _GLIBCXX_MOVE(___value), __comp);
    }

  template<typename _RandomAccessIterator, typename _Compare>
    inline void
    __pop_minmaxheap_min(_RandomAccessIterator __first, _RandomAccessIterator __last,
	       _RandomAccessIterator __result, _Compare __comp)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	_ValueType;
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
	_DistanceType;

      _ValueType ___value = _GLIBCXX_MOVE(*__result);	// Copy the value where the hole will be moved to
      // Determine min node
      const _DistanceType __oldlen = __last - __first + 1;
      _DistanceType __holeIndex(0);	// By default, e.g., if __oldlen == 1
      if (__oldlen == 2)
    	  __holeIndex = 1;	// Only one child on the first min level
      else if (__oldlen > 2)
    	  __holeIndex = __MINMAXHEAP_LARGESTNODE_COMPL(1, 2);	// Determine the smallest child

      *__result = _GLIBCXX_MOVE(*(__first + __holeIndex));
      std::__adjust_minmaxheap(__first, __holeIndex,
			 _DistanceType(__last - __first),
			 _GLIBCXX_MOVE(___value), __comp);
    }

  /**
   *  @brief  Pop an element off a minmaxheap using comparison functor.
   *  @param  first  Start of minmaxheap.
   *  @param  last   End of minmaxheap.
   *  @param  comp   Comparison functor to use.
   *  @ingroup minmaxheap_algorithms
   *
   *  This operation pops the top of the minmaxheap.  The elements first and last-1
   *  are swapped and [first,last-1) is made into a minmaxheap.  Comparisons are
   *  made using comp.
  */
  template<typename _RandomAccessIterator, typename _Compare>
    inline void
    pop_minmaxheap_max(_RandomAccessIterator __first,
	     _RandomAccessIterator __last, _Compare __comp)
    {
      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap_pred(__first, __last, __comp);

      --__last;
      std::__pop_minmaxheap_max(__first, __last, __last, __comp);
    }

  template<typename _RandomAccessIterator, typename _Compare>
    inline void
    pop_minmaxheap_min(_RandomAccessIterator __first,
	     _RandomAccessIterator __last, _Compare __comp)
    {
      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap_pred(__first, __last, __comp);

      --__last;
      std::__pop_minmaxheap_min(__first, __last, __last, __comp);
    }
  /**
   *  @brief  Construct a minmaxheap over a range.
   *  @param  first  Start of minmaxheap.
   *  @param  last   End of minmaxheap.
   *  @ingroup minmaxheap_algorithms
   *
   *  This operation makes the elements in [first,last) into a minmaxheap.
  */
  template<typename _RandomAccessIterator>
    void
    make_minmaxheap(_RandomAccessIterator __first, _RandomAccessIterator __last)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	  _ValueType;
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
	  _DistanceType;

      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_function_requires(_LessThanComparableConcept<_ValueType>)
      __glibcxx_requires_valid_range(__first, __last);

      if (__last - __first < 2)
	return;

      const _DistanceType __len = __last - __first;
      _DistanceType __parent = (__len - 2) / 2;
      while (true)
	{
	  _ValueType ___value = _GLIBCXX_MOVE(*(__first + __parent));
	  std::__adjust_minmaxheap(__first, __parent, __len, _GLIBCXX_MOVE(___value));
	  if (__parent == 0)
	    return;
	  __parent--;
	}
    }

  /**
   *  @brief  Construct a minmaxheap over a range using comparison functor.
   *  @param  first  Start of minmaxheap.
   *  @param  last   End of minmaxheap.
   *  @param  comp   Comparison functor to use.
   *  @ingroup minmaxheap_algorithms
   *
   *  This operation makes the elements in [first,last) into a minmaxheap.
   *  Comparisons are made using comp.
  */
  template<typename _RandomAccessIterator, typename _Compare>
    void
    make_minmaxheap(_RandomAccessIterator __first, _RandomAccessIterator __last,
	      _Compare __comp)
    {
      typedef typename iterator_traits<_RandomAccessIterator>::value_type
	  _ValueType;
      typedef typename iterator_traits<_RandomAccessIterator>::difference_type
	  _DistanceType;

      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_requires_valid_range(__first, __last);

      if (__last - __first < 2)
	return;

      const _DistanceType __len = __last - __first;
      _DistanceType __parent = (__len - 2) / 2;
      while (true)
	{
	  _ValueType ___value = _GLIBCXX_MOVE(*(__first + __parent));
	  std::__adjust_minmaxheap(__first, __parent, __len, _GLIBCXX_MOVE(___value),
			     __comp);
	  if (__parent == 0)
	    return;
	  __parent--;
	}
    }

  /**
   *  @brief  Sort a minmaxheap.
   *  @param  first  Start of minmaxheap.
   *  @param  last   End of minmaxheap.
   *  @ingroup minmaxheap_algorithms
   *
   *  This operation sorts the valid minmaxheap in the range [first,last).
  */
  template<typename _RandomAccessIterator>
    void
    sort_minmaxheap(_RandomAccessIterator __first, _RandomAccessIterator __last)
    {
      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_function_requires(_LessThanComparableConcept<
	    typename iterator_traits<_RandomAccessIterator>::value_type>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap(__first, __last);

      while (__last - __first > 1)
	{
	  --__last;
	  std::__pop_minmaxheap_max(__first, __last, __last);
	}
    }

  /**
   *  @brief  Sort a minmaxheap using comparison functor.
   *  @param  first  Start of minmaxheap.
   *  @param  last   End of minmaxheap.
   *  @param  comp   Comparison functor to use.
   *  @ingroup minmaxheap_algorithms
   *
   *  This operation sorts the valid minmaxheap in the range [first,last).
   *  Comparisons are made using comp.
  */
  template<typename _RandomAccessIterator, typename _Compare>
    void
    sort_minmaxheap(_RandomAccessIterator __first, _RandomAccessIterator __last,
	      _Compare __comp)
    {
      // concept requirements
      __glibcxx_function_requires(_Mutable_RandomAccessIteratorConcept<
	    _RandomAccessIterator>)
      __glibcxx_requires_valid_range(__first, __last);
      __glibcxx_requires_heap_pred(__first, __last, __comp);

      while (__last - __first > 1)
	{
	  --__last;
	  std::__pop_minmaxheap_max(__first, __last, __last, __comp);
	}
    }

  }

#endif /* _STL_MINMAXHEAP_H */
