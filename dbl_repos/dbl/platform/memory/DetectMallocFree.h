/*
 * DetectMallocFree.h
 *
 * Include this header AS THE VERY FIRST HEADER IN YOUR FINAL PROGRAM FILE
 *
 * USE WITH CARE - USE ONLY FOR DEBUGGING PURPOSES
 *
 *  Created on: Aug 19, 2010
 *      Author: Erik Schuitema
 */

#ifndef DETECTMALLOCFREE_H_
#define DETECTMALLOCFREE_H_

#include <cstdlib>
#include <stdio.h>

// Define alternative mem functions with reporting in namespace std

static bool gReportMallocFree=false;
// Turn reporting on or off
void enableReportMallocFree(bool enabled)	{gReportMallocFree = enabled;}


namespace reportMallocFree
{

inline void *report_malloc (size_t __size)
{
	if (gReportMallocFree)
		printf("*** Call to malloc() detected ***\n");
	return malloc(__size);
}

inline void *report_realloc (void *__ptr, size_t __size)
{
	if (gReportMallocFree)
		printf("*** Call to realloc() detected ***\n");
	return realloc(__ptr, __size);
}

inline void report_free (void *__ptr)
{
	if (gReportMallocFree)
		printf("*** Call to free() detected ***\n");
	free(__ptr);
}
}

//extern "C++" int report_posix_memalign (void **__memptr, size_t __alignment, size_t __size) __THROW;
extern "C" int report_posix_memalign (void **__memptr, size_t __alignment, size_t __size)
{
	if (gReportMallocFree)
		printf("*** Call to posix_memalign() detected ***\n");
#ifdef _MSC_VER
	*__memptr = _aligned_malloc(__size, __alignment);
	return !__memptr;
#else
	return posix_memalign(__memptr, __alignment, __size);
#endif
}

using namespace reportMallocFree;

// Include our namespace in std, so that calls to std::malloc etc. will also be caught
namespace std { using namespace reportMallocFree;}

// Define override macros
#define malloc(xyz) report_malloc(xyz)
#define realloc(xyz, yuv) report_realloc(xyz, yuv)
#define posix_memalign(abc, xyz, yuv) report_posix_memalign(abc, xyz, yuv)
#define free(xyz) report_free(xyz)

#endif /* DETECTMALLOCFREE_H_ */
