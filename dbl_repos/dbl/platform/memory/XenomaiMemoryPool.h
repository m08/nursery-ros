/*
 * XenomaiMemoryPool.h
 *
 *  Created on: Aug 17, 2010
 *      Author: erik
 */

#ifndef XENOMAIMEMORYPOOL_H_
#define XENOMAIMEMORYPOOL_H_

#include <native/heap.h>

class CXenoMemoryPool: public CMemoryPool
{
	protected:
		RT_HEAP	mHeap;

		std::string			getPIDStr()
		{
			std::stringstream pidstr;
			pidstr << "[" << getpid() << "]";
			return pidstr.str();
		}

	public:
		// poolsize is the memory pool size in bytes
		CXenoMemoryPool(size_t poolsize, const char* name=NULL):
			CMemoryPool(poolsize)
		{
			// Lock pages before creating the heap
			mlockall(MCL_CURRENT | MCL_FUTURE);

			// Create heap
			if (name != NULL)
				rt_heap_create(&mHeap, name, poolsize, 0);
			else
				// Name is compulsory, so create name from process ID
				rt_heap_create(&mHeap, getPIDStr().c_str(), poolsize, 0);

		}
		~CXenoMemoryPool()						{rt_heap_delete(&mHeap);}

		virtual void* alloc(size_t nbytes)
		{
			void *res;
			rt_heap_alloc(&mHeap, nbytes, TM_INFINITE, &res);
			return res;
		}

		virtual void dealloc(void* p)
		{
			rt_heap_free(&mHeap, p);
		}

		virtual size_t size()
		{
			RT_HEAP_INFO info;
			rt_heap_inquire(&mHeap, &info);
			return info.heapsize;
		}

		virtual size_t usedMem()
		{
			RT_HEAP_INFO info;
			rt_heap_inquire(&mHeap, &info);
			return info.usedmem;
		}

		virtual size_t freeMem()
		{
			RT_HEAP_INFO info;
			rt_heap_inquire(&mHeap, &info);
			return info.usablemem;
		}

};

#endif /* XENOMAIMEMORYPOOL_H_ */
