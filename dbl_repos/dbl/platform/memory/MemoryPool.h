/*
 * MemoryPool.h
 *
 *  Created on: Aug 17, 2010
 *      Author: erik
 */

#ifndef MEMORYPOOL_H_
#define MEMORYPOOL_H_

#ifdef WIN32
  #include <win32_compat.h>
#endif

#include <map>
#include <assert.h>
#ifdef NDEBUG
	// No debug
	#define assert_fail_txt(X)
#else
	#define assert_fail_txt(X) __assert_fail(X, __FILE__, __LINE__, __ASSERT_FUNCTION)
#endif

// Memory pool definition. Default implementation uses malloc/free for testing.
class CMemoryPool
{
	protected:
		size_t mPoolSize;
		size_t mUsedMem;
		std::map<void*, int>	mUsage;

	public:
		// poolsize is the memory pool size in bytes
		CMemoryPool(size_t poolsize)		{mPoolSize = poolsize; mUsedMem=0;}
		virtual ~CMemoryPool()				{assert(mUsedMem == 0);}

		virtual void* alloc(size_t nbytes)
		{
			void* res = NULL;
			// Check for space
			if (freeMem() >= nbytes)
				res = malloc(nbytes);
			else
				assert_fail_txt("Pool size must be big enough");

			if (res != NULL)
			{
				mUsedMem += nbytes;
				mUsage[res] = nbytes;
			}
			return res;
		}

		virtual void dealloc(void* p)
		{
			if (mUsage.find(p) != mUsage.end())
			{
				mUsedMem -= mUsage[p];
				mUsage.erase(p);
				free(p);
			}
			else
			{
				// Throw exception - attempt to deallocate mem that was not allocated here
				assert_fail_txt("Deallocation pointer must be valid");
			}
		}

		virtual size_t size()
		{
			return mPoolSize;
		}

		virtual size_t usedMem()
		{
			return mUsedMem;
		}

		virtual size_t freeMem()
		{
			return mPoolSize - mUsedMem;
		}
};

#endif /* MEMORYPOOL_H_ */
