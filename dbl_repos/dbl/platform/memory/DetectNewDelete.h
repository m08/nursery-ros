/*
 * DetectNewDelete.h
 *
 * Include this header AS THE VERY FIRST HEADER IN YOUR FINAL PROGRAM FILE
 *
 * USE WITH CARE - USE ONLY FOR DEBUGGING PURPOSES
 *
 *  Created on: Aug 19, 2010
 *      Author: Erik Schuitema
 */

#ifndef DETECTNEWDELETE_H_
#define DETECTNEWDELETE_H_

#include <new>
#include <cstdlib>
#include <stdio.h>

static bool gReportNewDelete=false;
// Turn reporting on or off
void enableReportNewDelete(bool enabled)	{gReportNewDelete = enabled;}

// Override new and delete operator.
void* operator new (size_t size)
{
	void *p=malloc(size);
	if (gReportNewDelete)
		printf("*** Call to operator new detected ***\n");
	return p;
}

void operator delete (void *p)
{
	if (gReportNewDelete)
		printf("*** Call to operator delete detected ***\n");
	free(p);
}

#endif /* DETECTNEWDELETE_H_ */
