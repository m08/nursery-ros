/*
 * heaptest.cpp
 *
 *  Created on: Sep 9, 2010
 *      Author: Erik Schuitema
 */

#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <randomc.h>
#include <stl_minmaxheap.h>
#include <algorithm>
#include <sys/time.h>
#include <agentgps.h>

// Define some possible heap types
// A heap type class should have a constructor taking a TRanrotBGenerator reference,
// a static compare(x, y) function, an int() cast operator and operators == and !=.

class CHeapTypeDDouble
{
	private:
		double A, B;
	public:
		CHeapTypeDDouble(TRanrotBGenerator& rg):
			A(rg.Random()),
			B(rg.Random())
		{}
		static bool compare(const CHeapTypeDDouble& x, const CHeapTypeDDouble& y)
		{
			if ((x.A+x.B) < (y.A+y.B))
				return true;
			else
				return false;
		}
		operator int ()	const {return 5000*(A + B);}
	    bool operator== (const CHeapTypeDDouble& y)	{return (A == y.A) && (B == y.B);}
	    bool operator!= (const CHeapTypeDDouble& y)	{return (A != y.A) || (B != y.B);}
};

class CHeapTypeInt
{
	private:
		int A;
	public:
		CHeapTypeInt(TRanrotBGenerator& rg):
			A(rg.IRandom(-5000, 5000))
		{}
		static bool compare(const CHeapTypeInt& x, const CHeapTypeInt& y)
		{
			if (x.A < y.A)
				return true;
			else
				return false;
		}
		operator int () const	{return A;}
	    bool operator== (const CHeapTypeInt& y)	{return A == y.A;}
	    bool operator!= (const CHeapTypeInt& y)	{return A != y.A;}
};

// Define the heap type that we want to use
typedef CHeapTypeDDouble CHeapType;

void printheap(const std::vector<CHeapType>& data)
{
	for (unsigned int i=0, l=0, layerdepth=1; i<data.size(); i++)
	{
		printf("%d ", (int)data[i]);
		l++;
		if (l == layerdepth)
		{
			l=0;
			layerdepth *= 2;
			printf("\n");
		}
	}
	printf("\n");
}
void printvector(const std::vector<CHeapType>& data)
{
	for (unsigned int i=0; i<data.size(); i++)
		printf("%d ", (int)data[i]);
	printf("\n");
}

void printvector_reverse(const std::vector<CHeapType>& data)
{
	for (int i=data.size()-1; i>=0; i--)
		printf("%d ", (int)data[i]);
	printf("\n");
}

void printheap_popmin(std::vector<CHeapType>& data)
{
	while (data.size() > 0)
	{
		pop_minmaxheap_min(data.begin(), data.end(), CHeapType::compare);
		printf("%d ", (int)data[data.size()-1]);
		// The hole is always moved to the back, so pop the back of the vector
		data.pop_back();
	}
	printf("\n");
}

void printheap_popmax(std::vector<CHeapType>& data)
{
	while (data.size() > 0)
	{
		pop_minmaxheap_max(data.begin(), data.end(), CHeapType::compare);
		printf("%d ", (int)data[data.size()-1]);
		// The hole is always moved to the back, so pop the back of the vector
		data.pop_back();
	}
	printf("\n");
}
void makeheap(std::vector<CHeapType>& data, const std::vector<CHeapType>& rawdata)
{
	data = rawdata;
	make_minmaxheap(data.begin(), data.end(), CHeapType::compare);
}

void makeheap_insert(std::vector<CHeapType>& data, const std::vector<CHeapType>& rawdata)
{
	data.clear();
	for (unsigned int i=0; i<rawdata.size(); i++)
	{
		data.push_back(rawdata[i]);
		push_minmaxheap(data.begin(), data.end(), CHeapType::compare);
	}
}

// Returns success
bool comparesort_pop_min(std::vector<CHeapType>& data, const std::vector<CHeapType>& sorteddata)
{
	std::vector<CHeapType>::const_iterator it = sorteddata.begin();
	while (data.size() > 0)
	{
		pop_minmaxheap_min(data.begin(), data.end(), CHeapType::compare);
		// The hole is always moved to the back, so pop the back of the vector
		if (data.back() != *it)
		{
			printf("comparesort_pop_min failed while popping min element %d (!= %d). Sorted data:\n", (int)data.back(), (int)*it);
			printvector(sorteddata);
			return false;
		}
		it++;
		data.pop_back();
	}
	return true;
}

// Returns success
bool comparesort_pop_max(std::vector<CHeapType>& data, const std::vector<CHeapType>& sorteddata)
{
	std::vector<CHeapType>::const_reverse_iterator it = sorteddata.rbegin();
	while (data.size() > 0)
	{
		pop_minmaxheap_max(data.begin(), data.end(), CHeapType::compare);
		// The hole is always moved to the back, so pop the back of the vector
		if (data.back() != *it)
		{
			printf("comparesort_pop_min failed while popping min element %d (!= %d). Sorted data:\n", (int)data.back(), (int)*it);
			printvector(sorteddata);
			return false;
		}
		it++;
		data.pop_back();
	}
	return true;
}

// getTimeDiff(): helper function
double getTimeDiff(timeval* start, timeval* end)
{
	int s = end->tv_sec - start->tv_sec;
	int us = end->tv_usec - start->tv_usec;
	if (us < 0)
	{
		s--;
		us += (int)1E6;
	}
	return (double)s + ((double)us)/1E6;
}

// Usage: llrtest [filename] [testpoint index]
// filename defaults to "leotest.txt"
int main(int argc, char** argv)
{
	// Heap debugging
	// Heap data

	// Usage
	if (argc < 4)
	{
		printf("Usage:\n\theaptest [randomseed] [maxheapsize] [numiterations]\n");
		return -1;
	}

	int randomseed = atoi(argv[1]);
	gRanrotB.RandomInit(randomseed);
	int maxHeapsize = atoi(argv[2]);
	int numIterations = atoi(argv[3]);


	// Iterate test
	// Report every percentage of progress
	int iterReport = std::max(numIterations/100, 1);
	for (int iIter=0; iIter<numIterations; iIter++)
	{
		if (iIter % iterReport == 0)
		{
			printf("%02d%% ", 100*iIter/numIterations);
			fflush(stdout);
		}

		for (int iHeapsize=0; iHeapsize<=maxHeapsize; iHeapsize++)
		{
			// Data vectors
			std::vector<CHeapType> rawdata;		// Unsorted, un'heaped' data
			std::vector<CHeapType> sorteddata;	// Sorted using regular sort
			std::vector<CHeapType> heapdata;		// Data used by the min-max-heap
			// Make raw data
			for (int i=0; i<iHeapsize; i++)
				rawdata.push_back(CHeapType(gRanrotB));

			// Make sorted data
			sorteddata = rawdata;
			std::sort(sorteddata.begin(), sorteddata.end(), CHeapType::compare);
			//printvector(sorteddata);
			//printvector_reverse(sorteddata);

			// Test 1: make heap directly; compare while popping min
			//printf("Test 1: make_heap + compare sorted data while popping the min element...\n");
			makeheap(heapdata, rawdata);
			// Assert min and max element
			if (iHeapsize > 0)
			{
				assert(heapdata.front() == sorteddata.back());
				assert(*minmaxheap_min(heapdata.begin(), heapdata.end(), CHeapType::compare) == sorteddata.front());
			}
			if (!comparesort_pop_min(heapdata, sorteddata))
				return -1;

			// Test 2: make heap directly; compare while popping max
			//printf("Test 2: make_heap + compare sorted data while popping the max element...\n");
			makeheap(heapdata, rawdata);
			if (!comparesort_pop_max(heapdata, sorteddata))
				return -2;

			// Test 3: make heap directly; compare while popping min
			//printf("Test 3: push_heap + compare sorted data while popping the min element...\n");
			makeheap_insert(heapdata, rawdata);
			if (!comparesort_pop_min(heapdata, sorteddata))
				return -3;

			// Test 4: make heap directly; compare while popping max
			//printf("Test 4: push_heap + compare sorted data while popping the max element...\n");
			makeheap_insert(heapdata, rawdata);
			if (!comparesort_pop_max(heapdata, sorteddata))
				return -4;
		}
	}


/*
	// Speed test:
	// Make one heap and keep pushing and popping
	// Make raw data
	std::vector<double> heapdata;		// Data used by the min-max-heap
	for (int i=0; i<maxHeapsize; i++)
		heapdata.push_back(gRanrotB.Random());
	make_minmaxheap(heapdata.begin(), heapdata.end());
	//make_heap(heapdata.begin(), heapdata.end());

	// Start timing
	timeval tStart, tEnd;
	gettimeofday(&tStart, NULL);
	for (int iIter=0; iIter<numIterations; iIter++)
	{

		// Test 7: timing of push and pop
		// Pop min and max
		pop_minmaxheap_max(heapdata.begin(), heapdata.end());
		int max = heapdata.back();
		heapdata.pop_back();
		pop_minmaxheap_min(heapdata.begin(), heapdata.end());
		int min = heapdata.back();
		heapdata.pop_back();
		// Now insert again
		heapdata.push_back(max);
		push_minmaxheap(heapdata.begin(), heapdata.end());
		heapdata.push_back(min);
		push_minmaxheap(heapdata.begin(), heapdata.end());


		// Test 8: timing of push and pop of regular heap
		// Pop min and max
//		pop_heap(heapdata.begin(), heapdata.end());
//		int max = heapdata.back();
//		heapdata.pop_back();
//		pop_heap(heapdata.begin(), heapdata.end());
//		int max2 = heapdata.back();
//		heapdata.pop_back();
//		// Now insert again
//		heapdata.push_back(max);
//		push_heap(heapdata.begin(), heapdata.end());
//		heapdata.push_back(max2);
//		push_heap(heapdata.begin(), heapdata.end());

	}

	gettimeofday(&tEnd, NULL);
	// Report back the timing
	printf("Timing test took %.4fs", getTimeDiff(&tStart, &tEnd));
*/

	// Agent priority queue test
	int					apqLength=4;
	CAgentPriorityQueue	apq;
	CAgentState			state;
	state.setNumStateVars(14);
	apq.setMaxLength(apqLength);
	const bool print=true;
	int numpushes = 2;
	printf("Filling agent priority queue with length %d:\n", apqLength);
	for (int i=0; i<10; i++)
	{
		if (apq.size() > 0)
		{
			if (print)
				printf("Popping top element %.3f\n", apq.top().priority);
			apq.pop();
		}
		for (int iPush=0; iPush<numpushes; iPush++)
		{
			double priority = gRanrotB.Random();
			state.setStateVar(0, priority);
			apq.push(state, priority);
			if (print)
			{
				printf("Added %.3f, priorities now between %.3f and %.3f (heap contents: ", priority, apq.top().priority, apq.back().priority);
				for (unsigned int iHE=0; iHE<apq.size(); iHE++)
					printf("%.3f ", apq.heapElement(iHE).priority);
				printf(")\n");
			}
		}
	}
	printf("Priorities ended up between %.3f and %.3f", apq.top().priority, apq.back().priority);
	printf("\n");

	printf("\nTest succeeded! End of heap debugging.\n");
	return 0;
}
