#
# CMake include file for qt library
# Wouter Caarls <w.caarls@tudelft.nl>
#
# 29-03-2010 (wcaarls): Initial revision
#

IF (WITH_GUI)
  IF (WITH_FFMPEG)
    INCLUDE (${WORKSPACE_DIR}/dbl/externals/qtffmpegwrapper/qtffmpegwrapper.cmake)
  ENDIF (WITH_FFMPEG)

  INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/platform/gui/qt)
  TARGET_LINK_LIBRARIES(${TARGET} qt)

  # GLWidget needs OpenGL
  FIND_PACKAGE (OpenGL)
  IF ( OPENGL_FOUND)
         INCLUDE_DIRECTORIES( ${OPENGL_INCLUDE_DIR} )
  ENDIF( OPENGL_FOUND)

  # Qt include dir
  FIND_PACKAGE(Qt5Widgets QUIET)
  IF(Qt5Widgets_FOUND)
    INCLUDE_DIRECTORIES(${Qt5Widgets_INCLUDES})
    INCLUDE_DIRECTORIES(${Qt5Widgets_INCLUDE_DIRS})
    ADD_DEFINITIONS(${Qt5Widgets_DEFINITIONS})
  ELSE(Qt5Widgets_FOUND)
    FIND_PACKAGE (Qt4 REQUIRED)
    INCLUDE_DIRECTORIES( ${QT_INCLUDE_DIR} )
  ENDIF(Qt5Widgets_FOUND)

  IF (NOT __QT_CMAKE_INCLUDED)
    SET(__QT_CMAKE_INCLUDED 1)

    ADD_SUBDIRECTORY(${WORKSPACE_DIR}/dbl/platform/gui/qt ${WORKSPACE_DIR}/build/qt/${CMAKE_BUILD_TYPE}${COMPILER_VERSION})
  ENDIF (NOT __QT_CMAKE_INCLUDED)
ENDIF (WITH_GUI)

