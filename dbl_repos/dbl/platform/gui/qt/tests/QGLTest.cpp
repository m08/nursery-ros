/*
 * QGLTest.cpp
 *
 *  Created on: Apr 29, 2010
 *      Author: Erik Schuitema
 */

#include "QGLTestApp.h"


int main(int argc, char** argv)
{
	QApplication app(argc, argv);
	CQGLTestApp *dialog = new CQGLTestApp;

	// Show dialog
	dialog->show();
	// Run app
	bool appResult = app.exec();
	//bool appResult = false;

	// Delete dialog when we're done
	delete dialog;
	// Return result
	return appResult;
}
