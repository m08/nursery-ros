/*
 * TwoLinkSimApp.cpp
 *
 *  Created on: Apr 8, 2009
 *      Author: Erik Schuitema
 */

// if we include <QtGui> there is no need to include every class used: <QString>, <QFileDialog>,...
#include <QtGui>
#include "QGLTestApp.h"

CQGLTestApp::CQGLTestApp(QWidget *parent)
{
	setupUi(this); // this sets up GUI
	setWindowFlags(Qt::WindowMaximizeButtonHint);
}

CQGLTestApp::~CQGLTestApp()
{
}

void CQGLTestApp::closeEvent(QCloseEvent *event)
{
	// Do something before closing
	event->accept();
}
