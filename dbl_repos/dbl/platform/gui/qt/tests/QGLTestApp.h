/*
 * QGLTestApp.h
 *
 *  Created on: Apr 8, 2009
 *      Author: Erik Schuitema
 */

#ifndef QGLTEST_H_
#define QGLTEST_H_

#include "ui_qgltest.h"

class CQGLTestApp : public QDialog, private Ui::Dialog
{
    Q_OBJECT

	protected:
		void closeEvent(QCloseEvent *event);

	public:
		CQGLTestApp(QWidget *parent = 0);
		~CQGLTestApp();
};



#endif /* TWOLINKSIM_H_ */
