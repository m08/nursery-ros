/*
 * LxRtSerial.cpp
 *
 *  Created on: Oct 10, 2008
 *      Author: erik
 */

#include "LxRtSerial.h"
#include <native/task.h>
#include <algorithm>
#include <rtdk.h>
#include <Log2.h>
//#include <sys/wait.h>

LxRtSerial::LxRtSerial(): mLog("LxRTSerial")
{
  mLog.setLevel(llDebug);
  // Set serial config to default values
  ser_config.config_mask        = 0xFFFF;
  //ser_config.baud_rate        = 57600;
  //ser_config.baud_rate        = 921600;
  ser_config.baud_rate          = 115200;
  ser_config.parity             = RTSER_NO_PARITY;
  ser_config.data_bits          = RTSER_8_BITS;
  ser_config.stop_bits          = RTSER_1_STOPB;
  ser_config.handshake          = RTSER_NO_HAND;
  ser_config.fifo_depth         = RTSER_DEF_FIFO_DEPTH; //RTSER_FIFO_DEPTH_8;
  ser_config.rx_timeout         = (nanosecs_rel_t)1E9;//RTSER_DEF_TIMEOUT;  // default == infinite == blocking
  ser_config.tx_timeout         = (nanosecs_rel_t)1E9;//RTSER_DEF_TIMEOUT;  // default == infinite == blocking
  ser_config.event_timeout      = (nanosecs_rel_t)1E9;//RTSER_DEF_TIMEOUT;  // default == infinite == blocking
  ser_config.timestamp_history  = RTSER_RX_TIMESTAMP_HISTORY;
  ser_config.event_mask         = 0;//RTSER_EVENT_RXPEND;
}

LxRtSerial::~LxRtSerial()
{
}

bool LxRtSerial::port_open(const std::string& portname, LxSerial::PortType port_type)
{
  // Set port type
  set_port_type(port_type);

  // Open the real time device
  hPort = rt_dev_open( portname.c_str(), 0);
  if (hPort < 0)
  {
    mLogErrorLn(" Could not open serial port, aborting.");
    return false;
  }

  // Write the configuration to the device
  int ret = 0;
  ret = rt_dev_ioctl(hPort, RTSER_RTIOC_SET_CONFIG, &ser_config);
  if (ret)
  {
    mLogErrorLn("error while RTSER_RTIOC_SET_CONFIG, code " << ret);
    return false;
  }

  s_port_name = portname;

  mLogInfoLn("Opened serial port with name "<< portname);

  return true;
}

bool LxRtSerial::port_close()
{
  if (hPort == INVALID_DEVICE_HANDLE)
    return true;

  int ret,i=0;
  do
  {
    i++;
    ret = rt_dev_close(hPort);
    switch(-ret)
    {
    case EBADF:
      mLogErrorLn("While closing RT serial device -> invalid fd or context");
      break;
    case EAGAIN:
      mLogErrorLn("While closing RT serial device -> EAGAIN (%"<< i << " times)");
      rt_task_sleep(50000); // wait 50us
      break;
    case 0:
      // Everything went well
      break;
    default:
      mLogErrorLn("While closing RT serial device");
      break;
    }
  }
  while (ret == -EAGAIN && i < 10);

  hPort = INVALID_DEVICE_HANDLE;
  mLogInfoLn("Closed serial port with name "<< s_port_name);
  return true;
}

bool LxRtSerial::set_speed_int(const int baudrate)
{
  rtser_config new_config;
  new_config.config_mask  = RTSER_SET_BAUD;
  new_config.baud_rate  = baudrate;
  int ret = rt_dev_ioctl(hPort, RTSER_RTIOC_SET_CONFIG, &new_config);
  if (ret == 0)
  {
    mLogDebugLn("Set baudrate to " << baudrate << " for "<< s_port_name);
  }
  return ret == 0;
}

bool LxRtSerial::set_speed(LxSerial::PortSpeed baudrate )
{
  int baud;
  // Baud rate conversion from index to real baud rate :(
  switch (baudrate)
  {
  case S4000000:  baud = 4000000; break;
  case S3500000:  baud = 3500000; break;
  case S3000000:  baud = 3000000; break;
  case S2500000:  baud = 2500000; break;
  case S2000000:  baud = 2000000; break;
  case S1500000:  baud = 1500000; break;
  case S1152000:  baud = 1152000; break;
  case S1000000:  baud = 1000000; break;
  case S921600:   baud = 921600;  break;
  case S576000:   baud = 576000;  break;
  case S500000:   baud = 500000;  break;
  case S460800:   baud = 460800;  break;
  case S230400:   baud = 230400;  break;
  case S115200:   baud = 115200;  break;
  case S57600:    baud = 57600;   break;
  case S38400:    baud = 38400;   break;
  case S19200:    baud = 19200;   break;
  case S9600:     baud = 9600;    break;
  case S4800:     baud = 4800;    break;
  case S2400:     baud = 2400;    break;
  case S1800:     baud = 1800;    break;
  case S1200:     baud = 1200;    break;
  case S600:      baud = 600;     break;
  case S300:      baud = 300;     break;
  case S200:      baud = 200;     break;
  case S150:      baud = 150;     break;
  case S134:      baud = 134;     break;
  case S110:      baud = 110;     break;
  case S75:       baud = 75;      break;
  case S50:       baud = 50;      break;
  default:        baud = 0;
  }

  rtser_config new_config;
  new_config.config_mask  = RTSER_SET_BAUD;
  new_config.baud_rate  = baud;
  int ret = rt_dev_ioctl(hPort, RTSER_RTIOC_SET_CONFIG, &new_config);
  if (ret == 0)
  {
    mLogDebugLn("Set baudrate to " << baud << " for "<< s_port_name);
  }
  return ret == 0;
}

bool LxRtSerial::set_fifo_depth(const int fifo_depth)
{
  rtser_config new_config;
  new_config.config_mask  = RTSER_SET_FIFO_DEPTH;
  new_config.fifo_depth  = fifo_depth;
  int ret = rt_dev_ioctl(hPort, RTSER_RTIOC_SET_CONFIG, &new_config);
  if (ret == 0)
  {
    mLogDebugLn("Set fifo depth to " << fifo_depth << " for "<< s_port_name);
  }
  return ret == 0;
}

int LxRtSerial::port_write(unsigned char* buffer, int numBytes)
{
  ssize_t written = rt_dev_write(hPort, buffer, numBytes);

  if(written < 0)
    mLogErrorLn("[LxRtSerial::port_write] Error " << written << " occured while writing to a device");

  mLogCrawlLn("writing data to port numBytes = " << numBytes << ",actually written: " << written);

  // Read echo from line and detect collisions if needed
  // Read in blocks of ECHO_READBACK_BUFFER_LENGTH to avoid the 'new' operator in RT tasks
  if (b_clear_echo)
  {
    unsigned char echo_buffer[ECHO_READBACK_BUFFER_LENGTH];
    int echoBytes = numBytes;
    while (echoBytes > 0)
    {
      // Read (part of) the write echo
      mLogCrawlLn("read_back echo started for "<< std::min(echoBytes, ECHO_READBACK_BUFFER_LENGTH) << " bytes");
      int nBytesRead = port_read(echo_buffer, std::min(echoBytes, ECHO_READBACK_BUFFER_LENGTH));  // Blocking call: always wait for the echo

      if(nBytesRead < 0)
      {
        mLogErrorLn("port_write: echo read back port_read returned "<< nBytesRead << " (errno says \" "<< strerror(-nBytesRead) <<" \")");
        return nBytesRead;
      }
      else
      {
        // Check the result
        if ((nBytesRead > 0) && (memcmp(buffer + numBytes - echoBytes, echo_buffer, nBytesRead)!=0))
        {
          mLogErrorLn("Collision detected! printing sent bytes followed by the read back bytes:");
          for (int i=0;i<nBytesRead;i++)
            mLogError( std::setw (2) << std::uppercase << std::hex <<(int)buffer[i] << " ");
          mLogErrorLn("("<< numBytes << " bytes written)");
          for (int i=0;i<nBytesRead;i++)
            mLogError( std::setw (2) << std::uppercase << std::hex << (int)echo_buffer[i] << " ");
          mLogErrorLn( std::dec << "("<< nBytesRead << " bytes read)");
          return COLLISION_DETECT_ERROR;
        }
      }

      echoBytes -= nBytesRead;
    }

    // Check if we read all (is this useful..?)
    if (echoBytes != 0)
    {
      mLogWarningLn("no. of characters echoed not correct");
      return READ_ERROR;
    }
  }
  else{
    mLogErrorLn("echoless write not implemented yet");
  }

  mLogCrawlLn("port_write -> rt_dev_write() wrote "<< written <<" of "<< numBytes <<" bytes");
  return written;
}

int LxRtSerial::port_read(unsigned char* buffer, int numBytes) const
{
  ssize_t read = rt_dev_read(hPort, buffer, numBytes);
  return read;
}

int LxRtSerial::port_read(unsigned char* buffer, int numBytes, int seconds, int microseconds)
{
  // This method sets the rx_timeout value of the RT serial driver
  nanosecs_rel_t newTimeout = (nanosecs_rel_t)1E9*(nanosecs_rel_t)seconds + (nanosecs_rel_t)1E3*(nanosecs_rel_t)microseconds;
  if (ser_config.rx_timeout != newTimeout)
  {
    mLogDebugLn("setting new timeout parameters for "<< s_port_name);
    mLogDebugLn("Timeout in " << seconds << " seconds and " << microseconds << " microseconds");

    //adjust parameters in local config copy
    ser_config.config_mask = RTSER_SET_TIMEOUT_RX;
    ser_config.rx_timeout = newTimeout;
    ser_config.event_timeout = RTSER_TIMEOUT_NONE;

    //send config to device
    int ret = rt_dev_ioctl(hPort, RTSER_RTIOC_SET_CONFIG, &ser_config);
    if (ret != 0)
    {
      mLogErrorLn("port_read: ERROR in setting rx_timeout value: code " << ret);
      return -1;
    }
  }


  //now read the data
  ssize_t read = port_read(buffer, numBytes);// rt_dev_read(hPort, buffer, numBytes);
  //
  //  if (read ==  -ETIMEDOUT)  //we had a timeout, so check how many chars we received and read them from buffer.
  //  {
  //    struct rtser_event rx_event;
  //    int io_return = rt_dev_ioctl(hPort, RTSER_RTIOC_WAIT_EVENT, &rx_event);
  //    mLogWarningLn("io return "<< io_return << ", received only " << (int)rx_event.rx_pending << " of " << numBytes );
  //    rt_printf("rx_event.rx_pending = %d\n",rx_event.rx_pending);
  //    if(rx_event.rx_pending > 0)
  //      read = port_read(buffer, rx_event.rx_pending);// read the chars we did receive in the buffer.
  //    else
  //      read = 0;
  //  }

  return read;
}

void LxRtSerial::flush_buffer()
{
  mLogDebugLn("Flushing Fifo's");
  rt_dev_ioctl(hPort, RTIOC_PURGE, RTDM_PURGE_RX_BUFFER | RTDM_PURGE_TX_BUFFER);
}
