/*
 * LxRtSerial.h
 *
 *  Created on: Oct 10, 2008
 *      Author: erik
 */

#ifndef LXRTSERIAL_H_
#define LXRTSERIAL_H_

#include "LxSerial.h"
#include <rtdm/rtserial.h>
#include <native/mutex.h>
#include <errno.h>
#include <Log2.h>

#define ECHO_READBACK_BUFFER_LENGTH	1024

class LxRtSerial: public LxSerial
{
	public:
		CLog2			mLog;
		LxRtSerial();
		virtual ~LxRtSerial();

		virtual bool	port_open(const std::string& portname, LxSerial::PortType port_type);
		virtual bool	set_speed(LxSerial::PortSpeed baudrate );						// enumerated
		virtual bool	set_speed_int(const int baudrate);	// Set speed by integer value directly - UNPROTECTED!
		virtual bool	set_fifo_depth(const int fifo_depth);
		virtual bool	port_close();
		virtual int		port_read(unsigned char* buffer, int numBytes) const;
		virtual int 	port_read(unsigned char* buffer, int numBytes, int seconds, int microseconds);
		virtual int		port_write(unsigned char* buffer, int numBytes);
		virtual void	flush_buffer();

		//void	set_clear_echo(bool clear);										// clear echoed charackters from input and detect collisions on write
		//void	flush_buffer();													// flush input and output buffers
	protected:
		rtser_config	ser_config;	// Real-time serial port config. Reflects the config as lastly sent to the port.
};


#endif /* LXRTSERIAL_H_ */
