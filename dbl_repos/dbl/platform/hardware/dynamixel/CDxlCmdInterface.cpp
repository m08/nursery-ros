
#include "CDxlCmdInterface.h"
#include "DynamixelAsync.h"

// ********* Receiver interface **********//


//TODO: change name to something without dxl (CCmdInterface)


CDxlCmdInterface::CDxlCmdInterface(const std::string &queueInName, mqd_t queueOut): mLog("CDxlCmdInterface")
{
  mNumPendingMsgs = 0;
  mQueueOut       = queueOut;  // Just copy the sending queue
  mLastError      = 0;
  // Create receiving queue
  mQueueInName    = queueInName;

  mq_attr queueAttr;
  queueAttr.mq_flags    = 0;
  queueAttr.mq_maxmsg   = DXLGRP_ASYNC_QUEUE_LENGTH;  /* Max. # of messages on queue */
  queueAttr.mq_msgsize  = sizeof(CDxlQueueMsg);       /* Max. message size (bytes) */
  queueAttr.mq_curmsgs  = 0;                          /* # of messages currently in queue */

  // Note: O_NONBLOCK is not set, thus mq_send and mq_receive will wait for task completeness
  mQueueIn = mq_open(mQueueInName.c_str(), O_RDWR | O_CREAT, S_IRWXU, &queueAttr);
  if (mQueueIn == (mqd_t)(-1))
    mLogErrorLn("Could not create \""<< mQueueInName.c_str() << "\" message queue! Errno says "<< errno << " ("<< strerror(errno) << ").");
  else
    mLogDebugLn("CDxlCmdInterface queue \""<< mQueueInName.c_str() << "\" created.");
}

CDxlCmdInterface::~CDxlCmdInterface()
{
  // Unlink queue
  if (mq_unlink(mQueueInName.c_str()) != 0)
    mLogErrorLn("Failed to unlink \""<< mQueueInName.c_str() << "\" queue!");
  // Close queue
  if (mq_close(mQueueIn) != 0)
    mLogErrorLn("Failed to close \""<< mQueueInName.c_str() <<  "\" queue!");
}

int CDxlCmdInterface::receiveQueueMsg()
{
  CDxlQueueMsg msg;

  int ret = mq_receive(mQueueIn, (char*)&msg, sizeof(msg), NULL/*, &absTimeout*/);
  if (ret == sizeof(msg))
  {
    mLogCrawlLn("received mq_message of size "<< ret);
    mNumPendingMsgs--;

    if (msg.mpCommand != NULL)
    {
      // mResult shows errors which occured while executing the command
      mLogCrawlLn("msg.mpCommand->getResult() = " << msg.mpCommand->getResult());
      return msg.mpCommand->getResult();
    }
    else
    {
      mLogCrawlLn("msg.mpCommand does not exist");
      return 0;
    }
  }
  else
  {
    if (ret != -1)
    {
      mLogErrorLn("CDxlCmdInterface queue \""<< mQueueInName.c_str() <<  "\" received a weird msg with size "<< ret << " instead of size " << sizeof(msg));
      return -1;
    }
    else
    {
      mLogErrorLn("CDxlCmdInterface queue \""<< mQueueInName.c_str() <<  "\" receiveQeueMsg failed (mq_receive returned -1, errno= "<<errno << ")!");
      //usleep(1000000);
      return -1;
    }
  }
}

int CDxlCmdInterface::waitForTransactions()
{
  mLogCrawlLn("mNumPendingMsgs = " << mNumPendingMsgs);
  // Receive the result of all pending messages
  while (mNumPendingMsgs > 0)
  {
    int ret = receiveQueueMsg();
    if (ret != 0)
    {
      mLastError = ret;
      mLogErrorLn("CDxlCmdInterface(QueueIn=\""<< mQueueInName.c_str() << "\")::receiveQueueMsg() received "<< ret);
      return ret;
    }
  }
  mLogCrawlLn("mNumPendingMsgs at exit of waitForTransactions = "<< mNumPendingMsgs);
  return 0;
}

int CDxlCmdInterface::sendCommand(CCommand *command, unsigned int priority)
{
  CDxlQueueMsg msg;
  msg.mInstruction  = DXLGRP_ASYNC_INSTR_COMMAND;
  msg.mpCommand     = command;
  msg.mReturnQueue  = mQueueIn;
  int ret = mq_send(mQueueOut, (char*)&msg, sizeof(msg), priority);
  if (ret != -1) // change to ret == 0??
  {
    // RX-28 executes command received from the Main controller and returns the result to
    // the Main Controller. The returned data is called Status Packet.
    // Thus, we need to record number of sent packets in order to receive the same number of them back.
    mNumPendingMsgs++;
    return ret;
  }
  else
  {
    mLogErrorLn("CDxlCmdInterface::sendCommand failed!");
    return -1;
  }
}

