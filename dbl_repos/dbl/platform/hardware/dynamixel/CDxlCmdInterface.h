#ifndef __CDXLCOMMANDINTERFACE__
#define __CDXLCOMMANDINTERFACE__

#include <mqueue.h>
#include "CDxlCmd.h"
#include <Log2.h>

// Command interface
class CDxlCmdInterface
{
	protected:
		mqd_t					mQueueOut;
		std::string		mQueueInName;
		mqd_t					mQueueIn;
		int						mNumPendingMsgs;
		int 					mLastError;
		CLog2 				mLog;
		int						receiveQueueMsg();
	public:
		CDxlCmdInterface(const std::string &queueInName, mqd_t queueOut);
		~CDxlCmdInterface();

		// Be careful when sending command pointers:
		// Make sure that the command exists until it is executed!
		// Use waitForCommand() to check whether it is executed.
		int				sendCommand(CCommand *command, unsigned int priority = 0);
		int 			getLastError() { return mLastError; }
		int 			waitForTransactions();
};

#endif //__CDXLCOMMANDINTERFACE__
