/*
 * DynamixelAsync.h
 *
 *  Created on: Nov 27, 2008
 *      Author: Erik Schuitema
 *
 *      12-04-10 (evanbreda) Added some documentation
 *
 *
 *
 *      Basic Usage example:
 *
LxRtSerial              mSerialPort[NUM_SERIAL_PORTS];          //create Serial port object
CDxlGroupAsync          mDxlGroup[NUM_SERIAL_PORTS];            //create Dynamixel group
CDxlCmdInterface*       mDxlCmdInterface[NUM_SERIAL_PORTS];       //create Dynamixel Command Interface
CDxlConfig         dxlConfig;                    //create Dynamixel config object
mDxlGroup[iPort].addNewDynamixel(dxlConfig.setID(iID));          //add a Dynamixel to group
mSerialPort[iPort].port_open(devicename[iPort], LxSerial::RS485_SMSC)  //open the serial port
mSerialPort[iPort].set_speed_int(baudrate);                //set port speed
mDxlGroup[iPort].setSerialPort(&mSerialPort[iPort]);          //Add serial port to dynamixel group
mDxlGroup[iPort].init()                          //Init dynamixel group
mDxlCmdInterface[iPort] = mDxlGroup[iPort].createCommandInterface();  //create commandInterface from group

CDxlGrpCmd_getPosAll* cmd[nrOfPorts];                  //create new getPosAll object *
cmd[0] = new CDxlGrpCmd_getPosAll(&mDxlGroup[0]);            //create new getPosAll object

mDxlCmdInterface[iGroup]->sendCommand(cmd[0]);              //send getPosAll command
mDxlCmdInterface[iGroup]->waitForTransactions();            //wait for transactions to finish
pos = mDxlGroup[iGroup].getDynamixel(iDxl)->presentPos();        //get position of dynamixel IDxl

delete cmd[0];                              //throw away getPosAll object

mDxlGroup[iPort].deinit();                        //deinit dynamixel group
mSerialPort[iPort].port_close();                    //close the serial port
 */

#ifndef DYNAMIXELASYNC_H_
#define DYNAMIXELASYNC_H_


#include <mqueue.h>
#include <AbstractXenomaiTask.hpp>
#include <vector>

#include "dynamixel/Dynamixel.h"
#include "CDxlCmdInterface.h"
#include "CDxlCmd.h"
#include <rtdk.h>
#include <Log2.h>

#define DXLGRP_ASYNC_INSTR_NONE           0

#define DXLGRP_ASYNC_INSTR_TERMINATE      5
#define DXLGRP_ASYNC_INSTR_COMMAND        8
#define DXLGRP_ASYNC_INSTR_DUMMY          9  // Dummy instruction. For debugging purposes

#define DXLGRP_ASYNC_QUEUE_LENGTH         50  // Maximum number of dynamixel instructions that can be queued

// Set data request and data write command priorities in the queue
// Set the SENDDATA priority to a lower value than the GETDATA priority
// to achieve undisturbed priority of state request commands.
#define DXLGRP_ASYNC_GETDATA_PRIORITY     (MQ_PRIO_MAX-1)
#define DXLGRP_ASYNC_SENDDATA_PRIORITY    (DXLGRP_ASYNC_GETDATA_PRIORITY-1)


// **** Implementation of the Dxl worker thread ****//

// Forward declaration
class CDxlGroupAsync;
class CDxlCmdInterface;

class CDxlQueueMsg
{
public:
  int             mInstruction;
  CCommand        *mpCommand;
  mqd_t           mReturnQueue;
  CDxlQueueMsg()  {mpCommand = NULL;}
};

class CDxlWorkerThread: public AbstractXenomaiTask  // TODO: remove the dependency on Xenomai tasks at this level
{
protected:
  std::string     mName;
  mqd_t           mQueueIn;
  CLog2           mLog;
  void            run();
  bool            returnMessage(CDxlQueueMsg *msg);
public:
  CDxlWorkerThread(const std::string& name, const int priority, const std::string& serialQueueOutName);
  ~CDxlWorkerThread();
};


class CDxlGroupAsync: public CDxlGroup
{
protected:
  std::string                     mQueueOutName;
  mqd_t                           mQueueOut;
  CLog2                           mLog;
  std::vector<CDxlCmdInterface*>  mCommandInterfaces;

  // The worker thread that does the actual serial port work
  CDxlWorkerThread                *mWorkerThread;

public:
  CDxlGroupAsync();
  virtual ~CDxlGroupAsync();

  // Init and deinit. Make sure you set the configuration before calling init!
  virtual bool init();
  virtual bool deinit();


  /*
   * The command interface is an interface to the queue and it is the preferred way to talk to that queue
   * It will send posix messages to the worker thread that handles the sending and receiving of a serial port
   *
   */
  // createCommandInterface(): creates an interface for you.
  // Through such a command interface, you can send command packets. Rules:
  // 1) You should create a command interface for EACH thread that
  //    wants to communicate with this async dynamixel group.
  //    But you don't have to create it from the communicating thread itself
  //    (i.e., one thread may create all interfaces, as long as it creates one for each thread).
  // 2) This function itself is NOT thread safe.
  // 3) Call this function AFTER calling init(), otherwise the installed queueOut is non-existent
  // 4) The interface is destroyed by this class (CDxlGroupAsync), so don't destroy it yourself.
  CDxlCmdInterface*  createCommandInterface();
};

#endif /* DYNAMIXELASYNC_H_ */
