#
# CMake include file for dynamixel library
# Wouter Caarls <w.caarls@tudelft.nl>
#
# 30-03-2010 (wcaarls): Initial revision
#

INCLUDE (${WORKSPACE_DIR}/dbl/platform/hardware/serial/serial.cmake)
INCLUDE (${WORKSPACE_DIR}/dbl/platform/io/configuration/configuration.cmake)
INCLUDE (${WORKSPACE_DIR}/dbl/platform/threading/threading.cmake)

INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/platform/hardware/dynamixel)
INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/platform/hardware/dynamixel/dynamixel)
INCLUDE_DIRECTORIES(${WORKSPACE_DIR}/dbl/platform/hardware/dynamixel/3mxl)
TARGET_LINK_LIBRARIES(${TARGET} dynamixel)
ADD_DEPENDENCIES(${TARGET} dynamixel)

IF (NOT __DYNAMIXEL_CMAKE_INCLUDED)
  SET(__DYNAMIXEL_CMAKE_INCLUDED 1)

  ADD_SUBDIRECTORY(${WORKSPACE_DIR}/dbl/platform/hardware/dynamixel ${WORKSPACE_DIR}/build/dynamixel/${CMAKE_BUILD_TYPE}${COMPILER_VERSION})
ENDIF (NOT __DYNAMIXEL_CMAKE_INCLUDED)
