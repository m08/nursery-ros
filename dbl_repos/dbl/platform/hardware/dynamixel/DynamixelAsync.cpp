/*
 * DynamixelAsync.cpp
 *
 *  Created on: Nov 27, 2008
 *      Author: Erik Schuitema
 */

#include "DynamixelAsync.h"
#include <string.h>
#include <sstream>

//***********************************************************//
//******************* LxRtMqWorkerThread ********************//
//***********************************************************//

/**
 * Worker thread just executes CCommand->execute() in a queued fashion
 * It has no responsibility or any check other than listening to the queue
 */

//TODO: Rename to CCmdWorkerThread and put in seperate file
//TODO: remove all references to DXL serial (serialQueue -> queue)

#define PRINTBUFFERSIZE  10*1024*1024 // oversized size of the rt printf buffer

CDxlWorkerThread::CDxlWorkerThread(const std::string& name, const int priority, const std::string& serialQueueOutName):
AbstractXenomaiTask(name, priority),
mLog("CDxlWorkerThread")
{
  mName = name;
  // Open queues:
  // The serial queue-out is our queue-in! Open blocking and read-only.
  mQueueIn  = mq_open(serialQueueOutName.c_str(), O_RDONLY);
  if (mQueueIn == (mqd_t)(-1))
    mLogErrorLn("Could not open \"" << serialQueueOutName.c_str() << "\" queue! Errno says" <<  errno);
  else
    mLogCrawlLn(mName.c_str() << " opened \""<< serialQueueOutName.c_str() <<"\" queue. ");
}

CDxlWorkerThread::~CDxlWorkerThread()
{
  // Close queues
  if (mq_close(mQueueIn) != 0)
    mLogErrorLn("Failed to close \"CDxlWorkerThread-Out\" queue!");
  //  if (mq_close(mQueueOut) != 0)
  //    printf("[ERROR] Failed to close \"CDxlWorkerThread-Out\" queue!\n");
  mLogCrawlLn("closed \"CDxlWorkerThread-Out\" queue!");
}


/**
 * Notifies the sender of the message that the work is done
 * It sends this message to the specified return queue in msg
 */
bool CDxlWorkerThread::returnMessage(CDxlQueueMsg *msg)
{
  if (msg->mReturnQueue != (mqd_t)0)
    return (mq_send(msg->mReturnQueue, (char*)msg, sizeof(*msg), 0) != -1);
  else
    // They don't need an answer..
    return true;
}

void CDxlWorkerThread::run()
{

  mLogDebugLn("RT_Printf resize buffersize return value:" << rt_print_init(PRINTBUFFERSIZE, NULL)); // reset the buffersize of the rtprintf system to the global def

  while (!shouldStop())
  {
    CDxlQueueMsg msg;

    // Wait for new queue message: blocking
    //printf("WR_%s+\n", mName.c_str());
    int receiveRet= mq_receive(mQueueIn, (char*)&msg, sizeof(msg), NULL);
    //printf("WR_%s-\n", mName.c_str());

    if (receiveRet == sizeof(msg))
    {
      // Process the instruction
      switch(msg.mInstruction)
      {
      case DXLGRP_ASYNC_INSTR_COMMAND:
        // Execute the attached command
        mLogDebugLn("executing command: "<< msg.mpCommand->getName());
        msg.mpCommand->execute();
        // respond that we processed this message

        if (!returnMessage(&msg))
        {
          mLogErrorLn("CDxlWorkerThread::returnMessage() returned false! Stopping.");
          stop();
        }

        break;
      case DXLGRP_ASYNC_INSTR_TERMINATE:
        mLogNoticeLn("DXLGRP_ASYNC_INSTR_TERMINATE");
        stop();
        break;
      case DXLGRP_ASYNC_INSTR_DUMMY:
        mLogNoticeLn("DXLGRP_ASYNC_INSTR_DUMMY");
        // Do nothing functional, just echo the message
        //printf("%s received dummy msg %d\n", mName.c_str(), msg.mID);
        //printf("WS_%s+\n", mName.c_str());
        //usleep(50000);
        if (!returnMessage(&msg))
        {
          mLogErrorLn("CDxlWorkerThread::returnMessage() returned false! Stopping.");
          stop();
        }
        //printf("WS_%s-\n", mName.c_str());
        break;
      default:
        mLogErrorLn("default: do nothing!");
        break;
      }
    }
    else
    {
      mLogErrorLn("CDxlWorkerThread -> mq_receive() returned "<< receiveRet << " (errno="<< errno << ")! Stopping.");
      stop();
    }

  }
  mLogNoticeLn("run() has come to an end.");
}



//***********************************************************//
//********************* CDxlGroupAsync **********************//
//***********************************************************//


/**
 * CDxlGroupAsync() is a dxl group that has a worker thread
 * It can give you a command interface to this worker thread
 */
CDxlGroupAsync::CDxlGroupAsync(): mLog("CDxlGroupAsync")
{
  mQueueOut       = (mqd_t)(-1);
  mWorkerThread    = NULL;
}

CDxlGroupAsync::~CDxlGroupAsync()
{
  // Delete all async command interfaces that were given out
  while (mCommandInterfaces.size() > 0)
  {
    delete mCommandInterfaces.back();
    mCommandInterfaces.pop_back();
  }
}

bool CDxlGroupAsync::init()
{
  // Set mQueueIn's and mQueueOut's name
  //mQueueInName  = mName + "-QIn";
  mQueueOutName  = "/" + mName + "-Out";

  // Create the input and output queues in blocking mode
  mq_attr queueAttr;
  queueAttr.mq_flags    = 0;
  queueAttr.mq_maxmsg    = DXLGRP_ASYNC_QUEUE_LENGTH;  /* Max. # of messages on queue */
  queueAttr.mq_msgsize  = sizeof(CDxlQueueMsg);      /* Max. message size (bytes) */
  queueAttr.mq_curmsgs  = 0;              /* # of messages currently in queue */
  /*
  mQueueIn = mq_open(mQueueInName.c_str(), O_RDONLY | O_CREAT, S_IRWXU, &queueAttr);
  if (mQueueIn == (mqd_t)(-1))
    // fail
  {
    printf("[ERROR] Could not create \"%s\" message queue!\n", mQueueInName.c_str());
    return false;
  }
  else
    printf("\"%s\" queue created. ", mQueueInName.c_str());
   */
  mQueueOut = mq_open(mQueueOutName.c_str(), O_WRONLY | O_CREAT, S_IRWXU, &queueAttr);
  if (mQueueOut == (mqd_t)(-1))// fail
  {
    mLogErrorLn("Could not create \""<< mQueueOutName.c_str() << "\" message queue! Errno says"<< errno);
    return false;
  }
  else
  {
    mLogNoticeLn("DxlAsyncInit: \""<< mQueueOutName.c_str() << "\" queue created. ");
  }

  // Create worker thread. Has to be done here because the worker thread needs a name depending on the port name
  std::string workerThreadName;
  workerThreadName  = mName + "-Worker";
  mWorkerThread = new CDxlWorkerThread(workerThreadName, 95, mQueueOutName);

  // Start the worker thread
  mWorkerThread->start();

  // Call parent's init
  int result =  CDxlGroup::initAll();

  if (result == DXL_SUCCESS)
  {
    return true;
  }
  else
  {
    mLogErrorLn("DxlGroupInit failed with "<< CDxlGroup::translateErrorCode(result));
    return false;
  }
}

bool CDxlGroupAsync::deinit()
{
  // Stop worker thread by sending a TERMINATE message
  if (mQueueOut != (mqd_t)(-1))
  {
    CDxlQueueMsg msg;
    msg.mInstruction  = DXLGRP_ASYNC_INSTR_TERMINATE;
    msg.mpCommand    = NULL;
    msg.mReturnQueue  = 0;  // We do not need results
    //printf("GS_%s+\n", mName.c_str());
    int ret = mq_send(mQueueOut, (char*)&msg, sizeof(msg), MQ_PRIO_MAX-1);
    if (ret == -1)
    {
      mLogErrorLn("FAILED to send TERMINATE msg to worker thread!");
      return false;
    }
  }
  else
  {
    mLogErrorLn("mQueueOut doesn't exist! on deinit");
  }

  // Delete worker thread
  mLogDebugLn("Deleting worker thread, awaiting its termination ... ");
  //  fflush(stdout); <disabled for testing> //TODO: uncomment this
  if (mWorkerThread != NULL)
  {
    mWorkerThread->awaitTermination();
    delete mWorkerThread;
    mWorkerThread = NULL;
  }
  // delete dynamixel objects
  CDxlGroup::deinit();

  // Unlink queues
  //  if (mq_unlink(mQueueInName.c_str()) != 0)
  //    printf("[ERROR] Failed to unlink \"%s\" queue!\n", mQueueInName.c_str());
  if (mq_unlink(mQueueOutName.c_str()) != 0)
  {
    mLogErrorLn("Failed to unlink \""<< mQueueOutName.c_str() << "\" queue!");
    return false;
  }

  // Close queues
  //  if (mq_close(mQueueIn) != 0)
  //    printf("[ERROR] Failed to close \"%s\" queue!\n", mQueueInName.c_str());
  if (mq_close(mQueueOut) != 0)
  {
    mLogErrorLn("Failed to close \""<< mQueueOutName.c_str() << "\" queue!");
    return false;
  }
  return true;
}

CDxlCmdInterface* CDxlGroupAsync::createCommandInterface()
{
  // Create a unique name
  std::stringstream number;
  number << mCommandInterfaces.size();
  std::string receivingQueueName = "/" + mName + "-QIn" + number.str();

  CDxlCmdInterface* newInterface = new CDxlCmdInterface(receivingQueueName, mQueueOut);
  mCommandInterfaces.push_back(newInterface);
  return newInterface;
}

