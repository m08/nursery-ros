// dxl-calibrate: Test program to manually calibrate the angles of a dynamixel
// 				  using the plate with the protractor ("gradenboog").
//
// (C) 2009 Erik Schuitema, TU Delft

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
//#include <istream>
//#include <signal.h>
//#include <unistd.h>
//#include <sys/mman.h>

//#include <native/timer.h>
#include <AbstractXenomaiTask.hpp>

#include <rtdm/rtserial.h>
#include <DynamixelAsync.h>
#include <LxRtSerial.h>
#include <Statistics.h>

#define MAX_DEVICE_NAME_LEN		20

#define NUM_CALIB_POINTS		301
#define NUM_MEASUREMENTS		100

class CDxlCalibrator: public AbstractXenomaiTask
{
	protected:
		LxRtSerial			mSerialPort;
		CDynamixel			mDynamixel;
		double				mCalibPoints[NUM_CALIB_POINTS];
		int					mCalibPointIndex;
		CSimpleStat			mCalibStat;

	public:
		CDxlCalibrator(const std::string& name, const int priority):
			AbstractXenomaiTask(name, priority),
			mCalibStat(NUM_MEASUREMENTS)
		{
			mCalibPointIndex = 0;
			// Init calibration memory
			for (int i=0; i<NUM_CALIB_POINTS; i++)
				mCalibPoints[i] = -1;
		}

		bool init(char* argv[])
		{
			char devicename[MAX_DEVICE_NAME_LEN];
			// Set serial device names and create dxl groups
			if (strlen(argv[1]) < MAX_DEVICE_NAME_LEN)
				strcpy(devicename, argv[1]);
			else
			{
				printf("Device name too long (probably not rtser0 or rtser1 ..)!\n");
				return false;
			}


			// Create serial ports and init dxl groups with the ports
			if (mSerialPort.port_open(devicename, LxSerial::RS485_SMSC))
			{
				// Set correct baud rate
				mSerialPort.set_speed(LxSerial::S500000);

				// Init dynamixel
				int dxlID = atoi(argv[2]);
				CDxlConfig dxlConfig;
				dxlConfig.mReturnDelay = 4;
				dxlConfig.mNullAngle = 0.0;
				mDynamixel.setSerialPort(&mSerialPort);
				mDynamixel.setConfig(dxlConfig.setID(dxlID));
//				mDynamixel.init(DXL_PING_TIMEOUT, true);
				mDynamixel.init(true);
			}
			else
			{
				printf("FAILED to open serial port \"%s\"!\n", devicename);
				return false;
			}

			return true;
		}

		bool deinit()
		{
			mSerialPort.port_close();
			return true;
		}

	protected:
		void writeResults()
		{
			printf("Please enter filename: ");
			char filename[300];
			std::cin.getline(filename, 300, '\n');
			if (filename[0] == '\0')
			{
				printf("Skipping saving data.\n");
			}
			else
			{
				FILE *fOut = fopen(filename, "w");
				for (int i=0; i<NUM_CALIB_POINTS; i++)
					fprintf(fOut, "%d\t%.3f\n", i, mCalibPoints[i]);
				fclose(fOut);
				printf("Calibration data written to file \"%s\"!\n", filename);
			}
		}

		void run()
		{
			while (!shouldStop())
			{
				printf("Please calibrate %d degrees: ", mCalibPointIndex);
				//std::cin.readsome
				char line[300];
				std::cin.getline(line, 300, '\n');
				switch (line[0])
				{
					case '\0': // just an enter
						// Save new value
						mCalibStat.clear();
						for (int iM=0; iM<NUM_MEASUREMENTS; iM++)
						{
							mDynamixel.getPos();
							mCalibStat.addValue(mDynamixel.presentPos());
						}
						mCalibPoints[mCalibPointIndex] = mCalibStat.getAverage();
						if (mCalibPointIndex < NUM_CALIB_POINTS-1)
						{
							printf("Value for %d degrees was %.3f (%.3f off nominal)\n", mCalibPointIndex, mCalibPoints[mCalibPointIndex], mCalibPoints[mCalibPointIndex] - (double)mCalibPointIndex*1023.0/300.0);
							mCalibPointIndex++;
						}
						else
						{
							printf("This was the last angle, you're done!\n");
							stop();
						}
						break;
					case 'b':
						// Going back one value
						printf("Going back one degree. ");
						if (mCalibPointIndex > 0)
							mCalibPointIndex--;
						break;
					case 'f':
						// Going forth one value
						printf("Going forth one degree. ");
						if (mCalibPointIndex < NUM_CALIB_POINTS-1)
							mCalibPointIndex++;
						break;
					case 'q':
					case 'x':
						stop();
						break;
					default:
						break;
				}
			}
			printf("Quitting. Attempting to save data. ");
			writeResults();
		}
};


//bool gQuit=false;

/*
void catch_signal(int sig)
{
	gQuit = true;
	printf("\nSignal received, stopping.\n");
}
*/

int main(int argc, char* argv[])
{
	printf("dxl-calibrate: Manual calibration tool for Dynamixel motors using a protractor (\"gradenboog\")\n");
	CDxlCalibrator dxlCalibrator("RT-dxl-calibrator", 99);

	if (argc < 3)
	{
		printf("Usage:\n");
		printf("dxl-calibrate [rtserialport1] [ID]\n");
		return -1;
	}

	dxlCalibrator.init(argv);

//	signal(SIGTERM, catch_signal);
//	signal(SIGINT, catch_signal);
//	printf("Use Ctrl-C to exit program.\n");

	dxlCalibrator.start();

	// Wait for break signal. Use the while loop + gQuit to prevent SIGWINCH (console resizing) from quitting the program
//	while (!gQuit)
//		pause();

//	dxlCalibrator.stop();
	dxlCalibrator.awaitTermination();

	dxlCalibrator.deinit();

	printf("End of program\n");

	return 0;
}
