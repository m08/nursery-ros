
#ifndef __CDXLCMD__
#define __CDXLCMD__

#include <stdio.h>
#include "CDxlGeneric.h"
#include <Log2.h>
// Dynamixel command classes.
// A CCommand object can be created and executed directly,
// but it can also be used to send to a worker thread which executes it for you.
class CCommand
{
  protected:
    int           mResult;
    const char*   mpName; //function name for debugging

  public:
    CCommand() {
      mResult = 0;
      mpName = "CCommand not specified";
    }
    CCommand(const char* name) {
      mResult = 0;
      mpName = name;
    }
    virtual ~CCommand()  {}
    int                 getResult() {return mResult;}
    virtual void        execute()   {printf("CCommand::execute() called (which is void)!\n"); mResult = -1;}
    virtual const char* getName()   {return mpName;}
};

// **** CDxlCommand and derived classes ****//
class CDxlCommand: public CCommand
{
  protected:
    CDxlGeneric *mpDxl;
  public:
    CDxlCommand(CDxlGeneric* pDynamixel) :
      CCommand(),
      mpDxl(pDynamixel) {
        mResult    = DXL_SUCCESS;
    }
};

class CDxlChangeModeCommand: public CDxlCommand
{
  protected:
    unsigned char mMode;
  public:
    CDxlChangeModeCommand(CDxlGeneric* pDynamixel, unsigned char mode):
      CDxlCommand(pDynamixel),
      mMode(mode)
    {}
    void execute() {
      mResult = mpDxl->set3MxlMode(mMode, false);
    }
};


/*
 * These classes are not used at the moment, however they should be functional.
 * It's probably better to move the parameters like enable and torque outside the constructor
 * into separate functions
class CDxlCmd_setEndlessTurnMode: public CDxlCommand
{
  protected:
    bool mEnabled;
  public:
    CDxlCmd_setEndlessTurnMode(CDynamixel* pDynamixel, bool enabled):
      CDxlCommand(pDynamixel), mEnabled(enabled)  {}
    void execute()      {mResult = mpDxl->setEndlessTurnMode(mEnabled);}
};

class CDxlCmd_setEndlessTurnTorque: public CDxlCommand
{
  protected:
    double mTorque;
  public:
    CDxlCmd_setEndlessTurnTorque(CDynamixel* pDynamixel, double torque):
      CDxlCommand(pDynamixel), mTorque(torque)  {}
    void execute()      {mResult = mpDxl->setEndlessTurnTorque(mTorque);}
};
 */

// **** CDxlGroupCommand and derived classes ****//
class CDxlGroupCommand: public CCommand
{
protected:
  CDxlGroup  *mpDxlGroup;
public:
  CDxlGroupCommand(CDxlGroup* pDxlGroup) :
    CCommand(),
    mpDxlGroup(pDxlGroup)
{
    mResult    = DXL_SUCCESS;
}
  CDxlGroupCommand(CDxlGroup* pDxlGroup, const char* name ) :
    CCommand(name),
    mpDxlGroup(pDxlGroup)
  {
    mResult    = DXL_SUCCESS;
  }
  virtual void execute()  {printf("CDxlGroupCommand::execute() called (which is void)!\n"); mResult = -1;}
};

class CDxlGrpCmd_getPosAll: public CDxlGroupCommand
{
  public:
    CDxlGrpCmd_getPosAll(CDxlGroup *pDxlGroup):
      CDxlGroupCommand(pDxlGroup, "CDxlGrpCmd_getPosAll")
    {}
    void execute() {mResult = mpDxlGroup->getPosAll();}
};

class CDxlGrpCmd_getStatusAll: public CDxlGroupCommand
{
  public:
    CDxlGrpCmd_getStatusAll(CDxlGroup *pDxlGroup):
      CDxlGroupCommand(pDxlGroup, "CDxlGrpCmd_getStatusAll")
    {}
    void execute() {mResult = mpDxlGroup->getStatusAll();}
};

class CDxlGrpCmd_getTempAll: public CDxlGroupCommand
{
  public:
    CDxlGrpCmd_getTempAll(CDxlGroup *pDxlGroup):
      CDxlGroupCommand(pDxlGroup, "CDxlGrpCmd_getTempAll")
    {}
    void execute() {mResult = mpDxlGroup->getTempAll();}
};

class CDxlGrpCmd_getPosAndSpeedAll: public CDxlGroupCommand
{
public:
  CDxlGrpCmd_getPosAndSpeedAll(CDxlGroup *pDxlGroup):
    CDxlGroupCommand(pDxlGroup,"CDxlGrpCmd_getPosAndSpeedAll")    {}
  void execute()      {mResult = mpDxlGroup->getPosAndSpeedAll();}
};

class CDxlGrpCmd_getPosSpeedTorqueAll: public CDxlGroupCommand
{
public:
  CDxlGrpCmd_getPosSpeedTorqueAll(CDxlGroup *pDxlGroup):
    CDxlGroupCommand(pDxlGroup,"CDxlGrpCmd_getPosSpeedTorqueAll")    {}
  void execute()      {mResult = mpDxlGroup->getPosSpeedTorqueAll();}
};

class CDxlGrpCmd_enableTorqueAll: public CDxlGroupCommand
{
protected:
  int        mState;
public:
  CDxlGrpCmd_enableTorqueAll(CDxlGroup *pDxlGroup):
    CDxlGroupCommand(pDxlGroup,"CDxlGrpCmd_enableTorqueAll")    {}
  void setTorqueState(int state)    {mState = state;}
  void execute()            {mResult = mpDxlGroup->enableTorqueAll(mState);}
};

// **** CDxlSyncWriteCommand and derived classes ****//
class CDxlSyncWriteCommand: public CDxlGroupCommand
{
protected:
  //    // This packet is going to be used by all dynamixels in the group dxlGroup, so
  //    // the dynamixel can use syncWriteLocation to add their data.
  //    CDxlSyncWritePacket  mSyncPacket;
  CLog2  mLog;
public:
  // instruction can be INST_SYNC_WRITE or INST_SYNC_REG_WRITE
  CDxlSyncWriteCommand(CDxlGroup* dxlGroup, const BYTE instruction=INST_SYNC_WRITE):
    CDxlGroupCommand(dxlGroup,"CDxlSyncWriteCommand"),
    mLog("CDxlSyncWriteCommand")
{
    dxlGroup->setSyncWriteType(instruction);
}
  void execute()          {mpDxlGroup->sendSyncWritePacket();}
};

class CDxlSyncWriteCmd_setPos: public CDxlSyncWriteCommand
{
public:
  // instruction can be INST_SYNC_WRITE or INST_SYNC_REG_WRITE
  CDxlSyncWriteCmd_setPos(CDxlGroup* dxlGroup, const BYTE instruction=INST_SYNC_WRITE):
    CDxlSyncWriteCommand(dxlGroup, instruction)
  {}

  // This sync_write packet can be used for setting position+speed as well as for setting speed only
  // since both commands require the same amount of bytes at the same dynamixel register
  void setPos(double pos, double absSpeed, int dynamixelIndex)
  {
    mpDxlGroup->getDynamixel(dynamixelIndex)->setPos(pos, absSpeed, true);
    mLogCrawlLn("set position for dynamixel " << dynamixelIndex << " to " << pos << " with speed " << absSpeed );
  }

  void setSpeed(double speed, int dynamixelIndex)
  {
    mpDxlGroup->getDynamixel(dynamixelIndex)->setSpeed(speed, true);
    mLogCrawlLn("set speed for dynamixel " << dynamixelIndex << " to " << speed );

  }

  void clear()
  {
    // Set positions and speeds to zero.
    for (int i=0; i<mpDxlGroup->getNumDynamixels(); i++)
      mpDxlGroup->getDynamixel(i)->setPos(0.0, 0.0, true);
  }
};

//class CDxlSyncWriteCmd_setPosSpeedTorquePPosDPos: public CDxlSyncWriteCommand
//{
//  public:
//    // instruction can be INST_SYNC_WRITE or INST_SYNC_REG_WRITE
//  CDxlSyncWriteCmd_setPosSpeedTorquePPosDPos(CDxlGroup* dxlGroup, const BYTE instruction=INST_SYNC_WRITE):
//      CDxlSyncWriteCommand(dxlGroup, instruction)
//    {}
//
//    // This sync_write packet can be used for setting position+speed as well as for setting speed only
//    // since both commands require the same amount of bytes at the same dynamixel register
//    void setPosSpeedTorquePPosDPos(double pos, double speed, double torque, double pPos, double dPos, int dynamixelIndex)
//    {
//      mpDxlGroup->getDynamixel(dynamixelIndex)->setPosSpeedTorquePPosDPos(pos, speed, torque, pPos, dPos, true);
//    }
//
//    void clear()
//    {
//      // Set positions and speeds to zero.
//      for (int i=0; i<mpDxlGroup->getNumDynamixels(); i++)
//        mpDxlGroup->getDynamixel(i)->setPosSpeedTorquePPosDPos(0.0, 0.0, 0.0, 0.0, 0.0, true);
//    }
//};
//
//class CDxlSyncWriteCmd_setTorque: public CDxlSyncWriteCommand
//{
//  public:
//    // instruction can be INST_SYNC_WRITE or INST_SYNC_REG_WRITE
//  CDxlSyncWriteCmd_setTorque(CDxlGroup* dxlGroup, const BYTE instruction=INST_SYNC_WRITE):
//      CDxlSyncWriteCommand(dxlGroup, instruction)
//    {}
//
//    // This sync_write packet can be used for setting position+speed as well as for setting speed only
//    // since both commands require the same amount of bytes at the same dynamixel register
//    void setTorque(double torque, int dynamixelIndex)
//    {
//      mpDxlGroup->getDynamixel(dynamixelIndex)->setTorque(torque, true);
//    }
//
//    void clear()
//    {
//      // Set positions and speeds to zero.
//      for (int i=0; i<mpDxlGroup->getNumDynamixels(); i++)
//        mpDxlGroup->getDynamixel(i)->setTorque(0.0, true);
//    }
//};

/*
 * ivan: Added by me because this class was missing everywhere in Erik's svns
 * Hopefully it will work, but may be potentially dangerous
 */
class CDxlSyncWriteCmd_enableTorque: public CDxlSyncWriteCommand
{
public:
  // instruction can be INST_SYNC_WRITE or INST_SYNC_REG_WRITE
  CDxlSyncWriteCmd_enableTorque(CDxlGroup* dxlGroup, const BYTE instruction=INST_SYNC_WRITE):
    CDxlSyncWriteCommand(dxlGroup, instruction)
  {}

  void setEnabled(bool enabled, int dynamixelIndex)
  {
    printf("[ivan!:enableTorque] Attempting to set torque to %d of motor %d\n", enabled, dynamixelIndex);
    mpDxlGroup->getDynamixel(dynamixelIndex)->enableTorque(enabled);
  }

  void setEnabledAll(bool enabled)
  {
    for (int i=0; i<mpDxlGroup->getNumDynamixels(); i++)
      setEnabled(enabled, i);
  }
};

class CDxlSyncWriteCmd_setEndlessTurnTorque: public CDxlSyncWriteCommand
{
public:
  // instruction can be INST_SYNC_WRITE or INST_SYNC_REG_WRITE
  CDxlSyncWriteCmd_setEndlessTurnTorque(CDxlGroup* dxlGroup, const BYTE instruction=INST_SYNC_WRITE):
    CDxlSyncWriteCommand(dxlGroup, instruction)
  {}

  void setEndlessTurnTorque(double torqueRatio, int dynamixelIndex)
  {
    mpDxlGroup->getDynamixel(dynamixelIndex)->setEndlessTurnTorque(torqueRatio, true);
  }

  void setEndlessTurnTorqueAll(double torqueRatio)
  {
    for (int i=0; i<mpDxlGroup->getNumDynamixels(); i++)
      setEndlessTurnTorque(torqueRatio, i);
  }

  void clear()
  {
    // Set endless turn torques to zero.
    for (int i=0; i<mpDxlGroup->getNumDynamixels(); i++)
      mpDxlGroup->getDynamixel(i)->setEndlessTurnTorque(0.0, true);
  }
};

class CDxlSyncWriteCmd_setEndlessTurnMode: public CDxlSyncWriteCommand
{
public:
  // instruction can be INST_SYNC_WRITE or INST_SYNC_REG_WRITE
  CDxlSyncWriteCmd_setEndlessTurnMode(CDxlGroup* dxlGroup):
    CDxlSyncWriteCommand(dxlGroup, INST_SYNC_WRITE)
  {}

  void setEnabled(bool enabled, int dynamixelIndex)
  {
    mpDxlGroup->getDynamixel(dynamixelIndex)->setEndlessTurnMode(enabled, true);
  }

  void setEnabledAll(bool enabled)
  {
    for (int i=0; i<mpDxlGroup->getNumDynamixels(); i++)
      setEnabled(enabled, i);
  }
};

#endif //__CDXLCMD__
