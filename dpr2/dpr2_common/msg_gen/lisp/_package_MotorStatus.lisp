(cl:in-package dpr2_common-msg)
(cl:export '(SPEED-VAL
          SPEED
          POSITION-VAL
          POSITION
          TORQUE-VAL
          TORQUE
          STATUS-VAL
          STATUS
))