/* Auto-generated by genmsg_cpp for file /home/agudek/Workspace/src/nursery/dpr2/dpr2_base/srv/SetDistance.srv */
#ifndef DPR2_BASE_SERVICE_SETDISTANCE_H
#define DPR2_BASE_SERVICE_SETDISTANCE_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"

#include "ros/service_traits.h"




namespace dpr2_base
{
template <class ContainerAllocator>
struct SetDistanceRequest_ {
  typedef SetDistanceRequest_<ContainerAllocator> Type;

  SetDistanceRequest_()
  : distance(0.0)
  , linear_speed(0.0)
  {
  }

  SetDistanceRequest_(const ContainerAllocator& _alloc)
  : distance(0.0)
  , linear_speed(0.0)
  {
  }

  typedef double _distance_type;
  double distance;

  typedef double _linear_speed_type;
  double linear_speed;


  typedef boost::shared_ptr< ::dpr2_base::SetDistanceRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::dpr2_base::SetDistanceRequest_<ContainerAllocator>  const> ConstPtr;
}; // struct SetDistanceRequest
typedef  ::dpr2_base::SetDistanceRequest_<std::allocator<void> > SetDistanceRequest;

typedef boost::shared_ptr< ::dpr2_base::SetDistanceRequest> SetDistanceRequestPtr;
typedef boost::shared_ptr< ::dpr2_base::SetDistanceRequest const> SetDistanceRequestConstPtr;



template <class ContainerAllocator>
struct SetDistanceResponse_ {
  typedef SetDistanceResponse_<ContainerAllocator> Type;

  SetDistanceResponse_()
  {
  }

  SetDistanceResponse_(const ContainerAllocator& _alloc)
  {
  }


  typedef boost::shared_ptr< ::dpr2_base::SetDistanceResponse_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::dpr2_base::SetDistanceResponse_<ContainerAllocator>  const> ConstPtr;
}; // struct SetDistanceResponse
typedef  ::dpr2_base::SetDistanceResponse_<std::allocator<void> > SetDistanceResponse;

typedef boost::shared_ptr< ::dpr2_base::SetDistanceResponse> SetDistanceResponsePtr;
typedef boost::shared_ptr< ::dpr2_base::SetDistanceResponse const> SetDistanceResponseConstPtr;


struct SetDistance
{

typedef SetDistanceRequest Request;
typedef SetDistanceResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;
}; // struct SetDistance
} // namespace dpr2_base

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::dpr2_base::SetDistanceRequest_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::dpr2_base::SetDistanceRequest_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::dpr2_base::SetDistanceRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "12010f2c3696283d0c587dd15fe21c47";
  }

  static const char* value(const  ::dpr2_base::SetDistanceRequest_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0x12010f2c3696283dULL;
  static const uint64_t static_value2 = 0x0c587dd15fe21c47ULL;
};

template<class ContainerAllocator>
struct DataType< ::dpr2_base::SetDistanceRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "dpr2_base/SetDistanceRequest";
  }

  static const char* value(const  ::dpr2_base::SetDistanceRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::dpr2_base::SetDistanceRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "\n\
float64 distance\n\
float64 linear_speed\n\
\n\
";
  }

  static const char* value(const  ::dpr2_base::SetDistanceRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct IsFixedSize< ::dpr2_base::SetDistanceRequest_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros


namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::dpr2_base::SetDistanceResponse_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::dpr2_base::SetDistanceResponse_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::dpr2_base::SetDistanceResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "d41d8cd98f00b204e9800998ecf8427e";
  }

  static const char* value(const  ::dpr2_base::SetDistanceResponse_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0xd41d8cd98f00b204ULL;
  static const uint64_t static_value2 = 0xe9800998ecf8427eULL;
};

template<class ContainerAllocator>
struct DataType< ::dpr2_base::SetDistanceResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "dpr2_base/SetDistanceResponse";
  }

  static const char* value(const  ::dpr2_base::SetDistanceResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::dpr2_base::SetDistanceResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "\n\
";
  }

  static const char* value(const  ::dpr2_base::SetDistanceResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct IsFixedSize< ::dpr2_base::SetDistanceResponse_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::dpr2_base::SetDistanceRequest_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.distance);
    stream.next(m.linear_speed);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct SetDistanceRequest_
} // namespace serialization
} // namespace ros


namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::dpr2_base::SetDistanceResponse_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct SetDistanceResponse_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace service_traits
{
template<>
struct MD5Sum<dpr2_base::SetDistance> {
  static const char* value() 
  {
    return "12010f2c3696283d0c587dd15fe21c47";
  }

  static const char* value(const dpr2_base::SetDistance&) { return value(); } 
};

template<>
struct DataType<dpr2_base::SetDistance> {
  static const char* value() 
  {
    return "dpr2_base/SetDistance";
  }

  static const char* value(const dpr2_base::SetDistance&) { return value(); } 
};

template<class ContainerAllocator>
struct MD5Sum<dpr2_base::SetDistanceRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "12010f2c3696283d0c587dd15fe21c47";
  }

  static const char* value(const dpr2_base::SetDistanceRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct DataType<dpr2_base::SetDistanceRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "dpr2_base/SetDistance";
  }

  static const char* value(const dpr2_base::SetDistanceRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct MD5Sum<dpr2_base::SetDistanceResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "12010f2c3696283d0c587dd15fe21c47";
  }

  static const char* value(const dpr2_base::SetDistanceResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct DataType<dpr2_base::SetDistanceResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "dpr2_base/SetDistance";
  }

  static const char* value(const dpr2_base::SetDistanceResponse_<ContainerAllocator> &) { return value(); } 
};

} // namespace service_traits
} // namespace ros

#endif // DPR2_BASE_SERVICE_SETDISTANCE_H

