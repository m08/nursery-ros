
(cl:in-package :asdf)

(defsystem "dpr2_base-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "SetDistance" :depends-on ("_package_SetDistance"))
    (:file "_package_SetDistance" :depends-on ("_package"))
    (:file "SetAngle" :depends-on ("_package_SetAngle"))
    (:file "_package_SetAngle" :depends-on ("_package"))
  ))