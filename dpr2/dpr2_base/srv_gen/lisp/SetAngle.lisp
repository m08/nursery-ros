; Auto-generated. Do not edit!


(cl:in-package dpr2_base-srv)


;//! \htmlinclude SetAngle-request.msg.html

(cl:defclass <SetAngle-request> (roslisp-msg-protocol:ros-message)
  ((angle
    :reader angle
    :initarg :angle
    :type cl:float
    :initform 0.0)
   (angular_speed
    :reader angular_speed
    :initarg :angular_speed
    :type cl:float
    :initform 0.0))
)

(cl:defclass SetAngle-request (<SetAngle-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetAngle-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetAngle-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dpr2_base-srv:<SetAngle-request> is deprecated: use dpr2_base-srv:SetAngle-request instead.")))

(cl:ensure-generic-function 'angle-val :lambda-list '(m))
(cl:defmethod angle-val ((m <SetAngle-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dpr2_base-srv:angle-val is deprecated.  Use dpr2_base-srv:angle instead.")
  (angle m))

(cl:ensure-generic-function 'angular_speed-val :lambda-list '(m))
(cl:defmethod angular_speed-val ((m <SetAngle-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dpr2_base-srv:angular_speed-val is deprecated.  Use dpr2_base-srv:angular_speed instead.")
  (angular_speed m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetAngle-request>) ostream)
  "Serializes a message object of type '<SetAngle-request>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'angular_speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetAngle-request>) istream)
  "Deserializes a message object of type '<SetAngle-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angle) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angular_speed) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetAngle-request>)))
  "Returns string type for a service object of type '<SetAngle-request>"
  "dpr2_base/SetAngleRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetAngle-request)))
  "Returns string type for a service object of type 'SetAngle-request"
  "dpr2_base/SetAngleRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetAngle-request>)))
  "Returns md5sum for a message object of type '<SetAngle-request>"
  "42b9709144f93a237a68b43f2ebe3dc0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetAngle-request)))
  "Returns md5sum for a message object of type 'SetAngle-request"
  "42b9709144f93a237a68b43f2ebe3dc0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetAngle-request>)))
  "Returns full string definition for message of type '<SetAngle-request>"
  (cl:format cl:nil "~%float64 angle~%float64 angular_speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetAngle-request)))
  "Returns full string definition for message of type 'SetAngle-request"
  (cl:format cl:nil "~%float64 angle~%float64 angular_speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetAngle-request>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetAngle-request>))
  "Converts a ROS message object to a list"
  (cl:list 'SetAngle-request
    (cl:cons ':angle (angle msg))
    (cl:cons ':angular_speed (angular_speed msg))
))
;//! \htmlinclude SetAngle-response.msg.html

(cl:defclass <SetAngle-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass SetAngle-response (<SetAngle-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetAngle-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetAngle-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dpr2_base-srv:<SetAngle-response> is deprecated: use dpr2_base-srv:SetAngle-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetAngle-response>) ostream)
  "Serializes a message object of type '<SetAngle-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetAngle-response>) istream)
  "Deserializes a message object of type '<SetAngle-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetAngle-response>)))
  "Returns string type for a service object of type '<SetAngle-response>"
  "dpr2_base/SetAngleResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetAngle-response)))
  "Returns string type for a service object of type 'SetAngle-response"
  "dpr2_base/SetAngleResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetAngle-response>)))
  "Returns md5sum for a message object of type '<SetAngle-response>"
  "42b9709144f93a237a68b43f2ebe3dc0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetAngle-response)))
  "Returns md5sum for a message object of type 'SetAngle-response"
  "42b9709144f93a237a68b43f2ebe3dc0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetAngle-response>)))
  "Returns full string definition for message of type '<SetAngle-response>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetAngle-response)))
  "Returns full string definition for message of type 'SetAngle-response"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetAngle-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetAngle-response>))
  "Converts a ROS message object to a list"
  (cl:list 'SetAngle-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'SetAngle)))
  'SetAngle-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'SetAngle)))
  'SetAngle-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetAngle)))
  "Returns string type for a service object of type '<SetAngle>"
  "dpr2_base/SetAngle")