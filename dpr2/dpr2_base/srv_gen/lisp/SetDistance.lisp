; Auto-generated. Do not edit!


(cl:in-package dpr2_base-srv)


;//! \htmlinclude SetDistance-request.msg.html

(cl:defclass <SetDistance-request> (roslisp-msg-protocol:ros-message)
  ((distance
    :reader distance
    :initarg :distance
    :type cl:float
    :initform 0.0)
   (linear_speed
    :reader linear_speed
    :initarg :linear_speed
    :type cl:float
    :initform 0.0))
)

(cl:defclass SetDistance-request (<SetDistance-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetDistance-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetDistance-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dpr2_base-srv:<SetDistance-request> is deprecated: use dpr2_base-srv:SetDistance-request instead.")))

(cl:ensure-generic-function 'distance-val :lambda-list '(m))
(cl:defmethod distance-val ((m <SetDistance-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dpr2_base-srv:distance-val is deprecated.  Use dpr2_base-srv:distance instead.")
  (distance m))

(cl:ensure-generic-function 'linear_speed-val :lambda-list '(m))
(cl:defmethod linear_speed-val ((m <SetDistance-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dpr2_base-srv:linear_speed-val is deprecated.  Use dpr2_base-srv:linear_speed instead.")
  (linear_speed m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetDistance-request>) ostream)
  "Serializes a message object of type '<SetDistance-request>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'distance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'linear_speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetDistance-request>) istream)
  "Deserializes a message object of type '<SetDistance-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'distance) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'linear_speed) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetDistance-request>)))
  "Returns string type for a service object of type '<SetDistance-request>"
  "dpr2_base/SetDistanceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetDistance-request)))
  "Returns string type for a service object of type 'SetDistance-request"
  "dpr2_base/SetDistanceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetDistance-request>)))
  "Returns md5sum for a message object of type '<SetDistance-request>"
  "12010f2c3696283d0c587dd15fe21c47")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetDistance-request)))
  "Returns md5sum for a message object of type 'SetDistance-request"
  "12010f2c3696283d0c587dd15fe21c47")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetDistance-request>)))
  "Returns full string definition for message of type '<SetDistance-request>"
  (cl:format cl:nil "~%float64 distance~%float64 linear_speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetDistance-request)))
  "Returns full string definition for message of type 'SetDistance-request"
  (cl:format cl:nil "~%float64 distance~%float64 linear_speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetDistance-request>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetDistance-request>))
  "Converts a ROS message object to a list"
  (cl:list 'SetDistance-request
    (cl:cons ':distance (distance msg))
    (cl:cons ':linear_speed (linear_speed msg))
))
;//! \htmlinclude SetDistance-response.msg.html

(cl:defclass <SetDistance-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass SetDistance-response (<SetDistance-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetDistance-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetDistance-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dpr2_base-srv:<SetDistance-response> is deprecated: use dpr2_base-srv:SetDistance-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetDistance-response>) ostream)
  "Serializes a message object of type '<SetDistance-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetDistance-response>) istream)
  "Deserializes a message object of type '<SetDistance-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetDistance-response>)))
  "Returns string type for a service object of type '<SetDistance-response>"
  "dpr2_base/SetDistanceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetDistance-response)))
  "Returns string type for a service object of type 'SetDistance-response"
  "dpr2_base/SetDistanceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetDistance-response>)))
  "Returns md5sum for a message object of type '<SetDistance-response>"
  "12010f2c3696283d0c587dd15fe21c47")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetDistance-response)))
  "Returns md5sum for a message object of type 'SetDistance-response"
  "12010f2c3696283d0c587dd15fe21c47")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetDistance-response>)))
  "Returns full string definition for message of type '<SetDistance-response>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetDistance-response)))
  "Returns full string definition for message of type 'SetDistance-response"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetDistance-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetDistance-response>))
  "Converts a ROS message object to a list"
  (cl:list 'SetDistance-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'SetDistance)))
  'SetDistance-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'SetDistance)))
  'SetDistance-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetDistance)))
  "Returns string type for a service object of type '<SetDistance>"
  "dpr2_base/SetDistance")