#include <dpr2_base/dpr2_base_motorless.h>

/// Entry point for DPR2 base controller node
int main(int argc, char **argv)
{
  ros::init(argc, argv, "dpr2_base");

  DPR2Base dpr2_base;
  dpr2_base.spin();

  return 0;
}
