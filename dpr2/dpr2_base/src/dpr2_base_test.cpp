#include <XMLConfiguration.h>

#include "dpr2_base/dpr2_base.h"

#define ONLY_WHEEL

#define CLIP(x, l, u) ((x)<(l))?(l):((x)>(u))?(u):(x)

void DPR2Base::init()
{
	ROS_INFO("Initializing base");

	// Read parameters
	std::string motor_port_name, motor_config_name;

	ROS_ASSERT(nh_.getParam("motor_port", motor_port_name));
	ROS_ASSERT(nh_.getParam("motor_config", motor_config_name));
	ROS_ASSERT(nh_.getParam("wheel_diameter", wheel_diameter_));
	ROS_ASSERT(nh_.getParam("wheel_base", wheel_base_));


wheel_base_ = 0.497;
wheel_diameter_ = 0.295;


	// Subscript to command topic
	vel_sub_ = nh_.subscribe("/cmd_vel", 10, &DPR2Base::velocityCallback, this);
	odom_pub_ = nh_.advertise<nav_msgs::Odometry>("/odom", 10);
//	ang_pos_sub_ = nh_.subscribe("cmd_pos_ang", 1, &DPR2Base::anglepositionCallback, this);
//	dist_pos_sub_ = nh_.subscribe("cmd_pos_dist", 1, &DPR2Base::distancepositionCallback, this);

	set_angle_server_ = nh_.advertiseService("set_angle",  &DPR2Base::setangleCallback, this);
	set_distance_server_ = nh_.advertiseService("set_distance",&DPR2Base::setdistanceCallback,this);


	// Load motor configuration
	CXMLConfiguration motor_config_xml;
	ROS_ASSERT(motor_config_xml.loadFile(motor_config_name));

	CDxlConfig motor_config_left;
	motor_config_left.readConfig(motor_config_xml.root().section("left"));
motor_config_left.setID(106);
motor_config_left.setMode(1);
motor_config_left.setGbRatioMotor(46.7);
//motor_config_left.setAcceleration(1.0);
	left_motor_ = new C3mxlROS(motor_port_name.c_str());
	left_motor_->setConfig(&motor_config_left);
	// Initialize left motor
	ros::Rate init_rate(1);
	while (ros::ok() && left_motor_->init() != DXL_SUCCESS)
	{
		ROS_WARN_ONCE("Couldn't initialize left wheel motor, will continue trying every second");
		init_rate.sleep();
	}

	CDxlConfig motor_config_right;
	motor_config_right.readConfig(motor_config_xml.root().section("right"));
motor_config_right.setID(107);
motor_config_right.setMode(1);
motor_config_right.setGbRatioMotor(46.7);
//motor_config_right.setAcceleration(1.0);
	right_motor_ = new C3mxlROS(motor_port_name.c_str());
	right_motor_->setConfig(&motor_config_right);
	// Initialize right motor
	while (ros::ok() && right_motor_->init() != DXL_SUCCESS)
	{
		ROS_WARN_ONCE("Couldn't initialize right wheel motor, will continue trying every second");
		init_rate.sleep();
	}
}

void DPR2Base::spin()
{
	ROS_INFO("Spinning");
	ros::Rate r(100);

	while(ros::ok())
	{
		ros::spinOnce();
		odometryPublish();
		r.sleep();
	}
}

void DPR2Base::velocityCallback(const geometry_msgs::Twist::ConstPtr &msg)
{
	// Base is nonholonomic, warn if sent a command we can't execute
	if (msg->linear.y || msg->linear.z || msg->angular.x || msg->angular.y)
	{
		ROS_WARN("I'm afraid I can't do that, Dave.");
		return;
	}

	if(isnan(msg->linear.x) || isnan(msg->angular.z) || isinf(msg->linear.x) || isinf(msg->angular.z))
	{
		ROS_WARN("I cant travel at infinite speed. Sorry");
		return;
	}

	// Calculate wheel velocities
	ROS_DEBUG_STREAM("Message on cmd_vel [" << msg->linear.x << ", " << msg->angular.z << "]");
	double vel_linear  = msg->linear.x/(wheel_diameter_/2.0);
	ROS_DEBUG_STREAM("" << wheel_base_ << "/" << wheel_diameter_ << " = " << (wheel_base_/wheel_diameter_));
	double vel_angular = msg->angular.z * (wheel_base_/wheel_diameter_);
	ROS_DEBUG_STREAM("Message converted [" << vel_linear << ", " << vel_angular << "]");

	double vel_left    = vel_linear - vel_angular;
	double vel_right   = vel_linear + vel_angular;

	// Actuate
	left_motor_->setSpeed(vel_left);
	right_motor_->setSpeed(vel_right);
	
	ROS_DEBUG_STREAM("Base velocity set to [" << vel_left << ", " << vel_right << "]");
}

bool DPR2Base::setangleCallback(dpr2_base::SetAngle::Request &req, dpr2_base::SetAngle::Response &res)
{
	// Calculate the direction to move
	// Move till the position is reached

	double angle_start = th_;
	double ang_vel;
	double err = req.angle - (th_ - angle_start);
	ros::Rate rate(100);

	while(fabs(err) > 0.005)
	{
		ang_vel = CLIP(err, -std::min(0.2, req.angular_speed), std::min(0.2, req.angular_speed));
		if(ang_vel < 0)
			ang_vel = CLIP(ang_vel, -0.4, -0.05);
		else if(ang_vel > 0)
			ang_vel = CLIP(ang_vel, 0.05, 0.4);

		// Actuate
		left_motor_->setSpeed(-ang_vel*(wheel_base_/wheel_diameter_));
		right_motor_->setSpeed(ang_vel*(wheel_base_/wheel_diameter_));


		//		ROS_INFO("Angular error: %f", fabs(req.angle - (th_ - angle_start)) );
		rate.sleep();
		odometryPublish();
		err = req.angle - (th_ - angle_start);
	}

	left_motor_->setSpeed(0);
	right_motor_->setSpeed(0);
	return true;

}
bool DPR2Base::setdistanceCallback(dpr2_base::SetDistance::Request &req, dpr2_base::SetDistance::Response &res)
{
	// Move straight forward or backward based on the required input
	double dist_start = dist_;
	double lin_vel, err;
	//	ROS_INFO("Start Distance: %f", dist_start);
	ros::Rate rate(100);

	err = req.distance - (dist_ - dist_start);
	while(fabs(err) > 0.005)
	{
		lin_vel = CLIP(err, -std::min(0.4, req.linear_speed), std::min(0.4, req.linear_speed));
		if(lin_vel < 0)
			lin_vel = CLIP(lin_vel, -0.4, -0.05);
		else if(lin_vel > 0)
			lin_vel = CLIP(lin_vel, 0.05, 0.4);


		//		// Actuate
		left_motor_->setSpeed(lin_vel/(wheel_diameter_/2));
		right_motor_->setSpeed(lin_vel/(wheel_diameter_/2));

		//		ROS_INFO("Distance error: %f, Linear Vel: %f", fabs(req.distance - (dist_ - dist_start)), lin_vel);
		rate.sleep();
		odometryPublish();
		err = req.distance - (dist_ - dist_start);
	}

	left_motor_->setSpeed(0);
	right_motor_->setSpeed(0);
	return true;
}


// reads the current change in wheel and publish as odometry
void DPR2Base::odometryPublish()
{
	left_motor_->getState();
	right_motor_->getState();

	double left = left_motor_->presentSpeed();
	double right = right_motor_->presentSpeed();

	vx_ = (wheel_diameter_ / 4) * (left + right);
	vth_ = ((wheel_diameter_ / 2) / wheel_base_) * (right - left);

	current_time_ = ros::Time::now();

	//compute odometry in a typical way given the velocities of the robot
	double dt = (current_time_ - last_time_).toSec();
	double delta_x = (vx_ * cos(th_)) * dt;
	double delta_y = (vx_ * sin(th_)) * dt;
	double delta_th = vth_ * dt;
	double delta_dist_ = vx_ *dt;

	x_ += delta_x;
	y_ += delta_y;
	th_ += delta_th;
	dist_ += delta_dist_;

	//since all odometry is 6DOF we'll need a quaternion created from yaw
	geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th_);

#ifdef ONLY_WHEEL
	//first, we'll publish the transform over tf
	geometry_msgs::TransformStamped odom_trans;
	odom_trans.header.stamp = current_time_;
	odom_trans.header.frame_id = "odom";
	odom_trans.child_frame_id = "base_link2";

	odom_trans.transform.translation.x = x_;
	odom_trans.transform.translation.y = y_;
	odom_trans.transform.translation.z = 0.0;
	odom_trans.transform.rotation = odom_quat;

	//send the transform
	odom_broadcaster_.sendTransform(odom_trans);
#endif

	//next, we'll publish the odometry message over ROS
	nav_msgs::Odometry odom;
	odom.header.stamp = current_time_;
	odom.header.frame_id = "odom";
	odom.child_frame_id = "base_link2";

	//set the position
	odom.pose.pose.position.x = x_;
	odom.pose.pose.position.y = y_;
	odom.pose.pose.position.z = 0.0;
	odom.pose.pose.orientation = odom_quat;

	//set the velocity
	odom.twist.twist.linear.x = vx_;
	odom.twist.twist.linear.y = 0.0;
	odom.twist.twist.angular.z = vth_;

	//publish the message
	odom_pub_.publish(odom);

	last_time_ = current_time_;
}

