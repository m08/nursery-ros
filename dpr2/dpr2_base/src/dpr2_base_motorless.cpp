#include <XMLConfiguration.h>

#include "dpr2_base/dpr2_base_motorless.h"

#define ONLY_WHEEL

#define CLIP(x, l, u) ((x)<(l))?(l):((x)>(u))?(u):(x)

void DPR2Base::init()
{
	ROS_INFO("Initializing base");

wheel_base_ = 0.541;
wheel_diameter_ = 0.295;


	// Subscript to command topic
	vel_sub_ = nh_.subscribe("/cmd_vel", 10, &DPR2Base::velocityCallback, this);
	odom_pub_ = nh_.advertise<nav_msgs::Odometry>("/odom", 10);
}

void DPR2Base::spin()
{
	ROS_INFO("Spinning");
	ros::Rate r(100);

	while(ros::ok())
	{
		ros::spinOnce();
		odometryPublish();
		r.sleep();
	}
}

void DPR2Base::velocityCallback(const geometry_msgs::Twist::ConstPtr &msg)
{
	// Base is nonholonomic, warn if sent a command we can't execute
	if (msg->linear.y || msg->linear.z || msg->angular.x || msg->angular.y)
	{
		ROS_WARN("I'm afraid I can't do that, Dave.");
		return;
	}

	if(isnan(msg->linear.x) || isnan(msg->angular.z) || isinf(msg->linear.x) || isinf(msg->angular.z))
	{
		ROS_WARN("I cant travel at infinite speed. Sorry");
		return;
	}

	// Calculate wheel velocities
	ROS_DEBUG_STREAM("Message on cmd_vel [" << msg->linear.x << ", " << msg->angular.z << "]");
	double vel_linear  = msg->linear.x/(wheel_diameter_/2.0);
	ROS_DEBUG_STREAM("" << wheel_base_ << "/" << wheel_diameter_ << " = " << (wheel_base_/wheel_diameter_));
	double vel_angular = msg->angular.z * (wheel_base_/wheel_diameter_);
	ROS_DEBUG_STREAM("Message converted [" << vel_linear << ", " << vel_angular << "]");

	double vel_left    = vel_linear - vel_angular;
	double vel_right   = vel_linear + vel_angular;

	// Actuate
	lspeed = vel_left;
	rspeed = vel_right;
	
	ROS_DEBUG_STREAM("Base velocity set to [" << vel_left << ", " << vel_right << "]");
}

// reads the current change in wheel and publish as odometry
void DPR2Base::odometryPublish()
{
	double left = lspeed;
	double right = rspeed;
	vx_ = (wheel_diameter_ / 4) * (left + right);
	vth_ = ((wheel_diameter_ / 2) / wheel_base_) * (right - left);

	current_time_ = ros::Time::now();

	//compute odometry in a typical way given the velocities of the robot
	double dt = (current_time_ - last_time_).toSec();
	double delta_x = (vx_ * cos(th_)) * dt;
	double delta_y = (vx_ * sin(th_)) * dt;
	double delta_th = vth_ * dt;
	double delta_dist_ = vx_ *dt;

	x_ += delta_x;
	y_ += delta_y;
	th_ += delta_th;
	dist_ += delta_dist_;

	//since all odometry is 6DOF we'll need a quaternion created from yaw
	geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th_);

#ifdef ONLY_WHEEL
	//first, we'll publish the transform over tf
	geometry_msgs::TransformStamped odom_trans;
	odom_trans.header.stamp = current_time_;
	odom_trans.header.frame_id = "odom";
	odom_trans.child_frame_id = "base_link";

	odom_trans.transform.translation.x = x_;
	odom_trans.transform.translation.y = y_;
	odom_trans.transform.translation.z = 0.0;
	odom_trans.transform.rotation = odom_quat;

	//send the transform
	odom_broadcaster_.sendTransform(odom_trans);
#endif

	//next, we'll publish the odometry message over ROS
	nav_msgs::Odometry odom;
	odom.header.stamp = current_time_;
	odom.header.frame_id = "odom";
	odom.child_frame_id = "base_link";

	//set the position
	odom.pose.pose.position.x = x_;
	odom.pose.pose.position.y = y_;
	odom.pose.pose.position.z = 0.0;
	odom.pose.pose.orientation = odom_quat;

	//set the velocity
	odom.twist.twist.linear.x = vx_;
	odom.twist.twist.linear.y = 0.0;
	odom.twist.twist.angular.z = vth_;

	//publish the message
	odom_pub_.publish(odom);

	last_time_ = current_time_;
}

