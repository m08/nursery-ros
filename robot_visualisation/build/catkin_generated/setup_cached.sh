#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel/lib:/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH"
export PATH="/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel/lib/pkgconfig:/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel/lib/x86_64-linux-gnu/pkgconfig:$PKG_CONFIG_PATH"
export PYTHONPATH="/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/agudek/Workspace/src/nursery/robot_visualisation/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/agudek/Workspace/src/nursery/robot_visualisation:$ROS_PACKAGE_PATH"