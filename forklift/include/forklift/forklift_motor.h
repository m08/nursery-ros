#include <ros/ros.h>
#include <threemxl/C3mxlROS.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Float64.h>
#include <cmath>

#define NONE 0
#define DOWN 1
#define UP 2

class Forklift
{
protected:
	ros::NodeHandle nh_;
	ros::Subscriber frk_sub_; 
	ros::Subscriber frk_pwm_sub_; 
	ros::Publisher joint_pub_;

	C3mxlROS *motor_;

	//int desiredState;
	//int currState;
	//int lastState;
	//int direction;
	//bool rotating;
protected:
	
	void forkliftStateCallback(const std_msgs::Int8 &msg);
	void forkliftPwmCallback(const std_msgs::Float64 &msg);
	int getPressedSensor();
	
	void statusPublish();

	void ext_init();

public:
	Forklift() : nh_("~")
	{
		init();
	}

	~Forklift()
	{
		delete motor_;

		nh_.shutdown();
	}

	/// Initialize the base motors
	/** \note Called during construction */
	void init();

	/// Await and process commands
	void spin();
};
