#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Float64.h>
#include <cmath>

#define NONE 0
#define DOWN 1
#define UP 2

class Forklift
{
protected:
	ros::NodeHandle nh_;
	ros::Subscriber frk_sub_; 
	ros::Publisher joint_pub_;

	double lpos;
protected:
	
	void forkliftStateCallback(const std_msgs::Int8 &msg);
	int getPressedSensor();
	
	void statusPublish();

public:
	Forklift() : nh_("~")
	{
		init();
	}

	~Forklift()
	{

		nh_.shutdown();
	}

	/// Initialize the base motors
	/** \note Called during construction */
	void init();

	/// Await and process commands
	void spin();
};
