#include <forklift/forklift_motor_motorless.h>

/// Entry point for forklift controller node
int main(int argc, char **argv)
{
  ros::init(argc, argv, "forklift_motor");

  Forklift fl;
  fl.spin();

  return 0;
}
