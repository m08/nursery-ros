#include "forklift/forklift_motor_motorless.h"

void Forklift::init()
{
	ROS_INFO("Initializing forklift");

	// Read parameters
	std::string motor_port_name;
	ROS_ASSERT(nh_.getParam("motor_port", motor_port_name));

	// Subscript to command topic
	frk_sub_ = nh_.subscribe("/cmd_frk", 10, &Forklift::forkliftStateCallback, this);
	joint_pub_ = nh_.advertise<sensor_msgs::JointState>("joint_states", 1);


}

double lposToBottomAngle(double pos) {
	//-9E-13*pos^5 + 8E-10*pos^4 - 2E-07*pos^3 + 4E-05*pos^2 + 0.1183*pos - 0.5357
	return -9*pow(10,-13)*pow(pos,5) + 8*pow(10,-10)*pow(pos,4) + -2*pow(10,-7)*pow(pos,3) + 4*pow(10,-5)*pow(pos,2) + 0.1183*pos - 0.5357;
}

double lposToInternalAngle(double pos) {
	//9E-13*pos^5 + 7E-10*pos^4 - 2E-07*pos^3 + 4E-05*pos^2 + 0.0986*pos - 0.5236
	return -9*pow(10,-13)*pow(pos,5) + 7*pow(10,-10)*pow(pos,4) + -2*pow(10,-7)*pow(pos,3) + 4*pow(10,-5)*pow(pos,2) + 0.0986*pos - 0.5236;
}

void Forklift::statusPublish() {

double pos = lpos;

sensor_msgs::JointState joint_state;

//update joint_state
joint_state.header.stamp = ros::Time::now();
joint_state.name.resize(2);
joint_state.position.resize(2);
joint_state.name[0] ="forklift_arm_angle";
joint_state.name[1] ="forklift_internal_angle";
joint_state.position[0] = lposToBottomAngle(pos);
joint_state.position[1] = lposToInternalAngle(pos);

/*} else if(currState==2) {
	joint_state.position[0] = -0.17 ;
	joint_state.position[1] = 0.14;
} else {
	joint_state.position[0] = -0.58;
	joint_state.position[1] = 0.49;
}*/

joint_pub_.publish(joint_state);
}

void Forklift::spin()
{
	ROS_INFO("Spinning");
	ros::Rate r(100);
	/*currState=getPressedSensor();
	if(!currState){
		ROS_WARN("Initialising forklift. Moving to closest pulled up position.");
		motor_->setSpeed(1);
		ros::Rate rate(100);
		while(!currState) {
			currState = getPressedSensor();
			rate.sleep();
		}
		motor_->setSpeed(0);
	}*/
	while(ros::ok())
	{
		ros::spinOnce();
		statusPublish();
		/*currState=getPressedSensor();
		if(currState)
			lastState=currState;
		if(rotating) {			
			if(currState==4 || (currState==1 && direction==DOWN) || (currState==3 && direction==UP) || currState==desiredState) {
				motor_->setSpeed(0);
				direction=0;
				rotating=false;
				statusPublish();
			}
		}*/
		r.sleep();
	}
}

void Forklift::forkliftStateCallback(const std_msgs::Int8 &msg) {

	if(msg.data<1 || msg.data>3) {
		ROS_WARN("Forklift state should be between 1 and 3! Given: %d",msg.data);
		return;
	}

	//desiredState=msg.data;
	/*currState=getPressedSensor();

	if(currState==4) {
		ROS_ERROR("Forklift is in an undesired state. Please fix it manually.");
		return;
	}*/

	/*if(currState==desiredState) {
		ROS_DEBUG_STREAM("Forklift already in desired state. Doing nothing.");
		return;
	}

	int speed = 1;
	
	if(currState) {
		if(desiredState<currState)
			speed = -1;
	} else {
		if(desiredState<lastState || (desiredState==lastState && direction==UP))
			speed = -1;
	}

	if(speed==1)
		direction=UP;
	else
		direction=DOWN;
	rotating=true;
	motor_->setSpeed(speed);*/
	if(msg.data==1){
		lpos=0.005;
	} else if(msg.data==2) {
		lpos=0.1;
	} else {
		lpos=0.28;
	}
}

/*int Forklift::getPressedSensor() {
	// get sensor data and find out which of the sensors is currently pressed
	//pressed sensor in lowered state = 1
	//pressed sensor in pulled up state = 3
	return 3;
}*/

