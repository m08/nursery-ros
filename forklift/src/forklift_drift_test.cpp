#include <ros/ros.h>
#include <threemxl/C3mxlROS.h>

C3mxlROS *motor_;

bool isAround(double lpos) {
	motor_->getLinearPos();
	double currPos = motor_->presentLinearPos();
	return lpos > currPos-0.005 && lpos < currPos+0.005;
}

bool isOver(double oldPos, double lpos) {
	motor_->getLinearPos();
	double currPos = motor_->presentLinearPos();
	if(oldPos>lpos)
		return currPos < lpos;
	else 
		return currPos > lpos;
}

void motorTo(double lpos) {
	motor_->getLinearPos();
	double oldPos = motor_->presentLinearPos();
	motor_->setLinearPos(lpos);
	bool done = false;
	ros::Rate r(100);
	while(!done) {
		if(isAround(lpos) || isOver(oldPos,lpos))
			done=true;
		else
			r.sleep();		
	}
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "forklift_motor");

	ROS_INFO("Initializing forklift");
ros::NodeHandle nh_;
	// Read parameters
	std::string motor_port_name;
	ROS_ASSERT(nh_.getParam("motor_port", motor_port_name));

	CDxlConfig motor_config;
	motor_config.setID(108);
	motor_config.setMode(0);
	motor_ = new C3mxlROS(motor_port_name.c_str());
	motor_->setConfig(&motor_config);
	// Initialize motor
	ros::Rate init_rate(1);
	while (ros::ok() && motor_->init() != DXL_SUCCESS)
	{
		ROS_WARN_ONCE("Couldn't initialize forklift motor, will continue trying every second");
		init_rate.sleep();
	}
	motor_->setLinearAcceleration(5.0);
	
	ROS_INFO("Moving to starting position of test : 5cm");
	motorTo(0.05);
	ROS_INFO("Starting test...");

	ROS_WARN("Measure forklift height to make sure it is at 5 cm. Then press Enter to continue");
	std::cin.ignore();

	for(int i = 0 ; i < 3 ; i++) {
		ROS_INFO("Moving up and down 10 times");
		for(int j = 0 ; j < 10 ; j++) {
			motorTo(0.2);
			motorTo(0.05);
		}
		ROS_WARN("Measure forklift height to make sure it is at 5 cm. Then press Enter to continue");
		std::cin.ignore();
	}
	motorTo(0.0);
	ROS_INFO("Test complete");
	return 0;
}
