; Auto-generated. Do not edit!


(cl:in-package forklift-msg)


;//! \htmlinclude forkliftState.msg.html

(cl:defclass <forkliftState> (roslisp-msg-protocol:ros-message)
  ((state
    :reader state
    :initarg :state
    :type cl:fixnum
    :initform 0))
)

(cl:defclass forkliftState (<forkliftState>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <forkliftState>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'forkliftState)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name forklift-msg:<forkliftState> is deprecated: use forklift-msg:forkliftState instead.")))

(cl:ensure-generic-function 'state-val :lambda-list '(m))
(cl:defmethod state-val ((m <forkliftState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader forklift-msg:state-val is deprecated.  Use forklift-msg:state instead.")
  (state m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <forkliftState>) ostream)
  "Serializes a message object of type '<forkliftState>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'state)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <forkliftState>) istream)
  "Deserializes a message object of type '<forkliftState>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'state)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<forkliftState>)))
  "Returns string type for a message object of type '<forkliftState>"
  "forklift/forkliftState")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'forkliftState)))
  "Returns string type for a message object of type 'forkliftState"
  "forklift/forkliftState")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<forkliftState>)))
  "Returns md5sum for a message object of type '<forkliftState>"
  "800f34bc468def1d86e2d42bea5648c0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'forkliftState)))
  "Returns md5sum for a message object of type 'forkliftState"
  "800f34bc468def1d86e2d42bea5648c0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<forkliftState>)))
  "Returns full string definition for message of type '<forkliftState>"
  (cl:format cl:nil "uint8 state~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'forkliftState)))
  "Returns full string definition for message of type 'forkliftState"
  (cl:format cl:nil "uint8 state~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <forkliftState>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <forkliftState>))
  "Converts a ROS message object to a list"
  (cl:list 'forkliftState
    (cl:cons ':state (state msg))
))
