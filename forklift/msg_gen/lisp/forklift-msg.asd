
(cl:in-package :asdf)

(defsystem "forklift-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "forkliftState" :depends-on ("_package_forkliftState"))
    (:file "_package_forkliftState" :depends-on ("_package"))
  ))